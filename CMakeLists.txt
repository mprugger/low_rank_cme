cmake_minimum_required(VERSION 3.10)
project(low_rank_cme)

if (NOT EXISTS ${CMAKE_BINARY_DIR}/CMakeCache.txt)
  if (NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE "Release" CACHE STRING "" FORCE)
  endif()
endif()


option(USE_MKL OFF)

if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE "Release")
endif()

if(CMAKE_BUILD_TYPE STREQUAL "Debug")
    #add_definitions("-D_GLIBCXX_DEBUG -Wall")
    add_definitions("-Wall")
else()
    add_definitions("-Wall")
endif()
set(CMAKE_CXX_STANDARD 17)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake-modules")

include_directories("eigen-3.3.7")

find_package(NetCDF)
include_directories(${NETCDF_INCLUDE_DIR})

# OpenMP
find_package(OpenMP)

# Boost
find_package(Boost REQUIRED COMPONENTS program_options unit_test_framework)
include_directories(${Boost_INCLUDE_DIR})

set(MKL_DEFINES    "-D__MKL__" "-DMKL_ILP64" "-m64")
set(MKL_INCLUDEDIR "$ENV{MKLROOT}/include")
set(MKL_LIBRARIES  "-L$ENV{MKLROOT}/lib/intel64 -Wl,--no-as-needed -lmkl_intel_ilp64 -lmkl_gnu_thread -lmkl_core -lgomp -lpthread -lm -ldl")
if(${USE_MKL})
    add_definitions(${MKL_DEFINES})
    include_directories(${MKL_INCLUDEDIR})
endif()

add_executable(low_rank_cme low_rank_cme.cpp)
target_link_libraries(low_rank_cme ${NETCDF_LIBRARY})
target_link_libraries(low_rank_cme ${Boost_PROGRAM_OPTIONS_LIBRARY})
if(OpenMP_CXX_FOUND)
    target_link_libraries(low_rank_cme OpenMP::OpenMP_CXX)
endif()
if(USE_MKL)
    target_link_libraries(low_rank_cme ${MKL_LIBRARIES})
endif()


enable_testing()
add_executable(test_lrcme test_low_rank_cme.cpp)
target_link_libraries(test_lrcme ${Boost_UNIT_TEST_FRAMEWORK_LIBRARY})
target_link_libraries(test_lrcme ${NETCDF_LIBRARY})
if(OpenMP_CXX_FOUND)
    target_link_libraries(test_lrcme OpenMP::OpenMP_CXX)
endif()
if(USE_MKL)
    target_link_libraries(test_lrcme ${MKL_LIBRARIES})
endif()
add_test(NAME test_lrcme COMMAND test_lrcme)

