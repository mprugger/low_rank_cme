from sympy import *
import itertools as it
import math as m

def rule(species_in, species_out):
	# ========= mTor ============
	#return And(species_in[0], Not(species_out[0]))
	#return Not(species_out[0])
	#return species_out[0]

	#return And(species_in[0], Not(species_out[0]))
	#return species_out[0]
	#return And( Not(And(species_in[0], species_in[1])) , Not(species_out[0]))

	#return species_out[0]
	#return species_out[0]
	#return species_out[0]

	#return species_out[0]
	#return And( Not(And(species_in[0], species_out[0])), Not(species_in[1]))
	#return And(And(And(species_out[0],species_out[1]),species_in[0]),species_in[1])

	#return And( Not( And(species_out[0], species_out[1])), Not(species_in[0]))
	#return species_out[0]
	#return And(And(And(species_in[0],species_out[0]),species_in[1]),species_in[2])

	#return species_out[0]
	#return And( species_out[0] , Not(species_in[0]))
	#return species_out[0]
	#return species_out[0]
	#return species_out[0]
	
	#return species_out[0]
	#return And(species_in[0], Not(species_out[0]))
	#return species_out[0]
	#return And( Not(And(species_in[0], species_in[1])) , Not(species_out[0]))
	
	#return And( Not(And(species_out[0], species_out[1])) , Not(species_in[0]) )
	#return And( species_in[0] , Not(species_out[0]))
	#return And( And( And(species_out[0],species_in[0]), species_in[1]), species_in[2])
	
	#return And( species_in[0] , Not(species_out[0]))
	#return species_out[0]
	#return And( And( And(species_in[0],species_out[0]),species_out[1]),species_out[2])
	
	#return And(species_out[0], Not(species_out[1]))
	#return species_out[0]
	#return species_out[0]
	#return species_out[0]
	#return And(And(And(species_out[0], species_in[0]), species_in[1]), species_in[2])

	# ========= pancreatic ======

	#return Or(species_in[0], species_out[0])
	#return Or(Or(species_in[0], species_in[1]), species_out[0])
	#return And(Not(species_out[0]), Or(Or(species_in[0], species_in[1]), species_in[2]))
	#return And( Not(species_in[0]), Or( Or( Or( species_out[0] , species_in[1] ) , species_in[2] ) , species_in[3]) )
	#return Or(species_in[0], species_out[0])
	#return And( Not(species_in[0]), Or(species_in[1], species_out[0]) )
	
	#return Or(species_in[0], species_out[0])
	#return Or(species_in[0], species_out[0])
	#return And( Not(species_in[0]), Or(species_in[1], species_out[0]) )
	#return Or(species_in[0], species_out[0])
	#return Or(species_in[0], species_out[0])
	#return And( Not(species_in[0]) , Or( Or( Or( species_out[0] , species_in[1] ) , species_in[2] ) , species_in[3] ) )

	#return And( Not(species_out[0]) , Or(species_in[0], species_in[1]) )
	#return Or(species_in[0], species_out[0])
	#return And( Not(species_in[0]) , Or(species_in[1], species_out[0]) )
	#return Or(species_out[0], species_in[0])
	#return And( Not(species_out[0]), species_in[0])
	#return Or( Or( species_in[1] , species_in[0]) , species_out[0])
	#return And( Not(species_in[0]) , Or( Or(species_out[0], species_in[1]) , species_in[2]) )
	
	#return Or(species_in[0], species_out[0])
	#return Or(species_in[0], species_out[0])
	#return Or(species_in[0], species_out[0])
	#return And( Not(species_out[0]) , Or( species_in[0], species_in[1]) )
	#return Or( Or(species_out[0],species_in[0]) , species_in[1])
	#return And( Not(species_in[0]), Or( Or(species_out[0],species_in[1]),species_in[2] ))
	
	#return And( Not(species_out[0]), species_in[0])
	#return And(Not(species_out[0]), Or(species_in[0],species_in[1]))
	#return And(Not(species_in[0]), Or(species_in[1],species_out[0]))
	#return And( Not(species_in[0]), Or(Or(species_out[0], species_in[1]),species_in[2]))
	#return Or(Or(species_out[0],species_in[1]),species_in[0])
	#return And( Not(species_out[0]), Or(Or(Or(species_in[0],species_in[1]),species_in[2]),species_in[3]))
	#return And( Not(Or(species_in[0], species_in[1])) , Or(Or(Or(species_out[0],species_in[2]),species_in[3]),species_in[4]) )
	
	#return Or(species_in[0], species_out[0])
	#return And(Not(species_out[0]),Or(species_in[0],species_in[1]))
	#return And(Not(species_out[0]),Or(species_in[0],species_in[1]))
	#return Or(Or(species_out[0], species_in[0]),species_in[1])
	#return And(Not(species_in[0]),Or(Or(species_out[0],species_in[1]),species_in[2]))
	#return And( Not(Or(species_in[0],species_in[1])) , Or(Or(Or(species_out[0],species_in[2]),species_in[3]),species_in[4]))
	
	#return Or(species_in[0], species_out[0])
	#return And(Not(species_in[0]), Or(species_in[1], species_out[0]))
	#return Or(species_in[0], species_out[0])
	#return And(species_in[0], Not(species_out[0]))
	#return And(Not(species_in[0]),Or(Or(species_out[0],species_in[1]),species_in[2]))
	#return Or(Or(species_in[0], species_in[1]),species_out[0])
	#return And(Not(species_out[0]), Or(Or(Or(species_in[0],species_in[1]),species_in[2]),species_in[3]))
	
	#return And(Not(species_out[0]), species_in[0])
	#return Or(species_in[0], species_out[0])
	#return And(Not(species_in[0]), Or(species_in[1], species_out[0]))
	#return Or(Or(species_in[0],species_in[1]),species_out[0])
	#return And(Not(species_in[0]),Or(Or(species_in[1],species_in[2]),species_out[0]))
	#return And(Not(species_out[0]), Or(species_in[0], species_in[1]))
	#return And(Not(species_in[0]), Or(Or(Or(species_out[0],species_in[1]),species_in[2]),species_in[3]))
	#return And(species_in[0], Not(Or(species_out[0], species_in[1])))
	#return And(Not(species_out[0]), Or(species_in[0], species_in[1]))
	
	#return And(Not(species_in[0]), Or(species_in[1], species_out[0]))
	#return Or(Or(species_in[0],species_in[1]),species_out[0])
	#return And(Not(species_out[0]), Or(species_in[0], species_in[1]))
	#return And(Not(species_in[0]), Or(Or(species_out[0],species_in[1]),species_in[2]))
	#return And(Not(species_in[0]), Or(Or(Or(species_out[0],species_out[1]),species_in[1]),species_out[2]))
	#return And(Not(Or(species_out[0], species_in[0])), Or(Or(Or(species_out[1],species_in[1]),species_out[2]),species_in[2]))


	# ---- paper -----
	#A = species_out[0]
	#H = 1-species_out[0]
	#A = species_out[0]+species_in[2]
	#H = species_in[1]
	#A = species_out[0]+species_in[1]
	#H = 3*species_in[2]+(1-species_out[0])*(1-species_in[1])
	#A = species_in[1]
	#H = 2*species_out[0]-species_in[1]+1
	#A = species_out[1]+species_in[1]
	#H = 3*species_out[0]+(1-species_out[1])*(1-species_in[1])
	#A = species_out[0]
	#H = 1-species_out[0]
	#A = species_in[2]+species_in[1]+species_out[0]
	#H = 4*species_in[3]+(1-species_in[2])*(1-species_in[1])*(1-species_out[0])
	#A = species_out[0]*species_in[2]+species_out[1]*species_in[2]
	#H = 3*species_in[1]-species_in[2]+1
	#A = species_out[0]
	#H = 2*species_in[2]-species_out[0]+species_in[1]+1
	#A = 3*species_in[2]+species_out[0]
	#H = 2*species_in[1]+(1-species_in[2])*(1-species_out[0])
	#A = species_out[0]
	#H = species_in[3]*species_in[2]+species_in[1]

	# ---- 1GFR ----
	#A = species_out[0]
	#H = 1-species_out[0]
	#A = species_out[0]
	#H = 1-species_out[0]

	# ---- 2Cas7 ----
	#A = species_in[1]+species_out[0]
	#H = 3*species_in[2]+(1-species_in[1])*(1-species_out[0])

	# ---- 3PI3K ----
	#A = species_out[0]*species_in[1]
	#H = -species_in[1]+2*species_in[2]+1

	# ---- 4Cas8 ----
	#A = species_in[1]+species_out[1]
	#H = species_out[0]
	#A = species_in[2]*species_in[1]+species_out[0]*species_in[1]
	#H = 3*species_in[3]-species_in[1]+1

	# ---- 5
	#A = species_out[0]*species_out[1]
	#H = -species_out[1]+2*species_in[1]+1

	# --- 6
	#A = species_out[0]
	#H = 1-species_out[0]

	# --- 7 
	#A = species_out[0]
	#H = 1-species_out[0]
	
	# --- 8
	#A = species_in[1]*species_in[2]
	#H = -species_in[2]+2*species_out[0]+1

	# --- 9
	#A = species_out[0]
	#H = 2*species_in[1]-species_out[0]+1



	# --- generated 51 ----
	#A = species_out[0]
	#H = 1-species_out[0]
	#A = species_out[0]
	#H = 1-species_out[0]
	#A = species_in[1] + species_in[2]
	#H = species_out[0] - species_in[1] + 2*species_in[3] + 1
	#A = species_out[0]
	#H = species_out[1]
	#A = species_out[0]
	#H = species_in[1]*species_in[2] + species_in[3]
	#A = 3*species_out[0] + species_in[1]
	#H = 2*species_in[2] + (1-species_out[0])*(1-species_in[1])
	#A = species_out[0]
	#H = 1-species_out[0]
	#A = species_out[0]*species_in[1] + species_in[2]*species_in[1]
	#H = 3*species_in[3] - species_in[1] + 1

	# ---- generated 41 ----
	#A = species_out[0]
	#H = 1 - species_out[0]
	#A = species_in[1] + species_out[0]
	#H = species_in[2]
	#A = species_out[0]
	#H = 2*species_in[1] - species_out[0]+1
	#A = species_out[0]+species_in[1]
	#H = 3*species_in[2] + (1 - species_out[0])*(1 - species_in[1])
	#A = species_out[0]
	#H = 2*species_in[2] - species_out[0] + species_in[1] +1

	#A = species_in[1] + species_out[0]
	#H = 3*species_in[2] + (1-species_in[1])*(1-species_out[0])
	#A = species_out[0]*species_in[1]*species_in[2]
	#H = -species_out[0] - species_in[1] + 2*species_in[3] - species_in[2] +3
	#A = species_out[0]
	#H = species_in[1] * species_in[2] + species_in[3]

	#A = species_in[2]*species_in[1]*species_out[0]
	#H = -species_in[2] - species_in[1] + 2* species_in[3] - species_out[0] + 3
	#A = species_out[0]
	#H = species_in[1]*species_in[2] + species_out[1]

	#A = species_in[1] + species_out[0]
	#H = species_out[1]
	#A = species_out[0] + species_in[1]
	#H = 3*species_in[2] + (1- species_out[0])*(1- species_in[1])
	A = species_out[0]
	H = species_in[3]*species_in[2]+species_in[1]

	if A>H:
		return True
	elif H>A:
		return False
	else:
		return species_in[0]
	

# ------------mTor--------------

#RICTOR, mir196b, Q6R327, RHEB, TSC12, P31749, AKT, Q15382, P27361, Q15418, HOAX9, P42345, Q9BVC4, Q15118, mir1976, RSK, mTor, GBL, PDK1, MLL, Q03164 = symbols('RICTOR mir196b Q6R327 RHEB TSC12 P31749 AKT Q15382 P27361 Q15418 HOAX9 P42345 Q9BVC4 Q15118 mir1976 RSK mTor GBL PDK1 MLL Q03164')

#species_in = [RICTOR, Q6R327] 
#species_out = [mir196b]
#species_in = [RHEB] 
#species_out = [TSC12]
#species_in = [P31749]
#species_out = [AKT]

#species_in = [RICTOR, Q6R327]
#species_out = [mir196b]
#species_in = [Q15382]
#species_out = [RHEB]
#species_in = [P27361, Q15418, TSC12]
#species_out = [P31749]

#species_in = [mir196b]
#species_out = [HOAX9]
#species_in = [Q15382]
#species_out = [RHEB]
#species_in = [P31749]
#species_out = [AKT]

#species_in = [RICTOR]
#species_out = [Q15382]
#species_in = [P27361, P31749, TSC12]
#species_out = [Q15418]
#species_in = [P42345, Q9BVC4, AKT]
#species_out = [Q15118, Q6R327]

#species_in = [P31749, TSC12]
#species_out = [P27361, Q15418]
#species_in = [RICTOR]
#species_out = [Q15382]
#species_in = [Q15118, P42345, Q9BVC4, AKT]
#species_out = [Q6R327]

#species_in = [mir1976]
#species_out = [RSK]
#species_in = [mir196b, Q6R327]
#species_out = [RSK]
#species_in = [P42345]
#species_out = [mTor]
#species_in = [Q9BVC4]
#species_out = [GBL]
#species_in = [P31749]
#species_out = [AKT]

#species_in = [Q15118]
#species_out = [PDK1]
#species_in=[RICTOR, Q6R327]
#species_out = [mir196b]
#species_in = [RHEB]
#species_out = [TSC12]
#species_in = [P27361, Q15418, TSC12]
#species_out = [P31749]

#species_in = [P31749, TSC12]
#species_out = [P27361, Q15418]
#species_in = [RICTOR, Q6R327]
#species_out = [mir196b]
#species_in = [Q6R327, P42345, Q9BVC4, AKT]
#species_out = [Q15118]

#species_in = [MLL, Q03164]
#species_out = [mir1976]
#species_in = [Q15382]
#species_out = [RHEB]
#species_in = [Q15118, AKT]
#species_out = [Q6R327, P42345, Q9BVC4]

#species_in = [Q6R327]
#species_out = [RICTOR, mir196b]
#species_in = [mTor]
#species_out = [Q15382]
#species_in = [Q9BVC4]
#species_out = [GBL]
#species_in = [RHEB]
#species_out = [TSC12]
#species_in = [Q6R327, P42345, Q9BVC4, AKT]
#species_out = [Q15118]


#-------------------pancreatic--------------

#IRAKs, MYD88, ERK, MEK, AKT, MDM2, P53, ARF, A20, IKK, TAB1, P21, Bcl_XL, NFkB, RAF, E2F, PI3K, PIP3, PTEN, IkB, AP1, Myc, INK4a, CyclinD, RB, CyclinE = symbols('IRAKs MYD88 ERK MEK AKT MDM2 P53 ARF A20 IKK TAB1 P21 Bcl_XL NFkB RAF E2F PI3K PIP3 PTEN IkB AP1 Myc INK4a CyclinD RB CyclinE')
#print(And(IRAKs, MYD88))

#species_in = [IRAKs]
#species_out = [MYD88]
#species_in = [ERK, IRAKs]
#species_out = [MEK]
#species_in = [AKT, MDM2, P53]
#species_out = [ARF]
#species_in = [A20, ERK, IKK, TAB1]
#species_out = [AKT]
#species_in = [P21]
#species_out = [AKT]
#species_in = [P53, Bcl_XL]
#species_out = [NFkB]

#species_in = [IRAKs]
#species_out = [MYD88]
#species_in = [P21]
#species_out = [P53]
#species_in = [P53, Bcl_XL]
#species_out = [NFkB]
#species_in = [MEK]
#species_out = [RAF]
#species_in = [ARF]
#species_out = [E2F]
#species_in = [A20, ERK, IKK, TAB1]
#species_out = [AKT]

#species_in = [PI3K, PIP3]
#species_out = [PTEN]
#species_in = [A20]
#species_out = [NFkB]
#species_in = [IKK, IkB]
#species_out = [NFkB]
#species_in = [AP1]
#species_out = [ERK]
#species_in = [NFkB]
#species_out = [IkB]
#species_in = [Myc, NFkB]
#species_out = [ERK]
#pecies_in = [ARF, MDM2, P53]
#species_out = [AKT]

#species_in = [A20]
#species_out = [NFkB]
#species_in = [PTEN]
#species_out = [P53]
#species_in = [AP1]
#species_out = [ERK]
#species_in = [IkB, NFkB]
#species_out = [IKK]
#species_in = [Myc, NFkB]
#species_out = [ERK]
#species_in = [ARF, MDM2, P53]
#species_out = [AKT]

#species_in = [NFkB]
#species_out = [IkB]
#species_in = [PI3K, PIP3]
#species_out = [PTEN]
#species_in = [IKK, IkB]
#species_out = [NFkB]
#species_in = [ARF, MDM2, P53]
#species_out = [AKT]
#species_in = [Myc, NFkB]
#species_out = [ERK]
#species_in = [AKT, ERK, IKK, TAB1]
#species_out = [A20]
#species_in = [INK4a, P21, CyclinD, Myc, NFkB]
#species_out = [AP1]

#species_in = [A20]
#species_out = [NFkB]
#species_in = [PI3K, PIP3]
#species_out = [PTEN]
#species_in = [IkB, NFkB]
#species_out = [IKK]
#species_in = [Myc, NFkB]
#species_out = [ERK]
#species_in = [ARF, MDM2, P53]
#species_out = [AKT]
#species_in = [INK4a, P21, CyclinD, Myc, NFkB]
#species_out = [AP1]

#species_in = [PTEN]
#species_out = [P53]
#species_in = [IKK, IkB]
#species_out = [NFkB]
#species_in = [AP1]
#species_out = [ERK]
#species_in = [NFkB]
#species_out = [IkB]
#species_in = [ARF, MDM2, P53]
#species_out = [AKT]
#species_in = [Myc, NFkB]
#species_out = [ERK]
#species_in = [AKT, ERK, IKK, TAB1]
#species_out = [A20]

#species_in = [P53]
#species_out = [MDM2]
#species_in = [IRAKs]
#species_out = [MYD88]
#species_in = [RB, E2F]
#species_out = [Myc]
#species_in = [ERK, IRAKs]
#species_out = [MEK]
#species_in = [ARF, AKT, MDM2]
#species_out = [P53]
#species_in = [PI3K, PIP3]
#species_out = [PTEN]
#species_in = [A20, ERK, IKK, TAB1]
#species_out = [AKT]
#species_in = [RB, CyclinE]
#species_out = [CyclinD]
#species_in = [CyclinE, E2F]
#species_out = [P21]

#species_in = [RB, E2F]
#species_out = [Myc]
#species_in = [ERK, Myc]
#species_out = [NFkB]
#species_in = [PI3K, PIP3]
#species_out = [PTEN]
#species_in = [ARF, MDM2, P53]
#species_out = [AKT]
#species_in = [A20, IKK]
#species_out = [AKT, ERK, TAB1]
#species_in = [P21, CyclinD, NFkB]
#species_out = [INK4a, AP1, Myc]



#----------------------apoptosis-------------------
GF, GFR, Cas6, Cas8, cIAP, FADD, IKK, NIK, A20, Akt, JNKK, MEKK1, Cas7, IAP, APC, Cas12, Cas3, BID, BclX, p53, JNK, BAD, NFkB, Mdm2, DNAdam, Mito, PI3K, PIP2, PIP3, PTEN, TRAF2, RIP, Cas9, Apaf1, TRAF = symbols('GF GFR Cas6 Cas8 cIAP FADD IKK NIK A20 Akt JNKK MEKK1 Cas7 IAP APC Cas12 Cas3 BID BclX p53 JNK BAD NFkB Mdm2 DNAdam Mito PI3K PIP2 PIP3 PTEN TRAF2 RIP Cas9 Apaf1 TRAF')

#species_in = [GFR]
#species_out = [GF]
#species_in = [Cas8, cIAP, FADD]
#species_out = [Cas6]
#species_in = [IKK, NIK, A20]
#species_out = [Akt]
#species_in = [JNKK, MEKK1]
#species_out = [Akt]
#species_in = [Cas7, Cas8]
#species_out = [IAP, APC]
#species_in = [Cas12]
#species_out = [Cas7]
#species_in = [Cas3, Cas6, APC, IAP]
#species_out = [Cas8]
#species_in = [BID, BclX, p53]
#species_out = [Cas8, JNK]
#species_in = [BclX, p53, BAD]
#species_out = [NFkB]
#species_in = [p53, Mdm2, DNAdam]
#species_out = [JNK]
#species_in = [IAP, Mito, Cas6, Cas3]
#species_out = [NFkB]

#species_in = [PI3K]
#species_out = [GFR]
#species_in = [PIP2]
#species_out = [GFR]

#species_in = [Cas7, APC, IAP]
#species_out = [Cas8]

#species_in = [PIP3, PIP2, PTEN]
#species_out = [PI3K]

#species_in = [Cas8, Cas6]
#species_out = [cIAP, FADD]
#species_in = [BID, p53, Cas8, BclX]
#species_out = [JNK]

#species_in = [PIP3, PTEN]
#species_out = [PI3K, PIP2]

#species_in = [JNK]
#species_out = [JNKK]

#species_in = [Akt]
#species_out = [PIP3]

#species_in = [PIP3, PI3K, PIP2]
#species_out = [PTEN]

#species_in = [JNKK, Akt]
#species_out = [MEKK1]

#species_in = [TRAF2]
#species_out = [NIK]
#species_in = [MEKK1]
#species_out = [RIP]
#species_in = [Cas9, Cas12, Cas3, IAP]
#species_out = [Akt]
#species_in = [Mito]
#species_out = [BID, BclX]
#species_in = [IAP, Cas3, Cas6, Mito]
#species_out = [NFkB]
#species_in = [p53, JNK, Mdm2]
#species_out = [DNAdam]
#species_in = [Apaf1]
#species_out = [p53]
#species_in = [BID, p53, JNK, BclX]
#species_out = [Cas8]

#species_in = [TRAF]
#species_out = [cIAP]
#species_in = [Cas8, Cas6, cIAP]
#species_out = [FADD]
#species_in = [JNKK, Akt]
#species_out = [MEKK1]
#species_in = [IKK, NIK, A20]
#species_out = [Akt]
#species_in = [BclX, p53, BAD]
#species_out = [NFkB]

#species_in = [IKK, Akt, A20]
#species_out = [NIK]
#species_in = [APC, Cas9, Mito, IAP]
#species_out = [Apaf1]
#species_in = [IAP, Cas3, Cas6, Mito]
#species_out = [NFkB]

#species_in = [APC, Cas9, Apaf1, IAP]
#species_out = [Mito]
#species_in = [IAP, Cas3, Cas6]
#species_out = [NFkB, Mito]

#species_in = [Cas8, Cas6]
#species_out = [FADD, cIAP]
#species_in = [IKK, NIK, A20]
#species_out = [Akt]
species_in = [IAP, Mito, Cas6, Cas3]
species_out = [NFkB]


entropy = 0
l = [False, True]
inner_list = list(it.product(l, repeat=len(species_in)))
outer_list = list(it.product(l, repeat=len(species_out)))
#for i in inner_list:
#	print(i)

for i in inner_list:
	count_0 = 0
	count_1 = 0
	print(species_in)
	for s in range(len(species_in)):
		species_in[s]=i[s]
	print(species_in)
 
	for j in outer_list:
		for s in range(len(species_out)):
			species_out[s]=j[s] 
		v = rule(species_in, species_out) 
		print('a', v)
		if v == True:
			count_1=count_1+1
		else:
			count_0=count_0+1
	print(count_1)
	print(count_0)
	s = count_1+count_0
	if count_0==0 or count_1==0:
		E = 0
	else:
		E = -count_0/s*m.log2(count_0/s)-count_1/s*m.log2(count_1/s)
	entropy += E
	print('entropy ', entropy)
entropy = entropy/(len(inner_list))
print(entropy)
