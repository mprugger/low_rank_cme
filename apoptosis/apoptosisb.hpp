RULE_SET(APOPTOSISb, 41, "GF","TRADD","TRAF","FADD","RIP","cIAP","Cas8","TRAF2","NIK","IKK","MEKK1","BID","BclX","BAD","PTEN","GFR","PI3K","PIP2","PIP3","Akt","Mdm2","TNF","TNFR1","TNFR2","IkB","NFkB","A20","JNKK","JNK","Cas7","Cas12","Cas9","APC","Cas3","Cas3prev","Cas6","p53","Apaf1","Mito","IAP","DNAdam")

template<> bool APOPTOSISb::rule<0>(bitset<41> x) {
    int A = x[0];
    int H = 1 - x[0];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[0];
}
template<> vector<ind> APOPTOSISb::depends_on<0>() {
    return { 0 };
}

template<> bool APOPTOSISb::rule<1>(bitset<41> x) {
    int A = x[22];
    int H = 1 - x[22];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[1];
}
template<> vector<ind> APOPTOSISb::depends_on<1>() {
    return { 1,22 };
}

template<> bool APOPTOSISb::rule<2>(bitset<41> x) {
    int A = x[1];
    int H = 1 - x[1];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[2];
}
template<> vector<ind> APOPTOSISb::depends_on<2>() {
    return { 1,2 };
}

template<> bool APOPTOSISb::rule<3>(bitset<41> x) {
    int A = x[1];
    int H = 1 - x[1];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[3];
}
template<> vector<ind> APOPTOSISb::depends_on<3>() {
    return { 1,3 };
}

template<> bool APOPTOSISb::rule<4>(bitset<41> x) {
    int A = x[1];
    int H = 1 - x[1];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[4];
}
template<> vector<ind> APOPTOSISb::depends_on<4>() {
    return { 1,4 };
}

template<> bool APOPTOSISb::rule<5>(bitset<41> x) {
    int A = x[2];
    int H = 1 - x[2];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[5];
}
template<> vector<ind> APOPTOSISb::depends_on<5>() {
    return { 2,5 };
}

template<> bool APOPTOSISb::rule<6>(bitset<41> x) {
    int A = x[35] + x[3];
    int H = x[5];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[6];
}
template<> vector<ind> APOPTOSISb::depends_on<6>() {
    return { 3,5,6,35 };
}

template<> bool APOPTOSISb::rule<7>(bitset<41> x) {
    int A = x[4];
    int H = -x[4] + 2*x[23] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[7];
}
template<> vector<ind> APOPTOSISb::depends_on<7>() {
    return { 4,7,23 };
}

template<> bool APOPTOSISb::rule<8>(bitset<41> x) {
    int A = x[7];
    int H = 1 - x[7];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[8];
}
template<> vector<ind> APOPTOSISb::depends_on<8>() {
    return { 7,8 };
}

template<> bool APOPTOSISb::rule<9>(bitset<41> x) {
    int A = x[19] + x[8];
    int H = 3*x[26] + (1 - x[19])*(1 - x[8]);
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[9];
}
template<> vector<ind> APOPTOSISb::depends_on<9>() {
    return { 8,9,19,26 };
}

template<> bool APOPTOSISb::rule<10>(bitset<41> x) {
    int A = x[2];
    int H = 1 - x[2];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[10];
}
template<> vector<ind> APOPTOSISb::depends_on<10>() {
    return { 2,10 };
}

template<> bool APOPTOSISb::rule<11>(bitset<41> x) {
    int A = x[6]*x[36] + x[28]*x[36];
    int H = 3*x[12] - x[36] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[11];
}
template<> vector<ind> APOPTOSISb::depends_on<11>() {
    return { 6,11,12,28,36 };
}

template<> bool APOPTOSISb::rule<12>(bitset<41> x) {
    int A = x[25];
    int H = 2*x[13] - x[25] + x[36] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[12];
}
template<> vector<ind> APOPTOSISb::depends_on<12>() {
    return { 12,13,25,36 };
}

template<> bool APOPTOSISb::rule<13>(bitset<41> x) {
    int A = x[36];
    int H = 2*x[19] - x[36] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[13];
}
template<> vector<ind> APOPTOSISb::depends_on<13>() {
    return { 13,19,36 };
}

template<> bool APOPTOSISb::rule<14>(bitset<41> x) {
    int A = x[36];
    int H = 1 - x[36];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[14];
}
template<> vector<ind> APOPTOSISb::depends_on<14>() {
    return { 14,36 };
}

template<> bool APOPTOSISb::rule<15>(bitset<41> x) {
    int A = x[0];
    int H = 1 - x[0];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[15];
}
template<> vector<ind> APOPTOSISb::depends_on<15>() {
    return { 0,15 };
}

template<> bool APOPTOSISb::rule<16>(bitset<41> x) {
    int A = x[15];
    int H = 1 - x[15];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[16];
}
template<> vector<ind> APOPTOSISb::depends_on<16>() {
    return { 15,16 };
}

template<> bool APOPTOSISb::rule<17>(bitset<41> x) {
    int A = x[15];
    int H = 1 - x[15];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[17];
}
template<> vector<ind> APOPTOSISb::depends_on<17>() {
    return { 15,17 };
}

template<> bool APOPTOSISb::rule<18>(bitset<41> x) {
    int A = x[16]*x[17];
    int H = -x[17] + 2*x[14] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[18];
}
template<> vector<ind> APOPTOSISb::depends_on<18>() {
    return { 14,16,17,18 };
}

template<> bool APOPTOSISb::rule<19>(bitset<41> x) {
    int A = x[18];
    int H = 1 - x[18];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[19];
}
template<> vector<ind> APOPTOSISb::depends_on<19>() {
    return { 18,19 };
}

template<> bool APOPTOSISb::rule<20>(bitset<41> x) {
    int A = x[19] + x[36];
    int H = 1 - x[19];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[20];
}
template<> vector<ind> APOPTOSISb::depends_on<20>() {
    return { 19,20,36 };
}

template<> bool APOPTOSISb::rule<21>(bitset<41> x) {
    int A = x[21];
    int H = 1 - x[21];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[21];
}
template<> vector<ind> APOPTOSISb::depends_on<21>() {
    return { 21 };
}

template<> bool APOPTOSISb::rule<22>(bitset<41> x) {
    int A = x[21];
    int H = 1 - x[21];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[22];
}
template<> vector<ind> APOPTOSISb::depends_on<22>() {
    return { 21,22 };
}

template<> bool APOPTOSISb::rule<23>(bitset<41> x) {
    int A = x[21];
    int H = 1 - x[21];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[23];
}
template<> vector<ind> APOPTOSISb::depends_on<23>() {
    return { 21,23 };
}

template<> bool APOPTOSISb::rule<24>(bitset<41> x) {
    int A = x[25];
    int H = 2*x[9];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[24];
}
template<> vector<ind> APOPTOSISb::depends_on<24>() {
    return { 9,24,25 };
}

template<> bool APOPTOSISb::rule<25>(bitset<41> x) {
    int A = 1 - x[24];
    int H = x[24];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[25];
}
template<> vector<ind> APOPTOSISb::depends_on<25>() {
    return { 24,25 };
}

template<> bool APOPTOSISb::rule<26>(bitset<41> x) {
    int A = x[25];
    int H = 1 - x[25];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[26];
}
template<> vector<ind> APOPTOSISb::depends_on<26>() {
    return { 25,26 };
}

template<> bool APOPTOSISb::rule<27>(bitset<41> x) {
    int A = x[10];
    int H = 2*x[19] - x[10] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[27];
}
template<> vector<ind> APOPTOSISb::depends_on<27>() {
    return { 10,19,27 };
}

template<> bool APOPTOSISb::rule<28>(bitset<41> x) {
    int A = x[27];
    int H = 1 - x[27];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[28];
}
template<> vector<ind> APOPTOSISb::depends_on<28>() {
    return { 27,28 };
}

template<> bool APOPTOSISb::rule<29>(bitset<41> x) {
    int A = x[32] + x[6];
    int H = 3*x[39] + (1 - x[32])*(1 - x[6]);
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[29];
}
template<> vector<ind> APOPTOSISb::depends_on<29>() {
    return { 6,29,32,39 };
}

template<> bool APOPTOSISb::rule<30>(bitset<41> x) {
    int A = x[29];
    int H = 1 - x[29];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[30];
}
template<> vector<ind> APOPTOSISb::depends_on<30>() {
    return { 29,30 };
}

template<> bool APOPTOSISb::rule<31>(bitset<41> x) {
    int A = x[30] + x[33];
    int H = x[19] - x[30] + 2*x[39] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[31];
}
template<> vector<ind> APOPTOSISb::depends_on<31>() {
    return { 19,30,31,33,39 };
}

template<> bool APOPTOSISb::rule<32>(bitset<41> x) {
    int A = x[37]*x[31]*x[38];
    int H = -x[37] - x[31] + 2*x[39] - x[38] + 3;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[32];
}
template<> vector<ind> APOPTOSISb::depends_on<32>() {
    return { 31,32,37,38,39 };
}

template<> bool APOPTOSISb::rule<33>(bitset<41> x) {
    int A = x[32] + x[35] + x[6];
    int H = 4*x[39] + (1 - x[32])*(1 - x[35])*(1 - x[6]);
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[33];
}
template<> vector<ind> APOPTOSISb::depends_on<33>() {
    return { 6,32,33,35,39 };
}

template<> bool APOPTOSISb::rule<34>(bitset<41> x) {
    return x[33];
}
template<> vector<ind> APOPTOSISb::depends_on<34>() {
    return { 33,34 };
}

template<> bool APOPTOSISb::rule<35>(bitset<41> x) {
    int A = x[33];
    int H = -x[33] + 2*x[39] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[35];
}
template<> vector<ind> APOPTOSISb::depends_on<35>() {
    return { 33,35,39 };
}

template<> bool APOPTOSISb::rule<36>(bitset<41> x) {
    int A = 3*x[40] + x[28];
    int H = 2*x[20] + (1 - x[40])*(1 - x[28]);
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[36];
}
template<> vector<ind> APOPTOSISb::depends_on<36>() {
    return { 20,28,36,40 };
}

template<> bool APOPTOSISb::rule<37>(bitset<41> x) {
    int A = x[36];
    int H = 1 - x[36];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[37];
}
template<> vector<ind> APOPTOSISb::depends_on<37>() {
    return { 36,37 };
}

template<> bool APOPTOSISb::rule<38>(bitset<41> x) {
    int A = x[11];
    int H = x[12];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[38];
}
template<> vector<ind> APOPTOSISb::depends_on<38>() {
    return { 11,12,38 };
}

template<> bool APOPTOSISb::rule<39>(bitset<41> x) {
    int A = x[25];
    int H = x[33]*x[35] + x[38];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[39];
}
template<> vector<ind> APOPTOSISb::depends_on<39>() {
    return { 25,33,35,38,39 };
}

template<> bool APOPTOSISb::rule<40>(bitset<41> x) {
    return x[33] && x[34];
}
template<> vector<ind> APOPTOSISb::depends_on<40>() {
    return { 33,34,40 };
}
