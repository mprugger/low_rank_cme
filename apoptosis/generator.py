from sympy import *
from sympy.printing.ccode import ccode
import re

names = ["TNF", "GF", "TNFR1", "TRADD", "TRAF", "FADD", "RIP", "cIAP", "Cas8", "TNFR2", "TRAF2", "NIK", "IKK", "IkB", "NFkB", "A20", "MEKK1", "JNKK", "JNK", "Cas7", "Cas12", "Cas9", "APC", "Cas3", "Cas3prev", "Cas6", "BID", "BclX", "BAD", "p53", "Apaf1", "PTEN", "Mito", "IAP", "GFR", "PI3K", "PIP2", "PIP3", "Akt", "Mdm2", "DNAdam"]

def idx_from_rule(rule, names):
    return [names.index(y) for y in [str(x) for x in rule.free_symbols]]

def idx_from_name(l_name, names):
    return [names.index(y) for y in l_name]

for name in names:
    locals()[name] = symbols(name)

def apply_perm(perm, states):
    N = len(perm)
    print(perm)
    states_t = [states[perm[i]] for i in range(N)]
    return states_t

def And(A, B, C=1):
    return A*B*C

def Not(A):
    return 1-A


AH = [(TNF, Not(TNF)),         # TNF 0
      (GF,  Not(GF)),          # GF 1
      (TNF, Not(TNF)),         # TNFR1 2
      (TNFR1, Not(TNFR1)),     # TRADD 3
      (TRADD, Not(TRADD)),     # TRAF 4
      (TRADD, Not(TRADD)),     # FADD 5
      (TRADD, Not(TRADD)),     # RIP 6
      (TRAF, Not(TRAF)),       # cIAP 7
      (FADD+Cas6, cIAP),       # Cas8 8
      (TNF, Not(TNF)),         # TNFR2 9
      (RIP, 2*TNFR2 + Not(RIP)),    # TRAF2 10
      (TRAF2, Not(TRAF2)),          # NIK 11
      (NIK + Akt, 3*A20 + And(Not(NIK), Not(Akt))),  # IKK 12
      (NFkB, 2*IKK),                # IkB 13
      (Not(IkB),  IkB),             # NFkB 14
      (NFkB, Not(NFkB)),            # A20 15
      (TRAF, Not(TRAF)),            # MEKK1 16
      (MEKK1, Not(MEKK1)+2*Akt),    # JNKK 17
      (JNKK, Not(JNKK)),            # JNK 18
      (Cas8 + APC, 3*IAP + And(Not(Cas8), Not(APC))), # Cas7 19
      (Cas7, Not(Cas7)),            # Cas12 20
      (Cas12 + Cas3, Akt + Not(Cas12) + 2*IAP),  # Cas9 21
      (And(Cas9, Apaf1, Mito), 2*IAP + Not(Cas9) + Not(Apaf1) + Not(Mito)), # APC 22
      (Cas8 + APC + Cas6, 4*IAP + And(Not(Cas8), Not(APC), Not(Cas6))),   # Cas3 23
      (0, 0), # Cas3prev (special rule) 24
      (Cas3, 2*IAP + Not(Cas3)),  # Cas6 25
      (And(p53, JNK) + And(p53, Cas8), 3*BclX + Not(p53)), # BID 26
      (NFkB, p53 + 2*BAD + Not(NFkB)),                 # BclX 27
      (p53, 2*Akt + Not(p53)),                         # BAD 28
      (JNK + 3*DNAdam, 2*Mdm2 + And(Not(JNK), Not(DNAdam))), # p53 29
      (p53, Not(p53)), # Apaf1 30
      (p53, Not(p53)), # PTEN  31
      (BID, BclX), # Mito 32
      (NFkB, Mito + And(Cas3, Cas6)), # IAP 33
      (GF, Not(GF)), # GFR 34
      (GFR, Not(GFR)), # PI3K 35
      (GFR, Not(GFR)), # PIP2 36
      (And(PI3K, PIP2), Not(PIP2) + 2*PTEN), # PIP3 37
      (PIP3, Not(PIP3)), # Akt 38
      (Akt + p53, Not(Akt)), # Mdm2 39
      (0, 0)]      # DNAdam (special rule) 40


def generate_rules(fn, label, perm):
    d = len(names)
    perm_names = apply_perm(perm, names)
    perm_AH = apply_perm(perm, AH)
    print('PERM: ', perm)

    fs = open(fn, 'w')
    fs.write("RULE_SET({}, {}, {})\n".format(label, d, ','.join(['"'+x+'"' for x in perm_names])));

    str_template_simple = """
template<> bool {}::rule<{}>(bitset<{}> x) {{
    return {};
}}
template<> vector<ind> {}::depends_on<{}>() {{
    return {{ {} }};
}}
"""

    str_template = """
template<> bool {}::rule<{}>(bitset<{}> x) {{
    int A = {};
    int H = {};
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[{}];
}}
template<> vector<ind> {}::depends_on<{}>() {{
    return {{ {} }};
}}
"""
    complete_deps_list = []
    for i, name in zip(range(d), perm_names):
        if name == 'Cas3prev':
            l1 = idx_from_name(["Cas3", "Cas3prev"], perm_names)
            l1_str = [str(x) for x in sorted(l1)]
            fs.write(str_template_simple.format(label, i, d, 'x[{}]'.format(l1[0]), label, i, ','.join(l1_str)))
            complete_deps_list.append(sorted(l1))
        elif name == 'DNAdam':
            l2 = idx_from_name(["DNAdam", "Cas3", "Cas3prev"], perm_names)
            l2_str = [str(x) for x in sorted(l2)]
            fs.write(str_template_simple.format(label, i, d, 'x[{}] && x[{}]'.format(l2[1], l2[2]), label, i, ','.join(l2_str)))
            complete_deps_list.append(sorted(l2))
        else:
            deps = sorted(list(set([i] + idx_from_rule(perm_AH[i][0], perm_names) + idx_from_rule(perm_AH[i][1], perm_names))))
            complete_deps_list.append(deps)

            code_A = ccode(perm_AH[i][0])
            code_H = ccode(perm_AH[i][1])
            for j, name in zip(range(d), perm_names):
                code_A = re.sub('{}([^0-9a-zA-Z]|$)'.format(name), 'x[{}]\\1'.format(j), code_A)
                code_H = re.sub('{}([^0-9a-zA-Z]|$)'.format(name), 'x[{}]\\1'.format(j), code_H)

            fs.write(str_template.format(label, i, d, code_A, code_H, i, label, i, ','.join([str(x) for x in deps])))

    fs.close()
    return complete_deps_list


complete_deps_list = generate_rules('apoptosisa.hpp', "APOPTOSISa", range(len(names)))


# how many edges go from one partition to the other
def metric_cuts(part1, part2, deps1, deps2):
    if len(part1)!=len(deps1) or len(part2)!=len(deps2):
        print('ERROR part1/2 and deps1/2 have to be equal in length')
        exit(1)
    part1_alldeps = sorted([x for l in deps1 for x in l if x not in part1])
    part2_alldeps = sorted([x for l in deps2 for x in l if x not in part2])
    return len(part1_alldeps)/len(part1), len(part2_alldeps)/len(part2)

def metric_gamma(part1, part2, deps1, deps2):
    if len(part1)!=len(deps1) or len(part2)!=len(deps2):
        print('ERROR part1/2 and deps1/2 have to be equal in length')
        exit(1)
    deps1_r = [set(x).intersection(part2) for x in deps1]
    deps2_r = [set(x).intersection(part1) for x in deps2]
    deps1_l = [len(x) for x in deps1_r]
    deps2_l = [len(x) for x in deps2_r]
    return max(deps1_l), max(deps2_l)


d = len(complete_deps_list)
print('partitioning_metric: ', metric_cuts(range(int(d/2)), range(int(d/2)+1,d), complete_deps_list[0:int(d/2)], complete_deps_list[int(d/2)+1:]))


import metis
import networkx as nx
from pylab import *
(edgecuts, parts) = metis.part_graph(complete_deps_list, 2)
print(edgecuts, parts)

part1 = [i for i in range(d) if parts[i]==0]
part2 = [i for i in range(d) if parts[i]==1]
print(len(part1), len(part2))
print(part1)
print(part2)

deps_part1 = [x for (i,x) in zip(range(d),complete_deps_list) if parts[i]==0]
deps_part2 = [x for (i,x) in zip(range(d),complete_deps_list) if parts[i]==1]
print('partitioning_metric new: ', metric_cuts(part1, part2, deps_part1, deps_part2))


perm = part1+part2
generate_rules('apoptosisb.hpp', "APOPTOSISb", perm)

