RULE_SET(APOPTOSIS_REASONABLE, 41, "tnf","gf","TNFR1","TRADD","FADD","RIP","TNFR2","TRAF2","NIK","IKK","IkB","NFkB","A20","PTEN","GFR","PI3K","PIP2","PIP3","Akt","Mdm2","traf","cIAP","Cas8","MEKK1","JNKK","jnk","Cas7","Cas12","Cas9","APC","cas3","Cas3prev","Cas6","BID","BclX","BAD","p53","Apaf1","Mito","iap","DNAdam")

template<> bool APOPTOSIS_REASONABLE::rule<0>(bitset<41> x) {
    int A = x[0];
    int H = 1 - x[0];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[0];
}
template<> vector<ind> APOPTOSIS_REASONABLE::depends_on<0>() {
    return { 0 };
}

template<> bool APOPTOSIS_REASONABLE::rule<1>(bitset<41> x) {
    int A = x[1];
    int H = 1 - x[1];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[1];
}
template<> vector<ind> APOPTOSIS_REASONABLE::depends_on<1>() {
    return { 1 };
}

template<> bool APOPTOSIS_REASONABLE::rule<2>(bitset<41> x) {
    int A = x[0];
    int H = 1 - x[0];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[2];
}
template<> vector<ind> APOPTOSIS_REASONABLE::depends_on<2>() {
    return { 0,2 };
}

template<> bool APOPTOSIS_REASONABLE::rule<3>(bitset<41> x) {
    int A = x[2];
    int H = 1 - x[2];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[3];
}
template<> vector<ind> APOPTOSIS_REASONABLE::depends_on<3>() {
    return { 2,3 };
}

template<> bool APOPTOSIS_REASONABLE::rule<4>(bitset<41> x) {
    int A = x[3];
    int H = 1 - x[3];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[4];
}
template<> vector<ind> APOPTOSIS_REASONABLE::depends_on<4>() {
    return { 3,4 };
}

template<> bool APOPTOSIS_REASONABLE::rule<5>(bitset<41> x) {
    int A = x[3];
    int H = 1 - x[3];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[5];
}
template<> vector<ind> APOPTOSIS_REASONABLE::depends_on<5>() {
    return { 3,5 };
}

template<> bool APOPTOSIS_REASONABLE::rule<6>(bitset<41> x) {
    int A = x[0];
    int H = 1 - x[0];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[6];
}
template<> vector<ind> APOPTOSIS_REASONABLE::depends_on<6>() {
    return { 0,6 };
}

template<> bool APOPTOSIS_REASONABLE::rule<7>(bitset<41> x) {
    int A = x[5];
    int H = -x[5] + 2*x[6] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[7];
}
template<> vector<ind> APOPTOSIS_REASONABLE::depends_on<7>() {
    return { 5,6,7 };
}

template<> bool APOPTOSIS_REASONABLE::rule<8>(bitset<41> x) {
    int A = x[7];
    int H = 1 - x[7];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[8];
}
template<> vector<ind> APOPTOSIS_REASONABLE::depends_on<8>() {
    return { 7,8 };
}

template<> bool APOPTOSIS_REASONABLE::rule<9>(bitset<41> x) {
    int A = x[18] + x[8];
    int H = 3*x[12] + (1 - x[18])*(1 - x[8]);
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[9];
}
template<> vector<ind> APOPTOSIS_REASONABLE::depends_on<9>() {
    return { 8,9,12,18 };
}

template<> bool APOPTOSIS_REASONABLE::rule<10>(bitset<41> x) {
    int A = x[11];
    int H = 2*x[9];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[10];
}
template<> vector<ind> APOPTOSIS_REASONABLE::depends_on<10>() {
    return { 9,10,11 };
}

template<> bool APOPTOSIS_REASONABLE::rule<11>(bitset<41> x) {
    int A = 1 - x[10];
    int H = x[10];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[11];
}
template<> vector<ind> APOPTOSIS_REASONABLE::depends_on<11>() {
    return { 10,11 };
}

template<> bool APOPTOSIS_REASONABLE::rule<12>(bitset<41> x) {
    int A = x[11];
    int H = 1 - x[11];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[12];
}
template<> vector<ind> APOPTOSIS_REASONABLE::depends_on<12>() {
    return { 11,12 };
}

template<> bool APOPTOSIS_REASONABLE::rule<13>(bitset<41> x) {
    int A = x[36];
    int H = 1 - x[36];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[13];
}
template<> vector<ind> APOPTOSIS_REASONABLE::depends_on<13>() {
    return { 13,36 };
}

template<> bool APOPTOSIS_REASONABLE::rule<14>(bitset<41> x) {
    int A = x[1];
    int H = 1 - x[1];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[14];
}
template<> vector<ind> APOPTOSIS_REASONABLE::depends_on<14>() {
    return { 1,14 };
}

template<> bool APOPTOSIS_REASONABLE::rule<15>(bitset<41> x) {
    int A = x[14];
    int H = 1 - x[14];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[15];
}
template<> vector<ind> APOPTOSIS_REASONABLE::depends_on<15>() {
    return { 14,15 };
}

template<> bool APOPTOSIS_REASONABLE::rule<16>(bitset<41> x) {
    int A = x[14];
    int H = 1 - x[14];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[16];
}
template<> vector<ind> APOPTOSIS_REASONABLE::depends_on<16>() {
    return { 14,16 };
}

template<> bool APOPTOSIS_REASONABLE::rule<17>(bitset<41> x) {
    int A = x[15]*x[16];
    int H = -x[16] + 2*x[13] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[17];
}
template<> vector<ind> APOPTOSIS_REASONABLE::depends_on<17>() {
    return { 13,15,16,17 };
}

template<> bool APOPTOSIS_REASONABLE::rule<18>(bitset<41> x) {
    int A = x[17];
    int H = 1 - x[17];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[18];
}
template<> vector<ind> APOPTOSIS_REASONABLE::depends_on<18>() {
    return { 17,18 };
}

template<> bool APOPTOSIS_REASONABLE::rule<19>(bitset<41> x) {
    int A = x[18] + x[36];
    int H = 1 - x[18];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[19];
}
template<> vector<ind> APOPTOSIS_REASONABLE::depends_on<19>() {
    return { 18,19,36 };
}

template<> bool APOPTOSIS_REASONABLE::rule<20>(bitset<41> x) {
    int A = x[3];
    int H = 1 - x[3];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[20];
}
template<> vector<ind> APOPTOSIS_REASONABLE::depends_on<20>() {
    return { 3,20 };
}

template<> bool APOPTOSIS_REASONABLE::rule<21>(bitset<41> x) {
    int A = x[20];
    int H = 1 - x[20];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[21];
}
template<> vector<ind> APOPTOSIS_REASONABLE::depends_on<21>() {
    return { 20,21 };
}

template<> bool APOPTOSIS_REASONABLE::rule<22>(bitset<41> x) {
    int A = x[32] + x[4];
    int H = x[21];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[22];
}
template<> vector<ind> APOPTOSIS_REASONABLE::depends_on<22>() {
    return { 4,21,22,32 };
}

template<> bool APOPTOSIS_REASONABLE::rule<23>(bitset<41> x) {
    int A = x[20];
    int H = 1 - x[20];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[23];
}
template<> vector<ind> APOPTOSIS_REASONABLE::depends_on<23>() {
    return { 20,23 };
}

template<> bool APOPTOSIS_REASONABLE::rule<24>(bitset<41> x) {
    int A = x[23];
    int H = 2*x[18] - x[23] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[24];
}
template<> vector<ind> APOPTOSIS_REASONABLE::depends_on<24>() {
    return { 18,23,24 };
}

template<> bool APOPTOSIS_REASONABLE::rule<25>(bitset<41> x) {
    int A = x[24];
    int H = 1 - x[24];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[25];
}
template<> vector<ind> APOPTOSIS_REASONABLE::depends_on<25>() {
    return { 24,25 };
}

template<> bool APOPTOSIS_REASONABLE::rule<26>(bitset<41> x) {
    int A = x[29] + x[22];
    int H = 3*x[39] + (1 - x[29])*(1 - x[22]);
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[26];
}
template<> vector<ind> APOPTOSIS_REASONABLE::depends_on<26>() {
    return { 22,26,29,39 };
}

template<> bool APOPTOSIS_REASONABLE::rule<27>(bitset<41> x) {
    int A = x[26];
    int H = 1 - x[26];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[27];
}
template<> vector<ind> APOPTOSIS_REASONABLE::depends_on<27>() {
    return { 26,27 };
}

template<> bool APOPTOSIS_REASONABLE::rule<28>(bitset<41> x) {
    int A = x[27] + x[30];
    int H = x[18] - x[27] + 2*x[39] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[28];
}
template<> vector<ind> APOPTOSIS_REASONABLE::depends_on<28>() {
    return { 18,27,28,30,39 };
}

template<> bool APOPTOSIS_REASONABLE::rule<29>(bitset<41> x) {
    int A = x[37]*x[28]*x[38];
    int H = -x[37] - x[28] - x[38] + 2*x[39] + 3;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[29];
}
template<> vector<ind> APOPTOSIS_REASONABLE::depends_on<29>() {
    return { 28,29,37,38,39 };
}

template<> bool APOPTOSIS_REASONABLE::rule<30>(bitset<41> x) {
    int A = x[29] + x[32] + x[22];
    int H = 4*x[39] + (1 - x[29])*(1 - x[32])*(1 - x[22]);
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[30];
}
template<> vector<ind> APOPTOSIS_REASONABLE::depends_on<30>() {
    return { 22,29,30,32,39 };
}

template<> bool APOPTOSIS_REASONABLE::rule<31>(bitset<41> x) {
    return x[30];
}
template<> vector<ind> APOPTOSIS_REASONABLE::depends_on<31>() {
    return { 30,31 };
}

template<> bool APOPTOSIS_REASONABLE::rule<32>(bitset<41> x) {
    int A = x[30];
    int H = -x[30] + 2*x[39] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[32];
}
template<> vector<ind> APOPTOSIS_REASONABLE::depends_on<32>() {
    return { 30,32,39 };
}

template<> bool APOPTOSIS_REASONABLE::rule<33>(bitset<41> x) {
    int A = x[22]*x[36] + x[25]*x[36];
    int H = 3*x[34] - x[36] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[33];
}
template<> vector<ind> APOPTOSIS_REASONABLE::depends_on<33>() {
    return { 22,25,33,34,36 };
}

template<> bool APOPTOSIS_REASONABLE::rule<34>(bitset<41> x) {
    int A = x[11];
    int H = 2*x[35] - x[11] + x[36] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[34];
}
template<> vector<ind> APOPTOSIS_REASONABLE::depends_on<34>() {
    return { 11,34,35,36 };
}

template<> bool APOPTOSIS_REASONABLE::rule<35>(bitset<41> x) {
    int A = x[36];
    int H = 2*x[18] - x[36] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[35];
}
template<> vector<ind> APOPTOSIS_REASONABLE::depends_on<35>() {
    return { 18,35,36 };
}

template<> bool APOPTOSIS_REASONABLE::rule<36>(bitset<41> x) {
    int A = 3*x[40] + x[25];
    int H = 2*x[19] + (1 - x[40])*(1 - x[25]);
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[36];
}
template<> vector<ind> APOPTOSIS_REASONABLE::depends_on<36>() {
    return { 19,25,36,40 };
}

template<> bool APOPTOSIS_REASONABLE::rule<37>(bitset<41> x) {
    int A = x[36];
    int H = 1 - x[36];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[37];
}
template<> vector<ind> APOPTOSIS_REASONABLE::depends_on<37>() {
    return { 36,37 };
}

template<> bool APOPTOSIS_REASONABLE::rule<38>(bitset<41> x) {
    int A = x[33];
    int H = x[34];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[38];
}
template<> vector<ind> APOPTOSIS_REASONABLE::depends_on<38>() {
    return { 33,34,38 };
}

template<> bool APOPTOSIS_REASONABLE::rule<39>(bitset<41> x) {
    int A = x[11];
    int H = x[32]*x[30] + x[38];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[39];
}
template<> vector<ind> APOPTOSIS_REASONABLE::depends_on<39>() {
    return { 11,30,32,38,39 };
}

template<> bool APOPTOSIS_REASONABLE::rule<40>(bitset<41> x) {
    return x[30] && x[31];
}
template<> vector<ind> APOPTOSIS_REASONABLE::depends_on<40>() {
    return { 30,31,40 };
}