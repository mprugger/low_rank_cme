RULE_SET(APOPTOSISref, 41, "tnf","gf","TNFR1","TRADD","traf","FADD","RIP","cIAP","Cas8","TNFR2","TRAF2","NIK","IKK","IkB","NFkB","A20","MEKK1","JNKK","jnk","Cas7","Cas12","Cas9","APC","cas3","Cas3prev","Cas6","BID","BclX","BAD","p53","Apaf1","PTEN","Mito","iap","GFR","PI3K","PIP2","PIP3","Akt","Mdm2","DNAdam")

template<> bool APOPTOSISref::rule<0>(bitset<41> x) {
    int A = x[0];
    int H = 1 - x[0];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[0];
}
template<> vector<ind> APOPTOSISref::depends_on<0>() {
    return { 0 };
}

template<> bool APOPTOSISref::rule<1>(bitset<41> x) {
    int A = x[1];
    int H = 1 - x[1];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[1];
}
template<> vector<ind> APOPTOSISref::depends_on<1>() {
    return { 1 };
}

template<> bool APOPTOSISref::rule<2>(bitset<41> x) {
    int A = x[0];
    int H = 1 - x[0];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[2];
}
template<> vector<ind> APOPTOSISref::depends_on<2>() {
    return { 0,2 };
}

template<> bool APOPTOSISref::rule<3>(bitset<41> x) {
    int A = x[2];
    int H = 1 - x[2];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[3];
}
template<> vector<ind> APOPTOSISref::depends_on<3>() {
    return { 2,3 };
}

template<> bool APOPTOSISref::rule<4>(bitset<41> x) {
    int A = x[3];
    int H = 1 - x[3];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[4];
}
template<> vector<ind> APOPTOSISref::depends_on<4>() {
    return { 3,4 };
}

template<> bool APOPTOSISref::rule<5>(bitset<41> x) {
    int A = x[3];
    int H = 1 - x[3];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[5];
}
template<> vector<ind> APOPTOSISref::depends_on<5>() {
    return { 3,5 };
}

template<> bool APOPTOSISref::rule<6>(bitset<41> x) {
    int A = x[3];
    int H = 1 - x[3];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[6];
}
template<> vector<ind> APOPTOSISref::depends_on<6>() {
    return { 3,6 };
}

template<> bool APOPTOSISref::rule<7>(bitset<41> x) {
    int A = x[4];
    int H = 1 - x[4];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[7];
}
template<> vector<ind> APOPTOSISref::depends_on<7>() {
    return { 4,7 };
}

template<> bool APOPTOSISref::rule<8>(bitset<41> x) {
    int A = x[25] + x[5];
    int H = x[7];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[8];
}
template<> vector<ind> APOPTOSISref::depends_on<8>() {
    return { 5,7,8,25 };
}

template<> bool APOPTOSISref::rule<9>(bitset<41> x) {
    int A = x[0];
    int H = 1 - x[0];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[9];
}
template<> vector<ind> APOPTOSISref::depends_on<9>() {
    return { 0,9 };
}

template<> bool APOPTOSISref::rule<10>(bitset<41> x) {
    int A = x[6];
    int H = -x[6] + 2*x[9] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[10];
}
template<> vector<ind> APOPTOSISref::depends_on<10>() {
    return { 6,9,10 };
}

template<> bool APOPTOSISref::rule<11>(bitset<41> x) {
    int A = x[10];
    int H = 1 - x[10];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[11];
}
template<> vector<ind> APOPTOSISref::depends_on<11>() {
    return { 10,11 };
}

template<> bool APOPTOSISref::rule<12>(bitset<41> x) {
    int A = x[38] + x[11];
    int H = 3*x[15] + (1 - x[38])*(1 - x[11]);
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[12];
}
template<> vector<ind> APOPTOSISref::depends_on<12>() {
    return { 11,12,15,38 };
}

template<> bool APOPTOSISref::rule<13>(bitset<41> x) {
    int A = x[14];
    int H = 2*x[12];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[13];
}
template<> vector<ind> APOPTOSISref::depends_on<13>() {
    return { 12,13,14 };
}

template<> bool APOPTOSISref::rule<14>(bitset<41> x) {
    int A = 1 - x[13];
    int H = x[13];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[14];
}
template<> vector<ind> APOPTOSISref::depends_on<14>() {
    return { 13,14 };
}

template<> bool APOPTOSISref::rule<15>(bitset<41> x) {
    int A = x[14];
    int H = 1 - x[14];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[15];
}
template<> vector<ind> APOPTOSISref::depends_on<15>() {
    return { 14,15 };
}

template<> bool APOPTOSISref::rule<16>(bitset<41> x) {
    int A = x[4];
    int H = 1 - x[4];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[16];
}
template<> vector<ind> APOPTOSISref::depends_on<16>() {
    return { 4,16 };
}

template<> bool APOPTOSISref::rule<17>(bitset<41> x) {
    int A = x[16];
    int H = 2*x[38] - x[16] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[17];
}
template<> vector<ind> APOPTOSISref::depends_on<17>() {
    return { 16,17,38 };
}

template<> bool APOPTOSISref::rule<18>(bitset<41> x) {
    int A = x[17];
    int H = 1 - x[17];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[18];
}
template<> vector<ind> APOPTOSISref::depends_on<18>() {
    return { 17,18 };
}

template<> bool APOPTOSISref::rule<19>(bitset<41> x) {
    int A = x[22] + x[8];
    int H = 3*x[33] + (1 - x[22])*(1 - x[8]);
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[19];
}
template<> vector<ind> APOPTOSISref::depends_on<19>() {
    return { 8,19,22,33 };
}

template<> bool APOPTOSISref::rule<20>(bitset<41> x) {
    int A = x[19];
    int H = 1 - x[19];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[20];
}
template<> vector<ind> APOPTOSISref::depends_on<20>() {
    return { 19,20 };
}

template<> bool APOPTOSISref::rule<21>(bitset<41> x) {
    int A = x[20] + x[23];
    int H = x[38] - x[20] + 2*x[33] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[21];
}
template<> vector<ind> APOPTOSISref::depends_on<21>() {
    return { 20,21,23,33,38 };
}

template<> bool APOPTOSISref::rule<22>(bitset<41> x) {
    int A = x[30]*x[21]*x[32];
    int H = -x[30] - x[21] - x[32] + 2*x[33] + 3;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[22];
}
template<> vector<ind> APOPTOSISref::depends_on<22>() {
    return { 21,22,30,32,33 };
}

template<> bool APOPTOSISref::rule<23>(bitset<41> x) {
    int A = x[22] + x[25] + x[8];
    int H = 4*x[33] + (1 - x[22])*(1 - x[25])*(1 - x[8]);
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[23];
}
template<> vector<ind> APOPTOSISref::depends_on<23>() {
    return { 8,22,23,25,33 };
}

template<> bool APOPTOSISref::rule<24>(bitset<41> x) {
    return x[23];
}
template<> vector<ind> APOPTOSISref::depends_on<24>() {
    return { 23,24 };
}

template<> bool APOPTOSISref::rule<25>(bitset<41> x) {
    int A = x[23];
    int H = -x[23] + 2*x[33] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[25];
}
template<> vector<ind> APOPTOSISref::depends_on<25>() {
    return { 23,25,33 };
}

template<> bool APOPTOSISref::rule<26>(bitset<41> x) {
    int A = x[8]*x[29] + x[18]*x[29];
    int H = 3*x[27] - x[29] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[26];
}
template<> vector<ind> APOPTOSISref::depends_on<26>() {
    return { 8,18,26,27,29 };
}

template<> bool APOPTOSISref::rule<27>(bitset<41> x) {
    int A = x[14];
    int H = 2*x[28] - x[14] + x[29] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[27];
}
template<> vector<ind> APOPTOSISref::depends_on<27>() {
    return { 14,27,28,29 };
}

template<> bool APOPTOSISref::rule<28>(bitset<41> x) {
    int A = x[29];
    int H = 2*x[38] - x[29] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[28];
}
template<> vector<ind> APOPTOSISref::depends_on<28>() {
    return { 28,29,38 };
}

template<> bool APOPTOSISref::rule<29>(bitset<41> x) {
    int A = 3*x[40] + x[18];
    int H = 2*x[39] + (1 - x[40])*(1 - x[18]);
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[29];
}
template<> vector<ind> APOPTOSISref::depends_on<29>() {
    return { 18,29,39,40 };
}

template<> bool APOPTOSISref::rule<30>(bitset<41> x) {
    int A = x[29];
    int H = 1 - x[29];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[30];
}
template<> vector<ind> APOPTOSISref::depends_on<30>() {
    return { 29,30 };
}

template<> bool APOPTOSISref::rule<31>(bitset<41> x) {
    int A = x[29];
    int H = 1 - x[29];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[31];
}
template<> vector<ind> APOPTOSISref::depends_on<31>() {
    return { 29,31 };
}

template<> bool APOPTOSISref::rule<32>(bitset<41> x) {
    int A = x[26];
    int H = x[27];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[32];
}
template<> vector<ind> APOPTOSISref::depends_on<32>() {
    return { 26,27,32 };
}

template<> bool APOPTOSISref::rule<33>(bitset<41> x) {
    int A = x[14];
    int H = x[25]*x[23] + x[32];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[33];
}
template<> vector<ind> APOPTOSISref::depends_on<33>() {
    return { 14,23,25,32,33 };
}

template<> bool APOPTOSISref::rule<34>(bitset<41> x) {
    int A = x[1];
    int H = 1 - x[1];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[34];
}
template<> vector<ind> APOPTOSISref::depends_on<34>() {
    return { 1,34 };
}

template<> bool APOPTOSISref::rule<35>(bitset<41> x) {
    int A = x[34];
    int H = 1 - x[34];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[35];
}
template<> vector<ind> APOPTOSISref::depends_on<35>() {
    return { 34,35 };
}

template<> bool APOPTOSISref::rule<36>(bitset<41> x) {
    int A = x[34];
    int H = 1 - x[34];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[36];
}
template<> vector<ind> APOPTOSISref::depends_on<36>() {
    return { 34,36 };
}

template<> bool APOPTOSISref::rule<37>(bitset<41> x) {
    int A = x[35]*x[36];
    int H = -x[36] + 2*x[31] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[37];
}
template<> vector<ind> APOPTOSISref::depends_on<37>() {
    return { 31,35,36,37 };
}

template<> bool APOPTOSISref::rule<38>(bitset<41> x) {
    int A = x[37];
    int H = 1 - x[37];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[38];
}
template<> vector<ind> APOPTOSISref::depends_on<38>() {
    return { 37,38 };
}

template<> bool APOPTOSISref::rule<39>(bitset<41> x) {
    int A = x[38] + x[29];
    int H = 1 - x[38];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[39];
}
template<> vector<ind> APOPTOSISref::depends_on<39>() {
    return { 29,38,39 };
}

template<> bool APOPTOSISref::rule<40>(bitset<41> x) {
    return x[23] && x[24];
}
template<> vector<ind> APOPTOSISref::depends_on<40>() {
    return { 23,24,40 };
}