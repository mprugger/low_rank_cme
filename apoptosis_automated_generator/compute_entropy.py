from sympy import *
import itertools as it
import math as m
import re
import matplotlib.pyplot as plt
import numpy as np


def rule(tA,tH,specs_in, specs_out, species_in, species_out): 
	
	if 'return' in tA:
		print('BOOOOH')
		exit()
	
	s = 0 
	for i in specs_in:  
		if str(i) in re.findall(r'\b\d+\b', tA):
			tA = tA.replace('x['+str(i)+']', 'species_in['+str(s)+']')
		if str(i) in re.findall(r'\b\d+\b', tH):
			tH = tH.replace('x['+str(i)+']', 'species_in['+str(s)+']')
		s=s+1
	
	s=0
	for i in specs_out: 
		if str(i) in re.findall(r'\b\d+\b', tA):	
			tA = tA.replace('x['+str(i)+']', 'species_out['+str(s)+']')
		if str(i) in re.findall(r'\b\d+\b', tH):	
			tH = tH.replace('x['+str(i)+']', 'species_out['+str(s)+']')
		
		s=s+1 
	
	A = eval(tA)
	H = eval(tH)
	if A>H:
		return True
	if A<H:
		return False
	else:
		return species_in[0]

#def ruleu(A,H,species_in, species_out):
#	return

def comp_entropy(tA, tH, specs_in, specs_out):
	entropy = 0
	l = [False, True]
	inner_list = list(it.product(l, repeat=len(specs_in)))
	outer_list = list(it.product(l, repeat=len(specs_out))) 
	#print(specs_out)

	for i in inner_list:
		count_0 = 0
		count_1 = 0 
		species_in=[]
		for s in range(len(specs_in)):
			species_in.append(i[s])  
		for j in outer_list:
			species_out = []
			for s in range(len(specs_out)):
				species_out.append(j[s])  
			#v = rule(species_in, species_out) 
			v = rule(tA, tH, specs_in, specs_out, species_in, species_out) 
			if v == True:
				count_1=count_1+1
			else:
				count_0=count_0+1 
		s = count_1+count_0
		if count_0==0 or count_1==0:
			E = 0
		else:
			E = -count_0/s*m.log2(count_0/s)-count_1/s*m.log2(count_1/s)
		entropy += E
	entropy = entropy/(len(inner_list))
	#print('entropy: ', entropy)
	return entropy

table = []
for rs in range(739):
	#print('file '+str(rs))
	f = open('analysis'+str(rs),'r')
	Lines = f.readlines()

	rules = []
	for l in Lines:
		x = l.split(';')
		rules.append(l.split(';'))
	#'''
	#print(rules)
	ct = int(rules[1][0])
	#print(ct)
	del rules[1]
	#print(rules)
	#exit()
	#'''
	r = []
	A = []
	H = []
	Astr = []
	Hstr = []
	for i in range(1,len(rules)):
		r.append([int(s) for s in re.findall(r'\b\d+\b', rules[i][0])]) 
		A.append([int(s) for s in re.findall(r'\b\d+\b', str(re.findall('x\[[0-9]{1,2}', rules[i][1])))])
		Astr.append(rules[i][1])
		H.append([int(s) for s in re.findall(r'\b\d+\b', str(re.findall('x\[[0-9]{1,2}', rules[i][2])))])
		Hstr.append(rules[i][2])  
 
	
	idx = []
	spec_inl = []
	spec_outl = []
	spec_inu = []
	spec_outu = []
	for i in range(len(r)):
		nl = list(set(r[i]+A[i]+H[i]))
		if any(item > 19 for item in nl) and any(item <= 19 for item in nl):
			#print(True, nl, i)
			idx.append(i)
			if i <= 19:
				specs = [x for x in nl if x <=19]
				specs.remove(i)
				specs.insert(0,i)
				spec_inl.append(specs)
				spec_outl.append([x for x in nl if x >19])
			else:
				specs = [x for x in nl if x >19]
				specs.remove(i)
				specs.insert(0,i)
				spec_inu.append(specs)
				spec_outu.append([x for x in nl if x <= 19])
			
 
	#print(spec_inl)
	#print(spec_outl)
	#print(spec_inu)
	#print(spec_outu) 
 
	cl = 0
	cu = 0
	entropy = 0
	for i in idx:
		#print(i)
		if i<=19:  
			if 'return' in Astr[i]:
				#print('find')
				if len(re.findall(r'\b\d+\b', Astr[i]))==1: 
					#print('entropy: 1')
					entropy = entropy +1  # Cas3prev is only returning Cas3 -> if they are in different partitions, entropy is max
				elif len(re.findall(r'\b\d+\b', Astr[i]))==2: 
					parts  =  [int(x) for x in (re.findall(r'\b\d+\b', Astr[i]))]
					if sum(i<19 for i in parts) ==1:
						#print('l entropy: 0.5')
						entropy = entropy + 0.5 # one species is not in the same partition as DNAdam
					if sum(i>=19 for i in parts) ==2:
						#print('l entropy 0.811')
						entropy = entropy + 0.811278 # both species are in the other partition as DNAdam
				else:
					print('HAAAEEEE?')
					exit()
				#print(Astr[i])
				cl = cl+1 
			else:  
				entropy = entropy + comp_entropy(Astr[i].split('{ A = ')[1], Hstr[i].split(' H = ')[1], spec_inl[cl], spec_outl[cl])  
				cl = cl+1
		else:  
			#print('rulesu')
			if 'return' in Astr[i]: 
				if len(re.findall(r'\b\d+\b', Astr[i]))==1:
					#print('entropy: 1')
					entropy = entropy +1  # Cas3prev is only returning Cas3 -> if they are in different partitions, entropy is max
				elif len(re.findall(r'\b\d+\b', Astr[i]))==2: 
					parts  =  [int(x) for x in (re.findall(r'\b\d+\b', Astr[i]))]
					if sum(i>=19 for i in parts) ==1:
						#print('u entropy: 0.5')
						entropy = entropy + 0.5 # one species is not in the same partition as DNAdam
					if sum(i<19 for i in parts) ==2:
						#print('u entropy 0.811')
						entropy = entropy + 0.811278 # both species are in the other partition as DNAdam
				else:
					print('HAAAEEEE?')
					exit()
				#print(Astr[i])
				cu = cu+1
			else: 
				entropy = entropy + comp_entropy(Astr[i].split('{ A = ')[1], Hstr[i].split(' H = ')[1], spec_inu[cu], spec_outu[cu]) 
				cu = cu+1

	#print('total entropy file '+str(rs), entropy, ' and was found ', ct, ' times')
	f.close()
	table.append((ct, entropy))



#print(table)
#print(type(table[0]))
#num = [tup[0] for tup in table]
#print(num)
#exit()
tab_sort = sorted(table, key = lambda x: x[0], reverse=True)
tab_sort_large = tab_sort[0:24]
tab_sort_large = sorted(tab_sort_large, key = lambda x: x[1], reverse=False)
#print(tab_sort)
#exit()
entl = [tup[1] for tup in tab_sort_large]
numl = [tup[0] for tup in tab_sort_large]
#print(num)
#exit()
#print(tab_sort)
x_pos = np.arange(len(numl))
plt.bar(x_pos, entl)
plt.title('most often found NWs (115-689)')
plt.xlabel('number of NW hits')
plt.ylabel('entropy')
plt.xticks(x_pos, numl)
plt.show()

tab_sort_l2 = tab_sort[24:43]
tab_sort_l2 = sorted(tab_sort_l2, key = lambda x: x[1], reverse=False)
entl2 = [tup[1] for tup in tab_sort_l2]
numl2 = [tup[0] for tup in tab_sort_l2]
x_pos = np.arange(len(numl2))
plt.bar(x_pos, entl2)
plt.title('2nd most often found (50-100)')
plt.xlabel('number of NW hits')
plt.ylabel('entropy')
plt.xticks(x_pos, numl2)
plt.show()

tab_sort_l3 = tab_sort[43:91]
tab_sort_l3 = sorted(tab_sort_l3, key = lambda x: x[1], reverse=False)
entl3 = [tup[1] for tup in tab_sort_l3]
numl3 = [tup[0] for tup in tab_sort_l3]
x_pos = np.arange(len(numl3))
plt.bar(x_pos, entl3)
plt.title('3rd most often found (12-50)')
plt.xlabel('number of NW hits')
plt.ylabel('entropy')
plt.xticks(x_pos, numl3)
plt.show()

tab_sort_l4 = tab_sort[91:110]
tab_sort_l4 = sorted(tab_sort_l4, key = lambda x: x[1], reverse=False)
entl4 = [tup[1] for tup in tab_sort_l4]
numl4 = [tup[0] for tup in tab_sort_l4]
x_pos = np.arange(len(numl4))
plt.bar(x_pos, entl4)
plt.title('4rd most often found (8-11)')
plt.xlabel('number of NW hits')
plt.ylabel('entropy')
plt.xticks(x_pos, numl4)
plt.show()

tab_sort_l5 = tab_sort[110:121]
tab_sort_l5 = sorted(tab_sort_l5, key = lambda x: x[1], reverse=False)
entl5 = [tup[1] for tup in tab_sort_l5]
numl5 = [tup[0] for tup in tab_sort_l5]
x_pos = np.arange(len(numl5))
plt.bar(x_pos, entl5)
plt.title('found 7 times')
plt.xlabel('number of NW hits')
plt.ylabel('entropy')
plt.xticks(x_pos, numl5)
plt.show()

tab_sort_l6 = tab_sort[121:133]
tab_sort_l6 = sorted(tab_sort_l6, key = lambda x: x[1], reverse=False)
entl6 = [tup[1] for tup in tab_sort_l6]
numl6 = [tup[0] for tup in tab_sort_l6]
x_pos = np.arange(len(numl6))
plt.bar(x_pos, entl6)
plt.title('found 6 times')
plt.xlabel('number of NW hits')
plt.ylabel('entropy')
plt.xticks(x_pos, numl6)
plt.show()

tab_sort_l7 = tab_sort[133:156]
tab_sort_l7 = sorted(tab_sort_l7, key = lambda x: x[1], reverse=False)
entl7 = [tup[1] for tup in tab_sort_l7]
numl7 = [tup[0] for tup in tab_sort_l7]
x_pos = np.arange(len(numl7))
plt.bar(x_pos, entl7)
plt.title('found 5 times')
plt.xlabel('number of NW hits')
plt.ylabel('entropy')
plt.xticks(x_pos, numl7)
plt.show()

tab_sort_l8 = tab_sort[156:191]
tab_sort_l8 = sorted(tab_sort_l8, key = lambda x: x[1], reverse=False)
entl8 = [tup[1] for tup in tab_sort_l8]
numl8 = [tup[0] for tup in tab_sort_l8]
x_pos = np.arange(len(numl8))
plt.bar(x_pos, entl8)
plt.title('found 4 times')
plt.xlabel('number of NW hits')
plt.ylabel('entropy')
plt.xticks(x_pos, numl8)
plt.show()

tab_sort_l9 = tab_sort[191:259]
tab_sort_l9 = sorted(tab_sort_l9, key = lambda x: x[1], reverse=False)
entl9 = [tup[1] for tup in tab_sort_l9]
numl9 = [tup[0] for tup in tab_sort_l9]
x_pos = np.arange(len(numl9))
plt.bar(x_pos, entl9)
plt.title('found 3 times')
plt.xlabel('number of NW hits')
plt.ylabel('entropy')
plt.xticks(x_pos, numl9)
plt.show()

tab_sort_l10 = tab_sort[259:380]
tab_sort_l10 = sorted(tab_sort_l10, key = lambda x: x[1], reverse=False)
entl10 = [tup[1] for tup in tab_sort_l10]
numl10 = [tup[0] for tup in tab_sort_l10]
x_pos = np.arange(len(numl10))
plt.bar(x_pos, entl10)
plt.title('found 2 times')
plt.xlabel('number of NW hits')
plt.ylabel('entropy')
plt.xticks(x_pos, numl10)
plt.show()

tab_sort_l11 = tab_sort[380:]
tab_sort_l11 = sorted(tab_sort_l11, key = lambda x: x[1], reverse=False)
entl11 = [tup[1] for tup in tab_sort_l11]
numl11 = [tup[0] for tup in tab_sort_l11]
x_pos = np.arange(len(numl11))
plt.bar(x_pos, entl11)
plt.title('found 1 time')
plt.xlabel('number of NW hits')
plt.ylabel('entropy')
plt.xticks(x_pos, numl11)
plt.show()