from sympy import *
import re
from collections import Counter
import networkx as nx
from pylab import *
#print(f"Complete list = {complete_deps_list}")

    
def metis2Edgeset(g):
    edgelist = []
    for (i, l) in enumerate(g):
        for e in l:
            if i != e:
                edgelist.append((i,e))
    return edgelist


# ================================= create initial setup =================================

names = ["tnf", "gf", "TNFR1", "TRADD", "traf", "FADD", "RIP", "cIAP", "Cas8", "TNFR2", "TRAF2", "NIK", "IKK", "IkB", "NFkB", "A20", "MEKK1", "JNKK", "jnk", "Cas7", "Cas12", "Cas9", "APC", "cas3", "Cas3prev", "Cas6", "BID", "BclX", "BAD", "p53", "Apaf1", "PTEN", "Mito", "iap", "GFR", "PI3K", "PIP2", "PIP3", "Akt", "Mdm2", "DNAdam"]

def idx_from_rule(rule, names):
    return [names.index(y) for y in [str(x) for x in rule.free_symbols]]

def idx_from_name(l_name, names):
    return [names.index(y) for y in l_name]

for name in names:
    locals()[name] = symbols(name)

def apply_perm(perm, states):
    N = len(perm)
    print(perm)
    states_t = [states[perm[i]] for i in range(N)]
    return states_t

def And(A, B, C=1):
    return A*B*C

def Not(A):
    return 1-A


AH = [(tnf, Not(tnf)),         # TNF 0
      (gf,  Not(gf)),          # GF 1
      (tnf, Not(tnf)),         # TNFR1 2
      (TNFR1, Not(TNFR1)),     # TRADD )
      (TRADD, Not(TRADD)),     # TRAF 4
      (TRADD, Not(TRADD)),     # FADD 5
      (TRADD, Not(TRADD)),     # RIP 6
      (traf, Not(traf)),       # cIAP 7
      (FADD+Cas6, cIAP),       # Cas8 8
      (tnf, Not(tnf)),         # TNFR2 9
      (RIP, 2*TNFR2 + Not(RIP)),    # TRAF2 10
      (TRAF2, Not(TRAF2)),          # NIK 11
      (NIK + Akt, 3*A20 + And(Not(NIK), Not(Akt))),  # IKK 12
      (NFkB, 2*IKK),                # IkB 13
      (Not(IkB),  IkB),             # NFkB 14
      (NFkB, Not(NFkB)),            # A20 15
      (traf, Not(traf)),            # MEKK1 16
      (MEKK1, Not(MEKK1)+2*Akt),    # JNKK 17
      (JNKK, Not(JNKK)),            # JNK 18
      (Cas8 + APC, 3*iap + And(Not(Cas8), Not(APC))), # Cas7 19
      (Cas7, Not(Cas7)),            # Cas12 20
      (Cas12 + cas3, Akt + Not(Cas12) + 2*iap),  # Cas9 21
      (And(Cas9, Apaf1, Mito), 2*iap + Not(Cas9) + Not(Apaf1) + Not(Mito)), # APC 22
      (Cas8 + APC + Cas6, 4*iap + And(Not(Cas8), Not(APC), Not(Cas6))),   # Cas3 23
      (0, 0), # Cas3prev (special rule) 24
      (cas3, 2*iap + Not(cas3)),  # Cas6 25
      (And(p53, jnk) + And(p53, Cas8), 3*BclX + Not(p53)), # BID 26
      (NFkB, p53 + 2*BAD + Not(NFkB)),                 # BclX 27
      (p53, 2*Akt + Not(p53)),                         # BAD 28
      (jnk + 3*DNAdam, 2*Mdm2 + And(Not(jnk), Not(DNAdam))), # p53 29
      (p53, Not(p53)), # Apaf1 30
      (p53, Not(p53)), # PTEN  31
      (BID, BclX), # Mito 32
      (NFkB, Mito + And(cas3, Cas6)), # IAP 33
      (gf, Not(gf)), # GFR 34
      (GFR, Not(GFR)), # PI3K 35
      (GFR, Not(GFR)), # PIP2 36
      (And(PI3K, PIP2), Not(PIP2) + 2*PTEN), # PIP3 37
      (PIP3, Not(PIP3)), # Akt 38
      (Akt + p53, Not(Akt)), # Mdm2 39
      (0, 0)]      # DNAdam (special rule) 40


def generate_rules(f_rules, f_analysis, label, perm, cts):
    d = len(names)
    perm_names = apply_perm(perm, names)
    perm_AH = apply_perm(perm, AH)
    #print('PERM: ', perm)

    name_string = ', '.join(perm_names)
    fa = open(f_analysis, 'w')
    fa.writelines(name_string)
    fa.write('\n')
    fa.write(str(cts))
    fa.write('\n')

    fs = open(f_rules, 'w')
    fs.write("RULE_SET({}, {}, {})\n".format(label, d, ','.join(['"'+x+'"' for x in perm_names])));

    str_template_simple = """
template<> bool {}::rule<{}>(bitset<{}> x) {{
    return {};
}}
template<> vector<ind> {}::depends_on<{}>() {{
    return {{ {} }};
}}
"""

    str_template = """
template<> bool {}::rule<{}>(bitset<{}> x) {{
    int A = {};
    int H = {};
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[{}];
}}
template<> vector<ind> {}::depends_on<{}>() {{
    return {{ {} }};
}}
"""

    analysis_template_simple = """rule<{}>; {{ return {}; }} 
"""
    analysis_template = """rule<{}>; {{ A = {}; H = {}; }} 
"""
    
    
    complete_deps_list = []
    for i, name in zip(range(d), perm_names):
        if name == 'Cas3prev':
            l1 = idx_from_name(["cas3", "Cas3prev"], perm_names)
            l1_str = [str(x) for x in sorted(l1)]
            fs.write(str_template_simple.format(label, i, d, 'x[{}]'.format(l1[0]), label, i, ','.join(l1_str)))
            complete_deps_list.append(sorted(l1))
            fa.write(analysis_template_simple.format(i,'x[{}]'.format(l1[0]), i, ','.join(l1_str)))
        elif name == 'DNAdam':
            l2 = idx_from_name(["DNAdam", "cas3", "Cas3prev"], perm_names)
            l2_str = [str(x) for x in sorted(l2)]
            fs.write(str_template_simple.format(label, i, d, 'x[{}] && x[{}]'.format(l2[1], l2[2]), label, i, ','.join(l2_str)))
            complete_deps_list.append(sorted(l2))
            fa.write(analysis_template_simple.format(i,'x[{}] && x[{}]'.format(l2[1], l2[2]), label, i, ','.join(l2_str)))
        else:
            deps = sorted(list(set([i] + idx_from_rule(perm_AH[i][0], perm_names) + idx_from_rule(perm_AH[i][1], perm_names))))
            complete_deps_list.append(deps)

            code_A = ccode(perm_AH[i][0])
            code_H = ccode(perm_AH[i][1])
            for j, name in zip(range(d), perm_names):
                code_A = re.sub('{}([^0-9a-zA-Z]|$)'.format(name), 'x[{}]\\1'.format(j), code_A)
                code_H = re.sub('{}([^0-9a-zA-Z]|$)'.format(name), 'x[{}]\\1'.format(j), code_H)

            fs.write(str_template.format(label, i, d, code_A, code_H, i, label, i, ','.join([str(x) for x in deps])))
            fa.write(analysis_template.format(i, code_A, code_H, ','.join([str(x) for x in deps])))

    fs.close()
    fa.close()
    return complete_deps_list

# ============================ create the reference rule file ==================================================
complete_deps_list = generate_rules('apoptosisref.hpp', 'analysis.txt', "APOPTOSISref", range(len(names)), 0)


 
# ============================= create networks with Kernighan Lin algorithm ====================================    
found_NWs = []    
for i in range(10):   
	G = nx.Graph(metis2Edgeset(complete_deps_list))
	GD = nx.DiGraph(metis2Edgeset(complete_deps_list))
	#startpartition = ({8, 3, 6, 14, 1, 12, 17, 10, 16, 9, 15}, {19, 18, 11, 2, 20, 7, 21, 0, 13, 5, 4}) # set initial seed if you want to recreate the same result all the time
	bisection = nx.algorithms.community.kernighan_lin_bisection(G, max_iter=2**32)
	found_NWs.append(str(bisection))
	print(found_NWs)
exit()

# ============================ count how often which network is found ==========================================
stats_NW = Counter(found_NWs)

perm = []
for i in stats_NW:
	perm.append(([int(s) for s in re.findall(r'\d+', i)], stats_NW[i]))


# ============================ create the rule-files (info of how often this network was found is the last number) ============================
for i in range(len(perm)):
	complete_deps_list = generate_rules('apoptosis'+str(i)+'.hpp','analysis'+str(i), 'APOPTOSIS'+str(i)+'_'+str(perm[i][1]), perm[i][0], perm[i][1])

