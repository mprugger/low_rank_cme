RULE_SET(APOPTOSIS41, 41, "TNF","GF","TNFR1","TRADD","TRAF","FADD","RIP","TNFR2","TRAF2","NIK","IKK","IkB","NFkB","A20","MEKK1","PTEN","GFR","PI3K","PIP2","PIP3","cIAP","Cas8","JNKK","JNK","Cas7","Cas12","Cas9","APC","Cas3","Cas3prev","Cas6","BID","BclX","BAD","p53","Apaf1","Mito","IAP","Akt","Mdm2","DNAdam")

template<> bool APOPTOSIS41::rule<0>(bitset<41> x) {
    int A = x[0];
    int H = 1 - x[0];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[0];
}
template<> vector<ind> APOPTOSIS41::depends_on<0>() {
    return { 0 };
}

template<> bool APOPTOSIS41::rule<1>(bitset<41> x) {
    int A = x[1];
    int H = 1 - x[1];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[1];
}
template<> vector<ind> APOPTOSIS41::depends_on<1>() {
    return { 1 };
}

template<> bool APOPTOSIS41::rule<2>(bitset<41> x) {
    int A = x[0];
    int H = 1 - x[0];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[2];
}
template<> vector<ind> APOPTOSIS41::depends_on<2>() {
    return { 0,2 };
}

template<> bool APOPTOSIS41::rule<3>(bitset<41> x) {
    int A = x[2];
    int H = 1 - x[2];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[3];
}
template<> vector<ind> APOPTOSIS41::depends_on<3>() {
    return { 2,3 };
}

template<> bool APOPTOSIS41::rule<4>(bitset<41> x) {
    int A = x[3];
    int H = 1 - x[3];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[4];
}
template<> vector<ind> APOPTOSIS41::depends_on<4>() {
    return { 3,4 };
}

template<> bool APOPTOSIS41::rule<5>(bitset<41> x) {
    int A = x[3];
    int H = 1 - x[3];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[5];
}
template<> vector<ind> APOPTOSIS41::depends_on<5>() {
    return { 3,5 };
}

template<> bool APOPTOSIS41::rule<6>(bitset<41> x) {
    int A = x[3];
    int H = 1 - x[3];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[6];
}
template<> vector<ind> APOPTOSIS41::depends_on<6>() {
    return { 3,6 };
}

template<> bool APOPTOSIS41::rule<7>(bitset<41> x) {
    int A = x[0];
    int H = 1 - x[0];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[7];
}
template<> vector<ind> APOPTOSIS41::depends_on<7>() {
    return { 0,7 };
}

template<> bool APOPTOSIS41::rule<8>(bitset<41> x) {
    int A = x[6];
    int H = -x[6] + 2*x[7] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[8];
}
template<> vector<ind> APOPTOSIS41::depends_on<8>() {
    return { 6,7,8 };
}

template<> bool APOPTOSIS41::rule<9>(bitset<41> x) {
    int A = x[8];
    int H = 1 - x[8];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[9];
}
template<> vector<ind> APOPTOSIS41::depends_on<9>() {
    return { 8,9 };
}

template<> bool APOPTOSIS41::rule<10>(bitset<41> x) {
    int A = x[38] + x[9];
    int H = 3*x[13] + (1 - x[38])*(1 - x[9]);
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[10];
}
template<> vector<ind> APOPTOSIS41::depends_on<10>() {
    return { 9,10,13,38 };
}

template<> bool APOPTOSIS41::rule<11>(bitset<41> x) {
    int A = x[12];
    int H = 2*x[10];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[11];
}
template<> vector<ind> APOPTOSIS41::depends_on<11>() {
    return { 10,11,12 };
}

template<> bool APOPTOSIS41::rule<12>(bitset<41> x) {
    int A = 1 - x[11];
    int H = x[11];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[12];
}
template<> vector<ind> APOPTOSIS41::depends_on<12>() {
    return { 11,12 };
}

template<> bool APOPTOSIS41::rule<13>(bitset<41> x) {
    int A = x[12];
    int H = 1 - x[12];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[13];
}
template<> vector<ind> APOPTOSIS41::depends_on<13>() {
    return { 12,13 };
}

template<> bool APOPTOSIS41::rule<14>(bitset<41> x) {
    int A = x[4];
    int H = 1 - x[4];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[14];
}
template<> vector<ind> APOPTOSIS41::depends_on<14>() {
    return { 4,14 };
}

template<> bool APOPTOSIS41::rule<15>(bitset<41> x) {
    int A = x[34];
    int H = 1 - x[34];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[15];
}
template<> vector<ind> APOPTOSIS41::depends_on<15>() {
    return { 15,34 };
}

template<> bool APOPTOSIS41::rule<16>(bitset<41> x) {
    int A = x[1];
    int H = 1 - x[1];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[16];
}
template<> vector<ind> APOPTOSIS41::depends_on<16>() {
    return { 1,16 };
}

template<> bool APOPTOSIS41::rule<17>(bitset<41> x) {
    int A = x[16];
    int H = 1 - x[16];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[17];
}
template<> vector<ind> APOPTOSIS41::depends_on<17>() {
    return { 16,17 };
}

template<> bool APOPTOSIS41::rule<18>(bitset<41> x) {
    int A = x[16];
    int H = 1 - x[16];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[18];
}
template<> vector<ind> APOPTOSIS41::depends_on<18>() {
    return { 16,18 };
}

template<> bool APOPTOSIS41::rule<19>(bitset<41> x) {
    int A = x[17]*x[18];
    int H = -x[18] + 2*x[15] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[19];
}
template<> vector<ind> APOPTOSIS41::depends_on<19>() {
    return { 15,17,18,19 };
}

template<> bool APOPTOSIS41::rule<20>(bitset<41> x) {
    int A = x[4];
    int H = 1 - x[4];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[20];
}
template<> vector<ind> APOPTOSIS41::depends_on<20>() {
    return { 4,20 };
}

template<> bool APOPTOSIS41::rule<21>(bitset<41> x) {
    int A = x[30] + x[5];
    int H = x[20];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[21];
}
template<> vector<ind> APOPTOSIS41::depends_on<21>() {
    return { 5,20,21,30 };
}

template<> bool APOPTOSIS41::rule<22>(bitset<41> x) {
    int A = x[14];
    int H = 2*x[38] - x[14] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[22];
}
template<> vector<ind> APOPTOSIS41::depends_on<22>() {
    return { 14,22,38 };
}

template<> bool APOPTOSIS41::rule<23>(bitset<41> x) {
    int A = x[22];
    int H = 1 - x[22];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[23];
}
template<> vector<ind> APOPTOSIS41::depends_on<23>() {
    return { 22,23 };
}

template<> bool APOPTOSIS41::rule<24>(bitset<41> x) {
    int A = x[27] + x[21];
    int H = 3*x[37] + (1 - x[27])*(1 - x[21]);
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[24];
}
template<> vector<ind> APOPTOSIS41::depends_on<24>() {
    return { 21,24,27,37 };
}

template<> bool APOPTOSIS41::rule<25>(bitset<41> x) {
    int A = x[24];
    int H = 1 - x[24];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[25];
}
template<> vector<ind> APOPTOSIS41::depends_on<25>() {
    return { 24,25 };
}

template<> bool APOPTOSIS41::rule<26>(bitset<41> x) {
    int A = x[25] + x[28];
    int H = x[38] - x[25] + 2*x[37] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[26];
}
template<> vector<ind> APOPTOSIS41::depends_on<26>() {
    return { 25,26,28,37,38 };
}

template<> bool APOPTOSIS41::rule<27>(bitset<41> x) {
    int A = x[35]*x[26]*x[36];
    int H = -x[35] - x[26] + 2*x[37] - x[36] + 3;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[27];
}
template<> vector<ind> APOPTOSIS41::depends_on<27>() {
    return { 26,27,35,36,37 };
}

template<> bool APOPTOSIS41::rule<28>(bitset<41> x) {
    int A = x[27] + x[30] + x[21];
    int H = 4*x[37] + (1 - x[27])*(1 - x[30])*(1 - x[21]);
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[28];
}
template<> vector<ind> APOPTOSIS41::depends_on<28>() {
    return { 21,27,28,30,37 };
}

template<> bool APOPTOSIS41::rule<29>(bitset<41> x) {
    return x[28];
}
template<> vector<ind> APOPTOSIS41::depends_on<29>() {
    return { 28,29 };
}

template<> bool APOPTOSIS41::rule<30>(bitset<41> x) {
    int A = x[28];
    int H = -x[28] + 2*x[37] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[30];
}
template<> vector<ind> APOPTOSIS41::depends_on<30>() {
    return { 28,30,37 };
}

template<> bool APOPTOSIS41::rule<31>(bitset<41> x) {
    int A = x[21]*x[34] + x[23]*x[34];
    int H = 3*x[32] - x[34] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[31];
}
template<> vector<ind> APOPTOSIS41::depends_on<31>() {
    return { 21,23,31,32,34 };
}

template<> bool APOPTOSIS41::rule<32>(bitset<41> x) {
    int A = x[12];
    int H = 2*x[33] - x[12] + x[34] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[32];
}
template<> vector<ind> APOPTOSIS41::depends_on<32>() {
    return { 12,32,33,34 };
}

template<> bool APOPTOSIS41::rule<33>(bitset<41> x) {
    int A = x[34];
    int H = 2*x[38] - x[34] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[33];
}
template<> vector<ind> APOPTOSIS41::depends_on<33>() {
    return { 33,34,38 };
}

template<> bool APOPTOSIS41::rule<34>(bitset<41> x) {
    int A = 3*x[40] + x[23];
    int H = 2*x[39] + (1 - x[40])*(1 - x[23]);
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[34];
}
template<> vector<ind> APOPTOSIS41::depends_on<34>() {
    return { 23,34,39,40 };
}

template<> bool APOPTOSIS41::rule<35>(bitset<41> x) {
    int A = x[34];
    int H = 1 - x[34];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[35];
}
template<> vector<ind> APOPTOSIS41::depends_on<35>() {
    return { 34,35 };
}

template<> bool APOPTOSIS41::rule<36>(bitset<41> x) {
    int A = x[31];
    int H = x[32];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[36];
}
template<> vector<ind> APOPTOSIS41::depends_on<36>() {
    return { 31,32,36 };
}

template<> bool APOPTOSIS41::rule<37>(bitset<41> x) {
    int A = x[12];
    int H = x[28]*x[30] + x[36];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[37];
}
template<> vector<ind> APOPTOSIS41::depends_on<37>() {
    return { 12,28,30,36,37 };
}

template<> bool APOPTOSIS41::rule<38>(bitset<41> x) {
    int A = x[19];
    int H = 1 - x[19];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[38];
}
template<> vector<ind> APOPTOSIS41::depends_on<38>() {
    return { 19,38 };
}

template<> bool APOPTOSIS41::rule<39>(bitset<41> x) {
    int A = x[38] + x[34];
    int H = 1 - x[38];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[39];
}
template<> vector<ind> APOPTOSIS41::depends_on<39>() {
    return { 34,38,39 };
}

template<> bool APOPTOSIS41::rule<40>(bitset<41> x) {
    return x[28] && x[29];
}
template<> vector<ind> APOPTOSIS41::depends_on<40>() {
    return { 28,29,40 };
}
