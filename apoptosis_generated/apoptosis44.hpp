RULE_SET(APOPTOSIS44, 41, "TNF","GF","TNFR1","TRADD","TRAF","FADD","RIP","cIAP","TNFR2","TRAF2","NIK","IKK","IkB","NFkB","A20","MEKK1","GFR","PI3K","PIP2","PIP3","Cas8","JNKK","JNK","Cas7","Cas12","Cas9","APC","Cas3","Cas3prev","Cas6","BID","BclX","BAD","p53","Apaf1","PTEN","Mito","IAP","Akt","Mdm2","DNAdam")

template<> bool APOPTOSIS44::rule<0>(bitset<41> x) {
    int A = x[0];
    int H = 1 - x[0];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[0];
}
template<> vector<ind> APOPTOSIS44::depends_on<0>() {
    return { 0 };
}

template<> bool APOPTOSIS44::rule<1>(bitset<41> x) {
    int A = x[1];
    int H = 1 - x[1];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[1];
}
template<> vector<ind> APOPTOSIS44::depends_on<1>() {
    return { 1 };
}

template<> bool APOPTOSIS44::rule<2>(bitset<41> x) {
    int A = x[0];
    int H = 1 - x[0];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[2];
}
template<> vector<ind> APOPTOSIS44::depends_on<2>() {
    return { 0,2 };
}

template<> bool APOPTOSIS44::rule<3>(bitset<41> x) {
    int A = x[2];
    int H = 1 - x[2];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[3];
}
template<> vector<ind> APOPTOSIS44::depends_on<3>() {
    return { 2,3 };
}

template<> bool APOPTOSIS44::rule<4>(bitset<41> x) {
    int A = x[3];
    int H = 1 - x[3];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[4];
}
template<> vector<ind> APOPTOSIS44::depends_on<4>() {
    return { 3,4 };
}

template<> bool APOPTOSIS44::rule<5>(bitset<41> x) {
    int A = x[3];
    int H = 1 - x[3];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[5];
}
template<> vector<ind> APOPTOSIS44::depends_on<5>() {
    return { 3,5 };
}

template<> bool APOPTOSIS44::rule<6>(bitset<41> x) {
    int A = x[3];
    int H = 1 - x[3];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[6];
}
template<> vector<ind> APOPTOSIS44::depends_on<6>() {
    return { 3,6 };
}

template<> bool APOPTOSIS44::rule<7>(bitset<41> x) {
    int A = x[4];
    int H = 1 - x[4];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[7];
}
template<> vector<ind> APOPTOSIS44::depends_on<7>() {
    return { 4,7 };
}

template<> bool APOPTOSIS44::rule<8>(bitset<41> x) {
    int A = x[0];
    int H = 1 - x[0];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[8];
}
template<> vector<ind> APOPTOSIS44::depends_on<8>() {
    return { 0,8 };
}

template<> bool APOPTOSIS44::rule<9>(bitset<41> x) {
    int A = x[6];
    int H = -x[6] + 2*x[8] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[9];
}
template<> vector<ind> APOPTOSIS44::depends_on<9>() {
    return { 6,8,9 };
}

template<> bool APOPTOSIS44::rule<10>(bitset<41> x) {
    int A = x[9];
    int H = 1 - x[9];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[10];
}
template<> vector<ind> APOPTOSIS44::depends_on<10>() {
    return { 9,10 };
}

template<> bool APOPTOSIS44::rule<11>(bitset<41> x) {
    int A = x[38] + x[10];
    int H = 3*x[14] + (1 - x[38])*(1 - x[10]);
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[11];
}
template<> vector<ind> APOPTOSIS44::depends_on<11>() {
    return { 10,11,14,38 };
}

template<> bool APOPTOSIS44::rule<12>(bitset<41> x) {
    int A = x[13];
    int H = 2*x[11];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[12];
}
template<> vector<ind> APOPTOSIS44::depends_on<12>() {
    return { 11,12,13 };
}

template<> bool APOPTOSIS44::rule<13>(bitset<41> x) {
    int A = 1 - x[12];
    int H = x[12];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[13];
}
template<> vector<ind> APOPTOSIS44::depends_on<13>() {
    return { 12,13 };
}

template<> bool APOPTOSIS44::rule<14>(bitset<41> x) {
    int A = x[13];
    int H = 1 - x[13];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[14];
}
template<> vector<ind> APOPTOSIS44::depends_on<14>() {
    return { 13,14 };
}

template<> bool APOPTOSIS44::rule<15>(bitset<41> x) {
    int A = x[4];
    int H = 1 - x[4];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[15];
}
template<> vector<ind> APOPTOSIS44::depends_on<15>() {
    return { 4,15 };
}

template<> bool APOPTOSIS44::rule<16>(bitset<41> x) {
    int A = x[1];
    int H = 1 - x[1];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[16];
}
template<> vector<ind> APOPTOSIS44::depends_on<16>() {
    return { 1,16 };
}

template<> bool APOPTOSIS44::rule<17>(bitset<41> x) {
    int A = x[16];
    int H = 1 - x[16];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[17];
}
template<> vector<ind> APOPTOSIS44::depends_on<17>() {
    return { 16,17 };
}

template<> bool APOPTOSIS44::rule<18>(bitset<41> x) {
    int A = x[16];
    int H = 1 - x[16];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[18];
}
template<> vector<ind> APOPTOSIS44::depends_on<18>() {
    return { 16,18 };
}

template<> bool APOPTOSIS44::rule<19>(bitset<41> x) {
    int A = x[17]*x[18];
    int H = -x[18] + 2*x[35] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[19];
}
template<> vector<ind> APOPTOSIS44::depends_on<19>() {
    return { 17,18,19,35 };
}

template<> bool APOPTOSIS44::rule<20>(bitset<41> x) {
    int A = x[29] + x[5];
    int H = x[7];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[20];
}
template<> vector<ind> APOPTOSIS44::depends_on<20>() {
    return { 5,7,20,29 };
}

template<> bool APOPTOSIS44::rule<21>(bitset<41> x) {
    int A = x[15];
    int H = 2*x[38] - x[15] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[21];
}
template<> vector<ind> APOPTOSIS44::depends_on<21>() {
    return { 15,21,38 };
}

template<> bool APOPTOSIS44::rule<22>(bitset<41> x) {
    int A = x[21];
    int H = 1 - x[21];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[22];
}
template<> vector<ind> APOPTOSIS44::depends_on<22>() {
    return { 21,22 };
}

template<> bool APOPTOSIS44::rule<23>(bitset<41> x) {
    int A = x[26] + x[20];
    int H = 3*x[37] + (1 - x[26])*(1 - x[20]);
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[23];
}
template<> vector<ind> APOPTOSIS44::depends_on<23>() {
    return { 20,23,26,37 };
}

template<> bool APOPTOSIS44::rule<24>(bitset<41> x) {
    int A = x[23];
    int H = 1 - x[23];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[24];
}
template<> vector<ind> APOPTOSIS44::depends_on<24>() {
    return { 23,24 };
}

template<> bool APOPTOSIS44::rule<25>(bitset<41> x) {
    int A = x[24] + x[27];
    int H = x[38] - x[24] + 2*x[37] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[25];
}
template<> vector<ind> APOPTOSIS44::depends_on<25>() {
    return { 24,25,27,37,38 };
}

template<> bool APOPTOSIS44::rule<26>(bitset<41> x) {
    int A = x[34]*x[25]*x[36];
    int H = -x[34] - x[25] + 2*x[37] - x[36] + 3;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[26];
}
template<> vector<ind> APOPTOSIS44::depends_on<26>() {
    return { 25,26,34,36,37 };
}

template<> bool APOPTOSIS44::rule<27>(bitset<41> x) {
    int A = x[26] + x[29] + x[20];
    int H = 4*x[37] + (1 - x[26])*(1 - x[29])*(1 - x[20]);
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[27];
}
template<> vector<ind> APOPTOSIS44::depends_on<27>() {
    return { 20,26,27,29,37 };
}

template<> bool APOPTOSIS44::rule<28>(bitset<41> x) {
    return x[27];
}
template<> vector<ind> APOPTOSIS44::depends_on<28>() {
    return { 27,28 };
}

template<> bool APOPTOSIS44::rule<29>(bitset<41> x) {
    int A = x[27];
    int H = -x[27] + 2*x[37] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[29];
}
template<> vector<ind> APOPTOSIS44::depends_on<29>() {
    return { 27,29,37 };
}

template<> bool APOPTOSIS44::rule<30>(bitset<41> x) {
    int A = x[20]*x[33] + x[22]*x[33];
    int H = 3*x[31] - x[33] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[30];
}
template<> vector<ind> APOPTOSIS44::depends_on<30>() {
    return { 20,22,30,31,33 };
}

template<> bool APOPTOSIS44::rule<31>(bitset<41> x) {
    int A = x[13];
    int H = 2*x[32] - x[13] + x[33] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[31];
}
template<> vector<ind> APOPTOSIS44::depends_on<31>() {
    return { 13,31,32,33 };
}

template<> bool APOPTOSIS44::rule<32>(bitset<41> x) {
    int A = x[33];
    int H = 2*x[38] - x[33] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[32];
}
template<> vector<ind> APOPTOSIS44::depends_on<32>() {
    return { 32,33,38 };
}

template<> bool APOPTOSIS44::rule<33>(bitset<41> x) {
    int A = 3*x[40] + x[22];
    int H = 2*x[39] + (1 - x[40])*(1 - x[22]);
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[33];
}
template<> vector<ind> APOPTOSIS44::depends_on<33>() {
    return { 22,33,39,40 };
}

template<> bool APOPTOSIS44::rule<34>(bitset<41> x) {
    int A = x[33];
    int H = 1 - x[33];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[34];
}
template<> vector<ind> APOPTOSIS44::depends_on<34>() {
    return { 33,34 };
}

template<> bool APOPTOSIS44::rule<35>(bitset<41> x) {
    int A = x[33];
    int H = 1 - x[33];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[35];
}
template<> vector<ind> APOPTOSIS44::depends_on<35>() {
    return { 33,35 };
}

template<> bool APOPTOSIS44::rule<36>(bitset<41> x) {
    int A = x[30];
    int H = x[31];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[36];
}
template<> vector<ind> APOPTOSIS44::depends_on<36>() {
    return { 30,31,36 };
}

template<> bool APOPTOSIS44::rule<37>(bitset<41> x) {
    int A = x[13];
    int H = x[27]*x[29] + x[36];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[37];
}
template<> vector<ind> APOPTOSIS44::depends_on<37>() {
    return { 13,27,29,36,37 };
}

template<> bool APOPTOSIS44::rule<38>(bitset<41> x) {
    int A = x[19];
    int H = 1 - x[19];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[38];
}
template<> vector<ind> APOPTOSIS44::depends_on<38>() {
    return { 19,38 };
}

template<> bool APOPTOSIS44::rule<39>(bitset<41> x) {
    int A = x[38] + x[33];
    int H = 1 - x[38];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[39];
}
template<> vector<ind> APOPTOSIS44::depends_on<39>() {
    return { 33,38,39 };
}

template<> bool APOPTOSIS44::rule<40>(bitset<41> x) {
    return x[27] && x[28];
}
template<> vector<ind> APOPTOSIS44::depends_on<40>() {
    return { 27,28,40 };
}
