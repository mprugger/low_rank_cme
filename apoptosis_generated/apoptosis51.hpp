RULE_SET(APOPTOSIS51, 41, "GF","NIK","IKK","IkB","NFkB","A20","MEKK1","JNKK","JNK","BID","BclX","BAD","p53","PTEN","GFR","PI3K","PIP2","PIP3","Akt","Mdm2","TNF","TNFR1","TRADD","TRAF","FADD","RIP","cIAP","Cas8","TNFR2","TRAF2","Cas7","Cas12","Cas9","APC","Cas3","Cas3prev","Cas6","Apaf1","Mito","IAP","DNAdam")

template<> bool APOPTOSIS51::rule<0>(bitset<41> x) {
    int A = x[0];
    int H = 1 - x[0];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[0];
}
template<> vector<ind> APOPTOSIS51::depends_on<0>() {
    return { 0 };
}

template<> bool APOPTOSIS51::rule<1>(bitset<41> x) {
    int A = x[29];
    int H = 1 - x[29];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[1];
}
template<> vector<ind> APOPTOSIS51::depends_on<1>() {
    return { 1,29 };
}

template<> bool APOPTOSIS51::rule<2>(bitset<41> x) {
    int A = x[18] + x[1];
    int H = 3*x[5] + (1 - x[18])*(1 - x[1]);
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[2];
}
template<> vector<ind> APOPTOSIS51::depends_on<2>() {
    return { 1,2,5,18 };
}

template<> bool APOPTOSIS51::rule<3>(bitset<41> x) {
    int A = x[4];
    int H = 2*x[2];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[3];
}
template<> vector<ind> APOPTOSIS51::depends_on<3>() {
    return { 2,3,4 };
}

template<> bool APOPTOSIS51::rule<4>(bitset<41> x) {
    int A = 1 - x[3];
    int H = x[3];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[4];
}
template<> vector<ind> APOPTOSIS51::depends_on<4>() {
    return { 3,4 };
}

template<> bool APOPTOSIS51::rule<5>(bitset<41> x) {
    int A = x[4];
    int H = 1 - x[4];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[5];
}
template<> vector<ind> APOPTOSIS51::depends_on<5>() {
    return { 4,5 };
}

template<> bool APOPTOSIS51::rule<6>(bitset<41> x) {
    int A = x[23];
    int H = 1 - x[23];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[6];
}
template<> vector<ind> APOPTOSIS51::depends_on<6>() {
    return { 6,23 };
}

template<> bool APOPTOSIS51::rule<7>(bitset<41> x) {
    int A = x[6];
    int H = 2*x[18] - x[6] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[7];
}
template<> vector<ind> APOPTOSIS51::depends_on<7>() {
    return { 6,7,18 };
}

template<> bool APOPTOSIS51::rule<8>(bitset<41> x) {
    int A = x[7];
    int H = 1 - x[7];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[8];
}
template<> vector<ind> APOPTOSIS51::depends_on<8>() {
    return { 7,8 };
}

template<> bool APOPTOSIS51::rule<9>(bitset<41> x) {
    int A = x[27]*x[12] + x[8]*x[12];
    int H = 3*x[10] - x[12] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[9];
}
template<> vector<ind> APOPTOSIS51::depends_on<9>() {
    return { 8,9,10,12,27 };
}

template<> bool APOPTOSIS51::rule<10>(bitset<41> x) {
    int A = x[4];
    int H = 2*x[11] - x[4] + x[12] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[10];
}
template<> vector<ind> APOPTOSIS51::depends_on<10>() {
    return { 4,10,11,12 };
}

template<> bool APOPTOSIS51::rule<11>(bitset<41> x) {
    int A = x[12];
    int H = 2*x[18] - x[12] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[11];
}
template<> vector<ind> APOPTOSIS51::depends_on<11>() {
    return { 11,12,18 };
}

template<> bool APOPTOSIS51::rule<12>(bitset<41> x) {
    int A = 3*x[40] + x[8];
    int H = 2*x[19] + (1 - x[40])*(1 - x[8]);
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[12];
}
template<> vector<ind> APOPTOSIS51::depends_on<12>() {
    return { 8,12,19,40 };
}

template<> bool APOPTOSIS51::rule<13>(bitset<41> x) {
    int A = x[12];
    int H = 1 - x[12];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[13];
}
template<> vector<ind> APOPTOSIS51::depends_on<13>() {
    return { 12,13 };
}

template<> bool APOPTOSIS51::rule<14>(bitset<41> x) {
    int A = x[0];
    int H = 1 - x[0];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[14];
}
template<> vector<ind> APOPTOSIS51::depends_on<14>() {
    return { 0,14 };
}

template<> bool APOPTOSIS51::rule<15>(bitset<41> x) {
    int A = x[14];
    int H = 1 - x[14];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[15];
}
template<> vector<ind> APOPTOSIS51::depends_on<15>() {
    return { 14,15 };
}

template<> bool APOPTOSIS51::rule<16>(bitset<41> x) {
    int A = x[14];
    int H = 1 - x[14];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[16];
}
template<> vector<ind> APOPTOSIS51::depends_on<16>() {
    return { 14,16 };
}

template<> bool APOPTOSIS51::rule<17>(bitset<41> x) {
    int A = x[15]*x[16];
    int H = -x[16] + 2*x[13] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[17];
}
template<> vector<ind> APOPTOSIS51::depends_on<17>() {
    return { 13,15,16,17 };
}

template<> bool APOPTOSIS51::rule<18>(bitset<41> x) {
    int A = x[17];
    int H = 1 - x[17];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[18];
}
template<> vector<ind> APOPTOSIS51::depends_on<18>() {
    return { 17,18 };
}

template<> bool APOPTOSIS51::rule<19>(bitset<41> x) {
    int A = x[18] + x[12];
    int H = 1 - x[18];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[19];
}
template<> vector<ind> APOPTOSIS51::depends_on<19>() {
    return { 12,18,19 };
}

template<> bool APOPTOSIS51::rule<20>(bitset<41> x) {
    int A = x[20];
    int H = 1 - x[20];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[20];
}
template<> vector<ind> APOPTOSIS51::depends_on<20>() {
    return { 20 };
}

template<> bool APOPTOSIS51::rule<21>(bitset<41> x) {
    int A = x[20];
    int H = 1 - x[20];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[21];
}
template<> vector<ind> APOPTOSIS51::depends_on<21>() {
    return { 20,21 };
}

template<> bool APOPTOSIS51::rule<22>(bitset<41> x) {
    int A = x[21];
    int H = 1 - x[21];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[22];
}
template<> vector<ind> APOPTOSIS51::depends_on<22>() {
    return { 21,22 };
}

template<> bool APOPTOSIS51::rule<23>(bitset<41> x) {
    int A = x[22];
    int H = 1 - x[22];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[23];
}
template<> vector<ind> APOPTOSIS51::depends_on<23>() {
    return { 22,23 };
}

template<> bool APOPTOSIS51::rule<24>(bitset<41> x) {
    int A = x[22];
    int H = 1 - x[22];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[24];
}
template<> vector<ind> APOPTOSIS51::depends_on<24>() {
    return { 22,24 };
}

template<> bool APOPTOSIS51::rule<25>(bitset<41> x) {
    int A = x[22];
    int H = 1 - x[22];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[25];
}
template<> vector<ind> APOPTOSIS51::depends_on<25>() {
    return { 22,25 };
}

template<> bool APOPTOSIS51::rule<26>(bitset<41> x) {
    int A = x[23];
    int H = 1 - x[23];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[26];
}
template<> vector<ind> APOPTOSIS51::depends_on<26>() {
    return { 23,26 };
}

template<> bool APOPTOSIS51::rule<27>(bitset<41> x) {
    int A = x[36] + x[24];
    int H = x[26];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[27];
}
template<> vector<ind> APOPTOSIS51::depends_on<27>() {
    return { 24,26,27,36 };
}

template<> bool APOPTOSIS51::rule<28>(bitset<41> x) {
    int A = x[20];
    int H = 1 - x[20];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[28];
}
template<> vector<ind> APOPTOSIS51::depends_on<28>() {
    return { 20,28 };
}

template<> bool APOPTOSIS51::rule<29>(bitset<41> x) {
    int A = x[25];
    int H = -x[25] + 2*x[28] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[29];
}
template<> vector<ind> APOPTOSIS51::depends_on<29>() {
    return { 25,28,29 };
}

template<> bool APOPTOSIS51::rule<30>(bitset<41> x) {
    int A = x[33] + x[27];
    int H = 3*x[39] + (1 - x[33])*(1 - x[27]);
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[30];
}
template<> vector<ind> APOPTOSIS51::depends_on<30>() {
    return { 27,30,33,39 };
}

template<> bool APOPTOSIS51::rule<31>(bitset<41> x) {
    int A = x[30];
    int H = 1 - x[30];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[31];
}
template<> vector<ind> APOPTOSIS51::depends_on<31>() {
    return { 30,31 };
}

template<> bool APOPTOSIS51::rule<32>(bitset<41> x) {
    int A = x[31] + x[34];
    int H = x[18] - x[31] + 2*x[39] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[32];
}
template<> vector<ind> APOPTOSIS51::depends_on<32>() {
    return { 18,31,32,34,39 };
}

template<> bool APOPTOSIS51::rule<33>(bitset<41> x) {
    int A = x[37]*x[32]*x[38];
    int H = -x[37] - x[32] + 2*x[39] - x[38] + 3;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[33];
}
template<> vector<ind> APOPTOSIS51::depends_on<33>() {
    return { 32,33,37,38,39 };
}

template<> bool APOPTOSIS51::rule<34>(bitset<41> x) {
    int A = x[33] + x[36] + x[27];
    int H = 4*x[39] + (1 - x[33])*(1 - x[36])*(1 - x[27]);
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[34];
}
template<> vector<ind> APOPTOSIS51::depends_on<34>() {
    return { 27,33,34,36,39 };
}

template<> bool APOPTOSIS51::rule<35>(bitset<41> x) {
    return x[34];
}
template<> vector<ind> APOPTOSIS51::depends_on<35>() {
    return { 34,35 };
}

template<> bool APOPTOSIS51::rule<36>(bitset<41> x) {
    int A = x[34];
    int H = -x[34] + 2*x[39] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[36];
}
template<> vector<ind> APOPTOSIS51::depends_on<36>() {
    return { 34,36,39 };
}

template<> bool APOPTOSIS51::rule<37>(bitset<41> x) {
    int A = x[12];
    int H = 1 - x[12];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[37];
}
template<> vector<ind> APOPTOSIS51::depends_on<37>() {
    return { 12,37 };
}

template<> bool APOPTOSIS51::rule<38>(bitset<41> x) {
    int A = x[9];
    int H = x[10];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[38];
}
template<> vector<ind> APOPTOSIS51::depends_on<38>() {
    return { 9,10,38 };
}

template<> bool APOPTOSIS51::rule<39>(bitset<41> x) {
    int A = x[4];
    int H = x[34]*x[36] + x[38];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[39];
}
template<> vector<ind> APOPTOSIS51::depends_on<39>() {
    return { 4,34,36,38,39 };
}

template<> bool APOPTOSIS51::rule<40>(bitset<41> x) {
    return x[34] && x[35];
}
template<> vector<ind> APOPTOSIS51::depends_on<40>() {
    return { 34,35,40 };
}
