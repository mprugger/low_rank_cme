RULE_SET(APOPTOSIS52, 41, "Cas8","JNKK","JNK","Cas7","Cas12","Cas9","APC","Cas3","Cas3prev","Cas6","BID","BclX","BAD","p53","Apaf1","Mito","IAP","Akt","Mdm2","DNAdam","TNF","GF","TNFR1","TRADD","TRAF","FADD","RIP","cIAP","TNFR2","TRAF2","NIK","IKK","IkB","NFkB","A20","MEKK1","PTEN","GFR","PI3K","PIP2","PIP3")

template<> bool APOPTOSIS52::rule<0>(bitset<41> x) {
    int A = x[9] + x[25];
    int H = x[16];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[0];
}
template<> vector<ind> APOPTOSIS52::depends_on<0>() {
    return { 0,9,25,27 };
}

template<> bool APOPTOSIS52::rule<1>(bitset<41> x) {
    int A = x[35];
    int H = 2*x[17] - x[35] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[1];
}
template<> vector<ind> APOPTOSIS52::depends_on<1>() {
    return { 1,17,35 };
}

template<> bool APOPTOSIS52::rule<2>(bitset<41> x) {
    int A = x[1];
    int H = 1 - x[1];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[2];
}
template<> vector<ind> APOPTOSIS52::depends_on<2>() {
    return { 1,2 };
}

template<> bool APOPTOSIS52::rule<3>(bitset<41> x) {
    int A = x[6] + x[0];
    int H = 3*x[16] + (1 - x[6])*(1 - x[0]);
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[3];
}
template<> vector<ind> APOPTOSIS52::depends_on<3>() {
    return { 0,3,6,16 };
}

template<> bool APOPTOSIS52::rule<4>(bitset<41> x) {
    int A = x[3];
    int H = 1 - x[3];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[4];
}
template<> vector<ind> APOPTOSIS52::depends_on<4>() {
    return { 3,4 };
}

template<> bool APOPTOSIS52::rule<5>(bitset<41> x) {
    int A = x[4] + x[7];
    int H = x[17] - x[4] + 2*x[16] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[5];
}
template<> vector<ind> APOPTOSIS52::depends_on<5>() {
    return { 4,5,7,16,17 };
}

template<> bool APOPTOSIS52::rule<6>(bitset<41> x) {
    int A = x[14]*x[5]*x[15];
    int H = -x[14] - x[5] + 2*x[16] - x[15] + 3;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[6];
}
template<> vector<ind> APOPTOSIS52::depends_on<6>() {
    return { 5,6,14,15,16 };
}

template<> bool APOPTOSIS52::rule<7>(bitset<41> x) {
    int A = x[6] + x[9] + x[0];
    int H = 4*x[16] + (1 - x[6])*(1 - x[9])*(1 - x[0]);
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[7];
}
template<> vector<ind> APOPTOSIS52::depends_on<7>() {
    return { 0,6,7,9,16 };
}

template<> bool APOPTOSIS52::rule<8>(bitset<41> x) {
    return x[7];
}
template<> vector<ind> APOPTOSIS52::depends_on<8>() {
    return { 7,8 };
}

template<> bool APOPTOSIS52::rule<9>(bitset<41> x) {
    int A = x[7];
    int H = -x[7] + 2*x[16] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[9];
}
template<> vector<ind> APOPTOSIS52::depends_on<9>() {
    return { 7,9,16 };
}

template<> bool APOPTOSIS52::rule<10>(bitset<41> x) {
    int A = x[0]*x[13] + x[2]*x[13];
    int H = 3*x[11] - x[13] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[10];
}
template<> vector<ind> APOPTOSIS52::depends_on<10>() {
    return { 0,2,10,11,13 };
}

template<> bool APOPTOSIS52::rule<11>(bitset<41> x) {
    int A = x[33];
    int H = 2*x[12] - x[33] + x[13] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[11];
}
template<> vector<ind> APOPTOSIS52::depends_on<11>() {
    return { 11,12,13,33 };
}

template<> bool APOPTOSIS52::rule<12>(bitset<41> x) {
    int A = x[13];
    int H = 2*x[17] - x[13] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[12];
}
template<> vector<ind> APOPTOSIS52::depends_on<12>() {
    return { 12,13,17 };
}

template<> bool APOPTOSIS52::rule<13>(bitset<41> x) {
    int A = 3*x[19] + x[2];
    int H = 2*x[18] + (1 - x[19])*(1 - x[2]);
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[13];
}
template<> vector<ind> APOPTOSIS52::depends_on<13>() {
    return { 2,13,18,19 };
}

template<> bool APOPTOSIS52::rule<14>(bitset<41> x) {
    int A = x[13];
    int H = 1 - x[13];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[14];
}
template<> vector<ind> APOPTOSIS52::depends_on<14>() {
    return { 13,14 };
}

template<> bool APOPTOSIS52::rule<15>(bitset<41> x) {
    int A = x[10];
    int H = x[11];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[15];
}
template<> vector<ind> APOPTOSIS52::depends_on<15>() {
    return { 10,11,15 };
}

template<> bool APOPTOSIS52::rule<16>(bitset<41> x) {
    int A = x[33];
    int H = x[7]*x[9] + x[15];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[16];
}
template<> vector<ind> APOPTOSIS52::depends_on<16>() {
    return { 7,9,15,16,33 };
}

template<> bool APOPTOSIS52::rule<17>(bitset<41> x) {
    int A = x[40];
    int H = 1 - x[40];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[17];
}
template<> vector<ind> APOPTOSIS52::depends_on<17>() {
    return { 17,40 };
}

template<> bool APOPTOSIS52::rule<18>(bitset<41> x) {
    int A = x[17] + x[13];
    int H = 1 - x[17];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[18];
}
template<> vector<ind> APOPTOSIS52::depends_on<18>() {
    return { 13,17,18 };
}

template<> bool APOPTOSIS52::rule<19>(bitset<41> x) {
    return x[7] && x[8];
}
template<> vector<ind> APOPTOSIS52::depends_on<19>() {
    return { 7,8,19 };
}

template<> bool APOPTOSIS52::rule<20>(bitset<41> x) {
    int A = x[20];
    int H = 1 - x[20];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[20];
}
template<> vector<ind> APOPTOSIS52::depends_on<20>() {
    return { 20 };
}

template<> bool APOPTOSIS52::rule<21>(bitset<41> x) {
    int A = x[21];
    int H = 1 - x[21];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[21];
}
template<> vector<ind> APOPTOSIS52::depends_on<21>() {
    return { 21 };
}

template<> bool APOPTOSIS52::rule<22>(bitset<41> x) {
    int A = x[20];
    int H = 1 - x[20];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[22];
}
template<> vector<ind> APOPTOSIS52::depends_on<22>() {
    return { 20,22 };
}

template<> bool APOPTOSIS52::rule<23>(bitset<41> x) {
    int A = x[22];
    int H = 1 - x[22];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[23];
}
template<> vector<ind> APOPTOSIS52::depends_on<23>() {
    return { 22,23 };
}

template<> bool APOPTOSIS52::rule<24>(bitset<41> x) {
    int A = x[23];
    int H = 1 - x[23];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[24];
}
template<> vector<ind> APOPTOSIS52::depends_on<24>() {
    return { 23,24 };
}

template<> bool APOPTOSIS52::rule<25>(bitset<41> x) {
    int A = x[23];
    int H = 1 - x[23];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[25];
}
template<> vector<ind> APOPTOSIS52::depends_on<25>() {
    return { 23,25 };
}

template<> bool APOPTOSIS52::rule<26>(bitset<41> x) {
    int A = x[23];
    int H = 1 - x[23];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[26];
}
template<> vector<ind> APOPTOSIS52::depends_on<26>() {
    return { 23,26 };
}

template<> bool APOPTOSIS52::rule<27>(bitset<41> x) {
    int A = x[24];
    int H = 1 - x[24];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[27];
}
template<> vector<ind> APOPTOSIS52::depends_on<27>() {
    return { 24,27 };
}

template<> bool APOPTOSIS52::rule<28>(bitset<41> x) {
    int A = x[20];
    int H = 1 - x[20];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[28];
}
template<> vector<ind> APOPTOSIS52::depends_on<28>() {
    return { 20,28 };
}

template<> bool APOPTOSIS52::rule<29>(bitset<41> x) {
    int A = x[26];
    int H = -x[26] + 2*x[28] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[29];
}
template<> vector<ind> APOPTOSIS52::depends_on<29>() {
    return { 26,28,29 };
}

template<> bool APOPTOSIS52::rule<30>(bitset<41> x) {
    int A = x[29];
    int H = 1 - x[29];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[30];
}
template<> vector<ind> APOPTOSIS52::depends_on<30>() {
    return { 29,30 };
}

template<> bool APOPTOSIS52::rule<31>(bitset<41> x) {
    int A = x[17] + x[30];
    int H = 3*x[34] + (1 - x[17])*(1 - x[30]);
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[31];
}
template<> vector<ind> APOPTOSIS52::depends_on<31>() {
    return { 17,30,31,34 };
}

template<> bool APOPTOSIS52::rule<32>(bitset<41> x) {
    int A = x[33];
    int H = 2*x[31];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[32];
}
template<> vector<ind> APOPTOSIS52::depends_on<32>() {
    return { 31,32,33 };
}

template<> bool APOPTOSIS52::rule<33>(bitset<41> x) {
    int A = 1 - x[32];
    int H = x[32];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[33];
}
template<> vector<ind> APOPTOSIS52::depends_on<33>() {
    return { 32,33 };
}

template<> bool APOPTOSIS52::rule<34>(bitset<41> x) {
    int A = x[33];
    int H = 1 - x[33];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[34];
}
template<> vector<ind> APOPTOSIS52::depends_on<34>() {
    return { 33,34 };
}

template<> bool APOPTOSIS52::rule<35>(bitset<41> x) {
    int A = x[24];
    int H = 1 - x[24];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[35];
}
template<> vector<ind> APOPTOSIS52::depends_on<35>() {
    return { 24,35 };
}

template<> bool APOPTOSIS52::rule<36>(bitset<41> x) {
    int A = x[13];
    int H = 1 - x[13];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[36];
}
template<> vector<ind> APOPTOSIS52::depends_on<36>() {
    return { 13,36 };
}

template<> bool APOPTOSIS52::rule<37>(bitset<41> x) {
    int A = x[21];
    int H = 1 - x[21];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[37];
}
template<> vector<ind> APOPTOSIS52::depends_on<37>() {
    return { 21,37 };
}

template<> bool APOPTOSIS52::rule<38>(bitset<41> x) {
    int A = x[37];
    int H = 1 - x[37];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[38];
}
template<> vector<ind> APOPTOSIS52::depends_on<38>() {
    return { 37,38 };
}

template<> bool APOPTOSIS52::rule<39>(bitset<41> x) {
    int A = x[37];
    int H = 1 - x[37];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[39];
}
template<> vector<ind> APOPTOSIS52::depends_on<39>() {
    return { 37,39 };
}

template<> bool APOPTOSIS52::rule<40>(bitset<41> x) {
    int A = x[38]*x[39];
    int H = -x[39] + 2*x[36] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[40];
}
template<> vector<ind> APOPTOSIS52::depends_on<40>() {
    return { 36,38,39,40 };
}
