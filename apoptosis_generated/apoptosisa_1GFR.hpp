RULE_SET(APOPTOSISa_1GFR, 41, "Cas12",
"Cas9","APC","Cas3","Cas3prev","Cas6","BID","BclX","BAD","p53","Apaf1",
"PTEN","Mito","IAP","PI3K","PIP2","PIP3","Akt","Mdm2","DNAdam","TNF",
"GF","TNFR1","TRADD","TRAF","FADD","RIP","cIAP","Cas8","TNFR2","TRAF2",
"NIK","IKK","IkB","NFkB","A20","MEKK1","JNKK","JNK","Cas7","GFR")

template<> bool APOPTOSISa_1GFR::rule<0>(bitset<41> x) {
    int A = x[39];
    int H = 1 - x[39];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[0];
}
template<> vector<ind> APOPTOSISa_1GFR::depends_on<0>() {
    return { 0,39 };
}

template<> bool APOPTOSISa_1GFR::rule<1>(bitset<41> x) {
    int A = x[0] + x[3];
    int H = x[17] - x[0] + 2*x[13] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[1];
}
template<> vector<ind> APOPTOSISa_1GFR::depends_on<1>() {
    return { 0,1,3,13,17 };
}

template<> bool APOPTOSISa_1GFR::rule<2>(bitset<41> x) {
    int A = x[10]*x[1]*x[12];
    int H = -x[10] - x[1] + 2*x[13] - x[12] + 3;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[2];
}
template<> vector<ind> APOPTOSISa_1GFR::depends_on<2>() {
    return { 1,2,10,12,13 };
}

template<> bool APOPTOSISa_1GFR::rule<3>(bitset<41> x) {
    int A = x[2] + x[5] + x[28];
    int H = 4*x[13] + (1 - x[2])*(1 - x[5])*(1 - x[28]);
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[3];
}
template<> vector<ind> APOPTOSISa_1GFR::depends_on<3>() {
    return { 2,3,5,13,28 };
}

template<> bool APOPTOSISa_1GFR::rule<4>(bitset<41> x) {
    return x[3];
}
template<> vector<ind> APOPTOSISa_1GFR::depends_on<4>() {
    return { 3,4 };
}

template<> bool APOPTOSISa_1GFR::rule<5>(bitset<41> x) {
    int A = x[3];
    int H = -x[3] + 2*x[13] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[5];
}
template<> vector<ind> APOPTOSISa_1GFR::depends_on<5>() {
    return { 3,5,13 };
}

template<> bool APOPTOSISa_1GFR::rule<6>(bitset<41> x) {
    int A = x[28]*x[9] + x[38]*x[9];
    int H = 3*x[7] - x[9] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[6];
}
template<> vector<ind> APOPTOSISa_1GFR::depends_on<6>() {
    return { 6,7,9,28,38 };
}

template<> bool APOPTOSISa_1GFR::rule<7>(bitset<41> x) {
    int A = x[34];
    int H = 2*x[8] - x[34] + x[9] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[7];
}
template<> vector<ind> APOPTOSISa_1GFR::depends_on<7>() {
    return { 7,8,9,34 };
}

template<> bool APOPTOSISa_1GFR::rule<8>(bitset<41> x) {
    int A = x[9];
    int H = 2*x[17] - x[9] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[8];
}
template<> vector<ind> APOPTOSISa_1GFR::depends_on<8>() {
    return { 8,9,17 };
}

template<> bool APOPTOSISa_1GFR::rule<9>(bitset<41> x) {
    int A = 3*x[19] + x[38];
    int H = 2*x[18] + (1 - x[19])*(1 - x[38]);
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[9];
}
template<> vector<ind> APOPTOSISa_1GFR::depends_on<9>() {
    return { 9,18,19,38 };
}

template<> bool APOPTOSISa_1GFR::rule<10>(bitset<41> x) {
    int A = x[9];
    int H = 1 - x[9];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[10];
}
template<> vector<ind> APOPTOSISa_1GFR::depends_on<10>() {
    return { 9,10 };
}

template<> bool APOPTOSISa_1GFR::rule<11>(bitset<41> x) {
    int A = x[9];
    int H = 1 - x[9];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[11];
}
template<> vector<ind> APOPTOSISa_1GFR::depends_on<11>() {
    return { 9,11 };
}

template<> bool APOPTOSISa_1GFR::rule<12>(bitset<41> x) {
    int A = x[6];
    int H = x[7];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[12];
}
template<> vector<ind> APOPTOSISa_1GFR::depends_on<12>() {
    return { 6,7,12 };
}

template<> bool APOPTOSISa_1GFR::rule<13>(bitset<41> x) {
    int A = x[34];
    int H = x[3]*x[5] + x[12];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[13];
}
template<> vector<ind> APOPTOSISa_1GFR::depends_on<13>() {
    return { 3,5,12,13,34 };
}

template<> bool APOPTOSISa_1GFR::rule<14>(bitset<41> x) {
    int A = x[40];
    int H = 1 - x[40];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[14];
}
template<> vector<ind> APOPTOSISa_1GFR::depends_on<14>() {
    return { 14,40 };
}

template<> bool APOPTOSISa_1GFR::rule<15>(bitset<41> x) {
    int A = x[40];
    int H = 1 - x[40];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[15];
}
template<> vector<ind> APOPTOSISa_1GFR::depends_on<15>() {
    return { 15,40 };
}

template<> bool APOPTOSISa_1GFR::rule<16>(bitset<41> x) {
    int A = x[14]*x[15];
    int H = -x[15] + 2*x[11] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[16];
}
template<> vector<ind> APOPTOSISa_1GFR::depends_on<16>() {
    return { 11,14,15,16 };
}

template<> bool APOPTOSISa_1GFR::rule<17>(bitset<41> x) {
    int A = x[16];
    int H = 1 - x[16];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[17];
}
template<> vector<ind> APOPTOSISa_1GFR::depends_on<17>() {
    return { 16,17 };
}

template<> bool APOPTOSISa_1GFR::rule<18>(bitset<41> x) {
    int A = x[17] + x[9];
    int H = 1 - x[17];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[18];
}
template<> vector<ind> APOPTOSISa_1GFR::depends_on<18>() {
    return { 9,17,18 };
}

template<> bool APOPTOSISa_1GFR::rule<19>(bitset<41> x) {
    return x[3] && x[4];
}
template<> vector<ind> APOPTOSISa_1GFR::depends_on<19>() {
    return { 3,4,19 };
}

template<> bool APOPTOSISa_1GFR::rule<20>(bitset<41> x) {
    int A = x[20];
    int H = 1 - x[20];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[20];
}
template<> vector<ind> APOPTOSISa_1GFR::depends_on<20>() {
    return { 20 };
}

template<> bool APOPTOSISa_1GFR::rule<21>(bitset<41> x) {
    int A = x[21];
    int H = 1 - x[21];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[21];
}
template<> vector<ind> APOPTOSISa_1GFR::depends_on<21>() {
    return { 21 };
}

template<> bool APOPTOSISa_1GFR::rule<22>(bitset<41> x) {
    int A = x[20];
    int H = 1 - x[20];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[22];
}
template<> vector<ind> APOPTOSISa_1GFR::depends_on<22>() {
    return { 20,22 };
}

template<> bool APOPTOSISa_1GFR::rule<23>(bitset<41> x) {
    int A = x[22];
    int H = 1 - x[22];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[23];
}
template<> vector<ind> APOPTOSISa_1GFR::depends_on<23>() {
    return { 22,23 };
}

template<> bool APOPTOSISa_1GFR::rule<24>(bitset<41> x) {
    int A = x[23];
    int H = 1 - x[23];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[24];
}
template<> vector<ind> APOPTOSISa_1GFR::depends_on<24>() {
    return { 23,24 };
}

template<> bool APOPTOSISa_1GFR::rule<25>(bitset<41> x) {
    int A = x[23];
    int H = 1 - x[23];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[25];
}
template<> vector<ind> APOPTOSISa_1GFR::depends_on<25>() {
    return { 23,25 };
}

template<> bool APOPTOSISa_1GFR::rule<26>(bitset<41> x) {
    int A = x[23];
    int H = 1 - x[23];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[26];
}
template<> vector<ind> APOPTOSISa_1GFR::depends_on<26>() {
    return { 23,26 };
}

template<> bool APOPTOSISa_1GFR::rule<27>(bitset<41> x) {
    int A = x[24];
    int H = 1 - x[24];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[27];
}
template<> vector<ind> APOPTOSISa_1GFR::depends_on<27>() {
    return { 24,27 };
}

template<> bool APOPTOSISa_1GFR::rule<28>(bitset<41> x) {
    int A = x[5] + x[25];
    int H = x[13];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[28];
}
template<> vector<ind> APOPTOSISa_1GFR::depends_on<28>() {
    return { 5,25,27,28 };
}

template<> bool APOPTOSISa_1GFR::rule<29>(bitset<41> x) {
    int A = x[20];
    int H = 1 - x[20];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[29];
}
template<> vector<ind> APOPTOSISa_1GFR::depends_on<29>() {
    return { 20,29 };
}

template<> bool APOPTOSISa_1GFR::rule<30>(bitset<41> x) {
    int A = x[26];
    int H = -x[26] + 2*x[29] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[30];
}
template<> vector<ind> APOPTOSISa_1GFR::depends_on<30>() {
    return { 26,29,30 };
}

template<> bool APOPTOSISa_1GFR::rule<31>(bitset<41> x) {
    int A = x[30];
    int H = 1 - x[30];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[31];
}
template<> vector<ind> APOPTOSISa_1GFR::depends_on<31>() {
    return { 30,31 };
}

template<> bool APOPTOSISa_1GFR::rule<32>(bitset<41> x) {
    int A = x[17] + x[31];
    int H = 3*x[35] + (1 - x[17])*(1 - x[31]);
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[32];
}
template<> vector<ind> APOPTOSISa_1GFR::depends_on<32>() {
    return { 17,31,32,35 };
}

template<> bool APOPTOSISa_1GFR::rule<33>(bitset<41> x) {
    int A = x[34];
    int H = 2*x[32];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[33];
}
template<> vector<ind> APOPTOSISa_1GFR::depends_on<33>() {
    return { 32,33,34 };
}

template<> bool APOPTOSISa_1GFR::rule<34>(bitset<41> x) {
    int A = 1 - x[33];
    int H = x[33];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[34];
}
template<> vector<ind> APOPTOSISa_1GFR::depends_on<34>() {
    return { 33,34 };
}

template<> bool APOPTOSISa_1GFR::rule<35>(bitset<41> x) {
    int A = x[34];
    int H = 1 - x[34];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[35];
}
template<> vector<ind> APOPTOSISa_1GFR::depends_on<35>() {
    return { 34,35 };
}

template<> bool APOPTOSISa_1GFR::rule<36>(bitset<41> x) {
    int A = x[24];
    int H = 1 - x[24];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[36];
}
template<> vector<ind> APOPTOSISa_1GFR::depends_on<36>() {
    return { 24,36 };
}

template<> bool APOPTOSISa_1GFR::rule<37>(bitset<41> x) {
    int A = x[36];
    int H = 2*x[17] - x[36] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[37];
}
template<> vector<ind> APOPTOSISa_1GFR::depends_on<37>() {
    return { 17,36,37 };
}

template<> bool APOPTOSISa_1GFR::rule<38>(bitset<41> x) {
    int A = x[37];
    int H = 1 - x[37];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[38];
}
template<> vector<ind> APOPTOSISa_1GFR::depends_on<38>() {
    return { 37,38 };
}

template<> bool APOPTOSISa_1GFR::rule<39>(bitset<41> x) {
    int A = x[2] + x[28];
    int H = 3*x[13] + (1 - x[2])*(1 - x[28]);
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[39];
}
template<> vector<ind> APOPTOSISa_1GFR::depends_on<39>() {
    return { 2,13,28,39 };
}

template<> bool APOPTOSISa_1GFR::rule<40>(bitset<41> x) {
    int A = x[21];
    int H = 1 - x[21];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[40];
}
template<> vector<ind> APOPTOSISa_1GFR::depends_on<40>() {
    return { 21,40 };
}
