RULE_SET(APOPTOSISa_8JNKK, 41, "TNF","GF","TNFR1","TRADD","TRAF","FADD","RIP","cIAP","TNFR2","TRAF2","NIK","IKK","IkB","NFkB","A20","MEKK1","GFR","PI3K","PIP2","PIP3","Cas12","Cas9","APC","Cas3","Cas3prev","Cas6","BID","BclX","BAD","p53","Apaf1","PTEN","Mito","IAP","Akt","Mdm2","DNAdam","Cas7","Cas8","JNK","JNKK")

template<> bool APOPTOSISa_8JNKK::rule<0>(bitset<41> x) {
    int A = x[0];
    int H = 1 - x[0];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[0];
}
template<> vector<ind> APOPTOSISa_8JNKK::depends_on<0>() {
    return { 0 };
}

template<> bool APOPTOSISa_8JNKK::rule<1>(bitset<41> x) {
    int A = x[1];
    int H = 1 - x[1];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[1];
}
template<> vector<ind> APOPTOSISa_8JNKK::depends_on<1>() {
    return { 1 };
}

template<> bool APOPTOSISa_8JNKK::rule<2>(bitset<41> x) {
    int A = x[0];
    int H = 1 - x[0];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[2];
}
template<> vector<ind> APOPTOSISa_8JNKK::depends_on<2>() {
    return { 0,2 };
}

template<> bool APOPTOSISa_8JNKK::rule<3>(bitset<41> x) {
    int A = x[2];
    int H = 1 - x[2];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[3];
}
template<> vector<ind> APOPTOSISa_8JNKK::depends_on<3>() {
    return { 2,3 };
}

template<> bool APOPTOSISa_8JNKK::rule<4>(bitset<41> x) {
    int A = x[3];
    int H = 1 - x[3];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[4];
}
template<> vector<ind> APOPTOSISa_8JNKK::depends_on<4>() {
    return { 3,4 };
}

template<> bool APOPTOSISa_8JNKK::rule<5>(bitset<41> x) {
    int A = x[3];
    int H = 1 - x[3];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[5];
}
template<> vector<ind> APOPTOSISa_8JNKK::depends_on<5>() {
    return { 3,5 };
}

template<> bool APOPTOSISa_8JNKK::rule<6>(bitset<41> x) {
    int A = x[3];
    int H = 1 - x[3];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[6];
}
template<> vector<ind> APOPTOSISa_8JNKK::depends_on<6>() {
    return { 3,6 };
}

template<> bool APOPTOSISa_8JNKK::rule<7>(bitset<41> x) {
    int A = x[4];
    int H = 1 - x[4];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[7];
}
template<> vector<ind> APOPTOSISa_8JNKK::depends_on<7>() {
    return { 4,7 };
}

template<> bool APOPTOSISa_8JNKK::rule<8>(bitset<41> x) {
    int A = x[0];
    int H = 1 - x[0];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[8];
}
template<> vector<ind> APOPTOSISa_8JNKK::depends_on<8>() {
    return { 0,8 };
}

template<> bool APOPTOSISa_8JNKK::rule<9>(bitset<41> x) {
    int A = x[6];
    int H = -x[6] + 2*x[8] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[9];
}
template<> vector<ind> APOPTOSISa_8JNKK::depends_on<9>() {
    return { 6,8,9 };
}

template<> bool APOPTOSISa_8JNKK::rule<10>(bitset<41> x) {
    int A = x[9];
    int H = 1 - x[9];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[10];
}
template<> vector<ind> APOPTOSISa_8JNKK::depends_on<10>() {
    return { 9,10 };
}

template<> bool APOPTOSISa_8JNKK::rule<11>(bitset<41> x) {
    int A = x[34] + x[10];
    int H = 3*x[14] + (1 - x[34])*(1 - x[10]);
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[11];
}
template<> vector<ind> APOPTOSISa_8JNKK::depends_on<11>() {
    return { 10,11,14,34 };
}

template<> bool APOPTOSISa_8JNKK::rule<12>(bitset<41> x) {
    int A = x[13];
    int H = 2*x[11];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[12];
}
template<> vector<ind> APOPTOSISa_8JNKK::depends_on<12>() {
    return { 11,12,13 };
}

template<> bool APOPTOSISa_8JNKK::rule<13>(bitset<41> x) {
    int A = 1 - x[12];
    int H = x[12];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[13];
}
template<> vector<ind> APOPTOSISa_8JNKK::depends_on<13>() {
    return { 12,13 };
}

template<> bool APOPTOSISa_8JNKK::rule<14>(bitset<41> x) {
    int A = x[13];
    int H = 1 - x[13];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[14];
}
template<> vector<ind> APOPTOSISa_8JNKK::depends_on<14>() {
    return { 13,14 };
}

template<> bool APOPTOSISa_8JNKK::rule<15>(bitset<41> x) {
    int A = x[4];
    int H = 1 - x[4];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[15];
}
template<> vector<ind> APOPTOSISa_8JNKK::depends_on<15>() {
    return { 4,15 };
}

template<> bool APOPTOSISa_8JNKK::rule<16>(bitset<41> x) {
    int A = x[1];
    int H = 1 - x[1];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[16];
}
template<> vector<ind> APOPTOSISa_8JNKK::depends_on<16>() {
    return { 1,16 };
}

template<> bool APOPTOSISa_8JNKK::rule<17>(bitset<41> x) {
    int A = x[16];
    int H = 1 - x[16];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[17];
}
template<> vector<ind> APOPTOSISa_8JNKK::depends_on<17>() {
    return { 16,17 };
}

template<> bool APOPTOSISa_8JNKK::rule<18>(bitset<41> x) {
    int A = x[16];
    int H = 1 - x[16];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[18];
}
template<> vector<ind> APOPTOSISa_8JNKK::depends_on<18>() {
    return { 16,18 };
}

template<> bool APOPTOSISa_8JNKK::rule<19>(bitset<41> x) {
    int A = x[17]*x[18];
    int H = -x[18] + 2*x[31] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[19];
}
template<> vector<ind> APOPTOSISa_8JNKK::depends_on<19>() {
    return { 17,18,19,31 };
}

template<> bool APOPTOSISa_8JNKK::rule<20>(bitset<41> x) {
    int A = x[37];
    int H = 1 - x[37];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[20];
}
template<> vector<ind> APOPTOSISa_8JNKK::depends_on<20>() {
    return { 20,37 };
}

template<> bool APOPTOSISa_8JNKK::rule<21>(bitset<41> x) {
    int A = x[20] + x[23];
    int H = x[34] - x[20] + 2*x[33] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[21];
}
template<> vector<ind> APOPTOSISa_8JNKK::depends_on<21>() {
    return { 20,21,23,33,34 };
}

template<> bool APOPTOSISa_8JNKK::rule<22>(bitset<41> x) {
    int A = x[30]*x[21]*x[32];
    int H = -x[30] - x[21] + 2*x[33] - x[32] + 3;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[22];
}
template<> vector<ind> APOPTOSISa_8JNKK::depends_on<22>() {
    return { 21,22,30,32,33 };
}

template<> bool APOPTOSISa_8JNKK::rule<23>(bitset<41> x) {
    int A = x[22] + x[25] + x[38];
    int H = 4*x[33] + (1 - x[22])*(1 - x[25])*(1 - x[38]);
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[23];
}
template<> vector<ind> APOPTOSISa_8JNKK::depends_on<23>() {
    return { 22,23,25,33,38 };
}

template<> bool APOPTOSISa_8JNKK::rule<24>(bitset<41> x) {
    return x[23];
}
template<> vector<ind> APOPTOSISa_8JNKK::depends_on<24>() {
    return { 23,24 };
}

template<> bool APOPTOSISa_8JNKK::rule<25>(bitset<41> x) {
    int A = x[23];
    int H = -x[23] + 2*x[33] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[25];
}
template<> vector<ind> APOPTOSISa_8JNKK::depends_on<25>() {
    return { 23,25,33 };
}

template<> bool APOPTOSISa_8JNKK::rule<26>(bitset<41> x) {
    int A = x[38]*x[29] + x[39]*x[29];
    int H = 3*x[27] - x[29] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[26];
}
template<> vector<ind> APOPTOSISa_8JNKK::depends_on<26>() {
    return { 26,27,29,38,39 };
}

template<> bool APOPTOSISa_8JNKK::rule<27>(bitset<41> x) {
    int A = x[13];
    int H = 2*x[28] - x[13] + x[29] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[27];
}
template<> vector<ind> APOPTOSISa_8JNKK::depends_on<27>() {
    return { 13,27,28,29 };
}

template<> bool APOPTOSISa_8JNKK::rule<28>(bitset<41> x) {
    int A = x[29];
    int H = 2*x[34] - x[29] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[28];
}
template<> vector<ind> APOPTOSISa_8JNKK::depends_on<28>() {
    return { 28,29,34 };
}

template<> bool APOPTOSISa_8JNKK::rule<29>(bitset<41> x) {
    int A = 3*x[36] + x[39];
    int H = 2*x[35] + (1 - x[36])*(1 - x[39]);
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[29];
}
template<> vector<ind> APOPTOSISa_8JNKK::depends_on<29>() {
    return { 29,35,36,39 };
}

template<> bool APOPTOSISa_8JNKK::rule<30>(bitset<41> x) {
    int A = x[29];
    int H = 1 - x[29];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[30];
}
template<> vector<ind> APOPTOSISa_8JNKK::depends_on<30>() {
    return { 29,30 };
}

template<> bool APOPTOSISa_8JNKK::rule<31>(bitset<41> x) {
    int A = x[29];
    int H = 1 - x[29];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[31];
}
template<> vector<ind> APOPTOSISa_8JNKK::depends_on<31>() {
    return { 29,31 };
}

template<> bool APOPTOSISa_8JNKK::rule<32>(bitset<41> x) {
    int A = x[26];
    int H = x[27];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[32];
}
template<> vector<ind> APOPTOSISa_8JNKK::depends_on<32>() {
    return { 26,27,32 };
}

template<> bool APOPTOSISa_8JNKK::rule<33>(bitset<41> x) {
    int A = x[13];
    int H = x[23]*x[25] + x[32];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[33];
}
template<> vector<ind> APOPTOSISa_8JNKK::depends_on<33>() {
    return { 13,23,25,32,33 };
}

template<> bool APOPTOSISa_8JNKK::rule<34>(bitset<41> x) {
    int A = x[19];
    int H = 1 - x[19];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[34];
}
template<> vector<ind> APOPTOSISa_8JNKK::depends_on<34>() {
    return { 19,34 };
}

template<> bool APOPTOSISa_8JNKK::rule<35>(bitset<41> x) {
    int A = x[34] + x[29];
    int H = 1 - x[34];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[35];
}
template<> vector<ind> APOPTOSISa_8JNKK::depends_on<35>() {
    return { 29,34,35 };
}

template<> bool APOPTOSISa_8JNKK::rule<36>(bitset<41> x) {
    return x[23] && x[24];
}
template<> vector<ind> APOPTOSISa_8JNKK::depends_on<36>() {
    return { 23,24,36 };
}

template<> bool APOPTOSISa_8JNKK::rule<37>(bitset<41> x) {
    int A = x[22] + x[38];
    int H = 3*x[33] + (1 - x[22])*(1 - x[38]);
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[37];
}
template<> vector<ind> APOPTOSISa_8JNKK::depends_on<37>() {
    return { 22,33,37,38 };
}

template<> bool APOPTOSISa_8JNKK::rule<38>(bitset<41> x) {
    int A = x[25] + x[5];
    int H = x[7];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[38];
}
template<> vector<ind> APOPTOSISa_8JNKK::depends_on<38>() {
    return { 5,7,25,38 };
}

template<> bool APOPTOSISa_8JNKK::rule<39>(bitset<41> x) {
    int A = x[40];
    int H = 1 - x[40];
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[39];
}
template<> vector<ind> APOPTOSISa_8JNKK::depends_on<39>() {
    return { 39,40 };
}

template<> bool APOPTOSISa_8JNKK::rule<40>(bitset<41> x) {
    int A = x[15];
    int H = 2*x[34] - x[15] + 1;
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[40];
}
template<> vector<ind> APOPTOSISa_8JNKK::depends_on<40>() {
    return { 15,34,40 };
}
