from sympy import *
import re

names = ["TNF", "GF", "TNFR1", "TRADD", "TRAF", "FADD", "RIP", "cIAP", "Cas8", "TNFR2", "TRAF2", "NIK", "IKK", "IkB", "NFkB", "A20", "MEKK1", "JNKK", "JNK", "Cas7", "Cas12", "Cas9", "APC", "Cas3", "Cas3prev", "Cas6", "BID", "BclX", "BAD", "p53", "Apaf1", "PTEN", "Mito", "IAP", "GFR", "PI3K", "PIP2", "PIP3", "Akt", "Mdm2", "DNAdam"]

def idx_from_rule(rule, names):
    return [names.index(y) for y in [str(x) for x in rule.free_symbols]]

def idx_from_name(l_name, names):
    return [names.index(y) for y in l_name]

for name in names:
    locals()[name] = symbols(name)

def apply_perm(perm, states):
    N = len(perm)
    print(perm)
    states_t = [states[perm[i]] for i in range(N)]
    return states_t

def And(A, B, C=1):
    return A*B*C

def Not(A):
    return 1-A


AH = [(TNF, Not(TNF)),         # TNF 0
      (GF,  Not(GF)),          # GF 1
      (TNF, Not(TNF)),         # TNFR1 2
      (TNFR1, Not(TNFR1)),     # TRADD )
      (TRADD, Not(TRADD)),     # TRAF 4
      (TRADD, Not(TRADD)),     # FADD 5
      (TRADD, Not(TRADD)),     # RIP 6
      (TRAF, Not(TRAF)),       # cIAP 7
      (FADD+Cas6, cIAP),       # Cas8 8
      (TNF, Not(TNF)),         # TNFR2 9
      (RIP, 2*TNFR2 + Not(RIP)),    # TRAF2 10
      (TRAF2, Not(TRAF2)),          # NIK 11
      (NIK + Akt, 3*A20 + And(Not(NIK), Not(Akt))),  # IKK 12
      (NFkB, 2*IKK),                # IkB 13
      (Not(IkB),  IkB),             # NFkB 14
      (NFkB, Not(NFkB)),            # A20 15
      (TRAF, Not(TRAF)),            # MEKK1 16
      (MEKK1, Not(MEKK1)+2*Akt),    # JNKK 17
      (JNKK, Not(JNKK)),            # JNK 18
      (Cas8 + APC, 3*IAP + And(Not(Cas8), Not(APC))), # Cas7 19
      (Cas7, Not(Cas7)),            # Cas12 20
      (Cas12 + Cas3, Akt + Not(Cas12) + 2*IAP),  # Cas9 21
      (And(Cas9, Apaf1, Mito), 2*IAP + Not(Cas9) + Not(Apaf1) + Not(Mito)), # APC 22
      (Cas8 + APC + Cas6, 4*IAP + And(Not(Cas8), Not(APC), Not(Cas6))),   # Cas3 23
      (0, 0), # Cas3prev (special rule) 24
      (Cas3, 2*IAP + Not(Cas3)),  # Cas6 25
      (And(p53, JNK) + And(p53, Cas8), 3*BclX + Not(p53)), # BID 26
      (NFkB, p53 + 2*BAD + Not(NFkB)),                 # BclX 27
      (p53, 2*Akt + Not(p53)),                         # BAD 28
      (JNK + 3*DNAdam, 2*Mdm2 + And(Not(JNK), Not(DNAdam))), # p53 29
      (p53, Not(p53)), # Apaf1 30
      (p53, Not(p53)), # PTEN  31
      (BID, BclX), # Mito 32
      (NFkB, Mito + And(Cas3, Cas6)), # IAP 33
      (GF, Not(GF)), # GFR 34
      (GFR, Not(GFR)), # PI3K 35
      (GFR, Not(GFR)), # PIP2 36
      (And(PI3K, PIP2), Not(PIP2) + 2*PTEN), # PIP3 37
      (PIP3, Not(PIP3)), # Akt 38
      (Akt + p53, Not(Akt)), # Mdm2 39
      (0, 0)]      # DNAdam (special rule) 40


def generate_rules(fn, label, perm):
    d = len(names)
    perm_names = apply_perm(perm, names)
    perm_AH = apply_perm(perm, AH)
    print('PERM: ', perm)

    fs = open(fn, 'w')
    fs.write("RULE_SET({}, {}, {})\n".format(label, d, ','.join(['"'+x+'"' for x in perm_names])));

    str_template_simple = """
template<> bool {}::rule<{}>(bitset<{}> x) {{
    return {};
}}
template<> vector<ind> {}::depends_on<{}>() {{
    return {{ {} }};
}}
"""

    str_template = """
template<> bool {}::rule<{}>(bitset<{}> x) {{
    int A = {};
    int H = {};
    if(A>H)      return true;
    else if(H>A) return false;
    else         return x[{}];
}}
template<> vector<ind> {}::depends_on<{}>() {{
    return {{ {} }};
}}
"""
    complete_deps_list = []
    for i, name in zip(range(d), perm_names):
        if name == 'Cas3prev':
            l1 = idx_from_name(["Cas3", "Cas3prev"], perm_names)
            l1_str = [str(x) for x in sorted(l1)]
            fs.write(str_template_simple.format(label, i, d, 'x[{}]'.format(l1[0]), label, i, ','.join(l1_str)))
            complete_deps_list.append(sorted(l1))
        elif name == 'DNAdam':
            l2 = idx_from_name(["DNAdam", "Cas3", "Cas3prev"], perm_names)
            l2_str = [str(x) for x in sorted(l2)]
            fs.write(str_template_simple.format(label, i, d, 'x[{}] && x[{}]'.format(l2[1], l2[2]), label, i, ','.join(l2_str)))
            complete_deps_list.append(sorted(l2))
        else:
            deps = sorted(list(set([i] + idx_from_rule(perm_AH[i][0], perm_names) + idx_from_rule(perm_AH[i][1], perm_names))))
            complete_deps_list.append(deps)

            code_A = ccode(perm_AH[i][0])
            code_H = ccode(perm_AH[i][1])
            for j, name in zip(range(d), perm_names):
                code_A = re.sub('{}([^0-9a-zA-Z]|$)'.format(name), 'x[{}]\\1'.format(j), code_A)
                code_H = re.sub('{}([^0-9a-zA-Z]|$)'.format(name), 'x[{}]\\1'.format(j), code_H)

            fs.write(str_template.format(label, i, d, code_A, code_H, i, label, i, ','.join([str(x) for x in deps])))

    fs.close()
    return complete_deps_list

'''
complete_deps_list = generate_rules('apoptosisa.hpp', "APOPTOSISa", range(len(names)))


import networkx as nx
from pylab import *
#print(f"Complete list = {complete_deps_list}")

def metis2Edgeset(g):
    edgelist = []
    for (i, l) in enumerate(g):
        for e in l:
            if i != e:
                edgelist.append((i,e))
    return edgelist
    
G = nx.Graph(metis2Edgeset(complete_deps_list))
GD = nx.DiGraph(metis2Edgeset(complete_deps_list))
#startpartition = ({8, 3, 6, 14, 1, 12, 17, 10, 16, 9, 15}, {19, 18, 11, 2, 20, 7, 21, 0, 13, 5, 4})
bisection = nx.algorithms.community.kernighan_lin_bisection(G, max_iter=2**32)# partition=startpartition)
print(bisection)
exit()
'''

#perm = [1, 11, 12, 13, 14, 15, 16, 17, 18, 26, 27, 28, 29, 31, 34, 35, 36, 37, 38, 39, 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 19, 20, 21, 22, 23, 24, 25, 30, 32, 33, 40]
#complete_deps_list = generate_rules('apoptosis51.hpp', "APOPTOSIS51", perm)
#perm = [8, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 32, 33, 38, 39, 40, 0, 1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 15, 16, 31, 34, 35, 36, 37]
#complete_deps_list = generate_rules('apoptosis52.hpp', "APOPTOSIS52", perm)
#perm = range(41)
#complete_deps_list = generate_rules('apoptosistest.hpp', "APOPTOSIStest", perm)
#perm = [20,21,22,23,24,25,26,27,28,29,30,31,32,33,35,36,37,38,39,40,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,34]
#complete_deps_list = generate_rules('apoptosisa_1GFR.hpp', "APOPTOSISa_1GFR", perm)
#perm = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,34,20,21,22,23,24,25,26,27,28,29,30,31,32,33,35,36,37,38,39,40,19]
#complete_deps_list = generate_rules('apoptosisa_2Cas7.hpp', "APOPTOSISa_2Cas7", perm)
#perm = [20,21,22,23,24,25,26,27,28,29,30,31,32,33,36,37,38,39,40,19,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,34,35]
#complete_deps_list = generate_rules('apoptosisa_3PI3K.hpp', "APOPTOSISa_3PI3K", perm)
#perm = [0,1,2,3,4,5,6,7,9,10,11,12,13,14,15,16,17,18,34,35,20,21,22,23,24,25,26,27,28,29,30,31,32,33,36,37,38,39,40,19,8]
#complete_deps_list = generate_rules('apoptosisa_4Cas8.hpp', "APOPTOSISa_4Cas8", perm)
#perm = [20,21,22,23,24,25,26,27,28,29,30,31,32,33,37,38,39,40,19,8,0,1,2,3,4,5,6,7,9,10,11,12,13,14,15,16,17,18,34,35,36]
#complete_deps_list = generate_rules('apoptosisa_5PIP2.hpp', "APOPTOSISa_5PIP2", perm)
#perm = [0,1,2,3,4,5,6,7,9,10,11,12,13,14,15,16,17,34,35,36,20,21,22,23,24,25,26,27,28,29,30,31,32,33,37,38,39,40,19,8,18]
#complete_deps_list = generate_rules('apoptosisa_6JNK.hpp', "APOPTOSISa_6JNK", perm)
#perm = [20,21,22,23,24,25,26,27,28,29,30,31,32,33,38,39,40,19,8,18,0,1,2,3,4,5,6,7,9,10,11,12,13,14,15,16,17,34,35,36,37]
#complete_deps_list = generate_rules('apoptosisa_7PIP3.hpp', "APOPTOSISa_7PIP3", perm)
#perm = [0,1,2,3,4,5,6,7,9,10,11,12,13,14,15,16,34,35,36,37,20,21,22,23,24,25,26,27,28,29,30,31,32,33,38,39,40,19,8,18,17]
#complete_deps_list = generate_rules('apoptosisa_8JNKK.hpp', "APOPTOSISa_8JNKK", perm)
#perm = [20,21,22,23,24,25,26,27,28,29,30,32,33,38,39,40,19,8,18,17,0,1,2,3,4,5,6,7,9,10,11,12,13,14,15,16,34,35,36,37,31]
#complete_deps_list = generate_rules('apoptosisa_9PTEN.hpp', "APOPTOSISa_9PTEN", perm)
perm = [0, 1, 2, 3, 4, 5, 6, 9, 10, 11, 12, 13, 14, 15, 16, 31, 34, 35, 36, 37, 7, 8, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 32, 33, 38, 39, 40]
complete_deps_list = generate_rules('apoptosis41.hpp', "APOPTOSIS41", perm)
perm = [1, 12, 13, 14, 15, 16, 17, 18, 26, 27, 28, 29, 30, 31, 34, 35, 36, 37, 38, 39, 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 19, 20, 21, 22, 23, 24, 25, 32, 33, 40]
complete_deps_list = generate_rules('apoptosis42.hpp', "APOPTOSIS42", perm)
perm = [1, 12, 13, 14, 15, 16, 17, 18, 26, 27, 28, 29, 31, 32, 34, 35, 36, 37, 38, 39, 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 19, 20, 21, 22, 23, 24, 25, 30, 33, 40]
complete_deps_list = generate_rules('apoptosis43.hpp', "APOPTOSIS43", perm)
perm = [0, 1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 15, 16, 34, 35, 36, 37, 8, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 38, 39, 40]
complete_deps_list = generate_rules('apoptosis44.hpp', "APOPTOSIS44", perm)
