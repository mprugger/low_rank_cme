import matplotlib.pyplot as plt

#print(sum([23, 21, 17, 5, 3, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]))
columns = ('51', '52', '41', '42', '43', '44', '31', '32', '33', '34', '21', '22', '23', '24', '25', '26', '27', '28', '29', '210', '211', '212', '11', '12', '13', '14', '15', '16', '17', '18', '19', '110', '111', '112', '113', '114', '115', '116', '117', '118', '119', '120', '121', '122', '123', '124', '125', '126', '127', '128', '129', '130', '131', '132', '133', '134', '135', '136', '137', '138')
plt.bar(range(60), [5, 5, 4,4,4,4,3,3,3,3,2,2,2,2,2,2,2,2,2,2,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1])#, color = ['skyblue', 'mediumblue', 'navy', 'plum', 'purple', 'lightgrey', 'lightgrey', 'lightgrey', 'lightgrey', 'green', 'lightgrey', 'lightgrey', 'lightgrey', 'lightgrey', 'lightgrey', 'lightgrey', 'lightgrey', 'lightgrey', 'lightgrey', 'lightgrey', 'lightgrey', 'lightgrey', 'lightgrey', 'lightgrey', 'cornflowerblue', 'lightgrey', 'blueviolet', 'lightgrey', 'lightgrey', 'lightgrey', 'lightgrey', 'lightgrey'])
plt.xticks(range(60), columns, rotation =90)
plt.tight_layout()
plt.show()
