#pragma once

#include <iostream>
#include <bitset>
#include <array>
#include <vector>
#include <iomanip>
#include <ctime>
#include <chrono>
using std::cout;
using std::endl;
using std::array;
using std::vector;
using std::bitset;

typedef long ind;

typedef std::chrono::time_point<std::chrono::high_resolution_clock> time_point;

static time_point now() {
    return std::chrono::high_resolution_clock::now();
}

double delta(const time_point t1, const time_point& t2) {
    return  std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count();
}

double delta(const time_point& t1) {
    return  std::chrono::duration_cast<std::chrono::duration<double>>(now() - t1).count();
}


#define RULE_SET(__name, __d, ...) \
struct __name { \
    template<ind i> \
    static bool rule(bitset<(__d)> x); \
    \
    template<ind i> \
    vector<ind> static depends_on(); \
    \
    static constexpr size_t d = (__d);\
    \
    static vector<std::string> names() { \
        return {__VA_ARGS__}; \
    } \
};


#define RULE(__name, __i, __rule, ...) \
template<> bool __name::rule<(__i)>(bitset<__name::d> x) { \
    return (__rule); \
} \
template<> vector<ind> __name::depends_on<(__i)>() { \
    return {__VA_ARGS__}; \
}


#ifdef __MKL__
#include<mkl.h>

void mkl_matmul(bool transpose_A, bool transpose_B, ind m, ind k, ind n, const double* A,
                const double* B, double* out) {
    cblas_dgemm(CblasColMajor,
                (transpose_A) ? CblasTrans : CblasNoTrans,
                (transpose_B) ? CblasTrans : CblasNoTrans, 
                m, n, k, 1.0, A, transpose_A ? k : m, B, transpose_B ? n : k, 0.0, out, m);

}
#endif

