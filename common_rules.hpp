#pragma once

#include "common.hpp"


// simple test example
struct SIMPLE {

    template<ind i>
    static bool rule(bitset<4> x);

    template<ind i>
    vector<ind> static depends_on();

    static vector<std::string> names() {
        return {"A1", "B2", "C3", "D4"};
    }
};


template<> bool SIMPLE::rule<0>(bitset<4> x) {
    return x[0] && x[2];
}
template<> bool SIMPLE::rule<1>(bitset<4> x) {
    return x[1];
}
template<> bool SIMPLE::rule<2>(bitset<4> x) {
    return x[0] && !x[3];
}
template<> bool SIMPLE::rule<3>(bitset<4> x) {
    return x[3];
}


template<> vector<ind> SIMPLE::depends_on<0>() {
    return {0, 2};
}
template<> vector<ind> SIMPLE::depends_on<1>() {
    return {1};
}
template<> vector<ind> SIMPLE::depends_on<2>() {
    return {0, 2, 3};
}
template<> vector<ind> SIMPLE::depends_on<3>() {
    return {3};
}


// simple test example with odd number of rules
struct SIMPLE_ODD {

    template<ind i>
    static bool rule(bitset<5> x);

    template<ind i>
    vector<ind> static depends_on();

    static vector<std::string> names() {
        return {"A1", "B2", "C3", "D4", "E5"};
    }
};


template<> bool SIMPLE_ODD::rule<0>(bitset<5> x) {
    return x[0] && x[2];
}
template<> bool SIMPLE_ODD::rule<1>(bitset<5> x) {
    return x[1];
}
template<> bool SIMPLE_ODD::rule<2>(bitset<5> x) {
    return x[0] && !x[3];
}
template<> bool SIMPLE_ODD::rule<3>(bitset<5> x) {
    return x[3];
}
template<> bool SIMPLE_ODD::rule<4>(bitset<5> x) {
    return x[2] && x[3];
}


template<> vector<ind> SIMPLE_ODD::depends_on<0>() {
    return {0, 2};
}
template<> vector<ind> SIMPLE_ODD::depends_on<1>() {
    return {1};
}
template<> vector<ind> SIMPLE_ODD::depends_on<2>() {
    return {0, 2, 3};
}
template<> vector<ind> SIMPLE_ODD::depends_on<3>() {
    return {3};
}
template<> vector<ind> SIMPLE_ODD::depends_on<4>() {
    return {2,3,4};
}

// simple test example with reordered odd number of rules
struct SIMPLE_ODD_REORDERED {

    template<ind i>
    static bool rule(bitset<5> x);

    template<ind i>
    vector<ind> static depends_on();

    static vector<std::string> names() {
        return {"E5", "D4", "C3", "A1", "B2"};
    }
};


template<> bool SIMPLE_ODD_REORDERED::rule<0>(bitset<5> x) {
    return x[2] && x[1];
}
template<> bool SIMPLE_ODD_REORDERED::rule<1>(bitset<5> x) {
    return x[1];
}
template<> bool SIMPLE_ODD_REORDERED::rule<2>(bitset<5> x) {
    return x[3] && !x[1];
}
template<> bool SIMPLE_ODD_REORDERED::rule<3>(bitset<5> x) {
    return x[3] && x[2];
}
template<> bool SIMPLE_ODD_REORDERED::rule<4>(bitset<5> x) {
    return x[4];
}


template<> vector<ind> SIMPLE_ODD_REORDERED::depends_on<0>() {
    return {0, 1, 2};
}
template<> vector<ind> SIMPLE_ODD_REORDERED::depends_on<1>() {
    return {1};
}
template<> vector<ind> SIMPLE_ODD_REORDERED::depends_on<2>() {
    return {1, 2, 3};
}
template<> vector<ind> SIMPLE_ODD_REORDERED::depends_on<3>() {
    return {2, 3};
}
template<> vector<ind> SIMPLE_ODD_REORDERED::depends_on<4>() {
    return {4};
}


// EMT
RULE_SET(EMT, 8, "NICD", "Notch", "TP53", "TP63_TP73", "miRNA", "EMTreg", "ECM", "DNAdam");

template<> bool EMT::rule<0>(bitset<8> x) {
    return x[1] && !x[3] && !x[2];
}
template<> bool EMT::rule<1>(bitset<8> x) {
    return x[6] && !x[4];
}
template<> bool EMT::rule<2>(bitset<8> x) {
    return (x[7] || x[0] || x[4]) && !x[5] && !x[3];
}
template<> bool EMT::rule<3>(bitset<8> x) {
    return x[7] && !x[4] && !x[0] && !x[2];
}
template<> bool EMT::rule<4>(bitset<8> x) {
    return (x[2] || x[3]) && !x[5];
}
template<> bool EMT::rule<5>(bitset<8> x) {
    return x[0] && !x[4];
}
template<> bool EMT::rule<6>(bitset<8> x) {
    return x[6];
}
template<> bool EMT::rule<7>(bitset<8> x) {
    return x[7];
}

template<> vector<ind> EMT::depends_on<0>() {
    return {0, 1, 2, 3};
}
template<> vector<ind> EMT::depends_on<1>() {
    return {1, 4, 6};
}
template<> vector<ind> EMT::depends_on<2>() {
    return {0, 2, 3, 4, 5, 7};
}
template<> vector<ind> EMT::depends_on<3>() {
    return {0, 2, 3, 4, 7};
}
template<> vector<ind> EMT::depends_on<4>() {
    return {2, 3, 4, 5};
}
template<> vector<ind> EMT::depends_on<5>() {
    return {0, 4, 5};
}
template<> vector<ind> EMT::depends_on<6>() {
    return {6};
}
template<> vector<ind> EMT::depends_on<7>() {
    return {7};
}

/*
// gene protein/products
struct GENE_PROTEIN_PRODUCTS {

    template<ind i>
    static bool rule(bitset<22> x);

    template<ind i>
    vector<ind> static depends_on();

};*/

RULE_SET(GENE_PROTEIN_PRODUCTS, 22, "ERK", "P27361", "RSK", "Q15418", "mir-1976", "MLL", "Q03164", "HOAX9", "mir-196b", "TSC1/2", "P31749", "PDK1", "Q15118", "AKT", "RHEB", "Q15382", "GBL", "Q9BVC4", "mTOR", "P42345", "RICTOR", "Q6R327");

template<> bool GENE_PROTEIN_PRODUCTS::rule<0>(bitset<22> x) {
    return x[0]; // ERK = ERK
}
template<> bool GENE_PROTEIN_PRODUCTS::rule<1>(bitset<22> x) {
    return x[0]; // P27361 = ERK
}
template<> bool GENE_PROTEIN_PRODUCTS::rule<2>(bitset<22> x) {
    return x[2]; // RSK = RSK
}
template<> bool GENE_PROTEIN_PRODUCTS::rule<3>(bitset<22> x) {
    return x[2]; // Q15418 = RSK
}
template<> bool GENE_PROTEIN_PRODUCTS::rule<4>(bitset<22> x) {
    return x[2]; // mir-1976 = RSK
}
template<> bool GENE_PROTEIN_PRODUCTS::rule<5>(bitset<22> x) {
    return x[5]; // MLL = MLL
}
template<> bool GENE_PROTEIN_PRODUCTS::rule<6>(bitset<22> x) {
    return x[5] && !x[4]; // Q03164 = MLL and not mir-1976
}
template<> bool GENE_PROTEIN_PRODUCTS::rule<7>(bitset<22> x) {
    return x[6]; // HOAX9 = Q03164
}
template<> bool GENE_PROTEIN_PRODUCTS::rule<8>(bitset<22> x) {
    return x[7]; // mir-196b = HOAX9
}
template<> bool GENE_PROTEIN_PRODUCTS::rule<9>(bitset<22> x) {
    return (!(x[1] && x[3])) && !x[10]; // TSC1/2 = not (P27361 and Q15418) and not P31749
}
template<> bool GENE_PROTEIN_PRODUCTS::rule<10>(bitset<22> x) {
    return x[13]; // P31749 = AKT
}
template<> bool GENE_PROTEIN_PRODUCTS::rule<11>(bitset<22> x) {
    return x[11]; // PDK1 = PDK1
}
template<> bool GENE_PROTEIN_PRODUCTS::rule<12>(bitset<22> x) {
    return x[11]; // Q15118 = PDK1
}
template<> bool GENE_PROTEIN_PRODUCTS::rule<13>(bitset<22> x) {
    return x[12] && x[21] && x[19] && x[17]; // AKT = Q15118 and Q6R327 and P42345 and Q9BVC4
}
template<> bool GENE_PROTEIN_PRODUCTS::rule<14>(bitset<22> x) {
    return !x[9]; // RHEB = not TSC1/2
}
template<> bool GENE_PROTEIN_PRODUCTS::rule<15>(bitset<22> x) {
    return x[14]; // Q15382 = RHEB
}
template<> bool GENE_PROTEIN_PRODUCTS::rule<16>(bitset<22> x) {
    return x[15]; // GBL = Q15382
}
template<> bool GENE_PROTEIN_PRODUCTS::rule<17>(bitset<22> x) {
    return x[16]; // Q9BVC4 = GBL
}
template<> bool GENE_PROTEIN_PRODUCTS::rule<18>(bitset<22> x) {
    return x[15]; // mTOR = Q15382
}
template<> bool GENE_PROTEIN_PRODUCTS::rule<19>(bitset<22> x) {
    return x[18]; // P42345 = mTOR
}
template<> bool GENE_PROTEIN_PRODUCTS::rule<20>(bitset<22> x) {
    return x[15]; // RICTOR = Q15382
}
template<> bool GENE_PROTEIN_PRODUCTS::rule<21>(bitset<22> x) {
    return x[20] && !x[8]; // Q6R327 = RICTOR and not mir-196bs
}


template<> vector<ind> GENE_PROTEIN_PRODUCTS::depends_on<0>() {
    return {0};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS::depends_on<1>() {
    return {0, 1};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS::depends_on<2>() {
    return {2};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS::depends_on<3>() {
    return {2, 3};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS::depends_on<4>() {
    return {2, 4};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS::depends_on<5>() {
    return {5};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS::depends_on<6>() {
    return {4, 5, 6};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS::depends_on<7>() {
    return {6, 7};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS::depends_on<8>() {
    return {7, 8};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS::depends_on<9>() {
    return {1, 3, 9, 10};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS::depends_on<10>() {
    return {10, 13};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS::depends_on<11>() {
    return {11};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS::depends_on<12>() {
    return {11, 12};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS::depends_on<13>() {
    return {12, 13, 17, 19, 21};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS::depends_on<14>() {
    return {9, 14};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS::depends_on<15>() {
    return {14, 15};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS::depends_on<16>() {
    return {15, 16};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS::depends_on<17>() {
    return {16, 17};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS::depends_on<18>() {
    return {15, 18};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS::depends_on<19>() {
    return {18, 19};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS::depends_on<20>() {
    return {15, 20};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS::depends_on<21>() {
    return {8, 20, 21};
}

// gene protein/products artificial
/*
struct GENE_PROTEIN_PRODUCTS_TMP {

    template<ind i>
    static bool rule(bitset<29> x);

    template<ind i>
    vector<ind> static depends_on();

};
*/
//RULE_SET(GENE_PROTEIN_PRODUCTS_TMP, 29); {

//RULES_START(GENE_PROTEIN_PRODUCTS_TMP, 29);
//    RULE(0);
//RULES_STOP();
//
RULE_SET(GENE_PROTEIN_PRODUCTS_TMP, 29);
RULE(GENE_PROTEIN_PRODUCTS_TMP, 0, x[0], 0);
/*
template<> bool GENE_PROTEIN_PRODUCTS_TMP ::template rule<0>(bitset<29> x) {
    return x[0];
}*/
//RULE(0, NAME, x[0] && x[1], {0,1});
/*
template<> bool GENE_PROTEIN_PRODUCTS_TMP::rule<0>(bitset<29> x) {
    return x[0]; // ERK = ERK
}
*/
template<> bool GENE_PROTEIN_PRODUCTS_TMP::rule<1>(bitset<29> x) {
    return x[0]; // P27361 = ERK
}
template<> bool GENE_PROTEIN_PRODUCTS_TMP::rule<2>(bitset<29> x) {
    return x[2]; // RSK = RSK
}
template<> bool GENE_PROTEIN_PRODUCTS_TMP::rule<3>(bitset<29> x) {
    return x[2]; // Q15418 = RSK
}
template<> bool GENE_PROTEIN_PRODUCTS_TMP::rule<4>(bitset<29> x) {
    return x[2]; // mir-1976 = RSK
}
template<> bool GENE_PROTEIN_PRODUCTS_TMP::rule<5>(bitset<29> x) {
    return x[5]; // MLL = MLL
}
template<> bool GENE_PROTEIN_PRODUCTS_TMP::rule<6>(bitset<29> x) {
    return x[5] && !x[4]; // Q03164 = MLL and not mir-1976
}
template<> bool GENE_PROTEIN_PRODUCTS_TMP::rule<7>(bitset<29> x) {
    return x[6]; // HOAX9 = Q03164
}
template<> bool GENE_PROTEIN_PRODUCTS_TMP::rule<8>(bitset<29> x) {
    return x[7]; // mir-196b = HOAX9
}
template<> bool GENE_PROTEIN_PRODUCTS_TMP::rule<9>(bitset<29> x) {
    return (!(x[1] && x[3])) && !x[10]; // TSC1/2 = not (P27361 and Q15418) and not P31749
}
template<> bool GENE_PROTEIN_PRODUCTS_TMP::rule<10>(bitset<29> x) {
    return x[13]; // P31749 = AKT
}
template<> bool GENE_PROTEIN_PRODUCTS_TMP::rule<11>(bitset<29> x) {
    return x[11]; // PDK1 = PDK1
}
template<> bool GENE_PROTEIN_PRODUCTS_TMP::rule<12>(bitset<29> x) {
    return x[11]; // Q15118 = PDK1
}
template<> bool GENE_PROTEIN_PRODUCTS_TMP::rule<13>(bitset<29> x) {
    return x[12] && x[21] && x[19] && x[17]; // AKT = Q15118 and Q6R327 and P42345 and Q9BVC4
}
template<> bool GENE_PROTEIN_PRODUCTS_TMP::rule<14>(bitset<29> x) {
    return !x[9]; // RHEB = not TSC1/2
}
template<> bool GENE_PROTEIN_PRODUCTS_TMP::rule<15>(bitset<29> x) {
    return x[14]; // Q15382 = RHEB
}
template<> bool GENE_PROTEIN_PRODUCTS_TMP::rule<16>(bitset<29> x) {
    return x[15]; // GBL = Q15382
}
template<> bool GENE_PROTEIN_PRODUCTS_TMP::rule<17>(bitset<29> x) {
    return x[16]; // Q9BVC4 = GBL
}
template<> bool GENE_PROTEIN_PRODUCTS_TMP::rule<18>(bitset<29> x) {
    return x[15]; // mTOR = Q15382
}
template<> bool GENE_PROTEIN_PRODUCTS_TMP::rule<19>(bitset<29> x) {
    return x[18]; // P42345 = mTOR
}
template<> bool GENE_PROTEIN_PRODUCTS_TMP::rule<20>(bitset<29> x) {
    return x[15]; // RICTOR = Q15382
}
template<> bool GENE_PROTEIN_PRODUCTS_TMP::rule<21>(bitset<29> x) {
    return x[20] && !x[8]; // Q6R327 = RICTOR and not mir-196bs
}

template<> bool GENE_PROTEIN_PRODUCTS_TMP::rule<22>(bitset<29> x) {
    return x[22] && x[23];
}
template<> bool GENE_PROTEIN_PRODUCTS_TMP::rule<23>(bitset<29> x) {
    return x[23];
}
template<> bool GENE_PROTEIN_PRODUCTS_TMP::rule<24>(bitset<29> x) {
    return x[3] && !x[25];
}
template<> bool GENE_PROTEIN_PRODUCTS_TMP::rule<25>(bitset<29> x) {
    return x[24] && x[25] && x[26];
}
template<> bool GENE_PROTEIN_PRODUCTS_TMP::rule<26>(bitset<29> x) {
    return x[26] && !x[25];
}
template<> bool GENE_PROTEIN_PRODUCTS_TMP::rule<27>(bitset<29> x) {
    return x[25] || x[27];
}
template<> bool GENE_PROTEIN_PRODUCTS_TMP::rule<28>(bitset<29> x) {
    return x[2] || !x[25] || x[28];
}


/*
template<> vector<ind> GENE_PROTEIN_PRODUCTS_TMP::depends_on<0>() {
    return {0};
}*/
template<> vector<ind> GENE_PROTEIN_PRODUCTS_TMP::depends_on<1>() {
    return {0, 1};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_TMP::depends_on<2>() {
    return {2};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_TMP::depends_on<3>() {
    return {2, 3};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_TMP::depends_on<4>() {
    return {2, 4};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_TMP::depends_on<5>() {
    return {5};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_TMP::depends_on<6>() {
    return {4, 5, 6};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_TMP::depends_on<7>() {
    return {6, 7};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_TMP::depends_on<8>() {
    return {7, 8};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_TMP::depends_on<9>() {
    return {1, 3, 9, 10};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_TMP::depends_on<10>() {
    return {10, 13};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_TMP::depends_on<11>() {
    return {11};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_TMP::depends_on<12>() {
    return {11, 12};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_TMP::depends_on<13>() {
    return {12, 13, 17, 19, 21};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_TMP::depends_on<14>() {
    return {9, 14};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_TMP::depends_on<15>() {
    return {14, 15};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_TMP::depends_on<16>() {
    return {15, 16};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_TMP::depends_on<17>() {
    return {16, 17};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_TMP::depends_on<18>() {
    return {15, 18};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_TMP::depends_on<19>() {
    return {18, 19};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_TMP::depends_on<20>() {
    return {15, 20};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_TMP::depends_on<21>() {
    return {8, 20, 21};
}

template<> vector<ind> GENE_PROTEIN_PRODUCTS_TMP::depends_on<22>() {
    return {22, 23};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_TMP::depends_on<23>() {
    return {23};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_TMP::depends_on<24>() {
    return {3, 24, 25};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_TMP::depends_on<25>() {
    return {24, 25, 26};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_TMP::depends_on<26>() {
    return {25, 26};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_TMP::depends_on<27>() {
    return {25, 27};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_TMP::depends_on<28>() {
    return {2, 25, 28};
}


/*
RULE_SET(GENE_PROTEIN_PRODUCTS_G, 22, "ERK", "P27361", "RSK", "Q15418", 
"mir-1976", // 4 -> 20
"MLL", // 5 -> 18
"Q03164", // 6 -> 16
"HOAX9", // 7 -> 15
"mir-196b", // 8 14
"TSC1/2", "P31749", "PDK1", "Q15118", "AKT", 
"RHEB", // 14 -> 8
"Q15382", // 15 -> 7
"GBL", // 16 -> 6
"Q9BVC4", 
"mTOR", // 18 -> 5
"P42345", 
"RICTOR", // 20 -> 4
"Q6R327");
*/

// Permutation green ("red small cut")
RULE_SET(GENE_PROTEIN_PRODUCTS_G, 22, "ERK", "P27361", "RSK", "Q15418", 
"RICTOR", //"mir-1976", 4 -> 20
"mTOR", // 5 -> 18
"GBL", // 6 -> 16
"Q15382", // 7 -> 15
"RHEB", // 8 14
"TSC1/2", "P31749", "PDK1", "Q15118", "AKT", 
"mir-196b", // 14 -> 8
"HOAX9", // 15 -> 7
"Q03164", // 16 -> 6
"Q9BVC4", 
"MLL", // 18 -> 5
"P42345", 
"mir-1976", // 20 -> 4
"Q6R327");

template<> bool GENE_PROTEIN_PRODUCTS_G::rule<0>(bitset<22> x) {
    return x[0]; // ERK = ERK
}
template<> bool GENE_PROTEIN_PRODUCTS_G::rule<1>(bitset<22> x) {
    return x[0]; // P27361 = ERK
}
template<> bool GENE_PROTEIN_PRODUCTS_G::rule<2>(bitset<22> x) {
    return x[2]; // RSK = RSK
}
template<> bool GENE_PROTEIN_PRODUCTS_G::rule<3>(bitset<22> x) {
    return x[2]; // Q15418 = RSK
}
template<> bool GENE_PROTEIN_PRODUCTS_G::rule<4>(bitset<22> x) {
    return x[7]; // RICTOR = Q15382
}
template<> bool GENE_PROTEIN_PRODUCTS_G::rule<5>(bitset<22> x) {
    return x[7]; // mTOR = Q15382
}
template<> bool GENE_PROTEIN_PRODUCTS_G::rule<6>(bitset<22> x) {
    return x[7]; // GBL = Q15382
}
template<> bool GENE_PROTEIN_PRODUCTS_G::rule<7>(bitset<22> x) {
    return x[8]; // Q15382 = RHEB
}
template<> bool GENE_PROTEIN_PRODUCTS_G::rule<8>(bitset<22> x) {
    return !x[9]; // RHEB = not TSC1/2
}
template<> bool GENE_PROTEIN_PRODUCTS_G::rule<9>(bitset<22> x) {
    return (!(x[1] && x[3])) && !x[10]; // TSC1/2 = not (P27361 and Q15418) and not P31749
}
template<> bool GENE_PROTEIN_PRODUCTS_G::rule<10>(bitset<22> x) {
    return x[13]; // P31749 = AKT
}
template<> bool GENE_PROTEIN_PRODUCTS_G::rule<11>(bitset<22> x) {
    return x[11]; // PDK1 = PDK1
}
template<> bool GENE_PROTEIN_PRODUCTS_G::rule<12>(bitset<22> x) {
    return x[11]; // Q15118 = PDK1
}
template<> bool GENE_PROTEIN_PRODUCTS_G::rule<13>(bitset<22> x) {
    return x[12] && x[21] && x[19] && x[17]; // AKT = Q15118 and Q6R327 and P42345 and Q9BVC4
}
template<> bool GENE_PROTEIN_PRODUCTS_G::rule<14>(bitset<22> x) {
    return x[15]; // mir-196b = HOAX9
}
template<> bool GENE_PROTEIN_PRODUCTS_G::rule<15>(bitset<22> x) {
    return x[16]; // HOAX9 = Q03164
}
template<> bool GENE_PROTEIN_PRODUCTS_G::rule<16>(bitset<22> x) {
    return x[18] && !x[20]; // Q03164 = MLL and not mir-1976
}
template<> bool GENE_PROTEIN_PRODUCTS_G::rule<17>(bitset<22> x) {
    return x[6]; // Q9BVC4 = GBL
}
template<> bool GENE_PROTEIN_PRODUCTS_G::rule<18>(bitset<22> x) {
    return x[18]; // MLL = MLL
}
template<> bool GENE_PROTEIN_PRODUCTS_G::rule<19>(bitset<22> x) {
    return x[5]; // P42345 = mTOR
}
template<> bool GENE_PROTEIN_PRODUCTS_G::rule<20>(bitset<22> x) {
    return x[2]; // mir-1976 = RSK
}
template<> bool GENE_PROTEIN_PRODUCTS_G::rule<21>(bitset<22> x) {
    return x[4] && !x[14]; // Q6R327 = RICTOR and not mir-196bs
}


template<> vector<ind> GENE_PROTEIN_PRODUCTS_G::depends_on<0>() {
    return {0};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_G::depends_on<1>() {
    return {0, 1};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_G::depends_on<2>() {
    return {2};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_G::depends_on<3>() {
    return {2, 3};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_G::depends_on<4>() {
    return {4, 7};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_G::depends_on<5>() {
    return {5, 7};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_G::depends_on<6>() {
    return {6, 7};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_G::depends_on<7>() {
    return {7, 8};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_G::depends_on<8>() {
    return {8, 9};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_G::depends_on<9>() {
    return {1, 3, 9, 10};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_G::depends_on<10>() {
    return {10, 13};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_G::depends_on<11>() {
    return {11};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_G::depends_on<12>() {
    return {11, 12};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_G::depends_on<13>() {
    return {12, 13, 17, 19, 21};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_G::depends_on<14>() {
    return {14, 15};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_G::depends_on<15>() {
    return {15, 16};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_G::depends_on<16>() {
    return {16, 18, 20};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_G::depends_on<17>() {
    return {6, 17};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_G::depends_on<18>() {
    return {18};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_G::depends_on<19>() {
    return {5, 19};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_G::depends_on<20>() {
    return {2, 20};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_G::depends_on<21>() {
    return {4, 14, 21};
}



/*
RULE_SET(GENE_PROTEIN_PRODUCTS_P, 22, "ERK", "P27361", "RSK", "Q15418", "mir-1976", "MLL", "Q03164", "HOAX9", "mir-196b", 
"TSC1/2", // 20 -> 9
"P31749", // 21 -> 10
"PDK1", "Q15118", "AKT", "RHEB", "Q15382", "GBL", "Q9BVC4", "mTOR", "P42345", 
"RICTOR", // 9 -> 20
"Q6R327"); // 10 -> 21
*/
// Permutation purple
RULE_SET(GENE_PROTEIN_PRODUCTS_P, 22, "ERK", "P27361", "RSK", "Q15418", "mir-1976", "MLL", "Q03164", "HOAX9", "mir-196b", 
"RICTOR", // 20 -> 9
"Q6R327", // 21 -> 10
"PDK1", "Q15118", "AKT", "RHEB", "Q15382", "GBL", "Q9BVC4", "mTOR", "P42345", 
"TSC1/2", // 9 -> 20
"P31749"); // 10 -> 21

template<> bool GENE_PROTEIN_PRODUCTS_P::rule<0>(bitset<22> x) {
    return x[0]; // ERK = ERK
}
template<> bool GENE_PROTEIN_PRODUCTS_P::rule<1>(bitset<22> x) {
    return x[0]; // P27361 = ERK
}
template<> bool GENE_PROTEIN_PRODUCTS_P::rule<2>(bitset<22> x) {
    return x[2]; // RSK = RSK
}
template<> bool GENE_PROTEIN_PRODUCTS_P::rule<3>(bitset<22> x) {
    return x[2]; // Q15418 = RSK
}
template<> bool GENE_PROTEIN_PRODUCTS_P::rule<4>(bitset<22> x) {
    return x[2]; // mir-1976 = RSK
}
template<> bool GENE_PROTEIN_PRODUCTS_P::rule<5>(bitset<22> x) {
    return x[5]; // MLL = MLL
}
template<> bool GENE_PROTEIN_PRODUCTS_P::rule<6>(bitset<22> x) {
    return x[5] && !x[4]; // Q03164 = MLL and not mir-1976
}
template<> bool GENE_PROTEIN_PRODUCTS_P::rule<7>(bitset<22> x) {
    return x[6]; // HOAX9 = Q03164
}
template<> bool GENE_PROTEIN_PRODUCTS_P::rule<8>(bitset<22> x) {
    return x[7]; // mir-196b = HOAX9
}
template<> bool GENE_PROTEIN_PRODUCTS_P::rule<9>(bitset<22> x) {
    return x[15]; // RICTOR = Q15382
}
template<> bool GENE_PROTEIN_PRODUCTS_P::rule<10>(bitset<22> x) {
    return x[9] && !x[8]; // Q6R327 = RICTOR and not mir-196bs
}
template<> bool GENE_PROTEIN_PRODUCTS_P::rule<11>(bitset<22> x) {
    return x[11]; // PDK1 = PDK1
}
template<> bool GENE_PROTEIN_PRODUCTS_P::rule<12>(bitset<22> x) {
    return x[11]; // Q15118 = PDK1
}
template<> bool GENE_PROTEIN_PRODUCTS_P::rule<13>(bitset<22> x) {
    return x[12] && x[10] && x[19] && x[17]; // AKT = Q15118 and Q6R327 and P42345 and Q9BVC4
}
template<> bool GENE_PROTEIN_PRODUCTS_P::rule<14>(bitset<22> x) {
    return !x[20]; // RHEB = not TSC1/2
}
template<> bool GENE_PROTEIN_PRODUCTS_P::rule<15>(bitset<22> x) {
    return x[14]; // Q15382 = RHEB
}
template<> bool GENE_PROTEIN_PRODUCTS_P::rule<16>(bitset<22> x) {
    return x[15]; // GBL = Q15382
}
template<> bool GENE_PROTEIN_PRODUCTS_P::rule<17>(bitset<22> x) {
    return x[16]; // Q9BVC4 = GBL
}
template<> bool GENE_PROTEIN_PRODUCTS_P::rule<18>(bitset<22> x) {
    return x[15]; // mTOR = Q15382
}
template<> bool GENE_PROTEIN_PRODUCTS_P::rule<19>(bitset<22> x) {
    return x[18]; // P42345 = mTOR
}
template<> bool GENE_PROTEIN_PRODUCTS_P::rule<20>(bitset<22> x) {
    return (!(x[1] && x[3])) && !x[21]; // TSC1/2 = not (P27361 and Q15418) and not P31749
}
template<> bool GENE_PROTEIN_PRODUCTS_P::rule<21>(bitset<22> x) {
    return x[13]; // P31749 = AKT
}



template<> vector<ind> GENE_PROTEIN_PRODUCTS_P::depends_on<0>() {
    return {0};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_P::depends_on<1>() {
    return {0, 1};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_P::depends_on<2>() {
    return {2};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_P::depends_on<3>() {
    return {2, 3};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_P::depends_on<4>() {
    return {2, 4};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_P::depends_on<5>() {
    return {5};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_P::depends_on<6>() {
    return {4, 5, 6};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_P::depends_on<7>() {
    return {6, 7};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_P::depends_on<8>() {
    return {7, 8};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_P::depends_on<9>() {
    return {9, 15};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_P::depends_on<10>() {
    return {8, 9, 10};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_P::depends_on<11>() {
    return {11};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_P::depends_on<12>() {
    return {11, 12};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_P::depends_on<13>() {
    return {10, 12, 13, 17, 19};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_P::depends_on<14>() {
    return {14, 20};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_P::depends_on<15>() {
    return {14, 15};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_P::depends_on<16>() {
    return {15, 16};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_P::depends_on<17>() {
    return {16, 17};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_P::depends_on<18>() {
    return {15, 18};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_P::depends_on<19>() {
    return {18, 19};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_P::depends_on<20>() {
    return {1, 3, 20, 21};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_P::depends_on<21>() {
    return {13, 21};
}





// Permutation red
/*
RULE_SET(GENE_PROTEIN_PRODUCTS_R, 22, "ERK", "P27361", "RSK", "Q15418", "mir-1976", 
"MLL", // 11 -> 5
"Q03164", // 12 -> 6
"HOAX9", // 13 -> 7
"mir-196b", // 14 -> 8
"TSC1/2", "P31749", 
"PDK1", // 5 -> 11
"Q15118", // 6 -> 12
"AKT",  // 7 -> 13
"RHEB", // 8 -> 14
"Q15382", "GBL", "Q9BVC4", "mTOR", "P42345", "RICTOR", "Q6R327");
*/
RULE_SET(GENE_PROTEIN_PRODUCTS_R, 22, "ERK", "P27361", "RSK", "Q15418", "mir-1976", 
"PDK1", // 11 -> 5
"Q15118", // 12 -> 6
"AKT", // 13 -> 7
"RHEB", // 14 -> 8
"TSC1/2", "P31749", 
"MLL", // 5 -> 11
"Q03164", // 6 -> 12
"HOAX9",  // 7 -> 13
"mir-196b", // 8 -> 14
"Q15382", "GBL", "Q9BVC4", "mTOR", "P42345", "RICTOR", "Q6R327");

template<> bool GENE_PROTEIN_PRODUCTS_R::rule<0>(bitset<22> x) {
    return x[0]; // ERK = ERK
}
template<> bool GENE_PROTEIN_PRODUCTS_R::rule<1>(bitset<22> x) {
    return x[0]; // P27361 = ERK
}
template<> bool GENE_PROTEIN_PRODUCTS_R::rule<2>(bitset<22> x) {
    return x[2]; // RSK = RSK
}
template<> bool GENE_PROTEIN_PRODUCTS_R::rule<3>(bitset<22> x) {
    return x[2]; // Q15418 = RSK
}
template<> bool GENE_PROTEIN_PRODUCTS_R::rule<4>(bitset<22> x) {
    return x[2]; // mir-1976 = RSK
}
template<> bool GENE_PROTEIN_PRODUCTS_R::rule<5>(bitset<22> x) {
    return x[5]; // PDK1 = PDK1
}
template<> bool GENE_PROTEIN_PRODUCTS_R::rule<6>(bitset<22> x) {
    return x[5]; // Q15118 = PDK1
}
template<> bool GENE_PROTEIN_PRODUCTS_R::rule<7>(bitset<22> x) {
    return x[6] && x[21] && x[19] && x[17]; // AKT = Q15118 and Q6R327 and P42345 and Q9BVC4
}
template<> bool GENE_PROTEIN_PRODUCTS_R::rule<8>(bitset<22> x) {
    return !x[9]; // RHEB = not TSC1/2
}
template<> bool GENE_PROTEIN_PRODUCTS_R::rule<9>(bitset<22> x) {
    return (!(x[1] && x[3])) && !x[10]; // TSC1/2 = not (P27361 and Q15418) and not P31749
}
template<> bool GENE_PROTEIN_PRODUCTS_R::rule<10>(bitset<22> x) {
    return x[7]; // P31749 = AKT
}
template<> bool GENE_PROTEIN_PRODUCTS_R::rule<11>(bitset<22> x) {
    return x[11]; // MLL = MLL
}
template<> bool GENE_PROTEIN_PRODUCTS_R::rule<12>(bitset<22> x) {
    return x[11] && !x[4]; // Q03164 = MLL and not mir-1976
}
template<> bool GENE_PROTEIN_PRODUCTS_R::rule<13>(bitset<22> x) {
    return x[12]; // HOAX9 = Q03164
}
template<> bool GENE_PROTEIN_PRODUCTS_R::rule<14>(bitset<22> x) {
    return x[13]; // mir-196b = HOAX9
}
template<> bool GENE_PROTEIN_PRODUCTS_R::rule<15>(bitset<22> x) {
    return x[8]; // Q15382 = RHEB
}
template<> bool GENE_PROTEIN_PRODUCTS_R::rule<16>(bitset<22> x) {
    return x[15]; // GBL = Q15382
}
template<> bool GENE_PROTEIN_PRODUCTS_R::rule<17>(bitset<22> x) {
    return x[16]; // Q9BVC4 = GBL
}
template<> bool GENE_PROTEIN_PRODUCTS_R::rule<18>(bitset<22> x) {
    return x[15]; // mTOR = Q15382
}
template<> bool GENE_PROTEIN_PRODUCTS_R::rule<19>(bitset<22> x) {
    return x[18]; // P42345 = mTOR
}
template<> bool GENE_PROTEIN_PRODUCTS_R::rule<20>(bitset<22> x) {
    return x[15]; // RICTOR = Q15382
}
template<> bool GENE_PROTEIN_PRODUCTS_R::rule<21>(bitset<22> x) {
    return x[20] && !x[14]; // Q6R327 = RICTOR and not mir-196bs
}


template<> vector<ind> GENE_PROTEIN_PRODUCTS_R::depends_on<0>() {
    return {0};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_R::depends_on<1>() {
    return {0, 1};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_R::depends_on<2>() {
    return {2};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_R::depends_on<3>() {
    return {2, 3};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_R::depends_on<4>() {
    return {2, 4};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_R::depends_on<5>() {
    return {5};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_R::depends_on<6>() {
    return {5, 6};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_R::depends_on<7>() {
    return {6, 7, 17, 19, 21};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_R::depends_on<8>() {
    return {8, 9};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_R::depends_on<9>() {
    return {1, 3, 9, 10};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_R::depends_on<10>() {
    return {7, 10};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_R::depends_on<11>() {
    return {11};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_R::depends_on<12>() {
    return {4, 11, 12};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_R::depends_on<13>() {
    return {12, 13};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_R::depends_on<14>() {
    return {13, 14};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_R::depends_on<15>() {
    return {8, 15};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_R::depends_on<16>() {
    return {15, 16};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_R::depends_on<17>() {
    return {16, 17};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_R::depends_on<18>() {
    return {15, 18};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_R::depends_on<19>() {
    return {18, 19};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_R::depends_on<20>() {
    return {15, 20};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_R::depends_on<21>() {
    return {14, 20, 21};
}


// Permutation yellow
/*
RULE_SET(GENE_PROTEIN_PRODUCTS_Y, 22, 
"ERK", // 8 -> 0
"P27361", // 3 -> 1
"RSK", // 6 -> 2
"Q15418", // 14 -> 3
"mir-1976", // 1 -> 4
"MLL",  // 12 -> 5
"Q03164", // 17 -> 6
"HOAX9", // 10 -> 7
"mir-196b", // 16 -> 8
"TSC1/2",  
"P31749", // 15 -> 10
"PDK1", // 19 -> 11
"Q15118", // 18 -> 12
"AKT", // 11 -> 13
"RHEB", // 2 -> 14
"Q15382", // 20 -> 15
"GBL", // 7 -> 16
"Q9BVC4", // 21 -> 17
"mTOR",  // 0 -> 18
"P42345",  // 13 -> 19
"RICTOR", // 5 -> 20
"Q6R327"); // 4 -> 21
*/
RULE_SET(GENE_PROTEIN_PRODUCTS_Y, 22, 
"mTOR", // 8 -> 0
"mir-1976", // 3 -> 1
"RHEB", // 6 -> 2
"P27361", // 14 -> 3
"Q6R327", // 1 -> 4
"RICTOR",  // 12 -> 5
"RSK", // 17 -> 6
"GBL", // 10 -> 7
"ERK", // 16 -> 8
"TSC1/2",  
"HOAX9", // 15 -> 10
"AKT", // 19 -> 11
"MLL", // 18 -> 12
"P42345", // 11 -> 13
"Q15418", // 2 -> 14
"P31749", // 20 -> 15
"mir-196b", // 7 -> 16
"Q03164", // 21 -> 17
"Q15118",  // 0 -> 18
"PDK1",  // 13 -> 19
"Q15382", // 5 -> 20
"Q9BVC4"); // 4 -> 21


template<> bool GENE_PROTEIN_PRODUCTS_Y::rule<0>(bitset<22> x) {
    return x[20]; // mTOR = Q15382
}
template<> bool GENE_PROTEIN_PRODUCTS_Y::rule<1>(bitset<22> x) {
    return x[6]; // mir-1976 = RSK
}
template<> bool GENE_PROTEIN_PRODUCTS_Y::rule<2>(bitset<22> x) {
    return !x[9]; // RHEB = not TSC1/2
}
template<> bool GENE_PROTEIN_PRODUCTS_Y::rule<3>(bitset<22> x) {
    return x[8]; // P27361 = ERK
}
template<> bool GENE_PROTEIN_PRODUCTS_Y::rule<4>(bitset<22> x) {
    return x[5] && !x[16]; // Q6R327 = RICTOR and not mir-196bs
}
template<> bool GENE_PROTEIN_PRODUCTS_Y::rule<5>(bitset<22> x) {
    return x[20]; // RICTOR = Q15382
}
template<> bool GENE_PROTEIN_PRODUCTS_Y::rule<6>(bitset<22> x) {
    return x[6]; // RSK = RSK
}
template<> bool GENE_PROTEIN_PRODUCTS_Y::rule<7>(bitset<22> x) {
    return x[20]; // GBL = Q15382
}
template<> bool GENE_PROTEIN_PRODUCTS_Y::rule<8>(bitset<22> x) {
    return x[8]; // ERK = ERK
}
template<> bool GENE_PROTEIN_PRODUCTS_Y::rule<9>(bitset<22> x) {
    return (!(x[3] && x[14])) && !x[15]; // TSC1/2 = not (P27361 and Q15418) and not P31749
}
template<> bool GENE_PROTEIN_PRODUCTS_Y::rule<10>(bitset<22> x) {
    return x[17]; // HOAX9 = Q03164
}
template<> bool GENE_PROTEIN_PRODUCTS_Y::rule<11>(bitset<22> x) {
    return x[18] && x[4] && x[13] && x[21]; // AKT = Q15118 and Q6R327 and P42345 and Q9BVC4
}
template<> bool GENE_PROTEIN_PRODUCTS_Y::rule<12>(bitset<22> x) {
    return x[12]; // MLL = MLL
}
template<> bool GENE_PROTEIN_PRODUCTS_Y::rule<13>(bitset<22> x) {
    return x[0]; // P42345 = mTOR
}
template<> bool GENE_PROTEIN_PRODUCTS_Y::rule<14>(bitset<22> x) {
    return x[6]; // Q15418 = RSK
}
template<> bool GENE_PROTEIN_PRODUCTS_Y::rule<15>(bitset<22> x) {
    return x[11]; // P31749 = AKT
}
template<> bool GENE_PROTEIN_PRODUCTS_Y::rule<16>(bitset<22> x) {
    return x[10]; // mir-196b = HOAX9
}
template<> bool GENE_PROTEIN_PRODUCTS_Y::rule<17>(bitset<22> x) {
    return x[12] && !x[1]; // Q03164 = MLL and not mir-1976
}
template<> bool GENE_PROTEIN_PRODUCTS_Y::rule<18>(bitset<22> x) {
    return x[19]; // Q15118 = PDK1
}
template<> bool GENE_PROTEIN_PRODUCTS_Y::rule<19>(bitset<22> x) {
    return x[19]; // PDK1 = PDK1
}
template<> bool GENE_PROTEIN_PRODUCTS_Y::rule<20>(bitset<22> x) {
    return x[2]; // Q15382 = RHEB
}
template<> bool GENE_PROTEIN_PRODUCTS_Y::rule<21>(bitset<22> x) {
    return x[7]; // Q9BVC4 = GBL
}


template<> vector<ind> GENE_PROTEIN_PRODUCTS_Y::depends_on<0>() {
    return {0, 20};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_Y::depends_on<1>() {
    return {1, 6};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_Y::depends_on<2>() {
    return {2, 9};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_Y::depends_on<3>() {
    return {3, 8};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_Y::depends_on<4>() {
    return {4, 5, 16};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_Y::depends_on<5>() {
    return {5, 20};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_Y::depends_on<6>() {
    return {6};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_Y::depends_on<7>() {
    return {7, 20};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_Y::depends_on<8>() {
    return {8};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_Y::depends_on<9>() {
    return {3, 9, 14, 15};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_Y::depends_on<10>() {
    return {10, 17};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_Y::depends_on<11>() {
    return {4, 11, 13, 18, 21};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_Y::depends_on<12>() {
    return {12};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_Y::depends_on<13>() {
    return {0, 13};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_Y::depends_on<14>() {
    return {6, 14};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_Y::depends_on<15>() {
    return {11, 15};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_Y::depends_on<16>() {
    return {10, 16};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_Y::depends_on<17>() {
    return {1, 12, 17};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_Y::depends_on<18>() {
    return {18, 19};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_Y::depends_on<19>() {
    return {19};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_Y::depends_on<20>() {
    return {2, 20};
}
template<> vector<ind> GENE_PROTEIN_PRODUCTS_Y::depends_on<21>() {
    return {7, 21};
}
