from sys import argv, exit
from netCDF4 import *
from numpy import *
import itertools as it

def normalize(_U, _V):
    r = _U.shape[0]

    S = zeros((r, r))
    U = _U.copy()
    V = _V.copy()
    for k in range(r):
        norm_U = linalg.norm(U[k, :], 2)
        norm_V = linalg.norm(V[k, :], 2)
        U[k, :] /= norm_U
        V[k, :] /= norm_V
        S[k, k] = norm_U*norm_V
    return U, S, V
        
def write_nc(fn, U, V, names):
    rank = U.shape[0]
    len1 = U.shape[1]
    len2 = V.shape[1]
    
    U, S, V = normalize(U, V)

    ncfile = Dataset(fn,mode='w') 

    d = ncfile.createDimension('d', len(names))
    r = ncfile.createDimension('r', rank)  
    n1 = ncfile.createDimension('n1', len1)
    n2 = ncfile.createDimension('n2', len2)  


    for dim in ncfile.dimensions.items():
        print(dim)

    file_U = ncfile.createVariable('U', float64, ('r', 'n1'))
    file_V = ncfile.createVariable('V', float64, ('r', 'n2'))
    file_S = ncfile.createVariable('S', float64, ('r', 'r'))
    spec = ncfile.createVariable('names', str, ('d'))

    file_U[:,:] = U
    file_V[:,:] = V
    file_S[:,:] = S
    for i in range(len(names)):
            spec[i] = names[i]

    ncfile.close()
    
'''    
len1 = 2**17
len2 = 2**17

# ------ first setup: order a, HMGB1 1 -------
names = ["HMGB1","TLR24","RAGE","MYD88","RAS","RAC1","IRAKs","RAF","MEK","PI3K","ERK","AP1","TAB1","PIP3","AKT","Myc","INK4a",
"IKK","CyclinD","PTEN","MDM2","A20","E2F","IkB","RB","P53","NFkB","ARF","P21","BAX","BclXL","CyclinE","Apoptosis","Proliferate"]

U = zeros((1, len1))

norm_u = 1/(2**(17-3))
print(norm_u)
index = 0
for i in it.product(range(2), repeat=17):
    #print(i)
    U[0, index] = norm_u if i[0]==1 else 0.0 # HMGB1 ON
    U[0, index] = norm_u if i[4]==1 else 0.0 # RAS ON
    U[0, index] = norm_u if i[16]==0 else 0.0 # INK4a  OFF  
    index += 1
print(U)
#exit()

V = zeros((1, len2))

norm_v = 1/(2**(17-1))
index = 0
for i in it.product(range(2), repeat=17):
    V[0, index] = norm_v if i[8]==0 else 0.0 # P53 OFF
    index += 1
#print(V)
write_nc('init_pancreatic1a_spec1.nc', U, V, names)
'''
    
'''    
#simple uniform
len1 = 2**2
len2 = 2**2
names = ['A1', 'B2', 'C3', 'D4']

# uniform
U = zeros((1, len1))
U[0,:] = 1.0/float(len1)

V = zeros((1, len2))
V[0,:] = 1.0/float(len2)

write_nc('new1.nc', U, V, names)
'''

'''
# simple
# A1=1, D4=0 all other uniform
len1 = 2**2
len2 = 2**2
names = ['A1', 'B2', 'C3', 'D4']
U = zeros((1, len1))

index = 0
print([list(reversed(x)) for x in list(it.product(range(2), repeat=2))])
for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=2))]:
    print(i)
    U[0, index] = 1.0 if i[0]==1 and i[1]==0 else 0.0
    index += 1

V = zeros((1, len2))
index = 0
for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=2))]:
    V[0, index] = 0.5 if i[1]==0 else 0.0
    index += 1

write_nc('new2.nc', U, V, names)
'''
    
'''   
# ------ first setup: order a, HMGB1 1 -------

len1 = 2**17
len2 = 2**17

names = ["HMGB1","TLR24","RAGE","MYD88","RAS","RAC1","IRAKs","RAF","MEK","PI3K","ERK","AP1","TAB1","PIP3","AKT","Myc","INK4a",
"IKK","CyclinD","PTEN","MDM2","A20","E2F","IkB","RB","P53","NFkB","ARF","P21","BAX","BclXL","CyclinE","Apoptosis","Proliferate"]

U = zeros((1, len1))

norm_u = 1.0/(2.0**(17-3))
print(norm_u)
index = 0

for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=17))]:
    U[0, index] = norm_u if i[0]==1 and i[4]==1 and i[16]==0 else 0.0
    index += 1
print(U)
#exit()

V = zeros((1, len2))

norm_v = 1.0/(2.0**(17-1))
index = 0
for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=17))]:
    V[0, index] = norm_v if i[8]==0 else 0.0 # P53 OFF
    index += 1
#print(V)
write_nc('init_pancreatic1a_spec1.nc', U, V, names)
'''

'''
# ------ second setup: order a, HMGB1 1, apoptosis 0, proliferate 0, cycline 0 -------
len1 = 2**17
len2 = 2**17

names = ["HMGB1","TLR24","RAGE","MYD88","RAS","RAC1","IRAKs","RAF","MEK","PI3K","ERK","AP1","TAB1","PIP3","AKT","Myc","INK4a",  #partition U
"IKK","CyclinD","PTEN","MDM2","A20","E2F","IkB","RB","P53","NFkB","ARF","P21","BAX","BclXL","CyclinE","Apoptosis","Proliferate"]  #partition V

U = zeros((1, len1))

norm_u = 1.0/(2.0**(17-3))
print(norm_u)
index = 0

for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=17))]:
                            #HMGB1         RAS         INK4a
    U[0, index] = norm_u if i[0]==1 and i[4]==1 and i[16]==0 else 0.0
    index += 1
print(U)
#exit()

V = zeros((1, len2))

norm_v = 1.0/(2.0**(17-4))
index = 0
for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=17))]:
                             #P53       CyclinE       apoptosis    proliferate
    V[0, index] = norm_v if i[8]==0 and i[14]==0 and i[15]==0 and i[16]==0 else 0.0 # P53 OFF
    index += 1
#print(V)
write_nc('init_pancreatic1a_spec_hmgb1_RAS1_INK4a0_P530_cycline0_apop0_prolif0.nc', U, V, names)
'''

'''
# ------ apoptosis, TNF=0, GF=0, DNAdam = 0 -------
len1 = 2**20
len2 = 2**21

names = ["TNF","GF","TNFR1","TRADD","TRAF","FADD","RIP","cIAP","Cas8","TNFR2","TRAF2","NIK","IKK","IkB","NFkB","A20","MEKK1","JNKK","JNK","Cas7",   # partition U
"Cas12","Cas9","APC","Cas3","Cas3prev","Cas6","BID","BclX","BAD","p53","Apaf1","PTEN","Mito","IAP","GFR","PI3K","PIP2","PIP3","Akt","Mdm2","DNAdam"]  # partition V

U = zeros((1, len1))

norm_u = 1.0/(2.0**(20-2))
print(norm_u)
index = 0

for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=20))]:
                            #TNF         GF        
    U[0, index] = norm_u if i[0]==0 and i[1]==0 else 0.0
    index += 1
print(U)
#exit()

V = zeros((1, len2))

norm_v = 1.0/(2.0**(21-1))
index = 0
for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=21))]:
                          
                             #DNAdam
    V[0, index] = norm_v if i[20]==0 else 0.0
    index += 1
#print(V)
write_nc('init_apop1_spec_TNF0_GF0_DNAdam0.nc', U, V, names)
'''


'''
# ------ apoptosis, TNF=0, GF=1, DNAdam 0-------
len1 = 2**20
len2 = 2**21

names = ["TNF","GF","TNFR1","TRADD","TRAF","FADD","RIP","cIAP","Cas8","TNFR2","TRAF2","NIK","IKK","IkB","NFkB","A20","MEKK1","JNKK","JNK","Cas7",   # partition U
"Cas12","Cas9","APC","Cas3","Cas3prev","Cas6","BID","BclX","BAD","p53","Apaf1","PTEN","Mito","IAP","GFR","PI3K","PIP2","PIP3","Akt","Mdm2","DNAdam"]  # partition V

U = zeros((1, len1))

norm_u = 1.0/(2.0**(20-2))
print(norm_u)
index = 0

for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=20))]:
                            #TNF         GF        
    U[0, index] = norm_u if i[0]==0 and i[1]==1 else 0.0
    index += 1
print(U)
#exit()

V = zeros((1, len2))

norm_v = 1.0/(2.0**(21-1))
index = 0
for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=21))]:
                          
                             #DNAdam
    V[0, index] = norm_v if i[20]==0 else 0.0
    index += 1
#print(V)
write_nc('init_apop1_spec_TNF0_GF1_DNAdam0.nc', U, V, names)
'''

'''
# ------ apoptosis, TNF=1, GF=0, DNAdam0 -------
len1 = 2**20
len2 = 2**21

names = ["TNF","GF","TNFR1","TRADD","TRAF","FADD","RIP","cIAP","Cas8","TNFR2","TRAF2","NIK","IKK","IkB","NFkB","A20","MEKK1","JNKK","JNK","Cas7",   # partition U
"Cas12","Cas9","APC","Cas3","Cas3prev","Cas6","BID","BclX","BAD","p53","Apaf1","PTEN","Mito","IAP","GFR","PI3K","PIP2","PIP3","Akt","Mdm2","DNAdam"]  # partition V

U = zeros((1, len1))

norm_u = 1.0/(2.0**(20-2))
print(norm_u)
index = 0

for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=20))]:
                            #TNF         GF        
    U[0, index] = norm_u if i[0]==1 and i[1]==0 else 0.0
    index += 1
print(U)
#exit()

V = zeros((1, len2))

norm_v = 1.0/(2.0**(21-1))
index = 0
for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=21))]:
                          
                             #DNAdam
    V[0, index] = norm_v if i[20]==0 else 0.0
    index += 1
#print(V)
write_nc('init_apop1_spec_TNF1_GF0_DNAdam0.nc', U, V, names)
'''

'''
# ------ apoptosis, TNF=1, GF=1, DNAdam0-------
len1 = 2**20
len2 = 2**21

names = ["TNF","GF","TNFR1","TRADD","TRAF","FADD","RIP","cIAP","Cas8","TNFR2","TRAF2","NIK","IKK","IkB","NFkB","A20","MEKK1","JNKK","JNK","Cas7",   # partition U
"Cas12","Cas9","APC","Cas3","Cas3prev","Cas6","BID","BclX","BAD","p53","Apaf1","PTEN","Mito","IAP","GFR","PI3K","PIP2","PIP3","Akt","Mdm2","DNAdam"]  # partition V

U = zeros((1, len1))

norm_u = 1.0/(2.0**(20-2))
print(norm_u)
index = 0

for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=20))]:
                            #TNF         GF        
    U[0, index] = norm_u if i[0]==1 and i[1]==1 else 0.0
    index += 1
print(U)
#exit()

V = zeros((1, len2))

norm_v = 1.0/(2.0**(21-1))
index = 0
for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=21))]:
                          
                             #DNAdam
    V[0, index] = norm_v if i[20]==0 else 0.0
    index += 1
#print(V)
write_nc('init_apop1_spec_TNF1_GF1_DNAdam0.nc', U, V, names)
'''



'''
len1 = 2**2
len2 = 2**2
names = ['A1', 'B2', 'C3', 'D4']

# uniform
U = zeros((1, len1))
U[0,:] = 1.0/float(len1)

V = zeros((1, len2))
V[0,:] = 1.0/float(len2)

write_nc('new1.nc', U, V, names)


# A1=1, D4=0 all other uniform
U = zeros((1, len1))

index = 0
for i in it.product(range(2), repeat=2):
    U[0, index] = 0.5 if i[0]==1 else 0.0
    index += 1

V = zeros((1, len2))
index = 0
for i in it.product(range(2), repeat=2):
    V[0, index] = 0.5 if i[1]==0 else 0.0
    index += 1

write_nc('new2.nc', U, V, names)
'''

'''    
len1 = 2**17
len2 = 2**17

# ------ pancreatic new partition 20-------
names = ["HMGB1","TLR24","RAGE","MYD88","RAS","RAC1","RAF","MEK","PI3K","PIP3","AKT","PTEN","MDM2","P53","BAX","BclXL","Apoptosis",
"Proliferate","IRAKs","ERK","AP1","TAB1","Myc","INK4a","IKK","CyclinD","A20","E2F","IkB","RB","NFkB","ARF","P21","CyclinE"]

U = zeros((1, len1))

norm_u = 1/(2**(17))
print(norm_u)
index = 0
for i in it.product(range(2), repeat=17):
    #print(i)
    U[0, index] = norm_u #if i[0]==1 else 0.0 # HMGB1 ON
    #U[0, index] = norm_u if i[4]==1 else 0.0 # RAS ON
    #U[0, index] = norm_u if i[13]==0 else 0.0 # P53 OFF   
    index += 1
print(U)
#exit()

V = zeros((1, len2))

norm_v = 1/(2**(17))
index = 0
for i in it.product(range(2), repeat=17):
    V[0, index] = norm_v #if i[6]==0 else 0.0 # INK4a  OFF  
    index += 1
print(V)
write_nc('init_pancreatic20_uni.nc', U, V, names)
'''

'''    
len1 = 2**17
len2 = 2**17

# ------ pancreatic new partition 17-------
names = ["HMGB1","TLR24","RAGE","MYD88","RAS","RAC1","RAF","PI3K","PIP3","AKT","PTEN","MDM2","P53","ARF","BAX","BclXL","Apoptosis",
"Proliferate","IRAKs","MEK","ERK","AP1","TAB1","Myc","INK4a","IKK","CyclinD","A20","E2F","IkB","RB","NFkB","P21","CyclinE"]

U = zeros((1, len1))

norm_u = 1/(2**(17))
print(norm_u)
index = 0
for i in it.product(range(2), repeat=17):
    #print(i)
    U[0, index] = norm_u #if i[0]==1 else 0.0 # HMGB1 ON
    #U[0, index] = norm_u if i[4]==1 else 0.0 # RAS ON
    #U[0, index] = norm_u if i[13]==0 else 0.0 # P53 OFF   
    index += 1
print(U)
#exit()

V = zeros((1, len2))

norm_v = 1/(2**(17))
index = 0
for i in it.product(range(2), repeat=17):
    V[0, index] = norm_v #if i[6]==0 else 0.0 # INK4a  OFF  
    index += 1
print(V)
write_nc('init_pancreatic17_uni.nc', U, V, names)
'''

'''    
len1 = 2**17
len2 = 2**17

# ------ pancreatic new partition 13-------
names = ["HMGB1","TLR24","RAGE","MYD88","RAS","RAC1","IRAKs","RAF","MEK","PI3K","ERK","TAB1","PIP3","AKT","IKK","A20","IkB",
"Apoptosis","Proliferate","AP1","Myc","INK4a","CyclinD","PTEN","MDM2","E2F","RB","P53","NFkB","ARF","P21","BAX","BclXL","CyclinE"]

U = zeros((1, len1))

norm_u = 1/(2**(17))
print(norm_u)
index = 0
for i in it.product(range(2), repeat=17):
    #print(i)
    U[0, index] = norm_u #if i[0]==1 else 0.0 # HMGB1 ON
    #U[0, index] = norm_u if i[4]==1 else 0.0 # RAS ON
    #U[0, index] = norm_u if i[13]==0 else 0.0 # P53 OFF   
    index += 1
print(U)
#exit()

V = zeros((1, len2))

norm_v = 1/(2**(17))
index = 0
for i in it.product(range(2), repeat=17):
    V[0, index] = norm_v #if i[6]==0 else 0.0 # INK4a  OFF  
    index += 1
print(V)
write_nc('init_pancreatic13_uni.nc', U, V, names)
'''

'''    
len1 = 2**17
len2 = 2**17

# ------ pancreatic new partition 9-------
names = ["HMGB1","TLR24","RAGE","MYD88","RAS","RAC1","IRAKs","RAF","MEK","PI3K","ERK","TAB1","PIP3","AKT","IKK","PTEN","A20",
"Apoptosis","Proliferate","AP1","Myc","INK4a","CyclinD","MDM2","E2F","IkB","RB","P53","NFkB","ARF","P21","BAX","BclXL","CyclinE"]

U = zeros((1, len1))

norm_u = 1/(2**(17))
print(norm_u)
index = 0
for i in it.product(range(2), repeat=17):
    #print(i)
    U[0, index] = norm_u #if i[0]==1 else 0.0 # HMGB1 ON
    #U[0, index] = norm_u if i[4]==1 else 0.0 # RAS ON
    #U[0, index] = norm_u if i[13]==0 else 0.0 # P53 OFF   
    index += 1
print(U)
#exit()

V = zeros((1, len2))

norm_v = 1/(2**(17))
index = 0
for i in it.product(range(2), repeat=17):
    V[0, index] = norm_v #if i[6]==0 else 0.0 # INK4a  OFF  
    index += 1
print(V)
write_nc('init_pancreatic9_uni.nc', U, V, names)
'''

'''    
len1 = 2**17
len2 = 2**17

# ------ pancreatic new partition 8_1-------
names = ["HMGB1","TLR24","RAGE","MYD88","RAS","RAC1","IRAKs","RAF","MEK","PI3K","ERK","AP1","TAB1","PIP3","AKT","IKK","IkB",
"Apoptosis","Proliferate","Myc","INK4a","CyclinD","PTEN","MDM2","A20","E2F","RB","P53","NFkB","ARF","P21","BAX","BclXL","CyclinE"]

U = zeros((1, len1))

norm_u = 1/(2**(17))
print(norm_u)
index = 0
for i in it.product(range(2), repeat=17):
    #print(i)
    U[0, index] = norm_u #if i[0]==1 else 0.0 # HMGB1 ON
    #U[0, index] = norm_u if i[4]==1 else 0.0 # RAS ON
    #U[0, index] = norm_u if i[13]==0 else 0.0 # P53 OFF   
    index += 1
print(U)
#exit()

V = zeros((1, len2))

norm_v = 1/(2**(17))
index = 0
for i in it.product(range(2), repeat=17):
    V[0, index] = norm_v #if i[6]==0 else 0.0 # INK4a  OFF  
    index += 1
print(V)
write_nc('init_pancreatic81_uni.nc', U, V, names)
'''

'''    
len1 = 2**17
len2 = 2**17

# ------ pancreatic new partition 8_2-------
names = ["HMGB1","TLR24","RAGE","MYD88","RAS","RAC1","IRAKs","RAF","MEK","PI3K","ERK","AP1","TAB1","PIP3","AKT","IKK","A20",
"Apoptosis","Proliferate","Myc","INK4a","CyclinD","PTEN","MDM2","E2F","IkB","RB","P53","NFkB","ARF","P21","BAX","BclXL","CyclinE"]

U = zeros((1, len1))

norm_u = 1/(2**(17))
print(norm_u)
index = 0
for i in it.product(range(2), repeat=17):
    #print(i)
    U[0, index] = norm_u #if i[0]==1 else 0.0 # HMGB1 ON
    #U[0, index] = norm_u if i[4]==1 else 0.0 # RAS ON
    #U[0, index] = norm_u if i[13]==0 else 0.0 # P53 OFF   
    index += 1
print(U)
#exit()

V = zeros((1, len2))

norm_v = 1/(2**(17))
index = 0
for i in it.product(range(2), repeat=17):
    V[0, index] = norm_v #if i[6]==0 else 0.0 # INK4a  OFF  
    index += 1
print(V)
write_nc('init_pancreatic82_uni.nc', U, V, names)
'''

'''
len1 = 2**17
len2 = 2**17

# ------ pancreatic new partition 5-------
names = ["HMGB1","TLR24","RAGE","MYD88","RAS","RAC1","IRAKs","RAF","MEK","PI3K","ERK","TAB1","PIP3","AKT","IKK","PTEN","IkB",
"Apoptosis","Proliferate","AP1","Myc","INK4a","CyclinD","MDM2","A20","E2F","RB","P53","NFkB","ARF","P21","BAX","BclXL","CyclinE"]

U = zeros((1, len1))

norm_u = 1/(2**(17))
print(norm_u)
index = 0
for i in it.product(range(2), repeat=17):
    #print(i)
    U[0, index] = norm_u #if i[0]==1 else 0.0 # HMGB1 ON
    #U[0, index] = norm_u if i[4]==1 else 0.0 # RAS ON
    #U[0, index] = norm_u if i[13]==0 else 0.0 # P53 OFF   
    index += 1
print(U)
#exit()

V = zeros((1, len2))

norm_v = 1/(2**(17))
index = 0
for i in it.product(range(2), repeat=17):
    V[0, index] = norm_v #if i[6]==0 else 0.0 # INK4a  OFF  
    index += 1
print(V)
write_nc('init_pancreatic5_uni.nc', U, V, names)
'''


'''
len1 = 2**17
len2 = 2**17

# ------ pancreatic new partition 3-------
names = ["HMGB1","TLR24","RAGE","MYD88","RAS","RAC1","RAF","MEK","PI3K","PIP3","AKT","MDM2","E2F","RB","ARF","CyclinE","Proliferate",
"Apoptosis","IRAKs","ERK","AP1","TAB1","Myc","INK4a","IKK","CyclinD","PTEN","A20","IkB","P53","NFkB","P21","BAX","BclXL"]

U = zeros((1, len1))

norm_u = 1/(2**(17))
print(norm_u)
index = 0
for i in it.product(range(2), repeat=17):
    #print(i)
    U[0, index] = norm_u #if i[0]==1 else 0.0 # HMGB1 ON
    #U[0, index] = norm_u if i[4]==1 else 0.0 # RAS ON
    #U[0, index] = norm_u if i[13]==0 else 0.0 # P53 OFF   
    index += 1
print(U)
#exit()

V = zeros((1, len2))

norm_v = 1/(2**(17))
index = 0
for i in it.product(range(2), repeat=17):
    V[0, index] = norm_v #if i[6]==0 else 0.0 # INK4a  OFF  
    index += 1
print(V)
write_nc('init_pancreatic3_uni.nc', U, V, names)
'''


'''
len1 = 2**17
len2 = 2**17

# ------ pancreatic orig setup, uniform input------
names = ["HMGB1","TLR24","RAGE","MYD88","RAS","RAC1","IRAKs","RAF","MEK","PI3K","ERK","AP1","TAB1","PIP3","AKT","Myc","INK4a",
"IKK","CyclinD","PTEN","MDM2","A20","E2F","IkB","RB","P53","NFkB","ARF","P21","BAX","BclXL","CyclinE","Apoptosis","Proliferate"]

U = zeros((1, len1))

norm_u = 1/(2**(17))
print(norm_u)
index = 0
for i in it.product(range(2), repeat=17):
    #print(i)
    U[0, index] = norm_u #if i[0]==1 else 0.0 # HMGB1 ON
    #U[0, index] = norm_u if i[4]==1 else 0.0 # RAS ON
    #U[0, index] = norm_u if i[13]==0 else 0.0 # P53 OFF   
    index += 1
print(U)
#exit()

V = zeros((1, len2))

norm_v = 1/(2**(17))
index = 0
for i in it.product(range(2), repeat=17):
    V[0, index] = norm_v #if i[6]==0 else 0.0 # INK4a  OFF  
    index += 1
print(V)
write_nc('init_pancreatic_orig_uni.nc', U, V, names)
'''


#'''
# ------ apoptosis, TNF=0, GF=0, DNAdam = 0 -------
len1 = 2**20
len2 = 2**21

names = ["GF","NIK","IKK","IkB","NFkB","A20","MEKK1","JNKK","JNK","BID","BclX","BAD","p53","PTEN","GFR","PI3K","PIP2","PIP3","Akt","Mdm2",   # partition U
"TNF","TNFR1","TRADD","TRAF","FADD","RIP","cIAP","Cas8","TNFR2","TRAF2","Cas7","Cas12","Cas9","APC","Cas3","Cas3prev","Cas6","Apaf1","Mito","IAP","DNAdam"]  # partition V

U = zeros((1, len1))

norm_u = 1.0/(2.0**(20-1))
print(norm_u)
index = 0

for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=20))]:
                            #GF              
    U[0, index] = norm_u if i[0]==0 else 0.0
    index += 1
print(U)
#exit()

V = zeros((1, len2))

norm_v = 1.0/(2.0**(21-2))
index = 0
for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=21))]:
                          
                             #TNF        DNAdam
    V[0, index] = norm_v if i[0]==0 and i[-1]==0 else 0.0
    index += 1
#print(V)
write_nc('init_apop51_spec_TNF0_GF0_DNAdam0.nc', U, V, names)
#'''




'''
# ------ apoptosis, TNF=0, GF=0, DNAdam = 0 -------
len1 = 2**20
len2 = 2**21

names = [ "Cas8","JNKK","JNK","Cas7","Cas12","Cas9","APC","Cas3","Cas3prev","Cas6","BID","BclX","BAD","p53","Apaf1","Mito","IAP","Akt","Mdm2","DNAdam",  # partition U
"TNF","GF","TNFR1","TRADD","TRAF","FADD","RIP","cIAP","TNFR2","TRAF2","NIK","IKK","IkB","NFkB","A20","MEKK1","PTEN","GFR","PI3K","PIP2","PIP3"]  # partition V

U = zeros((1, len1))

norm_u = 1.0/(2.0**(20-1))
print(norm_u)
index = 0

for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=20))]:
                            #DNAdam             
    U[0, index] = norm_u if i[-1]==0  else 0.0
    index += 1
print(U)
#exit()

V = zeros((1, len2))

norm_v = 1.0/(2.0**(21-2))
index = 0
for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=21))]:
                          
                             #TNF        GF
    V[0, index] = norm_v if i[0]==0 and i[1]==0 else 0.0
    index += 1
#print(V)
write_nc('init_apop52_spec_TNF0_GF0_DNAdam0.nc', U, V, names)
'''

'''
# ------ apoptosis, TNF=0, GF=0, DNAdam = 0 -------
len1 = 2**20
len2 = 2**21

names = [ "Cas12","Cas9","APC","Cas3","Cas3prev","Cas6","BID","BclX","BAD","p53","Apaf1","PTEN","Mito","IAP","PI3K","PIP2","PIP3","Akt","Mdm2","DNAdam",  # partition U
"TNF","GF","TNFR1","TRADD","TRAF","FADD","RIP","cIAP","Cas8","TNFR2","TRAF2","NIK","IKK","IkB","NFkB","A20","MEKK1","JNKK","JNK","Cas7","GFR"]  # partition V

U = zeros((1, len1))

norm_u = 1.0/(2.0**(20-1))
print(norm_u)
index = 0

for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=20))]:
                            #DNAdam             
    U[0, index] = norm_u if i[-1]==0  else 0.0
    index += 1
print(U)
#exit()

V = zeros((1, len2))

norm_v = 1.0/(2.0**(21-2))
index = 0
for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=21))]:
                          
                             #TNF        GF
    V[0, index] = norm_v if i[0]==0 and i[1]==0 else 0.0
    index += 1
#print(V)
write_nc('init_apop1GF_spec_TNF0_GF0_DNAdam0.nc', U, V, names)
'''

'''
# ------ apoptosis, TNF=0, GF=0, DNAdam = 0 -------
len1 = 2**20
len2 = 2**21

names = [ "TNF","GF","TNFR1","TRADD","TRAF","FADD","RIP","cIAP","Cas8","TNFR2","TRAF2","NIK","IKK","IkB","NFkB","A20","MEKK1","JNKK","JNK","GFR",  # partition U
"Cas12","Cas9","APC","Cas3","Cas3prev","Cas6","BID","BclX","BAD","p53","Apaf1","PTEN","Mito","IAP","PI3K","PIP2","PIP3","Akt","Mdm2","DNAdam","Cas7"]  # partition V


U = zeros((1, len1))

norm_u = 1.0/(2.0**(20-2))
print(norm_u)
index = 0

for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=20))]:
                            #TNF         GF                
    U[0, index] = norm_u if i[0]==0 and i[1]==0  else 0.0
    index += 1
print(U)
#exit()

V = zeros((1, len2))

norm_v = 1.0/(2.0**(21-1))
index = 0
for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=21))]:
                          
                             #DNAdam
    V[0, index] = norm_v if i[19]==0 else 0.0
    index += 1
#print(V)
write_nc('init_apop2Cas7_spec_TNF0_GF0_DNAdam0.nc', U, V, names)
'''

'''
# ------ apoptosis, TNF=0, GF=0, DNAdam = 0 -------
len1 = 2**20
len2 = 2**21

names = ["Cas12","Cas9","APC","Cas3","Cas3prev","Cas6","BID","BclX","BAD","p53","Apaf1","PTEN","Mito","IAP","PIP2","PIP3","Akt","Mdm2","DNAdam","Cas7",   # partition U
"TNF","GF","TNFR1","TRADD","TRAF","FADD","RIP","cIAP","Cas8","TNFR2","TRAF2","NIK","IKK","IkB","NFkB","A20","MEKK1","JNKK","JNK","GFR","PI3K"]  # partition V

U = zeros((1, len1))

norm_u = 1.0/(2.0**(20-1))
print(norm_u)
index = 0

for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=20))]:
                            #DNAdam                 
    U[0, index] = norm_u if i[18]==0 else 0.0
    index += 1
print(U)
#exit()

V = zeros((1, len2))

norm_v = 1.0/(2.0**(21-2))
index = 0
for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=21))]:
                          
                             #TNF         GF
    V[0, index] = norm_v if i[0]==0 and i[1]==0 else 0.0
    index += 1
#print(V)
write_nc('init_apop3PI3K_spec_TNF0_GF0_DNAdam0.nc', U, V, names)
'''

'''
# ------ apoptosis, TNF=0, GF=0, DNAdam = 0 -------
len1 = 2**20
len2 = 2**21

names = ["TNF","GF","TNFR1","TRADD","TRAF","FADD","RIP","cIAP","TNFR2","TRAF2","NIK","IKK","IkB","NFkB","A20","MEKK1","JNKK","JNK","GFR","PI3K",   # partition U
"Cas12","Cas9","APC","Cas3","Cas3prev","Cas6","BID","BclX","BAD","p53","Apaf1","PTEN","Mito","IAP","PIP2","PIP3","Akt","Mdm2","DNAdam","Cas7","Cas8"]  # partition V

U = zeros((1, len1))

norm_u = 1.0/(2.0**(20-2))
print(norm_u)
index = 0

for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=20))]:
                            #TNF          GF                 
    U[0, index] = norm_u if i[0]==0 and i[1]==0 else 0.0
    index += 1
print(U)
#exit()

V = zeros((1, len2))

norm_v = 1.0/(2.0**(21-1))
index = 0
for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=21))]:
                          
                             #DNAdam     
    V[0, index] = norm_v if i[18]==0 else 0.0
    index += 1
#print(V)
write_nc('init_apop4Cas8_spec_TNF0_GF0_DNAdam0.nc', U, V, names)
'''

'''
# ------ apoptosis, TNF=0, GF=0, DNAdam = 0 -------
len1 = 2**20
len2 = 2**21

names = ["Cas12","Cas9","APC","Cas3","Cas3prev","Cas6","BID","BclX","BAD","p53","Apaf1","PTEN","Mito","IAP","PIP3","Akt","Mdm2","DNAdam","Cas7","Cas8",   # partition U
"TNF","GF","TNFR1","TRADD","TRAF","FADD","RIP","cIAP","TNFR2","TRAF2","NIK","IKK","IkB","NFkB","A20","MEKK1","JNKK","JNK","GFR","PI3K","PIP2"]  # partition V
 
U = zeros((1, len1))

norm_u = 1.0/(2.0**(20-1))
print(norm_u)
index = 0

for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=20))]:
                            #DNAdam                 
    U[0, index] = norm_u if i[17]==0 else 0.0
    index += 1
print(U)
#exit()

V = zeros((1, len2))

norm_v = 1.0/(2.0**(21-2))
index = 0
for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=21))]:
                          
                             #TNF           GF     
    V[0, index] = norm_v if i[0]==0 and i[1]== 0 else 0.0
    index += 1
#print(V)
write_nc('init_apop5PIP2_spec_TNF0_GF0_DNAdam0.nc', U, V, names)
'''

'''
# ------ apoptosis, TNF=0, GF=0, DNAdam = 0 -------
len1 = 2**20
len2 = 2**21

names = ["TNF","GF","TNFR1","TRADD","TRAF","FADD","RIP","cIAP","TNFR2","TRAF2","NIK","IKK","IkB","NFkB","A20","MEKK1","JNKK","GFR","PI3K","PIP2",   # partition U
 "Cas12","Cas9","APC","Cas3","Cas3prev","Cas6","BID","BclX","BAD","p53","Apaf1","PTEN","Mito","IAP","PIP3","Akt","Mdm2","DNAdam","Cas7","Cas8","JNK"]  # partition V
 
U = zeros((1, len1))

norm_u = 1.0/(2.0**(20-2))
print(norm_u)
index = 0

for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=20))]:
                            #TNF           GF                
    U[0, index] = norm_u if i[0]==0 and i[1]==0 else 0.0
    index += 1
print(U)
#exit()

V = zeros((1, len2))

norm_v = 1.0/(2.0**(21-1))
index = 0
for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=21))]:
                          
                             #DNAdam   
    V[0, index] = norm_v if i[17]==0 else 0.0
    index += 1
#print(V)
write_nc('init_apop6JNK_spec_TNF0_GF0_DNAdam0.nc', U, V, names)
'''

'''
# ------ apoptosis, TNF=0, GF=0, DNAdam = 0 -------
len1 = 2**20
len2 = 2**21

names = ["Cas12","Cas9","APC","Cas3","Cas3prev","Cas6","BID","BclX","BAD","p53","Apaf1","PTEN","Mito","IAP","Akt","Mdm2","DNAdam","Cas7","Cas8","JNK",   # partition U
  "TNF","GF","TNFR1","TRADD","TRAF","FADD","RIP","cIAP","TNFR2","TRAF2","NIK","IKK","IkB","NFkB","A20","MEKK1","JNKK","GFR","PI3K","PIP2","PIP3"]  # partition V

U = zeros((1, len1))

norm_u = 1.0/(2.0**(20-1))
print(norm_u)
index = 0

for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=20))]:
                            #DNAdam                
    U[0, index] = norm_u if i[16]==0 else 0.0
    index += 1
print(U)
#exit()

V = zeros((1, len2))

norm_v = 1.0/(2.0**(21-2))
index = 0
for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=21))]:
                          
                             #TNF        GF   
    V[0, index] = norm_v if i[0]==0 and i[1]==0 else 0.0
    index += 1
#print(V)
write_nc('init_apop7PIP3_spec_TNF0_GF0_DNAdam0.nc', U, V, names)
'''

'''
# ------ apoptosis, TNF=0, GF=0, DNAdam = 0 -------
len1 = 2**20
len2 = 2**21

names = ["TNF","GF","TNFR1","TRADD","TRAF","FADD","RIP","cIAP","TNFR2","TRAF2","NIK","IKK","IkB","NFkB","A20","MEKK1","GFR","PI3K","PIP2","PIP3",   # partition U
  "Cas12","Cas9","APC","Cas3","Cas3prev","Cas6","BID","BclX","BAD","p53","Apaf1","PTEN","Mito","IAP","Akt","Mdm2","DNAdam","Cas7","Cas8","JNK","JNKK"]  # partition V

U = zeros((1, len1))

norm_u = 1.0/(2.0**(20-2))
print(norm_u)
index = 0

for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=20))]:
                            #TNF           GF                
    U[0, index] = norm_u if i[0]==0 and i[1]==0 else 0.0
    index += 1
print(U)
#exit()

V = zeros((1, len2))

norm_v = 1.0/(2.0**(21-1))
index = 0
for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=21))]:
                          
                             #DNAdam
    V[0, index] = norm_v if i[16]==0 else 0.0
    index += 1
#print(V)
write_nc('init_apop8JNKK_spec_TNF0_GF0_DNAdam0.nc', U, V, names)
'''

'''
# ------ apoptosis, TNF=0, GF=0, DNAdam = 0 -------
len1 = 2**20
len2 = 2**21

names = ["Cas12","Cas9","APC","Cas3","Cas3prev","Cas6","BID","BclX","BAD","p53","Apaf1","Mito","IAP","Akt","Mdm2","DNAdam","Cas7","Cas8","JNK","JNKK",   # partition U
 "TNF","GF","TNFR1","TRADD","TRAF","FADD","RIP","cIAP","TNFR2","TRAF2","NIK","IKK","IkB","NFkB","A20","MEKK1","GFR","PI3K","PIP2","PIP3","PTEN"]  # partition V

U = zeros((1, len1))

norm_u = 1.0/(2.0**(20-1))
print(norm_u)
index = 0

for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=20))]:
                            #DNAdam                
    U[0, index] = norm_u if i[15]==0 else 0.0
    index += 1
print(U)
#exit()

V = zeros((1, len2))

norm_v = 1.0/(2.0**(21-2))
index = 0
for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=21))]:
                          
                             #TNF        GF   
    V[0, index] = norm_v if i[0]==0 and i[1]==0 else 0.0
    index += 1
#print(V)
write_nc('init_apop9PTEN_spec_TNF0_GF0_DNAdam0.nc', U, V, names)
'''



'''    
len1 = 2**11
len2 = 2**11

# ------ mTor mix blue partition up -------
names = ["Q15418","RSK","mir_1976","ERK","P27361","MLL","Q03164","HOAX9","mir_196b","TSC12","P31749",
"PDK1","Q15118","AKT","P42345","Q15382","GBL","RHEB","mTOR","Q9BVC4","RICTOR","Q6R327"]


U = zeros((1, len1))

norm_u = 1/(2**(11))
print(norm_u)
index = 0
for i in it.product(range(2), repeat=11):
    #print(i)
    U[0, index] = norm_u 
    index += 1
print(U)
#exit()

V = zeros((1, len2))

norm_v = 1/(2**(17))
index = 0
for i in it.product(range(2), repeat=11):
    V[0, index] = norm_v 
    index += 1
#print(V)
write_nc('init_mtormix.nc', U, V, names)
'''

'''    
len1 = 2**11
len2 = 2**11

# ------ mTor original partition -------
names = ["ERK","P27361","RSK","Q15418","mir-1976","MLL","Q03164","HOAX9","mir-196b","TSC1/2","P31749",
"PDK1","Q15118","AKT","RHEB","Q15382","GBL","Q9BVC4","mTOR","P42345","RICTOR","Q6R327"]


U = zeros((1, len1))

norm_u = 1/(2**(11))
print(norm_u)
index = 0
for i in it.product(range(2), repeat=11):
    #print(i)
    U[0, index] = norm_u 
    index += 1
print(U)
#exit()

V = zeros((1, len2))

norm_v = 1/(2**(17))
index = 0
for i in it.product(range(2), repeat=11):
    V[0, index] = norm_v 
    index += 1
#print(V)
write_nc('init_orig.nc', U, V, names)
'''

'''
len1 = 2**11
len2 = 2**11
# ------ mTor yellow partition -------
names = ["ERK","RSK","mir_1976","MLL","HOAX9","PDK1","AKT","mTOR","P42345","RICTOR","Q6R327",
"P27361","Q15418","Q03164","mir_196b","TSC12","P31749","Q15118","RHEB","Q15382","GBL","Q9BVC4"]


U = zeros((1, len1))

norm_u = 1/(2**(11))
print(norm_u)
index = 0
for i in it.product(range(2), repeat=11):
    #print(i)
    U[0, index] = norm_u 
    index += 1
print(U)
#exit()

V = zeros((1, len2))

norm_v = 1/(2**(17))
index = 0
for i in it.product(range(2), repeat=11):
    V[0, index] = norm_v 
    index += 1
#print(V)
write_nc('init_yellow.nc', U, V, names)
'''



'''
# ------ apoptosis 41, TNF=0, GF=0, DNAdam = 0 -------
len1 = 2**20
len2 = 2**21

names = ["TNF","GF","TNFR1","TRADD","TRAF","FADD","RIP","TNFR2","TRAF2","NIK","IKK","IkB","NFkB","A20","MEKK1","PTEN","GFR","PI3K","PIP2","PIP3",
"cIAP","Cas8","JNKK","JNK","Cas7","Cas12","Cas9","APC","Cas3","Cas3prev","Cas6","BID","BclX","BAD","p53","Apaf1","Mito","IAP","Akt","Mdm2","DNAdam"]  # partition V

U = zeros((1, len1))

norm_u = 1.0/(2.0**(20-2))
print(norm_u)
index = 0

for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=20))]:
                            #TNF         GF       
    U[0, index] = norm_u if i[0]==0 and i[1]==0 else 0.0
    index += 1
print(U)
#exit()

V = zeros((1, len2))

norm_v = 1.0/(2.0**(21-1))
index = 0
for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=21))]:
                          
                             #DNAdam  
    V[0, index] = norm_v if i[-1]==0 else 0.0
    index += 1
#print(V)
write_nc('init_apop41_spec_TNF0_GF0_DNAdam0.nc', U, V, names)
'''


'''
# ------ apoptosis 42, TNF=0, GF=0, DNAdam = 0 -------
len1 = 2**20
len2 = 2**21

names = ["GF","IKK","IkB","NFkB","A20","MEKK1","JNKK","JNK","BID","BclX","BAD","p53","Apaf1","PTEN","GFR","PI3K","PIP2","PIP3","Akt","Mdm2",
"TNF","TNFR1","TRADD","TRAF","FADD","RIP","cIAP","Cas8","TNFR2","TRAF2","NIK","Cas7","Cas12","Cas9","APC","Cas3","Cas3prev","Cas6","Mito","IAP","DNAdam"]  # partition V

U = zeros((1, len1))

norm_u = 1.0/(2.0**(20-1))
print(norm_u)
index = 0

for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=20))]:
                            #GF      
    U[0, index] = norm_u if i[0]==0 else 0.0
    index += 1
print(U)
#exit()

V = zeros((1, len2))

norm_v = 1.0/(2.0**(21-2))
index = 0
for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=21))]:
                          
                             #TNF         DNAdam  
    V[0, index] = norm_v if   i[0]==0 and i[-1]==0 else 0.0
    index += 1
#print(V)
write_nc('init_apop42_spec_TNF0_GF0_DNAdam0.nc', U, V, names)
'''




'''
# ------ apoptosis 43, TNF=0, GF=0, DNAdam = 0 -------
len1 = 2**20
len2 = 2**21

names = ["GF","IKK","IkB","NFkB","A20","MEKK1","JNKK","JNK","BID","BclX","BAD","p53","PTEN","Mito","GFR","PI3K","PIP2","PIP3","Akt","Mdm2",
"TNF","TNFR1","TRADD","TRAF","FADD","RIP","cIAP","Cas8","TNFR2","TRAF2","NIK","Cas7","Cas12","Cas9","APC","Cas3","Cas3prev","Cas6","Apaf1","IAP","DNAdam"]  # partition V

U = zeros((1, len1))

norm_u = 1.0/(2.0**(20-1))
print(norm_u)
index = 0

for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=20))]:
                            #GF            
    U[0, index] = norm_u if i[0]==0 else 0.0
    index += 1
print(U)
#exit()

V = zeros((1, len2))

norm_v = 1.0/(2.0**(21-2))
index = 0
for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=21))]:
                          
                             #TNF         DNAdam  
    V[0, index] = norm_v if   i[0]==0 and i[-1]==0 else 0.0
    index += 1
#print(V)
write_nc('init_apop43_spec_TNF0_GF0_DNAdam0.nc', U, V, names)
'''


#'''
# ------ apoptosis 44, TNF=0, GF=0, DNAdam = 0 -------
len1 = 2**20
len2 = 2**21

names = ["TNF","GF","TNFR1","TRADD","TRAF","FADD","RIP","cIAP","TNFR2","TRAF2","NIK","IKK","IkB","NFkB","A20","MEKK1","GFR","PI3K","PIP2","PIP3",
"Cas8","JNKK","JNK","Cas7","Cas12","Cas9","APC","Cas3","Cas3prev","Cas6","BID","BclX","BAD","p53","Apaf1","PTEN","Mito","IAP","Akt","Mdm2","DNAdam"]  # partition V

U = zeros((1, len1))

norm_u = 1.0/(2.0**(20-2))
print(norm_u)
index = 0

for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=20))]:
                            #TNF         GF     
    U[0, index] = norm_u if i[0]==0 and i[1]==0 else 0.0
    index += 1
print(U)
#exit()

V = zeros((1, len2))

norm_v = 1.0/(2.0**(21-1))
index = 0
for i in [list(reversed(x)) for x in list(it.product(range(2), repeat=21))]:
                          
                             #DNAdam  
    V[0, index] = norm_v if   i[-1]==0 else 0.0
    index += 1
#print(V)
write_nc('init_apop44_spec_TNF0_GF0_DNAdam0.nc', U, V, names)
#'''
