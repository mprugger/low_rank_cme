from sys import argv, exit
from netCDF4 import *
from pylab import *

if len(argv)!=2:
    print('ERROR: exactly one argument, the file name is required.')
    exit(1)

fn = argv[1]

with Dataset(fn,'r') as fs:
    U = fs.variables['U'][:].transpose()
    S = fs.variables['S'][:].transpose()
    V = fs.variables['V'][:].transpose()
    names = fs.variables['names'][:].transpose()

print(names)
print(U.shape)
print(S.shape)
print(V.shape)

print(U)
print(S)
print(V)
