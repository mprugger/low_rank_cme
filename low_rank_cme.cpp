#include "low_rank_cme.hpp"
#include "netcdf.hpp"
#include "pancreaticcancer1/pancreaticcancer1a.hpp"
#include "pancreaticcancer1/pancreaticcancer1b.hpp"
#include "pancreaticcancer1/pancreaticcancer1c.hpp"
#include "pancreatic_20/pancreaticcancer20.hpp"
#include "pancreatic_20/pancreaticcancer17.hpp"
#include "pancreatic_20/pancreaticcancer13.hpp"
#include "pancreatic_20/pancreaticcancer9.hpp"
#include "pancreatic_20/pancreaticcancer8_1.hpp"
#include "pancreatic_20/pancreaticcancer8_2.hpp"
#include "pancreatic_20/pancreaticcancer5.hpp"
#include "pancreatic_20/pancreaticcancer3.hpp"
#include "apoptosis/apoptosisa.hpp"
#include "apoptosis/apoptosisb.hpp"
#include "apoptosis_generated/apoptosis41.hpp"
#include "apoptosis_generated/apoptosis42.hpp"
#include "apoptosis_generated/apoptosis43.hpp"
#include "apoptosis_generated/apoptosis44.hpp"
#include "apoptosis_generated/apoptosis51.hpp"
#include "apoptosis_generated/apoptosis52.hpp"
#include "apoptosis_generated/apoptosisa_1GFR.hpp"
#include "apoptosis_generated/apoptosisa_2Cas7.hpp"
#include "apoptosis_generated/apoptosisa_3PI3K.hpp"
#include "apoptosis_generated/apoptosisa_4Cas8.hpp"
#include "apoptosis_generated/apoptosisa_5PIP2.hpp"
#include "apoptosis_generated/apoptosisa_6JNK.hpp"
#include "apoptosis_generated/apoptosisa_7PIP3.hpp"
#include "apoptosis_generated/apoptosisa_8JNKK.hpp"
#include "mTor/MixMtor.hpp"
#include "mTor/orig.hpp"
#include "mTor/yellow.hpp"
#include "apoptosis_automatic_generator/apoptosis_worst.hpp"
#include "apoptosis_automatic_generator/apoptosis_best.hpp"
#include "apoptosis_automatic_generator/apoptosis_reasonable.hpp"

#include <string>
#include <sstream>

#include <boost/program_options.hpp>
using namespace boost::program_options;


void write_snapshot(ind i, double t, ind n_steps, ind num_snapshots, const LR& P,
        const vector<std::string> names) {
    if(i % int(ceil(n_steps/double(num_snapshots-1))) == 0 || i==n_steps) {
        std::stringstream ss;
        ss << "P-t" << t << ".nc";
        write_nc(ss.str(), P.U, P.S, P.V, names);
    }
}

int main(int argc, char* argv[]) {
    std::string arg_string="";
    for(int i=0;i<argc;i++)
        arg_string += std::string(" ") + argv[i];
    cout << arg_string << endl;

    std::string problem, initial_value, integrator;
    ind r, num_snapshots;
    double T, deltat;

    options_description desc("Allowed options");
    desc.add_options()
        ("help", "produces help message")
        ("problem",value<std::string>(&problem)->default_value("EMT"),
         "rules that specify the problem")
        ("rank,r", value<ind>(&r)->default_value(4),
         "rank used for the simulation")
        ("final_time,T", value<double>(&T)->default_value(20.0),
         "final time of the simulation")
        ("deltat", value<double>(&deltat)->default_value(0.1),
         "time step used for the simulation")
        ("num_snapshots", value<ind>(&num_snapshots)->default_value(2),
         "number of snapshots that are written to disk")
        ("initial", value<std::string>(&initial_value)->default_value("uniform"),
         "initial value to be used (either a predefined specifier or a filename)")
        ("integrator", value<std::string>(&integrator)->default_value("projector"),
         "integrator to be used: either projector, unconventional, or augmented");
        

    variables_map vm;
    store(command_line_parser(argc, argv).options(desc).run(), vm);
    notify(vm);

    if(vm.count("help")) {
        cout << desc << endl;
        return 1;
    }

    if(problem == "EMT") {
        ind len = 1<<4;

        LR P0(r, {len,len});
        MatrixXd U = MatrixXd::Ones(len, 1)/double(len);
        MatrixXd V = MatrixXd::Ones(len, 1)/double(len);

        P0.init(1, U, V);
        VectorXd P_full = P0.full();
        write<8>(P_full);

        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, EMT::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;
		if(integrator == "projector")
	                P1 = time_step<4,8,EMT>(deltat, P0);
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<4,8,EMT>(deltat, P0); 
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<4,8,EMT>(deltat, P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}	
                P0 = P1;
                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
            }
        }


        P_full = P1.full();
        P_full /= P_full.sum(); // normalize probability
        write<8>(P_full, 0.001);
    } else if(problem == "SIMPLE") {
        ind len1 = 1<<2;
        ind len2 = 1<<2;

        LR P0(r, {len1, len2});
        vector<std::string> names;
        MatrixXd U, S, V;
        read_nc(initial_value, U, S, V, names);

        if(names != SIMPLE::names()) {
            cout << "ERROR: names in input file do not match names in problem specification" << endl;
            exit(1);
        }

        P0.init(U.cols(), U*S, V);

        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, SIMPLE::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;

                auto a = now();
		if(integrator == "projector")
	                P1 = time_step<2,4,SIMPLE>(deltat, P0);
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<2,4,SIMPLE>(deltat, P0); 
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<2,4,SIMPLE>(deltat, P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}
                P0 = P1;
                cout << "time step: " << delta(a) << endl;

                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
            }
        }
        MatrixXd P_full = P1.full();
        P_full /= P_full.sum(); // normalize probability
        write<4>(P_full, 0.001);
    } else if(problem == "SIMPLE_ODD") {
        ind len1 = 1<<2;
        ind len2 = 1<<3;

        LR P0(r, {len1, len2});
        MatrixXd U = MatrixXd::Ones(len1, 1)/double(len1);
        MatrixXd V = MatrixXd::Ones(len2, 1)/double(len2);
        //MatrixXd U = MatrixXd::Zero(len1, 1)/double(len1);
        //MatrixXd V = MatrixXd::Zero(len2, 1)/double(len2);
        //U(2,0) = 1.0;
        //V(7,0) = 1.0;

        P0.init(1, U, V);

        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, SIMPLE_ODD::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;

                auto a = now();
		if(integrator == "projector")
	                P1 = time_step<2,5,SIMPLE_ODD>(deltat, P0);
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<2,5,SIMPLE_ODD>(deltat, P0);
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<2,5,SIMPLE_ODD>(deltat, P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}	                
                P0 = P1;
                cout << "time step: " << delta(a) << endl;

                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
            }
        }
        MatrixXd P_full = P1.full();
        P_full /= P_full.sum(); // normalize probability
        write<5>(P_full, 0.001);
    } else if(problem == "SIMPLE_ODD_REORDERED") {
        ind len1 = 1<<2;
        ind len2 = 1<<3;

        LR P0(r, {len1, len2});
        MatrixXd U = MatrixXd::Ones(len1, 1)/double(len1);
        MatrixXd V = MatrixXd::Ones(len2, 1)/double(len2);
        //MatrixXd U = MatrixXd::Zero(len1, 1)/double(len1);
        //MatrixXd V = MatrixXd::Zero(len2, 1)/double(len2);
        //U(3,0) = 1.0;
        //V(5,0) = 1.0;

        P0.init(1, U, V);

        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, SIMPLE_ODD_REORDERED::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;

                auto a = now();
      		if(integrator == "projector")
	                P1 = time_step<2,5,SIMPLE_ODD_REORDERED>(deltat, P0);
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<2,5,SIMPLE_ODD_REORDERED>(deltat, P0);
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<2,5,SIMPLE_ODD_REORDERED>(deltat,P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}		                
                P0 = P1;
                cout << "time step: " << delta(a) << endl;

                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
            }
        }
        MatrixXd P_full = P1.full();
        P_full /= P_full.sum(); // normalize probability
        write<5>(P_full, 0.001);
    } else if (problem == "GENE_PROTEIN_PRODUCTS") {
        ind len = 1<<11;

        // init from file
        LR P0(r, {len, len});
        vector<std::string> names;
        MatrixXd U, S, V;
        read_nc(initial_value, U, S, V, names);

        if(names != GENE_PROTEIN_PRODUCTS::names()) {
            cout << "ERROR: names in input file do not match names in problem specification" << endl;
            //cout << "input:" << endl;
            //for (int i = 0; i<GENE_PROTEIN_PRODUCTS::names().size(); ++i)
            //	std::cout << GENE_PROTEIN_PRODUCTS::names()[i] << ' ';
            exit(1);
        }
        P0.init(U.cols(), U*S, V);

        write<22>(P0, 0.001);

        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, GENE_PROTEIN_PRODUCTS::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;

                auto a = now();
      		if(integrator == "projector")                
	                P1 = time_step<11,22,GENE_PROTEIN_PRODUCTS>(deltat, P0);
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<11,22,GENE_PROTEIN_PRODUCTS>(deltat, P0);
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<11,22,GENE_PROTEIN_PRODUCTS>(deltat,P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}	                
                P0 = P1;
                cout << "time step: " << delta(a) << endl;

                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
            }
        }
        
        write<22>(P1, 0.001);
    } else if (problem == "GENE_PROTEIN_PRODUCTS_G") {
        ind len = 1<<11;

        // init from file
        LR P0(r, {len, len});
        vector<std::string> names;
        MatrixXd U, S, V;
        read_nc(initial_value, U, S, V, names);

        if(names != GENE_PROTEIN_PRODUCTS_G::names()) {
            cout << "ERROR: names in input file do not match names in problem specification" << endl;
            exit(1);
        }
        P0.init(U.cols(), U*S, V);


        write<22>(P0, 0.001);

        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, GENE_PROTEIN_PRODUCTS_G::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;

                auto a = now();
		if(integrator == "projector")                
	                P1 = time_step<11,22,GENE_PROTEIN_PRODUCTS_G>(deltat, P0);
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<11,22,GENE_PROTEIN_PRODUCTS_G>(deltat, P0);
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<11,22,GENE_PROTEIN_PRODUCTS_G>(deltat,P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}		                
                P0 = P1;
                cout << "time step: " << delta(a) << endl;

                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
            }
        }
        
        write<22>(P1, 0.001);
    } else if (problem == "GENE_PROTEIN_PRODUCTS_P") {
        ind len = 1<<11;

        // init from file
        LR P0(r, {len, len});
        vector<std::string> names;
        MatrixXd U, S, V;
        read_nc(initial_value, U, S, V, names);

        if(names != GENE_PROTEIN_PRODUCTS_P::names()) {
            cout << "ERROR: names in input file do not match names in problem specification" << endl;
            exit(1);
        }
        P0.init(U.cols(), U*S, V);


        write<22>(P0, 0.001);

        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, GENE_PROTEIN_PRODUCTS_P::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;

                auto a = now();
		if(integrator == "projector")   
                	P1 = time_step<11,22,GENE_PROTEIN_PRODUCTS_P>(deltat, P0);
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<11,22,GENE_PROTEIN_PRODUCTS_P>(deltat, P0);
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<11,22,GENE_PROTEIN_PRODUCTS_P>(deltat,P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}                	
                P0 = P1;
                cout << "time step: " << delta(a) << endl;

                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
            }
        }
        
        write<22>(P1, 0.001);        
        
    } else if (problem == "GENE_PROTEIN_PRODUCTS_R") {
        ind len = 1<<11;

        // init from file
        LR P0(r, {len, len});
        vector<std::string> names;
        MatrixXd U, S, V;
        read_nc(initial_value, U, S, V, names);

        if(names != GENE_PROTEIN_PRODUCTS_R::names()) {
            cout << "ERROR: names in input file do not match names in problem specification" << endl;
            exit(1);
        }
        P0.init(U.cols(), U*S, V);

        write<22>(P0, 0.001);

        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, GENE_PROTEIN_PRODUCTS_R::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;

                auto a = now();
		if(integrator == "projector") 
                	P1 = time_step<11,22,GENE_PROTEIN_PRODUCTS_R>(deltat, P0); 
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<11,22,GENE_PROTEIN_PRODUCTS_R>(deltat, P0);
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<11,22,GENE_PROTEIN_PRODUCTS_R>(deltat,P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}    
                P0 = P1;
                cout << "time step: " << delta(a) << endl;

                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
            }
        }
        
        write<22>(P1, 0.001);    
        
        } else if (problem == "GENE_PROTEIN_PRODUCTS_Y") {
        ind len = 1<<11;

        // init from file
        LR P0(r, {len, len});
        vector<std::string> names;
        MatrixXd U, S, V;
        read_nc(initial_value, U, S, V, names);

        if(names != GENE_PROTEIN_PRODUCTS_Y::names()) {
            cout << "ERROR: names in input file do not match names in problem specification" << endl;
            exit(1);
        }
        P0.init(U.cols(), U*S, V);

        write<22>(P0, 0.001);

        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, GENE_PROTEIN_PRODUCTS_Y::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;

                auto a = now();
                if(integrator == "projector") 
                	P1 = time_step<11,22,GENE_PROTEIN_PRODUCTS_Y>(deltat, P0);
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<11,22,GENE_PROTEIN_PRODUCTS_Y>(deltat, P0);
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<11,22,GENE_PROTEIN_PRODUCTS_Y>(deltat, P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}    
                P0 = P1;
                cout << "time step: " << delta(a) << endl;

                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
            }
        }
        
        write<22>(P1, 0.001);       
        
    
    } else if(problem == "PANCREATICCANCER1a") {
        ind len1 = 1<<17;
        ind len2 = 1<<17;

        // init from file
        LR P0(r, {len1, len2});
        vector<std::string> names;
        MatrixXd U, S, V;
        read_nc(initial_value, U, S, V, names);

        if(names != PANCREATICCANCER1a::names()) {
            cout << "ERROR: names in input file do not match names in problem specification" << endl;
            exit(1);
        }
        P0.init(U.cols(), U*S, V);
        
        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, PANCREATICCANCER1a::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;

                auto a = now();
		if(integrator == "projector") 
        	        P1 = time_step<17,34,PANCREATICCANCER1a>(deltat, P0);
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<17,34,PANCREATICCANCER1a>(deltat, P0);
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<17,34,PANCREATICCANCER1a>(deltat,P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}    
                P0 = P1;
                cout << "time step: " << delta(a) << endl;

                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
            }
        }

        write<34>(P1, 0.01); 
    } else if(problem == "PANCREATICCANCER1b") {
        ind len = 1<<17;

        // init from file
        LR P0(r, {len, len});
        vector<std::string> names;
        MatrixXd U, S, V;
        read_nc(initial_value, U, S, V, names);

        if(names != PANCREATICCANCER1b::names()) {
            cout << "ERROR: names in input file do not match names in problem specification" << endl;
            exit(1);
        }
        P0.init(U.cols(), U*S, V);


        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, PANCREATICCANCER1b::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;

                auto a = now();
		if(integrator == "projector") 
	                P1 = time_step<17,34,PANCREATICCANCER1b>(deltat, P0);
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<17,34,PANCREATICCANCER1b>(deltat, P0);
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<17,34,PANCREATICCANCER1b>(deltat,P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}    
                P0 = P1;
                cout << "time step: " << delta(a) << endl;

                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
                cout << t << "\t" << mass(P1) << endl;
            }
        }

        write<34>(P1, 0.01);
    } else if(problem == "PANCREATICCANCER1c") {
        ind len = 1<<17;

        // init from file
        LR P0(r, {len, len});
        vector<std::string> names;
        MatrixXd U, S, V;
        read_nc(initial_value, U, S, V, names);

        if(names != PANCREATICCANCER1c::names()) {
            cout << "ERROR: names in input file do not match names in problem specification" << endl;
            exit(1);
        }
        P0.init(U.cols(), U*S, V);


        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, PANCREATICCANCER1c::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;

                auto a = now();
		if(integrator == "projector")                 
               		P1 = time_step<17,34,PANCREATICCANCER1c>(deltat, P0);
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<17,34,PANCREATICCANCER1c>(deltat, P0);
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<17,34,PANCREATICCANCER1c>(deltat,P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}    
                P0 = P1;
                cout << "time step: " << delta(a) << endl;

                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
                cout << t << "\t" << mass(P1) << endl;
            }
        }

        write<34>(P1, 0.01);
    } else if(problem == "PANCREATICCANCER20") {
        ind len1 = 1<<17;
        ind len2 = 1<<17;

        // init from file
        LR P0(r, {len1, len2});
        vector<std::string> names;
        MatrixXd U, S, V;
        read_nc(initial_value, U, S, V, names);

        if(names != PANCREATICCANCER20::names()) {
            cout << "ERROR: names in input file do not match names in problem specification" << endl;
            exit(1);
        }
        P0.init(U.cols(), U*S, V);
        
        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, PANCREATICCANCER20::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;

                auto a = now();
		if(integrator == "projector") 
	                P1 = time_step<17,34,PANCREATICCANCER20>(deltat, P0);
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<17,34,PANCREATICCANCER20>(deltat, P0);
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<17,34,PANCREATICCANCER20>(deltat,P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}    
                P0 = P1;
                cout << "time step: " << delta(a) << endl;

                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
            }
        }

        write<34>(P1, 0.01); 
    } else if(problem == "PANCREATICCANCER17") {
        ind len1 = 1<<17;
        ind len2 = 1<<17;

        // init from file
        LR P0(r, {len1, len2});
        vector<std::string> names;
        MatrixXd U, S, V;
        read_nc(initial_value, U, S, V, names);

        if(names != PANCREATICCANCER17::names()) {
            cout << "ERROR: names in input file do not match names in problem specification" << endl;
            exit(1);
        }
        P0.init(U.cols(), U*S, V);
        
        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, PANCREATICCANCER17::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;

                auto a = now();
		if(integrator == "projector") 
	                P1 = time_step<17,34,PANCREATICCANCER17>(deltat, P0);
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<17,34,PANCREATICCANCER17>(deltat, P0);
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<17,34,PANCREATICCANCER17>(deltat,P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}    
                P0 = P1;
                cout << "time step: " << delta(a) << endl;

                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
            }
        }

        write<34>(P1, 0.01); 
    } else if(problem == "PANCREATICCANCER13") {
        ind len1 = 1<<17;
        ind len2 = 1<<17;

        // init from file
        LR P0(r, {len1, len2});
        vector<std::string> names;
        MatrixXd U, S, V;
        read_nc(initial_value, U, S, V, names);

        if(names != PANCREATICCANCER13::names()) {
            cout << "ERROR: names in input file do not match names in problem specification" << endl;
            exit(1);
        }
        P0.init(U.cols(), U*S, V);
        
        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, PANCREATICCANCER13::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;

                auto a = now();
		if(integrator == "projector") 
	                P1 = time_step<17,34,PANCREATICCANCER13>(deltat, P0);
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<17,34,PANCREATICCANCER13>(deltat, P0);
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<17,34,PANCREATICCANCER13>(deltat, P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}    
                P0 = P1;
                cout << "time step: " << delta(a) << endl;

                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
            }
        }

        write<34>(P1, 0.01);
            } else if(problem == "PANCREATICCANCER9") {
        ind len1 = 1<<17;
        ind len2 = 1<<17;

        // init from file
        LR P0(r, {len1, len2});
        vector<std::string> names;
        MatrixXd U, S, V;
        read_nc(initial_value, U, S, V, names);

        if(names != PANCREATICCANCER9::names()) {
            cout << "ERROR: names in input file do not match names in problem specification" << endl;
            exit(1);
        }
        P0.init(U.cols(), U*S, V);
        
        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, PANCREATICCANCER9::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;

                auto a = now();
		if(integrator == "projector") 
	                P1 = time_step<17,34,PANCREATICCANCER9>(deltat, P0);
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<17,34,PANCREATICCANCER9>(deltat, P0);
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<17,34,PANCREATICCANCER9>(deltat, P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}    
                P0 = P1;
                cout << "time step: " << delta(a) << endl;

                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
            }
        }

        write<34>(P1, 0.01);
            } else if(problem == "PANCREATICCANCER8_1") {
        ind len1 = 1<<17;
        ind len2 = 1<<17;

        // init from file
        LR P0(r, {len1, len2});
        vector<std::string> names;
        MatrixXd U, S, V;
        read_nc(initial_value, U, S, V, names);

        if(names != PANCREATICCANCER8_1::names()) {
            cout << "ERROR: names in input file do not match names in problem specification" << endl;
            exit(1);
        }
        P0.init(U.cols(), U*S, V);
        
        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, PANCREATICCANCER8_1::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;

                auto a = now();
		if(integrator == "projector") 
	                P1 = time_step<17,34,PANCREATICCANCER8_1>(deltat, P0);
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<17,34,PANCREATICCANCER8_1>(deltat, P0);
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<17,34,PANCREATICCANCER8_1>(deltat,P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}    
                P0 = P1;
                cout << "time step: " << delta(a) << endl;

                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
            }
        }

        write<34>(P1, 0.01);
            } else if(problem == "PANCREATICCANCER8_2") {
        ind len1 = 1<<17;
        ind len2 = 1<<17;

        // init from file
        LR P0(r, {len1, len2});
        vector<std::string> names;
        MatrixXd U, S, V;
        read_nc(initial_value, U, S, V, names);

        if(names != PANCREATICCANCER8_2::names()) {
            cout << "ERROR: names in input file do not match names in problem specification" << endl;
            exit(1);
        }
        P0.init(U.cols(), U*S, V);
        
        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, PANCREATICCANCER8_2::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;

                auto a = now();
		if(integrator == "projector") 
	                P1 = time_step<17,34,PANCREATICCANCER8_2>(deltat, P0);
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<17,34,PANCREATICCANCER8_2>(deltat, P0);
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<17,34,PANCREATICCANCER8_2>(deltat, P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}    
                P0 = P1;
                cout << "time step: " << delta(a) << endl;

                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
            }
        }

        write<34>(P1, 0.01);
            } else if(problem == "PANCREATICCANCER5") {
        ind len1 = 1<<17;
        ind len2 = 1<<17;

        // init from file
        LR P0(r, {len1, len2});
        vector<std::string> names;
        MatrixXd U, S, V;
        read_nc(initial_value, U, S, V, names);

        if(names != PANCREATICCANCER5::names()) {
            cout << "ERROR: names in input file do not match names in problem specification" << endl;
            exit(1);
        }
        P0.init(U.cols(), U*S, V);
        
        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, PANCREATICCANCER5::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;

                auto a = now();
		if(integrator == "projector") 
	                P1 = time_step<17,34,PANCREATICCANCER5>(deltat, P0);
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<17,34,PANCREATICCANCER5>(deltat, P0);
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<17,34,PANCREATICCANCER5>(deltat, P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}    
                P0 = P1;
                cout << "time step: " << delta(a) << endl;

                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
            }
        }

        write<34>(P1, 0.01);
        } else if(problem == "PANCREATICCANCER3") {
        ind len1 = 1<<17;
        ind len2 = 1<<17;

        // init from file
        LR P0(r, {len1, len2});
        vector<std::string> names;
        MatrixXd U, S, V;
        read_nc(initial_value, U, S, V, names);

        if(names != PANCREATICCANCER3::names()) {
            cout << "ERROR: names in input file do not match names in problem specification" << endl;
            exit(1);
        }
        P0.init(U.cols(), U*S, V);
        
        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, PANCREATICCANCER3::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;

                auto a = now();
		if(integrator == "projector") 
	                P1 = time_step<17,34,PANCREATICCANCER3>(deltat, P0);
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<17,34,PANCREATICCANCER3>(deltat, P0);
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<17,34,PANCREATICCANCER3>(deltat, P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}    
                P0 = P1;
                cout << "time step: " << delta(a) << endl;

                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
            }
        }

        write<34>(P1, 0.01);
    } else if(problem == "APOPTOSISa") {
        ind len1 = 1<<20;
        ind len2 = 1<<21;

        // init from file
        LR P0(r, {len1, len2});
        vector<std::string> names;
        MatrixXd U, S, V;
        read_nc(initial_value, U, S, V, names);

        if(names != APOPTOSISa::names()) {
            cout << "ERROR: names in input file do not match names in problem specification" << endl;
            exit(1);
        }
        P0.init(U.cols(), U*S, V);


        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, APOPTOSISa::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;

                auto a = now();
		if(integrator == "projector") 
	                P1 = time_step<20,41,APOPTOSISa>(deltat, P0);
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<20,41,APOPTOSISa>(deltat, P0);
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<20,41,APOPTOSISa>(deltat, P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}    
                P0 = P1;
                cout << "time step: " << delta(a) << endl;

                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
            }
        }
    } else if(problem == "APOPTOSISb") {
        ind len1 = 1<<20;
        ind len2 = 1<<21;

        // init from file
        LR P0(r, {len1, len2});
        vector<std::string> names;
        MatrixXd U, S, V;
        read_nc(initial_value, U, S, V, names);

        if(names != APOPTOSISb::names()) {
            cout << "ERROR: names in input file do not match names in problem specification" << endl;
            exit(1);
        }
        P0.init(U.cols(), U*S, V);

        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, APOPTOSISb::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;

                auto a = now();
		if(integrator == "projector") 
	                P1 = time_step<20,41,APOPTOSISb>(deltat, P0);
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<20,41,APOPTOSISb>(deltat, P0);
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<20,41,APOPTOSISb>(deltat, P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}    
                P0 = P1;
                cout << "time step: " << delta(a) << endl;

                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
            }
        }
    } else if(problem == "APOPTOSIS51") {
        ind len1 = 1<<20;
        ind len2 = 1<<21;

        // init from file
        LR P0(r, {len1, len2});
        vector<std::string> names;
        MatrixXd U, S, V;
        read_nc(initial_value, U, S, V, names);

        if(names != APOPTOSIS51::names()) {
            cout << "ERROR: names in input file do not match names in problem specification" << endl;
            exit(1);
        }
        P0.init(U.cols(), U*S, V);


        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, APOPTOSIS51::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;

                auto a = now();
		if(integrator == "projector") 
	                P1 = time_step<20,41,APOPTOSIS51>(deltat, P0);
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<20,41,APOPTOSIS51>(deltat, P0);
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<20,41,APOPTOSIS51>(deltat, P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}    
                P0 = P1;
                cout << "time step: " << delta(a) << endl;

                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
            }
        }
    } else if(problem == "APOPTOSIS52") {
        ind len1 = 1<<20;
        ind len2 = 1<<21;

        // init from file
        LR P0(r, {len1, len2});
        vector<std::string> names;
        MatrixXd U, S, V;
        read_nc(initial_value, U, S, V, names);

        if(names != APOPTOSIS52::names()) {
            cout << "ERROR: names in input file do not match names in problem specification" << endl;
            exit(1);
        }
        P0.init(U.cols(), U*S, V);


        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, APOPTOSIS52::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;

                auto a = now();
		if(integrator == "projector") 
	                P1 = time_step<20,41,APOPTOSIS52>(deltat, P0);
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<20,41,APOPTOSIS52>(deltat, P0);
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<20,41,APOPTOSIS52>(deltat, P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}    
                P0 = P1;
                cout << "time step: " << delta(a) << endl;

                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
            }
        }
     } else if(problem == "APOPTOSISa_1GFR") {
        ind len1 = 1<<20;
        ind len2 = 1<<21;

        // init from file
        LR P0(r, {len1, len2});
        vector<std::string> names;
        MatrixXd U, S, V;
        read_nc(initial_value, U, S, V, names);

        if(names != APOPTOSISa_1GFR::names()) {
            cout << "ERROR: names in input file do not match names in problem specification" << endl;
            exit(1);
        }
        P0.init(U.cols(), U*S, V);


        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, APOPTOSISa_1GFR::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;

                auto a = now();
		if(integrator == "projector") 
	                P1 = time_step<20,41,APOPTOSISa_1GFR>(deltat, P0);
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<20,41,APOPTOSISa_1GFR>(deltat, P0);
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<20,41,APOPTOSISa_1GFR>(deltat,P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}    
                P0 = P1;
                cout << "time step: " << delta(a) << endl;

                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
            }
        }
     } else if(problem == "APOPTOSISa_2Cas7") {
        ind len1 = 1<<20;
        ind len2 = 1<<21;

        // init from file
        LR P0(r, {len1, len2});
        vector<std::string> names;
        MatrixXd U, S, V;
        read_nc(initial_value, U, S, V, names);

        if(names != APOPTOSISa_2Cas7::names()) {
            cout << "ERROR: names in input file do not match names in problem specification" << endl;
            exit(1);
        }
        P0.init(U.cols(), U*S, V);


        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, APOPTOSISa_2Cas7::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;

                auto a = now();
		if(integrator == "projector") 
	                P1 = time_step<20,41,APOPTOSISa_2Cas7>(deltat, P0);
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<20,41,APOPTOSISa_2Cas7>(deltat, P0);
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<20,41,APOPTOSISa_2Cas7>(deltat, P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}    
                P0 = P1;
                cout << "time step: " << delta(a) << endl;

                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
            }
        }
     } else if(problem == "APOPTOSISa_3PI3K") {
        ind len1 = 1<<20;
        ind len2 = 1<<21;

        // init from file
        LR P0(r, {len1, len2});
        vector<std::string> names;
        MatrixXd U, S, V;
        read_nc(initial_value, U, S, V, names);

        if(names != APOPTOSISa_3PI3K::names()) {
            cout << "ERROR: names in input file do not match names in problem specification" << endl;
            exit(1);
        }
        P0.init(U.cols(), U*S, V);


        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, APOPTOSISa_3PI3K::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;

                auto a = now();
		if(integrator == "projector") 
	                P1 = time_step<20,41,APOPTOSISa_3PI3K>(deltat, P0);
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<20,41,APOPTOSISa_3PI3K>(deltat, P0);
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<20,41,APOPTOSISa_3PI3K>(deltat,P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}    
                P0 = P1;
                cout << "time step: " << delta(a) << endl;

                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
            }
        }
     } else if(problem == "APOPTOSISa_4Cas8") {
        ind len1 = 1<<20;
        ind len2 = 1<<21;

        // init from file
        LR P0(r, {len1, len2});
        vector<std::string> names;
        MatrixXd U, S, V;
        read_nc(initial_value, U, S, V, names);

        if(names != APOPTOSISa_4Cas8::names()) {
            cout << "ERROR: names in input file do not match names in problem specification" << endl;
            exit(1);
        }
        P0.init(U.cols(), U*S, V);


        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, APOPTOSISa_4Cas8::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;

                auto a = now();
		if(integrator == "projector") 
	                P1 = time_step<20,41,APOPTOSISa_4Cas8>(deltat, P0);
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<20,41,APOPTOSISa_4Cas8>(deltat, P0);
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<20,41,APOPTOSISa_4Cas8>(deltat, P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}    
                P0 = P1;
                cout << "time step: " << delta(a) << endl;

                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
            }
        }
     } else if(problem == "APOPTOSISa_5PIP2") {
        ind len1 = 1<<20;
        ind len2 = 1<<21;

        // init from file
        LR P0(r, {len1, len2});
        vector<std::string> names;
        MatrixXd U, S, V;
        read_nc(initial_value, U, S, V, names);

        if(names != APOPTOSISa_5PIP2::names()) {
            cout << "ERROR: names in input file do not match names in problem specification" << endl;
            exit(1);
        }
        P0.init(U.cols(), U*S, V);


        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, APOPTOSISa_5PIP2::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;

                auto a = now();
		if(integrator == "projector") 
	                P1 = time_step<20,41,APOPTOSISa_5PIP2>(deltat, P0);
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<20,41,APOPTOSISa_5PIP2>(deltat, P0);
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<20,41,APOPTOSISa_5PIP2>(deltat, P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}    
                P0 = P1;
                cout << "time step: " << delta(a) << endl;

                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
            }
        }
     } else if(problem == "APOPTOSISa_6JNK") {
        ind len1 = 1<<20;
        ind len2 = 1<<21;

        // init from file
        LR P0(r, {len1, len2});
        vector<std::string> names;
        MatrixXd U, S, V;
        read_nc(initial_value, U, S, V, names);

        if(names != APOPTOSISa_6JNK::names()) {
            cout << "ERROR: names in input file do not match names in problem specification" << endl;
            exit(1);
        }
        P0.init(U.cols(), U*S, V);


        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, APOPTOSISa_6JNK::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;

                auto a = now();
		if(integrator == "projector") 
	                P1 = time_step<20,41,APOPTOSISa_6JNK>(deltat, P0);
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<20,41,APOPTOSISa_6JNK>(deltat, P0);
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<20,41,APOPTOSISa_6JNK>(deltat, P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}    
                P0 = P1;
                cout << "time step: " << delta(a) << endl;

                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
            }
        }
     } else if(problem == "APOPTOSISa_7PIP3") {
        ind len1 = 1<<20;
        ind len2 = 1<<21;

        // init from file
        LR P0(r, {len1, len2});
        vector<std::string> names;
        MatrixXd U, S, V;
        read_nc(initial_value, U, S, V, names);

        if(names != APOPTOSISa_7PIP3::names()) {
            cout << "ERROR: names in input file do not match names in problem specification" << endl;
            exit(1);
        }
        P0.init(U.cols(), U*S, V);


        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, APOPTOSISa_7PIP3::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;

                auto a = now();
		if(integrator == "projector") 
	                P1 = time_step<20,41,APOPTOSISa_7PIP3>(deltat, P0);
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<20,41,APOPTOSISa_7PIP3>(deltat, P0);
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<20,41,APOPTOSISa_7PIP3>(deltat, P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}    
                P0 = P1;
                cout << "time step: " << delta(a) << endl;

                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
            }
        }
     } else if(problem == "APOPTOSISa_8JNKK") {
        ind len1 = 1<<20;
        ind len2 = 1<<21;

        // init from file
        LR P0(r, {len1, len2});
        vector<std::string> names;
        MatrixXd U, S, V;
        read_nc(initial_value, U, S, V, names);

        if(names != APOPTOSISa_8JNKK::names()) {
            cout << "ERROR: names in input file do not match names in problem specification" << endl;
            exit(1);
        }
        P0.init(U.cols(), U*S, V);


        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, APOPTOSISa_8JNKK::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;

                auto a = now();
		if(integrator == "projector") 
	                P1 = time_step<20,41,APOPTOSISa_8JNKK>(deltat, P0);
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<20,41,APOPTOSISa_8JNKK>(deltat, P0);
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<20,41,APOPTOSISa_8JNKK>(deltat, P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}    
                P0 = P1;
                cout << "time step: " << delta(a) << endl;

                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
            }
        }
/*     } else if(problem == "APOPTOSISa_9PTEN") {
        ind len1 = 1<<20;
        ind len2 = 1<<21;

        // init from file
        LR P0(r, {len1, len2});
        vector<std::string> names;
        MatrixXd U, S, V;
        read_nc(initial_value, U, S, V, names);

        if(names != APOPTOSISa_9PTEN::names()) {
            cout << "ERROR: names in input file do not match names in problem specification" << endl;
            exit(1);
        }
        P0.init(U.cols(), U*S, V);


        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, APOPTOSISa_9PTEN::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;

                auto a = now();
		if(integrator == "projector") 
	                P1 = time_step<20,41,APOPTOSISa_9PTEN>(deltat, P0);
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<20,41,APOPTOSISa_9PTEN>(deltat, P0);
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<20,41,APOPTOSISa_9PTEN>(deltat,P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}    
                P0 = P1;
                cout << "time step: " << delta(a) << endl;

                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
            }
        }*/
    } else if(problem == "APOPTOSIS41") {
        ind len1 = 1<<20;
        ind len2 = 1<<21;

        // init from file
        LR P0(r, {len1, len2});
        vector<std::string> names;
        MatrixXd U, S, V;
        read_nc(initial_value, U, S, V, names);

        if(names != APOPTOSIS41::names()) {
            cout << "ERROR: names in input file do not match names in problem specification" << endl;
            exit(1);
        }
        P0.init(U.cols(), U*S, V);


        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, APOPTOSIS41::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;

                auto a = now();
		if(integrator == "projector") 
	                P1 = time_step<20,41,APOPTOSIS41>(deltat, P0);
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<20,41,APOPTOSIS41>(deltat, P0);
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<20,41,APOPTOSIS41>(deltat, P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}    
                P0 = P1;
                cout << "time step: " << delta(a) << endl;

                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
            }
        }
    } else if(problem == "APOPTOSIS42") {
        ind len1 = 1<<20;
        ind len2 = 1<<21;

        // init from file
        LR P0(r, {len1, len2});
        vector<std::string> names;
        MatrixXd U, S, V;
        read_nc(initial_value, U, S, V, names);

        if(names != APOPTOSIS42::names()) {
            cout << "ERROR: names in input file do not match names in problem specification" << endl;
            exit(1);
        }
        P0.init(U.cols(), U*S, V);


        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, APOPTOSIS42::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;

                auto a = now();
		if(integrator == "projector") 
	                P1 = time_step<20,41,APOPTOSIS42>(deltat, P0);
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<20,41,APOPTOSIS42>(deltat, P0);
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<20,41,APOPTOSIS42>(deltat,P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}    
                P0 = P1;
                cout << "time step: " << delta(a) << endl;

                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
            }
        }     
        
    } else if(problem == "APOPTOSIS43") {
        ind len1 = 1<<20;
        ind len2 = 1<<21;

        // init from file
        LR P0(r, {len1, len2});
        vector<std::string> names;
        MatrixXd U, S, V;
        read_nc(initial_value, U, S, V, names);

        if(names != APOPTOSIS43::names()) {
            cout << "ERROR: names in input file do not match names in problem specification" << endl;
            exit(1);
        }
        P0.init(U.cols(), U*S, V);


        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, APOPTOSIS43::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;

                auto a = now();
		if(integrator == "projector") 
	                P1 = time_step<20,41,APOPTOSIS43>(deltat, P0);
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<20,41,APOPTOSIS43>(deltat, P0);
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<20,41,APOPTOSIS43>(deltat, P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}    
                P0 = P1;
                cout << "time step: " << delta(a) << endl;

                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
            }
        }
    } else if(problem == "APOPTOSIS44") {
        ind len1 = 1<<20;
        ind len2 = 1<<21;

        // init from file
        LR P0(r, {len1, len2});
        vector<std::string> names;
        MatrixXd U, S, V;
        read_nc(initial_value, U, S, V, names);

        if(names != APOPTOSIS44::names()) {
            cout << "ERROR: names in input file do not match names in problem specification" << endl;
            exit(1);
        }
        P0.init(U.cols(), U*S, V);


        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, APOPTOSIS44::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;

                auto a = now();
		if(integrator == "projector") 
	                P1 = time_step<20,41,APOPTOSIS44>(deltat, P0);
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<20,41,APOPTOSIS44>(deltat, P0);
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<20,41,APOPTOSIS44>(deltat,P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}    
                P0 = P1;
                cout << "time step: " << delta(a) << endl;

                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
            }
        } 
    } else if(problem == "MixMtor") {
        ind len = 1<<11;

        // init from file
        LR P0(r, {len, len});
        vector<std::string> names;
        MatrixXd U, S, V;
        read_nc(initial_value, U, S, V, names);

        if(names != MixMtor::names()) {
            cout << "ERROR: names in input file do not match names in problem specification" << endl;
            exit(1);
        }
        P0.init(U.cols(), U*S, V);

      //  write<22>(P0, 0.001);

        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, MixMtor::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;

                auto a = now();
		if(integrator == "projector") 
	                P1 = time_step<11,22,MixMtor>(deltat, P0);
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<11,22,MixMtor>(deltat, P0);
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<11,22,MixMtor>(deltat, P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}    
                P0 = P1;
                cout << "time step: " << delta(a) << endl;

                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
            }
        }
        
      //  write<22>(P1, 0.001);    
 
    } else if(problem == "orig") {
        ind len = 1<<11;

        // init from file
        LR P0(r, {len, len});
        vector<std::string> names;
        MatrixXd U, S, V;
        read_nc(initial_value, U, S, V, names);

        if(names != orig::names()) {
            cout << "ERROR: names in input file do not match names in problem specification" << endl;
            exit(1);
        }
        P0.init(U.cols(), U*S, V);

      //  write<22>(P0, 0.001);

        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, orig::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;

                auto a = now();
		if(integrator == "projector") 
	                P1 = time_step<11,22,orig>(deltat, P0);
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<11,22,orig>(deltat, P0);
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<11,22,orig>(deltat, P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}    
                P0 = P1;
                cout << "time step: " << delta(a) << endl;

                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
            }
        }
        
      //  write<22>(P1, 0.001);    

    } else if(problem == "yellow") {
        ind len = 1<<11;

        // init from file
        LR P0(r, {len, len});
        vector<std::string> names;
        MatrixXd U, S, V;
        read_nc(initial_value, U, S, V, names);

        if(names != yellow::names()) {
            cout << "ERROR: names in input file do not match names in problem specification" << endl;
            exit(1);
        }
        P0.init(U.cols(), U*S, V);

      //  write<22>(P0, 0.001);

        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, yellow::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;

                auto a = now();
		if(integrator == "projector") 
	                P1 = time_step<11,22,yellow>(deltat, P0);
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<11,22,yellow>(deltat, P0);
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<11,22,yellow>(deltat, P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}    
                P0 = P1;
                cout << "time step: " << delta(a) << endl;

                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
            }
        }
        
      //  write<22>(P1, 0.001);    


    } else if(problem == "APOPTOSIS_WORST") {
        ind len1 = 1<<20;
        ind len2 = 1<<21;

        // init from file
        LR P0(r, {len1, len2});
        vector<std::string> names;
        MatrixXd U, S, V;
        read_nc(initial_value, U, S, V, names);

        if(names != APOPTOSIS_WORST::names()) {
            cout << "ERROR: names in input file do not match names in problem specification" << endl;
            exit(1);
        }
        P0.init(U.cols(), U*S, V);


        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, APOPTOSIS_WORST::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;

                auto a = now();
		if(integrator == "projector") 
	                P1 = time_step<20,41,APOPTOSIS_WORST>(deltat, P0);
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<20,41,APOPTOSIS_WORST>(deltat, P0);
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<20,41,APOPTOSIS_WORST>(deltat,P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}    
                P0 = P1;
                cout << "time step: " << delta(a) << endl;

                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
            }
        }   
      
      } else if(problem == "APOPTOSIS_BEST") {
        ind len1 = 1<<20;
        ind len2 = 1<<21;

        // init from file
        LR P0(r, {len1, len2});
        vector<std::string> names;
        MatrixXd U, S, V;
        read_nc(initial_value, U, S, V, names);

        if(names != APOPTOSIS_BEST::names()) {
            cout << "ERROR: names in input file do not match names in problem specification" << endl;
            exit(1);
        }
        P0.init(U.cols(), U*S, V);


        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, APOPTOSIS_BEST::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;

                auto a = now();
		if(integrator == "projector") 
	                P1 = time_step<20,41,APOPTOSIS_BEST>(deltat, P0);
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<20,41,APOPTOSIS_BEST>(deltat, P0);
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<20,41,APOPTOSIS_BEST>(deltat,P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}    
                P0 = P1;
                cout << "time step: " << delta(a) << endl;

                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
            }
        } 
    } else if(problem == "APOPTOSIS_REASONABLE") {
        ind len1 = 1<<20;
        ind len2 = 1<<21;

        // init from file
        LR P0(r, {len1, len2});
        vector<std::string> names;
        MatrixXd U, S, V;
        read_nc(initial_value, U, S, V, names);

        if(names != APOPTOSIS_REASONABLE::names()) {
            cout << "ERROR: names in input file do not match names in problem specification" << endl;
            exit(1);
        }
        P0.init(U.cols(), U*S, V);


        LR P1;
        double t=0.0;
        ind n_steps = ceil(T/deltat);
        for(int i=0;i<n_steps+1;i++) {
            write_snapshot(i, t, n_steps, num_snapshots, P0, APOPTOSIS_REASONABLE::names());

            if(i < n_steps) {
                if(T - t < deltat)
                    deltat = T-t;

                auto a = now();
		if(integrator == "projector") 
	                P1 = time_step<20,41,APOPTOSIS_REASONABLE>(deltat, P0);
	        else if(integrator == "unconventional")
	        	P1 = time_step_unconventional<20,41,APOPTOSIS_REASONABLE>(deltat, P0);
            else if(integrator == "augmented")
                P1 = time_step_unconventional_augmented<20,41,APOPTOSIS_REASONABLE>(deltat,P0);
	       	else{
	       	
	       		cout << "integrator not correctly specified" << endl;
	       		exit(1);
	       	}    
                P0 = P1;
                cout << "time step: " << delta(a) << endl;

                t += deltat;
                cout << "t=" << t << "\t" << "mass=" << mass(P1) << endl;
            }
        }     

     } else {
        cout << "ERROR: the problem " << problem << " is not available" << endl;
        return 1;
    }
}

