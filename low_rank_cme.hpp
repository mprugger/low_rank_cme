#pragma once

#include <Eigen/Dense>
using Eigen::MatrixXd;
using Eigen::VectorXd;

#include <Eigen/QR>
#include <Eigen/SVD>

#include "common.hpp"
#include "common_rules.hpp"

#include <map>
#include <string>

std::map<std::string,double> _times;


struct rectangular_QR {
    int n, r;
    Eigen::HouseholderQR<MatrixXd> qr;

    rectangular_QR(int _n, int _r) : n(_n), r(_r), qr(_n,_r) {}

    rectangular_QR(MatrixXd& A) : qr(A) {
        n = A.rows();
        r = A.cols();
    }

    void compute(MatrixXd& A) {
        qr.compute(A);
    }

    MatrixXd Q() {
        MatrixXd q = qr.householderQ()*MatrixXd::Identity(n,r);
        return q;
    }

    MatrixXd R() {
        MatrixXd mr = qr.matrixQR().triangularView<Eigen::Upper>();
        return mr.block(0,0,r,r);
    }

};


struct LR {
    MatrixXd U, S, V;

    LR() { }

    LR(int r, array<ind,2> n) {
        U.resize(n[0], r);
        V.resize(n[1], r);
        S.resize(r, r);
    }

    array<ind,2> n() const {
        return {U.rows(), V.rows()};
    }

    ind r() const {
        return U.cols();
    }

    void init(int rank, MatrixXd _U, MatrixXd _V) {
        U.setIdentity();
        V.setIdentity();
        S.setZero();

        U.block(0,0,_U.rows(),_U.cols()) = _U;
        V.block(0,0,_V.rows(),_V.cols()) = _V;


        for(int k=0;k<rank;k++) {
            double l1 = _U.col(k).norm();
            double l2 = _V.col(k).norm();

            U.col(k) /= l1;
            V.col(k) /= l2;

            S(k,k) = l1*l2;
        }

        orthogonalize();
    }

    void orthogonalize() {

        rectangular_QR qrx(U);
        U = qrx.Q();

        rectangular_QR qrv(V);
        V = qrv.Q();

        S = qrx.R()*S*qrv.R().transpose();
    }

    VectorXd full() {
        MatrixXd P = U*S*V.transpose();
        VectorXd out(P.rows()*P.cols());
        for(ind j=0;j<P.cols();j++)
            for(ind i=0;i<P.rows();i++)
                out(i + P.rows()*j) = P(i,j);
        return out;
    }
};


template<ind d>
void write(const VectorXd& P, double tol=0.0) {
    if(1l<<d != P.size()) {
        cout << "ERROR: write has been called with incompatible dimensions" << endl;
        exit(1);
    }
    double mass = 0.0;
    for(ind i=0;i<P.size();i++) {
        mass += P[i];
        if(fabs(P[i]) >= tol) {
            cout << bitset<d>(i) << "\t\t" << P[i] << endl;
        }
    }
    cout << "Total mass: " << mass << endl;
}

template<ind d>
void write(const LR& P, double tol=0.0) {
    if(1l<<d != P.U.rows()*P.V.rows()) {
        cout << "ERROR: write has been called with incompatible dimensions" << endl;
        exit(1);
    }
    double mass = 0.0;

    // Avoids forming the full matrix P
    MatrixXd US = P.U*P.S;
    for(ind i=0;i<P.V.rows();i++) {
        VectorXd P_rowwise = US*P.V.row(i).transpose();
        for(ind j=0;j<P_rowwise.size();j++) {
            mass += P_rowwise[j];
            if(fabs(P_rowwise[j]) >= tol) {
                cout << bitset<d>(j + i*P_rowwise.size()) << "\t\t" << P_rowwise[j] << endl;
            }
        }
    }
    cout << "Total mass: " << mass << endl;
}


double mass(const VectorXd& P) {
    double mass = 0.0;
    for(ind i=0;i<P.size();i++)
        mass += P[i];
    return mass;
}

double mass(const LR& P) {
    // Avoids forming the full matrix P
    MatrixXd mass_V = P.V.transpose()*MatrixXd::Ones(P.V.rows(),1);
    MatrixXd mass_U = MatrixXd::Ones(1,P.U.rows())*P.U;
    return (mass_U*P.S*mass_V)(0,0);
}

LR normalize_LR(const LR& P) {
    LR ret = P;
    ret.S /= mass(P);
    return ret;
}

template<ind rule_no, ind part, ind m, ind bs_size>
bitset<bs_size> T_flip(bitset<bs_size> x) {
    bitset<bs_size> out = x;
    if(part==0 && rule_no < m)
        out.flip(rule_no);
    else if(part == 1 && rule_no >= m)
        out.flip(rule_no - m);
    return out;
}


template<ind rule_no, ind part, ind m, ind d, class rule>
void write_dependent_bits(bitset<d>& x, ind i_xg, vector<ind>& don) {
    bitset<64> xg(i_xg);
    ind idx = 0;
    
    for(ind k=0;k<(ind)don.size();k++) {
        if( (part == 0 && don[k] < m) || (part == 1 && don[k] >= m)) {
            x[don[k]] = xg[idx];
            idx++;
        }
    }
}

template<ind rule_no, ind m, ind d, class rule>
void Gamma12(ind gamma_idx, const MatrixXd& V, const MatrixXd& V0,
             vector<double>& Gamma1, vector<double>& Gamma2) {

    ind r = V.cols();
    ind len_xg2 = V.rows();

    vector<ind> don = rule::template depends_on<rule_no>();
    ind len_depends_on_part0 = std::count_if(begin(don), end(don), [](ind i) { return i<m; });
    
    ind max_cap = (1 << len_depends_on_part0);
    Gamma1.resize(r*r*max_cap);
    Gamma2.resize(r*r*max_cap);
    fill(begin(Gamma1), end(Gamma1), 0.0);
    fill(begin(Gamma2), end(Gamma2), 0.0);

    MatrixXd VA(len_xg2, r), VD(len_xg2, r);
    VectorXd Alpha(len_xg2), Delta(len_xg2);
    for(ind i_xg1=0;i_xg1<max_cap;i_xg1++) {

        // compute alpha and delta for fixed x_R cap x_G1
        auto t = now();
        #pragma omp parallel for num_threads(8)
        for(ind i_xg2=0;i_xg2<len_xg2;i_xg2++) {
            bitset<d> x(i_xg2);
            x <<= m;

            write_dependent_bits<rule_no, 0, m, d, rule>(x, i_xg1, don);

            Alpha[i_xg2] = 1.0 - (x[rule_no] == rule::template rule<rule_no>(x));

            bitset<d> x_flipped = T_flip<rule_no, 0, d, d>(x);
            Delta[i_xg2] = (x[rule_no] == rule::template rule<rule_no>(x_flipped));
        }
        _times["Gamma12::alpha"] += delta(t);

        // multiply each column pointwise with alpha/delta (modulo the bit flip)
        t = now();
        #pragma omp parallel for num_threads(8)
        for(ind k=0;k<r;k++) {
            for(ind i_xg2=0;i_xg2<len_xg2;i_xg2++) {
                VA(i_xg2,k) = Alpha(i_xg2)*V(i_xg2, k);
                                    
                bitset<d-m> xg2(i_xg2);
                bitset<d-m> xg2_flip = T_flip<rule_no, 1, m, d-m>(xg2);
                VD(i_xg2,k) = Delta(i_xg2)*V(xg2_flip.to_ulong(),k);
            }
        }
        _times["Gamma12::pointwise"] += delta(t);

        // compute Gamma1 and Gamma2 using a matmul
        t = now();
        #ifdef __MKL__
        mkl_matmul(true, false, VD.cols(), VD.rows(), V0.cols(), VD.data(), V0.data(), Gamma1.data()+i_xg1*r*r);
        mkl_matmul(true, false, VA.cols(), VA.rows(), V0.cols(), VA.data(), V0.data(), Gamma2.data()+i_xg1*r*r);
        #else
        MatrixXd G1 = VD.transpose()*V0;
        MatrixXd G2 = VA.transpose()*V0;
        
        memcpy(Gamma1.data()+i_xg1*r*r, G1.data(), sizeof(double)*r*r);
        memcpy(Gamma2.data()+i_xg1*r*r, G2.data(), sizeof(double)*r*r);
        #endif
        _times["Gamma12::matmul"] += delta(t);
    }
}

template<ind rule_no, ind m, ind d, class rule>
void Gamma34(ind gamma_idx, const MatrixXd& U, const MatrixXd& U1,
             vector<double>& Gamma3, vector<double>& Gamma4) {

    ind r = U.cols();
    ind len_xg1 = U.rows();

    vector<ind> don = rule::template depends_on<rule_no>();
    ind len_depends_on_part1 = std::count_if(begin(don), end(don), [](ind i) { return i>=m; });

    ind max_cap = (1 << len_depends_on_part1);
    Gamma3.resize(r*r*max_cap);
    Gamma4.resize(r*r*max_cap);
    fill(begin(Gamma3), end(Gamma3), 0.0);
    fill(begin(Gamma4), end(Gamma4), 0.0);


    MatrixXd VG(len_xg1, r), VD(len_xg1, r);
    VectorXd Gamma(len_xg1), Delta(len_xg1);
    for(ind i_xg2=0;i_xg2<max_cap;i_xg2++) {

        // compute gamma and delta
        auto t = now();
        #pragma omp parallel for num_threads(8)
        for(ind i_xg1=0;i_xg1<len_xg1;i_xg1++) {
            bitset<d> x(i_xg1);

            write_dependent_bits<rule_no, 1, m, d, rule>(x, i_xg2, don);

            Gamma[i_xg1] = 1.0 - (x[rule_no] == rule::template rule<rule_no>(x));

            bitset<d> x_flipped = T_flip<rule_no, 0, d, d>(x);
            Delta[i_xg1] = (x[rule_no] == rule::template rule<rule_no>(x_flipped));
        }
        _times["Gamma34::gamma"] += delta(t);

        // multiply each column pointwise with gamma/delta (modulo the bit flip)
        #pragma omp parallel for num_threads(8)
        for(ind k=0;k<r;k++) {
            for(ind i_xg1=0;i_xg1<len_xg1;i_xg1++) {
                VG(i_xg1,k) = Gamma(i_xg1)*U(i_xg1, k);

                bitset<m> xg1(i_xg1);
                bitset<m> xg1_flip = T_flip<rule_no, 0, m, m>(xg1);
                VD(i_xg1,k) = Delta(i_xg1)*U(xg1_flip.to_ulong(), k);
            }
        }
        _times["Gamma34::pointwise"] += delta(t);

        // compute Gamma1 and Gamma2 using a matmul
        t = now();
        #ifdef __MKL__
        mkl_matmul(true, false, VD.cols(), VD.rows(), U1.cols(), VD.data(), U1.data(), Gamma3.data()+i_xg2*r*r);
        mkl_matmul(true, false, VG.cols(), VG.rows(), U1.cols(), VG.data(), U1.data(), Gamma4.data()+i_xg2*r*r);
        #else
        MatrixXd G3 = VD.transpose()*U1;
        MatrixXd G4 = VG.transpose()*U1;
        
        memcpy(Gamma3.data()+i_xg2*r*r, G3.data(), sizeof(double)*r*r);
        memcpy(Gamma4.data()+i_xg2*r*r, G4.data(), sizeof(double)*r*r);
        #endif
        _times["Gamma34::matmul"] += delta(t);
    }
}

template<ind rule_no, ind part, ind m, ind bs_len, class rule>
ind linear_idx_rule(bitset<bs_len> xg_part) {
    ind idx = 0;
    vector<ind> don = rule::template depends_on<rule_no>();
    ind fac = 1;
    for(ind k=0;k<(ind)don.size();k++) {
        if(part == 0 && don[k] < m) {
            idx += xg_part[don[k]]*fac;
            fac *= 2;
        } else if(part == 1 && don[k] >= m) {
            idx += xg_part[don[k]-m]*fac;
            fac *= 2;
        }
    }

    return idx;
}


template<ind rule_no, ind d, int m, class rule>
MatrixXd compute_DeltaPV0(const LR& P, const MatrixXd& V0) {
    ind r = P.r();
    array<ind,2> n = P.n();
    ind len_xg1 = n[0];

    MatrixXd out(n[0], r);
    out.setZero();

    auto t1 = now();
    vector<double> Gamma1, Gamma2;
    Gamma12<rule_no, m, d, rule>(1, P.V, V0, Gamma1, Gamma2);
    _times["Gamma12"] += delta(t1);

    auto t2 = now();

    MatrixXd H(P.U.rows(), P.S.cols());
    #ifdef __MKL__
    mkl_matmul(false, false, P.U.rows(), P.U.cols(), P.S.cols(), P.U.data(), P.S.data(), H.data());
    #else
    H = P.U*P.S;
    #endif
    _times["b1"] += delta(t2);

    MatrixXd MatGamma1(r, Gamma1.size()/r);
    for(ind i=0;i<(ind)Gamma1.size();i++)
        MatGamma1.data()[i] = Gamma1[i];
    MatrixXd MatGamma2(r, Gamma2.size()/r);
    for(ind i=0;i<(ind)Gamma2.size();i++)
        MatGamma2.data()[i] = Gamma2[i];
    
    t2 = now();
    MatrixXd M1(H.rows(), MatGamma1.cols()), M2(H.rows(), MatGamma2.cols());
    #ifdef __MKL__
    mkl_matmul(false, false, H.rows(), H.cols(), MatGamma1.cols(), H.data(), MatGamma1.data(), M1.data());
    mkl_matmul(false, false, H.rows(), H.cols(), MatGamma2.cols(), H.data(), MatGamma2.data(), M2.data());
    #else
    M1 = H*MatGamma1;
    M2 = H*MatGamma2;
    #endif
    _times["b2"] += delta(t2);
    

    t2 = now();
    #pragma omp parallel for num_threads(8)
    for(ind i_xg1=0;i_xg1<len_xg1;i_xg1++) {
        bitset<m> xg1(i_xg1);
        bitset<m> xg1_flip = T_flip<rule_no, 0, m, m>(xg1);

        ind idx_gamma = linear_idx_rule<rule_no, 0, m, m, rule>(xg1);

        for(ind l=0;l<r;l++)
            out(i_xg1, l) = M1(xg1_flip.to_ulong(), l + r*idx_gamma) - M2(i_xg1, l + r*idx_gamma);
    }
    _times["DPV0"] += delta(t2);

    return out;
}


template<ind rule_no, ind d, int m, class rule>
MatrixXd compute_U1TDeltaPT(const LR& P, const MatrixXd& U1) {
    auto t1 = now();

    ind r = P.r();
    array<ind,2> n = P.n();
    ind len_xg2 = n[1];


    MatrixXd out(n[1], r);
    out.setZero();

    auto t2 = now();

    vector<double> Gamma3, Gamma4;
    Gamma34<rule_no, m, d, rule>(1, P.U, U1, Gamma3, Gamma4);
    _times["Gamma34"] += delta(t2);

    t2 = now();
    MatrixXd H(P.S.rows(), P.V.rows());
    #ifdef __MKL__
    mkl_matmul(false, true, P.S.rows(), P.S.cols(), P.V.rows(), P.S.data(), P.V.data(), H.data());
    #else
    H = P.S*P.V.transpose();
    #endif
    _times["a1"] += delta(t2);

    t2 = now();
    MatrixXd MatGamma3(r, Gamma3.size()/r);
    for(ind i=0;i<(ind)Gamma3.size();i++)
        MatGamma3.data()[i] = Gamma3[i];
    MatrixXd MatGamma4(r, Gamma4.size()/r);
    for(ind i=0;i<(ind)Gamma4.size();i++)
        MatGamma4.data()[i] = Gamma4[i];
    _times["a2"] += delta(t2);

    t2 = now();
    MatrixXd M3(H.cols(), MatGamma3.cols());
    MatrixXd M4(H.cols(), MatGamma4.cols());
    #ifdef __MKL__
    mkl_matmul(true, false, H.cols(), H.rows(), MatGamma3.cols(), H.data(), MatGamma3.data(), M3.data());
    mkl_matmul(true, false, H.cols(), H.rows(), MatGamma4.cols(), H.data(), MatGamma4.data(), M4.data());
    #else
    M3 = H.transpose()*MatGamma3;
    M4 = H.transpose()*MatGamma4;
    #endif
    _times["a3"] += delta(t2);

    auto t3 = now();
    #pragma omp parallel for num_threads(8)
    for(ind i_xg2=0;i_xg2<len_xg2;i_xg2++) {
        bitset<d-m> xg2(i_xg2);
        bitset<d-m> xg2_flip = T_flip<rule_no, 1, m, d-m>(xg2);
        ind i_xg2_flip = xg2_flip.to_ulong();

        ind idx_gamma = linear_idx_rule<rule_no, 1, m, d-m, rule>(xg2);

        for(ind l=0;l<r;l++)
            out(i_xg2, l) = M3(i_xg2_flip, l + r*idx_gamma) - M4(i_xg2, l + r*idx_gamma);
    }
    
    _times["U1TDPT"] += delta(t3);

    cout << "U1TDeltaPT: " << rule_no << " " << delta(t1,t2) << " " << delta(t2,t3) << " " << delta(t3) << " " << delta(t1) << endl;

    return out;
}




template<ind m, ind d, class rule>
LR time_step(double tau, const LR& P0) {
    array<ind,2> n = P0.n();
    ind          r = P0.r();
    LR P1;

    auto a = now();
    _times["Gamma12"] = 0.0;
    _times["Gamma12::alpha"] = 0.0;
    _times["Gamma12::pointwise"] = 0.0;
    _times["Gamma12::matmul"] = 0.0;
    _times["Gamma34"] = 0.0;
    _times["Gamma34::gamma"] = 0.0;
    _times["Gamma34::pointwise"] = 0.0;
    _times["Gamma34::matmul"] = 0.0;
    _times["U1TDPT"]  = 0.0;
    _times["DPV0"]    = 0.0;
    _times["QR"]      = 0.0;
    _times["a1"]      = 0.0;
    _times["a2"]      = 0.0;
    _times["a3"]      = 0.0;

    // step 1
    MatrixXd DeltaPV0 = MatrixXd::Zero(n[0], r);
    if constexpr(0 < d)  DeltaPV0 += compute_DeltaPV0<0, d, m, rule>(P0, P0.V);
    if constexpr(1 < d)  DeltaPV0 += compute_DeltaPV0<1, d, m, rule>(P0, P0.V);
    if constexpr(2 < d)  DeltaPV0 += compute_DeltaPV0<2, d, m, rule>(P0, P0.V);
    if constexpr(3 < d)  DeltaPV0 += compute_DeltaPV0<3, d, m, rule>(P0, P0.V);
    if constexpr(4 < d)  DeltaPV0 += compute_DeltaPV0<4, d, m, rule>(P0, P0.V);
    if constexpr(5 < d)  DeltaPV0 += compute_DeltaPV0<5, d, m, rule>(P0, P0.V);
    if constexpr(6 < d)  DeltaPV0 += compute_DeltaPV0<6, d, m, rule>(P0, P0.V);
    if constexpr(7 < d)  DeltaPV0 += compute_DeltaPV0<7, d, m, rule>(P0, P0.V);
    if constexpr(8 < d)  DeltaPV0 += compute_DeltaPV0<8, d, m, rule>(P0, P0.V);
    if constexpr(9 < d)  DeltaPV0 += compute_DeltaPV0<9, d, m, rule>(P0, P0.V);
    if constexpr(10 < d) DeltaPV0 += compute_DeltaPV0<10, d, m, rule>(P0, P0.V);
    if constexpr(11 < d) DeltaPV0 += compute_DeltaPV0<11, d, m, rule>(P0, P0.V);
    if constexpr(12 < d) DeltaPV0 += compute_DeltaPV0<12, d, m, rule>(P0, P0.V);
    if constexpr(13 < d) DeltaPV0 += compute_DeltaPV0<13, d, m, rule>(P0, P0.V);
    if constexpr(14 < d) DeltaPV0 += compute_DeltaPV0<14, d, m, rule>(P0, P0.V);
    if constexpr(15 < d) DeltaPV0 += compute_DeltaPV0<15, d, m, rule>(P0, P0.V);
    if constexpr(16 < d) DeltaPV0 += compute_DeltaPV0<16, d, m, rule>(P0, P0.V);
    if constexpr(17 < d) DeltaPV0 += compute_DeltaPV0<17, d, m, rule>(P0, P0.V);
    if constexpr(18 < d) DeltaPV0 += compute_DeltaPV0<18, d, m, rule>(P0, P0.V);
    if constexpr(19 < d) DeltaPV0 += compute_DeltaPV0<19, d, m, rule>(P0, P0.V);
    if constexpr(20 < d) DeltaPV0 += compute_DeltaPV0<20, d, m, rule>(P0, P0.V);
    if constexpr(21 < d) DeltaPV0 += compute_DeltaPV0<21, d, m, rule>(P0, P0.V);
    if constexpr(22 < d) DeltaPV0 += compute_DeltaPV0<22, d, m, rule>(P0, P0.V);
    if constexpr(23 < d) DeltaPV0 += compute_DeltaPV0<23, d, m, rule>(P0, P0.V);
    if constexpr(24 < d) DeltaPV0 += compute_DeltaPV0<24, d, m, rule>(P0, P0.V);
    if constexpr(25 < d) DeltaPV0 += compute_DeltaPV0<25, d, m, rule>(P0, P0.V);
    if constexpr(26 < d) DeltaPV0 += compute_DeltaPV0<26, d, m, rule>(P0, P0.V);
    if constexpr(27 < d) DeltaPV0 += compute_DeltaPV0<27, d, m, rule>(P0, P0.V);
    if constexpr(28 < d) DeltaPV0 += compute_DeltaPV0<28, d, m, rule>(P0, P0.V);
    if constexpr(29 < d) DeltaPV0 += compute_DeltaPV0<29, d, m, rule>(P0, P0.V);
    if constexpr(30 < d) DeltaPV0 += compute_DeltaPV0<30, d, m, rule>(P0, P0.V);
    if constexpr(31 < d) DeltaPV0 += compute_DeltaPV0<31, d, m, rule>(P0, P0.V);
    if constexpr(32 < d) DeltaPV0 += compute_DeltaPV0<32, d, m, rule>(P0, P0.V);
    if constexpr(33 < d) DeltaPV0 += compute_DeltaPV0<33, d, m, rule>(P0, P0.V);
    if constexpr(34 < d) DeltaPV0 += compute_DeltaPV0<34, d, m, rule>(P0, P0.V);
    if constexpr(35 < d) DeltaPV0 += compute_DeltaPV0<35, d, m, rule>(P0, P0.V);
    if constexpr(36 < d) DeltaPV0 += compute_DeltaPV0<36, d, m, rule>(P0, P0.V);
    if constexpr(37 < d) DeltaPV0 += compute_DeltaPV0<37, d, m, rule>(P0, P0.V);
    if constexpr(38 < d) DeltaPV0 += compute_DeltaPV0<38, d, m, rule>(P0, P0.V);
    if constexpr(39 < d) DeltaPV0 += compute_DeltaPV0<39, d, m, rule>(P0, P0.V);
    if constexpr(40 < d) DeltaPV0 += compute_DeltaPV0<40, d, m, rule>(P0, P0.V);
    if constexpr(41 < d) DeltaPV0 += compute_DeltaPV0<41, d, m, rule>(P0, P0.V);
    if constexpr(42 < d) DeltaPV0 += compute_DeltaPV0<42, d, m, rule>(P0, P0.V);
    if constexpr(43 < d) DeltaPV0 += compute_DeltaPV0<43, d, m, rule>(P0, P0.V);
    if constexpr(44 < d) DeltaPV0 += compute_DeltaPV0<44, d, m, rule>(P0, P0.V);
    if constexpr(45 < d) DeltaPV0 += compute_DeltaPV0<45, d, m, rule>(P0, P0.V);
    if constexpr(46 < d) DeltaPV0 += compute_DeltaPV0<46, d, m, rule>(P0, P0.V);
    if constexpr(47 < d) DeltaPV0 += compute_DeltaPV0<47, d, m, rule>(P0, P0.V);
    if constexpr(48 < d) DeltaPV0 += compute_DeltaPV0<48, d, m, rule>(P0, P0.V);
    if constexpr(49 < d) DeltaPV0 += compute_DeltaPV0<49, d, m, rule>(P0, P0.V);
    if constexpr(50 < d) DeltaPV0 += compute_DeltaPV0<50, d, m, rule>(P0, P0.V);
    if constexpr(51 < d) DeltaPV0 += compute_DeltaPV0<51, d, m, rule>(P0, P0.V);
    if constexpr(52 < d) DeltaPV0 += compute_DeltaPV0<52, d, m, rule>(P0, P0.V);
    if constexpr(53 < d) DeltaPV0 += compute_DeltaPV0<53, d, m, rule>(P0, P0.V);
    if constexpr(54 < d) DeltaPV0 += compute_DeltaPV0<54, d, m, rule>(P0, P0.V);
    if constexpr(55 < d) DeltaPV0 += compute_DeltaPV0<55, d, m, rule>(P0, P0.V);
    if constexpr(56 < d) DeltaPV0 += compute_DeltaPV0<56, d, m, rule>(P0, P0.V);
    if constexpr(57 < d) DeltaPV0 += compute_DeltaPV0<57, d, m, rule>(P0, P0.V);
    if constexpr(58 < d) DeltaPV0 += compute_DeltaPV0<58, d, m, rule>(P0, P0.V);
    if constexpr(59 < d) DeltaPV0 += compute_DeltaPV0<59, d, m, rule>(P0, P0.V);
    if constexpr(60 < d) DeltaPV0 += compute_DeltaPV0<60, d, m, rule>(P0, P0.V);
    if constexpr(61 < d) DeltaPV0 += compute_DeltaPV0<61, d, m, rule>(P0, P0.V);
    if constexpr(62 < d) DeltaPV0 += compute_DeltaPV0<62, d, m, rule>(P0, P0.V);
    if constexpr(63 < d) DeltaPV0 += compute_DeltaPV0<63, d, m, rule>(P0, P0.V);
    MatrixXd K1 = P0.U*P0.S + tau*DeltaPV0;

    cout << "DeltaPV0: " << delta(a) << endl;
    cout << "b1 " << _times["b1"] << endl;
    cout << "b2 " << _times["b2"] << endl;
    cout << "Gamma12: " << _times["Gamma12"] << endl;
    cout << "Gamma12::alpha: " << _times["Gamma12::alpha"] << endl;
    cout << "Gamma12::pointwise: " << _times["Gamma12::pointwise"] << endl;
    cout << "Gamma12::matmul: " << _times["Gamma12::matmul"] << endl;
    cout << "DPV0: " << _times["DPV0"] << endl;
    auto b = now();

    // step 2
    rectangular_QR qr1 = rectangular_QR(n[0], r);
    qr1.compute(K1);
    P1.U = qr1.Q();
    MatrixXd S1hat = qr1.R();
    
    cout << "QR: " << delta(b) << endl;
    _times["QR"] += delta(b);
    auto c = now();

    // step 3
    MatrixXd S0tilde = S1hat - tau*P1.U.transpose()*DeltaPV0;

    cout << "S0tilde: " << delta(c) << endl;
    auto dd = now();

    // step 4
    MatrixXd U1TDeltaPT = MatrixXd::Zero(n[1], r);
    if constexpr(0 < d)   U1TDeltaPT += compute_U1TDeltaPT<0, d, m, rule>(P0, P1.U);
    if constexpr(1 < d)   U1TDeltaPT += compute_U1TDeltaPT<1, d, m, rule>(P0, P1.U);
    if constexpr(2 < d)   U1TDeltaPT += compute_U1TDeltaPT<2, d, m, rule>(P0, P1.U);
    if constexpr(3 < d)   U1TDeltaPT += compute_U1TDeltaPT<3, d, m, rule>(P0, P1.U);
    if constexpr(4 < d)   U1TDeltaPT += compute_U1TDeltaPT<4, d, m, rule>(P0, P1.U);
    if constexpr(5 < d)   U1TDeltaPT += compute_U1TDeltaPT<5, d, m, rule>(P0, P1.U);
    if constexpr(6 < d)   U1TDeltaPT += compute_U1TDeltaPT<6, d, m, rule>(P0, P1.U);
    if constexpr(7 < d)   U1TDeltaPT += compute_U1TDeltaPT<7, d, m, rule>(P0, P1.U);
    if constexpr(8 < d)   U1TDeltaPT += compute_U1TDeltaPT<8, d, m, rule>(P0, P1.U);
    if constexpr(9 < d)   U1TDeltaPT += compute_U1TDeltaPT<9, d, m, rule>(P0, P1.U);
    if constexpr(10 < d)  U1TDeltaPT += compute_U1TDeltaPT<10, d, m, rule>(P0, P1.U);
    if constexpr(11 < d)  U1TDeltaPT += compute_U1TDeltaPT<11, d, m, rule>(P0, P1.U);
    if constexpr(12 < d)  U1TDeltaPT += compute_U1TDeltaPT<12, d, m, rule>(P0, P1.U);
    if constexpr(13 < d)  U1TDeltaPT += compute_U1TDeltaPT<13, d, m, rule>(P0, P1.U);
    if constexpr(14 < d)  U1TDeltaPT += compute_U1TDeltaPT<14, d, m, rule>(P0, P1.U);
    if constexpr(15 < d)  U1TDeltaPT += compute_U1TDeltaPT<15, d, m, rule>(P0, P1.U);
    if constexpr(16 < d)  U1TDeltaPT += compute_U1TDeltaPT<16, d, m, rule>(P0, P1.U);
    if constexpr(17 < d)  U1TDeltaPT += compute_U1TDeltaPT<17, d, m, rule>(P0, P1.U);
    if constexpr(18 < d)  U1TDeltaPT += compute_U1TDeltaPT<18, d, m, rule>(P0, P1.U);
    if constexpr(19 < d)  U1TDeltaPT += compute_U1TDeltaPT<19, d, m, rule>(P0, P1.U);
    if constexpr(20 < d)  U1TDeltaPT += compute_U1TDeltaPT<20, d, m, rule>(P0, P1.U);
    if constexpr(21 < d)  U1TDeltaPT += compute_U1TDeltaPT<21, d, m, rule>(P0, P1.U);
    if constexpr(22 < d)  U1TDeltaPT += compute_U1TDeltaPT<22, d, m, rule>(P0, P1.U);
    if constexpr(23 < d)  U1TDeltaPT += compute_U1TDeltaPT<23, d, m, rule>(P0, P1.U);
    if constexpr(24 < d)  U1TDeltaPT += compute_U1TDeltaPT<24, d, m, rule>(P0, P1.U);
    if constexpr(25 < d)  U1TDeltaPT += compute_U1TDeltaPT<25, d, m, rule>(P0, P1.U);
    if constexpr(26 < d)  U1TDeltaPT += compute_U1TDeltaPT<26, d, m, rule>(P0, P1.U);
    if constexpr(27 < d)  U1TDeltaPT += compute_U1TDeltaPT<27, d, m, rule>(P0, P1.U);
    if constexpr(28 < d)  U1TDeltaPT += compute_U1TDeltaPT<28, d, m, rule>(P0, P1.U);
    if constexpr(29 < d)  U1TDeltaPT += compute_U1TDeltaPT<29, d, m, rule>(P0, P1.U);
    if constexpr(30 < d)  U1TDeltaPT += compute_U1TDeltaPT<30, d, m, rule>(P0, P1.U);
    if constexpr(31 < d)  U1TDeltaPT += compute_U1TDeltaPT<31, d, m, rule>(P0, P1.U);
    if constexpr(32 < d)  U1TDeltaPT += compute_U1TDeltaPT<32, d, m, rule>(P0, P1.U);
    if constexpr(33 < d)  U1TDeltaPT += compute_U1TDeltaPT<33, d, m, rule>(P0, P1.U);
    if constexpr(34 < d)  U1TDeltaPT += compute_U1TDeltaPT<34, d, m, rule>(P0, P1.U);
    if constexpr(35 < d)  U1TDeltaPT += compute_U1TDeltaPT<35, d, m, rule>(P0, P1.U);
    if constexpr(36 < d)  U1TDeltaPT += compute_U1TDeltaPT<36, d, m, rule>(P0, P1.U);
    if constexpr(37 < d)  U1TDeltaPT += compute_U1TDeltaPT<37, d, m, rule>(P0, P1.U);
    if constexpr(38 < d)  U1TDeltaPT += compute_U1TDeltaPT<38, d, m, rule>(P0, P1.U);
    if constexpr(39 < d)  U1TDeltaPT += compute_U1TDeltaPT<39, d, m, rule>(P0, P1.U);
    if constexpr(40 < d)  U1TDeltaPT += compute_U1TDeltaPT<40, d, m, rule>(P0, P1.U);
    if constexpr(41 < d)  U1TDeltaPT += compute_U1TDeltaPT<41, d, m, rule>(P0, P1.U);
    if constexpr(42 < d)  U1TDeltaPT += compute_U1TDeltaPT<42, d, m, rule>(P0, P1.U);
    if constexpr(43 < d)  U1TDeltaPT += compute_U1TDeltaPT<43, d, m, rule>(P0, P1.U);
    if constexpr(44 < d)  U1TDeltaPT += compute_U1TDeltaPT<44, d, m, rule>(P0, P1.U);
    if constexpr(45 < d)  U1TDeltaPT += compute_U1TDeltaPT<45, d, m, rule>(P0, P1.U);
    if constexpr(46 < d)  U1TDeltaPT += compute_U1TDeltaPT<46, d, m, rule>(P0, P1.U);
    if constexpr(47 < d)  U1TDeltaPT += compute_U1TDeltaPT<47, d, m, rule>(P0, P1.U);
    if constexpr(48 < d)  U1TDeltaPT += compute_U1TDeltaPT<48, d, m, rule>(P0, P1.U);
    if constexpr(49 < d)  U1TDeltaPT += compute_U1TDeltaPT<49, d, m, rule>(P0, P1.U);
    if constexpr(50 < d)  U1TDeltaPT += compute_U1TDeltaPT<50, d, m, rule>(P0, P1.U);
    if constexpr(51 < d)  U1TDeltaPT += compute_U1TDeltaPT<51, d, m, rule>(P0, P1.U);
    if constexpr(52 < d)  U1TDeltaPT += compute_U1TDeltaPT<52, d, m, rule>(P0, P1.U);
    if constexpr(53 < d)  U1TDeltaPT += compute_U1TDeltaPT<53, d, m, rule>(P0, P1.U);
    if constexpr(54 < d)  U1TDeltaPT += compute_U1TDeltaPT<54, d, m, rule>(P0, P1.U);
    if constexpr(55 < d)  U1TDeltaPT += compute_U1TDeltaPT<55, d, m, rule>(P0, P1.U);
    if constexpr(56 < d)  U1TDeltaPT += compute_U1TDeltaPT<56, d, m, rule>(P0, P1.U);
    if constexpr(57 < d)  U1TDeltaPT += compute_U1TDeltaPT<57, d, m, rule>(P0, P1.U);
    if constexpr(58 < d)  U1TDeltaPT += compute_U1TDeltaPT<58, d, m, rule>(P0, P1.U);
    if constexpr(59 < d)  U1TDeltaPT += compute_U1TDeltaPT<59, d, m, rule>(P0, P1.U);
    if constexpr(60 < d)  U1TDeltaPT += compute_U1TDeltaPT<60, d, m, rule>(P0, P1.U);
    if constexpr(61 < d)  U1TDeltaPT += compute_U1TDeltaPT<61, d, m, rule>(P0, P1.U);
    if constexpr(62 < d)  U1TDeltaPT += compute_U1TDeltaPT<62, d, m, rule>(P0, P1.U);
    if constexpr(63 < d)  U1TDeltaPT += compute_U1TDeltaPT<63, d, m, rule>(P0, P1.U);
    MatrixXd L1 = P0.V*S0tilde.transpose() + tau*U1TDeltaPT;
    
    cout << "U1TDeltaPT: " << delta(dd) << endl;
    auto e = now();

    // step 5
    rectangular_QR qr2 = rectangular_QR(n[1], r);
    qr2.compute(L1);
    P1.V = qr2.Q();
    P1.S = qr2.R().transpose();
    
    cout << "QR2: " << delta(e) << endl;
    _times["QR"] += delta(e);
    
    cout << "Gamma34: " << _times["Gamma34"] << endl;
    cout << "Gamma34::alpha: " << _times["Gamma34::alpha"] << endl;
    cout << "Gamma34::pointwise: " << _times["Gamma34::pointwise"] << endl;
    cout << "Gamma34::matmul: " << _times["Gamma34::matmul"] << endl;
    cout << "DPV0: " << _times["DPV0"] << endl;
    cout << "U1TDPT: " << _times["U1TDPT"] << endl;
    cout << "QR: " << _times["QR"] << endl;
    cout << "a1: " << _times["a1"] << endl;
    cout << "a2: " << _times["a2"] << endl;
    cout << "a3: " << _times["a3"] << endl;

    return P1;
}




template<ind m, ind d, class rule>
LR time_step_unconventional(double tau, const LR& P0) {
    array<ind,2> n = P0.n();
    ind          r = P0.r();
    LR P1;

    auto a = now();
    _times["Gamma12"] = 0.0;
    _times["Gamma12::alpha"] = 0.0;
    _times["Gamma12::pointwise"] = 0.0;
    _times["Gamma12::matmul"] = 0.0;
    _times["Gamma34"] = 0.0;
    _times["Gamma34::gamma"] = 0.0;
    _times["Gamma34::pointwise"] = 0.0;
    _times["Gamma34::matmul"] = 0.0;
    _times["U1TDPT"]  = 0.0;
    _times["DPV0"]    = 0.0;
    _times["QR"]      = 0.0;
    _times["a1"]      = 0.0;
    _times["a2"]      = 0.0;
    _times["a3"]      = 0.0;

    // K step
    MatrixXd DeltaPV0 = MatrixXd::Zero(n[0], r);
    if constexpr(0 < d)  DeltaPV0 += compute_DeltaPV0<0, d, m, rule>(P0, P0.V);
    if constexpr(1 < d)  DeltaPV0 += compute_DeltaPV0<1, d, m, rule>(P0, P0.V);
    if constexpr(2 < d)  DeltaPV0 += compute_DeltaPV0<2, d, m, rule>(P0, P0.V);
    if constexpr(3 < d)  DeltaPV0 += compute_DeltaPV0<3, d, m, rule>(P0, P0.V);
    if constexpr(4 < d)  DeltaPV0 += compute_DeltaPV0<4, d, m, rule>(P0, P0.V);
    if constexpr(5 < d)  DeltaPV0 += compute_DeltaPV0<5, d, m, rule>(P0, P0.V);
    if constexpr(6 < d)  DeltaPV0 += compute_DeltaPV0<6, d, m, rule>(P0, P0.V);
    if constexpr(7 < d)  DeltaPV0 += compute_DeltaPV0<7, d, m, rule>(P0, P0.V);
    if constexpr(8 < d)  DeltaPV0 += compute_DeltaPV0<8, d, m, rule>(P0, P0.V);
    if constexpr(9 < d)  DeltaPV0 += compute_DeltaPV0<9, d, m, rule>(P0, P0.V);
    if constexpr(10 < d) DeltaPV0 += compute_DeltaPV0<10, d, m, rule>(P0, P0.V);
    if constexpr(11 < d) DeltaPV0 += compute_DeltaPV0<11, d, m, rule>(P0, P0.V);
    if constexpr(12 < d) DeltaPV0 += compute_DeltaPV0<12, d, m, rule>(P0, P0.V);
    if constexpr(13 < d) DeltaPV0 += compute_DeltaPV0<13, d, m, rule>(P0, P0.V);
    if constexpr(14 < d) DeltaPV0 += compute_DeltaPV0<14, d, m, rule>(P0, P0.V);
    if constexpr(15 < d) DeltaPV0 += compute_DeltaPV0<15, d, m, rule>(P0, P0.V);
    if constexpr(16 < d) DeltaPV0 += compute_DeltaPV0<16, d, m, rule>(P0, P0.V);
    if constexpr(17 < d) DeltaPV0 += compute_DeltaPV0<17, d, m, rule>(P0, P0.V);
    if constexpr(18 < d) DeltaPV0 += compute_DeltaPV0<18, d, m, rule>(P0, P0.V);
    if constexpr(19 < d) DeltaPV0 += compute_DeltaPV0<19, d, m, rule>(P0, P0.V);
    if constexpr(20 < d) DeltaPV0 += compute_DeltaPV0<20, d, m, rule>(P0, P0.V);
    if constexpr(21 < d) DeltaPV0 += compute_DeltaPV0<21, d, m, rule>(P0, P0.V);
    if constexpr(22 < d) DeltaPV0 += compute_DeltaPV0<22, d, m, rule>(P0, P0.V);
    if constexpr(23 < d) DeltaPV0 += compute_DeltaPV0<23, d, m, rule>(P0, P0.V);
    if constexpr(24 < d) DeltaPV0 += compute_DeltaPV0<24, d, m, rule>(P0, P0.V);
    if constexpr(25 < d) DeltaPV0 += compute_DeltaPV0<25, d, m, rule>(P0, P0.V);
    if constexpr(26 < d) DeltaPV0 += compute_DeltaPV0<26, d, m, rule>(P0, P0.V);
    if constexpr(27 < d) DeltaPV0 += compute_DeltaPV0<27, d, m, rule>(P0, P0.V);
    if constexpr(28 < d) DeltaPV0 += compute_DeltaPV0<28, d, m, rule>(P0, P0.V);
    if constexpr(29 < d) DeltaPV0 += compute_DeltaPV0<29, d, m, rule>(P0, P0.V);
    if constexpr(30 < d) DeltaPV0 += compute_DeltaPV0<30, d, m, rule>(P0, P0.V);
    if constexpr(31 < d) DeltaPV0 += compute_DeltaPV0<31, d, m, rule>(P0, P0.V);
    if constexpr(32 < d) DeltaPV0 += compute_DeltaPV0<32, d, m, rule>(P0, P0.V);
    if constexpr(33 < d) DeltaPV0 += compute_DeltaPV0<33, d, m, rule>(P0, P0.V);
    if constexpr(34 < d) DeltaPV0 += compute_DeltaPV0<34, d, m, rule>(P0, P0.V);
    if constexpr(35 < d) DeltaPV0 += compute_DeltaPV0<35, d, m, rule>(P0, P0.V);
    if constexpr(36 < d) DeltaPV0 += compute_DeltaPV0<36, d, m, rule>(P0, P0.V);
    if constexpr(37 < d) DeltaPV0 += compute_DeltaPV0<37, d, m, rule>(P0, P0.V);
    if constexpr(38 < d) DeltaPV0 += compute_DeltaPV0<38, d, m, rule>(P0, P0.V);
    if constexpr(39 < d) DeltaPV0 += compute_DeltaPV0<39, d, m, rule>(P0, P0.V);
    if constexpr(40 < d) DeltaPV0 += compute_DeltaPV0<40, d, m, rule>(P0, P0.V);
    if constexpr(41 < d) DeltaPV0 += compute_DeltaPV0<41, d, m, rule>(P0, P0.V);
    if constexpr(42 < d) DeltaPV0 += compute_DeltaPV0<42, d, m, rule>(P0, P0.V);
    if constexpr(43 < d) DeltaPV0 += compute_DeltaPV0<43, d, m, rule>(P0, P0.V);
    if constexpr(44 < d) DeltaPV0 += compute_DeltaPV0<44, d, m, rule>(P0, P0.V);
    if constexpr(45 < d) DeltaPV0 += compute_DeltaPV0<45, d, m, rule>(P0, P0.V);
    if constexpr(46 < d) DeltaPV0 += compute_DeltaPV0<46, d, m, rule>(P0, P0.V);
    if constexpr(47 < d) DeltaPV0 += compute_DeltaPV0<47, d, m, rule>(P0, P0.V);
    if constexpr(48 < d) DeltaPV0 += compute_DeltaPV0<48, d, m, rule>(P0, P0.V);
    if constexpr(49 < d) DeltaPV0 += compute_DeltaPV0<49, d, m, rule>(P0, P0.V);
    if constexpr(50 < d) DeltaPV0 += compute_DeltaPV0<50, d, m, rule>(P0, P0.V);
    if constexpr(51 < d) DeltaPV0 += compute_DeltaPV0<51, d, m, rule>(P0, P0.V);
    if constexpr(52 < d) DeltaPV0 += compute_DeltaPV0<52, d, m, rule>(P0, P0.V);
    if constexpr(53 < d) DeltaPV0 += compute_DeltaPV0<53, d, m, rule>(P0, P0.V);
    if constexpr(54 < d) DeltaPV0 += compute_DeltaPV0<54, d, m, rule>(P0, P0.V);
    if constexpr(55 < d) DeltaPV0 += compute_DeltaPV0<55, d, m, rule>(P0, P0.V);
    if constexpr(56 < d) DeltaPV0 += compute_DeltaPV0<56, d, m, rule>(P0, P0.V);
    if constexpr(57 < d) DeltaPV0 += compute_DeltaPV0<57, d, m, rule>(P0, P0.V);
    if constexpr(58 < d) DeltaPV0 += compute_DeltaPV0<58, d, m, rule>(P0, P0.V);
    if constexpr(59 < d) DeltaPV0 += compute_DeltaPV0<59, d, m, rule>(P0, P0.V);
    if constexpr(60 < d) DeltaPV0 += compute_DeltaPV0<60, d, m, rule>(P0, P0.V);
    if constexpr(61 < d) DeltaPV0 += compute_DeltaPV0<61, d, m, rule>(P0, P0.V);
    if constexpr(62 < d) DeltaPV0 += compute_DeltaPV0<62, d, m, rule>(P0, P0.V);
    if constexpr(63 < d) DeltaPV0 += compute_DeltaPV0<63, d, m, rule>(P0, P0.V);
    MatrixXd K1 = P0.U*P0.S + tau*DeltaPV0;

    cout << "DeltaPV0: " << delta(a) << endl;
    cout << "b1 " << _times["b1"] << endl;
    cout << "b2 " << _times["b2"] << endl;
    cout << "Gamma12: " << _times["Gamma12"] << endl;
    cout << "Gamma12::alpha: " << _times["Gamma12::alpha"] << endl;
    cout << "Gamma12::pointwise: " << _times["Gamma12::pointwise"] << endl;
    cout << "Gamma12::matmul: " << _times["Gamma12::matmul"] << endl;
    cout << "DPV0: " << _times["DPV0"] << endl;

    // QR
    auto b = now();
    rectangular_QR qr1 = rectangular_QR(n[0], r);
    qr1.compute(K1);
    P1.U = qr1.Q();
    
    cout << "QR: " << delta(b) << endl;
    _times["QR"] += delta(b);
    auto c = now();

    // L step
    MatrixXd U1TDeltaPT = MatrixXd::Zero(n[1], r);
    if constexpr(0 < d)   U1TDeltaPT += compute_U1TDeltaPT<0, d, m, rule>(P0, P0.U);
    if constexpr(1 < d)   U1TDeltaPT += compute_U1TDeltaPT<1, d, m, rule>(P0, P0.U);
    if constexpr(2 < d)   U1TDeltaPT += compute_U1TDeltaPT<2, d, m, rule>(P0, P0.U);
    if constexpr(3 < d)   U1TDeltaPT += compute_U1TDeltaPT<3, d, m, rule>(P0, P0.U);
    if constexpr(4 < d)   U1TDeltaPT += compute_U1TDeltaPT<4, d, m, rule>(P0, P0.U);
    if constexpr(5 < d)   U1TDeltaPT += compute_U1TDeltaPT<5, d, m, rule>(P0, P0.U);
    if constexpr(6 < d)   U1TDeltaPT += compute_U1TDeltaPT<6, d, m, rule>(P0, P0.U);
    if constexpr(7 < d)   U1TDeltaPT += compute_U1TDeltaPT<7, d, m, rule>(P0, P0.U);
    if constexpr(8 < d)   U1TDeltaPT += compute_U1TDeltaPT<8, d, m, rule>(P0, P0.U);
    if constexpr(9 < d)   U1TDeltaPT += compute_U1TDeltaPT<9, d, m, rule>(P0, P0.U);
    if constexpr(10 < d)  U1TDeltaPT += compute_U1TDeltaPT<10, d, m, rule>(P0, P0.U);
    if constexpr(11 < d)  U1TDeltaPT += compute_U1TDeltaPT<11, d, m, rule>(P0, P0.U);
    if constexpr(12 < d)  U1TDeltaPT += compute_U1TDeltaPT<12, d, m, rule>(P0, P0.U);
    if constexpr(13 < d)  U1TDeltaPT += compute_U1TDeltaPT<13, d, m, rule>(P0, P0.U);
    if constexpr(14 < d)  U1TDeltaPT += compute_U1TDeltaPT<14, d, m, rule>(P0, P0.U);
    if constexpr(15 < d)  U1TDeltaPT += compute_U1TDeltaPT<15, d, m, rule>(P0, P0.U);
    if constexpr(16 < d)  U1TDeltaPT += compute_U1TDeltaPT<16, d, m, rule>(P0, P0.U);
    if constexpr(17 < d)  U1TDeltaPT += compute_U1TDeltaPT<17, d, m, rule>(P0, P0.U);
    if constexpr(18 < d)  U1TDeltaPT += compute_U1TDeltaPT<18, d, m, rule>(P0, P0.U);
    if constexpr(19 < d)  U1TDeltaPT += compute_U1TDeltaPT<19, d, m, rule>(P0, P0.U);
    if constexpr(20 < d)  U1TDeltaPT += compute_U1TDeltaPT<20, d, m, rule>(P0, P0.U);
    if constexpr(21 < d)  U1TDeltaPT += compute_U1TDeltaPT<21, d, m, rule>(P0, P0.U);
    if constexpr(22 < d)  U1TDeltaPT += compute_U1TDeltaPT<22, d, m, rule>(P0, P0.U);
    if constexpr(23 < d)  U1TDeltaPT += compute_U1TDeltaPT<23, d, m, rule>(P0, P0.U);
    if constexpr(24 < d)  U1TDeltaPT += compute_U1TDeltaPT<24, d, m, rule>(P0, P0.U);
    if constexpr(25 < d)  U1TDeltaPT += compute_U1TDeltaPT<25, d, m, rule>(P0, P0.U);
    if constexpr(26 < d)  U1TDeltaPT += compute_U1TDeltaPT<26, d, m, rule>(P0, P0.U);
    if constexpr(27 < d)  U1TDeltaPT += compute_U1TDeltaPT<27, d, m, rule>(P0, P0.U);
    if constexpr(28 < d)  U1TDeltaPT += compute_U1TDeltaPT<28, d, m, rule>(P0, P0.U);
    if constexpr(29 < d)  U1TDeltaPT += compute_U1TDeltaPT<29, d, m, rule>(P0, P0.U);
    if constexpr(30 < d)  U1TDeltaPT += compute_U1TDeltaPT<30, d, m, rule>(P0, P0.U);
    if constexpr(31 < d)  U1TDeltaPT += compute_U1TDeltaPT<31, d, m, rule>(P0, P0.U);
    if constexpr(32 < d)  U1TDeltaPT += compute_U1TDeltaPT<32, d, m, rule>(P0, P0.U);
    if constexpr(33 < d)  U1TDeltaPT += compute_U1TDeltaPT<33, d, m, rule>(P0, P0.U);
    if constexpr(34 < d)  U1TDeltaPT += compute_U1TDeltaPT<34, d, m, rule>(P0, P0.U);
    if constexpr(35 < d)  U1TDeltaPT += compute_U1TDeltaPT<35, d, m, rule>(P0, P0.U);
    if constexpr(36 < d)  U1TDeltaPT += compute_U1TDeltaPT<36, d, m, rule>(P0, P0.U);
    if constexpr(37 < d)  U1TDeltaPT += compute_U1TDeltaPT<37, d, m, rule>(P0, P0.U);
    if constexpr(38 < d)  U1TDeltaPT += compute_U1TDeltaPT<38, d, m, rule>(P0, P0.U);
    if constexpr(39 < d)  U1TDeltaPT += compute_U1TDeltaPT<39, d, m, rule>(P0, P0.U);
    if constexpr(40 < d)  U1TDeltaPT += compute_U1TDeltaPT<40, d, m, rule>(P0, P0.U);
    if constexpr(41 < d)  U1TDeltaPT += compute_U1TDeltaPT<41, d, m, rule>(P0, P0.U);
    if constexpr(42 < d)  U1TDeltaPT += compute_U1TDeltaPT<42, d, m, rule>(P0, P0.U);
    if constexpr(43 < d)  U1TDeltaPT += compute_U1TDeltaPT<43, d, m, rule>(P0, P0.U);
    if constexpr(44 < d)  U1TDeltaPT += compute_U1TDeltaPT<44, d, m, rule>(P0, P0.U);
    if constexpr(45 < d)  U1TDeltaPT += compute_U1TDeltaPT<45, d, m, rule>(P0, P0.U);
    if constexpr(46 < d)  U1TDeltaPT += compute_U1TDeltaPT<46, d, m, rule>(P0, P0.U);
    if constexpr(47 < d)  U1TDeltaPT += compute_U1TDeltaPT<47, d, m, rule>(P0, P0.U);
    if constexpr(48 < d)  U1TDeltaPT += compute_U1TDeltaPT<48, d, m, rule>(P0, P0.U);
    if constexpr(49 < d)  U1TDeltaPT += compute_U1TDeltaPT<49, d, m, rule>(P0, P0.U);
    if constexpr(50 < d)  U1TDeltaPT += compute_U1TDeltaPT<50, d, m, rule>(P0, P0.U);
    if constexpr(51 < d)  U1TDeltaPT += compute_U1TDeltaPT<51, d, m, rule>(P0, P0.U);
    if constexpr(52 < d)  U1TDeltaPT += compute_U1TDeltaPT<52, d, m, rule>(P0, P0.U);
    if constexpr(53 < d)  U1TDeltaPT += compute_U1TDeltaPT<53, d, m, rule>(P0, P0.U);
    if constexpr(54 < d)  U1TDeltaPT += compute_U1TDeltaPT<54, d, m, rule>(P0, P0.U);
    if constexpr(55 < d)  U1TDeltaPT += compute_U1TDeltaPT<55, d, m, rule>(P0, P0.U);
    if constexpr(56 < d)  U1TDeltaPT += compute_U1TDeltaPT<56, d, m, rule>(P0, P0.U);
    if constexpr(57 < d)  U1TDeltaPT += compute_U1TDeltaPT<57, d, m, rule>(P0, P0.U);
    if constexpr(58 < d)  U1TDeltaPT += compute_U1TDeltaPT<58, d, m, rule>(P0, P0.U);
    if constexpr(59 < d)  U1TDeltaPT += compute_U1TDeltaPT<59, d, m, rule>(P0, P0.U);
    if constexpr(60 < d)  U1TDeltaPT += compute_U1TDeltaPT<60, d, m, rule>(P0, P0.U);
    if constexpr(61 < d)  U1TDeltaPT += compute_U1TDeltaPT<61, d, m, rule>(P0, P0.U);
    if constexpr(62 < d)  U1TDeltaPT += compute_U1TDeltaPT<62, d, m, rule>(P0, P0.U);
    if constexpr(63 < d)  U1TDeltaPT += compute_U1TDeltaPT<63, d, m, rule>(P0, P0.U);
    MatrixXd L1 = P0.V*P0.S.transpose() + tau*U1TDeltaPT;
    
    auto e = now();

    // QR
    rectangular_QR qr2 = rectangular_QR(n[1], r);
    qr2.compute(L1);
    P1.V = qr2.Q();
    
    cout << "QR2: " << delta(e) << endl;
    _times["QR"] += delta(e);


    // S step
    MatrixXd M = P1.U.transpose()*P0.U;
    MatrixXd N = P1.V.transpose()*P0.V;
    P1.S = M*P0.S*N.transpose();

    MatrixXd DeltaPV_unconv = MatrixXd::Zero(n[0], r);
    if constexpr(0 < d)  DeltaPV_unconv += compute_DeltaPV0<0, d, m, rule>(P1,  P1.V);
    if constexpr(1 < d)  DeltaPV_unconv += compute_DeltaPV0<1, d, m, rule>(P1,  P1.V);
    if constexpr(2 < d)  DeltaPV_unconv += compute_DeltaPV0<2, d, m, rule>(P1,  P1.V);
    if constexpr(3 < d)  DeltaPV_unconv += compute_DeltaPV0<3, d, m, rule>(P1,  P1.V);
    if constexpr(4 < d)  DeltaPV_unconv += compute_DeltaPV0<4, d, m, rule>(P1,  P1.V);
    if constexpr(5 < d)  DeltaPV_unconv += compute_DeltaPV0<5, d, m, rule>(P1,  P1.V);
    if constexpr(6 < d)  DeltaPV_unconv += compute_DeltaPV0<6, d, m, rule>(P1,  P1.V);
    if constexpr(7 < d)  DeltaPV_unconv += compute_DeltaPV0<7, d, m, rule>(P1,  P1.V);
    if constexpr(8 < d)  DeltaPV_unconv += compute_DeltaPV0<8, d, m, rule>(P1,  P1.V);
    if constexpr(9 < d)  DeltaPV_unconv += compute_DeltaPV0<9, d, m, rule>(P1,  P1.V);
    if constexpr(10 < d) DeltaPV_unconv += compute_DeltaPV0<10, d, m, rule>(P1, P1.V);
    if constexpr(11 < d) DeltaPV_unconv += compute_DeltaPV0<11, d, m, rule>(P1, P1.V);
    if constexpr(12 < d) DeltaPV_unconv += compute_DeltaPV0<12, d, m, rule>(P1, P1.V);
    if constexpr(13 < d) DeltaPV_unconv += compute_DeltaPV0<13, d, m, rule>(P1, P1.V);
    if constexpr(14 < d) DeltaPV_unconv += compute_DeltaPV0<14, d, m, rule>(P1, P1.V);
    if constexpr(15 < d) DeltaPV_unconv += compute_DeltaPV0<15, d, m, rule>(P1, P1.V);
    if constexpr(16 < d) DeltaPV_unconv += compute_DeltaPV0<16, d, m, rule>(P1, P1.V);
    if constexpr(17 < d) DeltaPV_unconv += compute_DeltaPV0<17, d, m, rule>(P1, P1.V);
    if constexpr(18 < d) DeltaPV_unconv += compute_DeltaPV0<18, d, m, rule>(P1, P1.V);
    if constexpr(19 < d) DeltaPV_unconv += compute_DeltaPV0<19, d, m, rule>(P1, P1.V);
    if constexpr(20 < d) DeltaPV_unconv += compute_DeltaPV0<20, d, m, rule>(P1, P1.V);
    if constexpr(21 < d) DeltaPV_unconv += compute_DeltaPV0<21, d, m, rule>(P1, P1.V);
    if constexpr(22 < d) DeltaPV_unconv += compute_DeltaPV0<22, d, m, rule>(P1, P1.V);
    if constexpr(23 < d) DeltaPV_unconv += compute_DeltaPV0<23, d, m, rule>(P1, P1.V);
    if constexpr(24 < d) DeltaPV_unconv += compute_DeltaPV0<24, d, m, rule>(P1, P1.V);
    if constexpr(25 < d) DeltaPV_unconv += compute_DeltaPV0<25, d, m, rule>(P1, P1.V);
    if constexpr(26 < d) DeltaPV_unconv += compute_DeltaPV0<26, d, m, rule>(P1, P1.V);
    if constexpr(27 < d) DeltaPV_unconv += compute_DeltaPV0<27, d, m, rule>(P1, P1.V);
    if constexpr(28 < d) DeltaPV_unconv += compute_DeltaPV0<28, d, m, rule>(P1, P1.V);
    if constexpr(29 < d) DeltaPV_unconv += compute_DeltaPV0<29, d, m, rule>(P1, P1.V);
    if constexpr(30 < d) DeltaPV_unconv += compute_DeltaPV0<30, d, m, rule>(P1, P1.V);
    if constexpr(31 < d) DeltaPV_unconv += compute_DeltaPV0<31, d, m, rule>(P1, P1.V);
    if constexpr(32 < d) DeltaPV_unconv += compute_DeltaPV0<32, d, m, rule>(P1, P1.V);
    if constexpr(33 < d) DeltaPV_unconv += compute_DeltaPV0<33, d, m, rule>(P1, P1.V);
    if constexpr(34 < d) DeltaPV_unconv += compute_DeltaPV0<34, d, m, rule>(P1, P1.V);
    if constexpr(35 < d) DeltaPV_unconv += compute_DeltaPV0<35, d, m, rule>(P1, P1.V);
    if constexpr(36 < d) DeltaPV_unconv += compute_DeltaPV0<36, d, m, rule>(P1, P1.V);
    if constexpr(37 < d) DeltaPV_unconv += compute_DeltaPV0<37, d, m, rule>(P1, P1.V);
    if constexpr(38 < d) DeltaPV_unconv += compute_DeltaPV0<38, d, m, rule>(P1, P1.V);
    if constexpr(39 < d) DeltaPV_unconv += compute_DeltaPV0<39, d, m, rule>(P1, P1.V);
    if constexpr(40 < d) DeltaPV_unconv += compute_DeltaPV0<40, d, m, rule>(P1, P1.V);
    if constexpr(41 < d) DeltaPV_unconv += compute_DeltaPV0<41, d, m, rule>(P1, P1.V);
    if constexpr(42 < d) DeltaPV_unconv += compute_DeltaPV0<42, d, m, rule>(P1, P1.V);
    if constexpr(43 < d) DeltaPV_unconv += compute_DeltaPV0<43, d, m, rule>(P1, P1.V);
    if constexpr(44 < d) DeltaPV_unconv += compute_DeltaPV0<44, d, m, rule>(P1, P1.V);
    if constexpr(45 < d) DeltaPV_unconv += compute_DeltaPV0<45, d, m, rule>(P1, P1.V);
    if constexpr(46 < d) DeltaPV_unconv += compute_DeltaPV0<46, d, m, rule>(P1, P1.V);
    if constexpr(47 < d) DeltaPV_unconv += compute_DeltaPV0<47, d, m, rule>(P1, P1.V);
    if constexpr(48 < d) DeltaPV_unconv += compute_DeltaPV0<48, d, m, rule>(P1, P1.V);
    if constexpr(49 < d) DeltaPV_unconv += compute_DeltaPV0<49, d, m, rule>(P1, P1.V);
    if constexpr(50 < d) DeltaPV_unconv += compute_DeltaPV0<50, d, m, rule>(P1, P1.V);
    if constexpr(51 < d) DeltaPV_unconv += compute_DeltaPV0<51, d, m, rule>(P1, P1.V);
    if constexpr(52 < d) DeltaPV_unconv += compute_DeltaPV0<52, d, m, rule>(P1, P1.V);
    if constexpr(53 < d) DeltaPV_unconv += compute_DeltaPV0<53, d, m, rule>(P1, P1.V);
    if constexpr(54 < d) DeltaPV_unconv += compute_DeltaPV0<54, d, m, rule>(P1, P1.V);
    if constexpr(55 < d) DeltaPV_unconv += compute_DeltaPV0<55, d, m, rule>(P1, P1.V);
    if constexpr(56 < d) DeltaPV_unconv += compute_DeltaPV0<56, d, m, rule>(P1, P1.V);
    if constexpr(57 < d) DeltaPV_unconv += compute_DeltaPV0<57, d, m, rule>(P1, P1.V);
    if constexpr(58 < d) DeltaPV_unconv += compute_DeltaPV0<58, d, m, rule>(P1, P1.V);
    if constexpr(59 < d) DeltaPV_unconv += compute_DeltaPV0<59, d, m, rule>(P1, P1.V);
    if constexpr(60 < d) DeltaPV_unconv += compute_DeltaPV0<60, d, m, rule>(P1, P1.V);
    if constexpr(61 < d) DeltaPV_unconv += compute_DeltaPV0<61, d, m, rule>(P1, P1.V);
    if constexpr(62 < d) DeltaPV_unconv += compute_DeltaPV0<62, d, m, rule>(P1, P1.V);
    if constexpr(63 < d) DeltaPV_unconv += compute_DeltaPV0<63, d, m, rule>(P1, P1.V);


    P1.S = P1.S + tau*P1.U.transpose()*DeltaPV_unconv;

    cout << "S0tilde: " << delta(c) << endl;
    auto dd = now();

    
    // timing output
    cout << "Gamma34: " << _times["Gamma34"] << endl;
    cout << "Gamma34::alpha: " << _times["Gamma34::alpha"] << endl;
    cout << "Gamma34::pointwise: " << _times["Gamma34::pointwise"] << endl;
    cout << "Gamma34::matmul: " << _times["Gamma34::matmul"] << endl;
    cout << "DPV0: " << _times["DPV0"] << endl;
    cout << "U1TDPT: " << _times["U1TDPT"] << endl;
    cout << "QR: " << _times["QR"] << endl;
    cout << "a1: " << _times["a1"] << endl;
    cout << "a2: " << _times["a2"] << endl;
    cout << "a3: " << _times["a3"] << endl;

    return P1;
}


template<ind m, ind d, class rule>
LR time_step_unconventional_augmented(double tau, const LR& P0) {
    array<ind,2> n = P0.n();
    ind          r = P0.r();
    LR P1;

    auto a = now();
    _times["Gamma12"] = 0.0;
    _times["Gamma12::alpha"] = 0.0;
    _times["Gamma12::pointwise"] = 0.0;
    _times["Gamma12::matmul"] = 0.0;
    _times["Gamma34"] = 0.0;
    _times["Gamma34::gamma"] = 0.0;
    _times["Gamma34::pointwise"] = 0.0;
    _times["Gamma34::matmul"] = 0.0;
    _times["U1TDPT"]  = 0.0;
    _times["DPV0"]    = 0.0;
    _times["QR"]      = 0.0;
    _times["a1"]      = 0.0;
    _times["a2"]      = 0.0;
    _times["a3"]      = 0.0;

    // K step
    MatrixXd DeltaPV0 = MatrixXd::Zero(n[0], r);
    if constexpr(0 < d)  DeltaPV0 += compute_DeltaPV0<0, d, m, rule>(P0, P0.V);
    if constexpr(1 < d)  DeltaPV0 += compute_DeltaPV0<1, d, m, rule>(P0, P0.V);
    if constexpr(2 < d)  DeltaPV0 += compute_DeltaPV0<2, d, m, rule>(P0, P0.V);
    if constexpr(3 < d)  DeltaPV0 += compute_DeltaPV0<3, d, m, rule>(P0, P0.V);
    if constexpr(4 < d)  DeltaPV0 += compute_DeltaPV0<4, d, m, rule>(P0, P0.V);
    if constexpr(5 < d)  DeltaPV0 += compute_DeltaPV0<5, d, m, rule>(P0, P0.V);
    if constexpr(6 < d)  DeltaPV0 += compute_DeltaPV0<6, d, m, rule>(P0, P0.V);
    if constexpr(7 < d)  DeltaPV0 += compute_DeltaPV0<7, d, m, rule>(P0, P0.V);
    if constexpr(8 < d)  DeltaPV0 += compute_DeltaPV0<8, d, m, rule>(P0, P0.V);
    if constexpr(9 < d)  DeltaPV0 += compute_DeltaPV0<9, d, m, rule>(P0, P0.V);
    if constexpr(10 < d) DeltaPV0 += compute_DeltaPV0<10, d, m, rule>(P0, P0.V);
    if constexpr(11 < d) DeltaPV0 += compute_DeltaPV0<11, d, m, rule>(P0, P0.V);
    if constexpr(12 < d) DeltaPV0 += compute_DeltaPV0<12, d, m, rule>(P0, P0.V);
    if constexpr(13 < d) DeltaPV0 += compute_DeltaPV0<13, d, m, rule>(P0, P0.V);
    if constexpr(14 < d) DeltaPV0 += compute_DeltaPV0<14, d, m, rule>(P0, P0.V);
    if constexpr(15 < d) DeltaPV0 += compute_DeltaPV0<15, d, m, rule>(P0, P0.V);
    if constexpr(16 < d) DeltaPV0 += compute_DeltaPV0<16, d, m, rule>(P0, P0.V);
    if constexpr(17 < d) DeltaPV0 += compute_DeltaPV0<17, d, m, rule>(P0, P0.V);
    if constexpr(18 < d) DeltaPV0 += compute_DeltaPV0<18, d, m, rule>(P0, P0.V);
    if constexpr(19 < d) DeltaPV0 += compute_DeltaPV0<19, d, m, rule>(P0, P0.V);
    if constexpr(20 < d) DeltaPV0 += compute_DeltaPV0<20, d, m, rule>(P0, P0.V);
    if constexpr(21 < d) DeltaPV0 += compute_DeltaPV0<21, d, m, rule>(P0, P0.V);
    if constexpr(22 < d) DeltaPV0 += compute_DeltaPV0<22, d, m, rule>(P0, P0.V);
    if constexpr(23 < d) DeltaPV0 += compute_DeltaPV0<23, d, m, rule>(P0, P0.V);
    if constexpr(24 < d) DeltaPV0 += compute_DeltaPV0<24, d, m, rule>(P0, P0.V);
    if constexpr(25 < d) DeltaPV0 += compute_DeltaPV0<25, d, m, rule>(P0, P0.V);
    if constexpr(26 < d) DeltaPV0 += compute_DeltaPV0<26, d, m, rule>(P0, P0.V);
    if constexpr(27 < d) DeltaPV0 += compute_DeltaPV0<27, d, m, rule>(P0, P0.V);
    if constexpr(28 < d) DeltaPV0 += compute_DeltaPV0<28, d, m, rule>(P0, P0.V);
    if constexpr(29 < d) DeltaPV0 += compute_DeltaPV0<29, d, m, rule>(P0, P0.V);
    if constexpr(30 < d) DeltaPV0 += compute_DeltaPV0<30, d, m, rule>(P0, P0.V);
    if constexpr(31 < d) DeltaPV0 += compute_DeltaPV0<31, d, m, rule>(P0, P0.V);
    if constexpr(32 < d) DeltaPV0 += compute_DeltaPV0<32, d, m, rule>(P0, P0.V);
    if constexpr(33 < d) DeltaPV0 += compute_DeltaPV0<33, d, m, rule>(P0, P0.V);
    if constexpr(34 < d) DeltaPV0 += compute_DeltaPV0<34, d, m, rule>(P0, P0.V);
    if constexpr(35 < d) DeltaPV0 += compute_DeltaPV0<35, d, m, rule>(P0, P0.V);
    if constexpr(36 < d) DeltaPV0 += compute_DeltaPV0<36, d, m, rule>(P0, P0.V);
    if constexpr(37 < d) DeltaPV0 += compute_DeltaPV0<37, d, m, rule>(P0, P0.V);
    if constexpr(38 < d) DeltaPV0 += compute_DeltaPV0<38, d, m, rule>(P0, P0.V);
    if constexpr(39 < d) DeltaPV0 += compute_DeltaPV0<39, d, m, rule>(P0, P0.V);
    if constexpr(40 < d) DeltaPV0 += compute_DeltaPV0<40, d, m, rule>(P0, P0.V);
    if constexpr(41 < d) DeltaPV0 += compute_DeltaPV0<41, d, m, rule>(P0, P0.V);
    if constexpr(42 < d) DeltaPV0 += compute_DeltaPV0<42, d, m, rule>(P0, P0.V);
    if constexpr(43 < d) DeltaPV0 += compute_DeltaPV0<43, d, m, rule>(P0, P0.V);
    if constexpr(44 < d) DeltaPV0 += compute_DeltaPV0<44, d, m, rule>(P0, P0.V);
    if constexpr(45 < d) DeltaPV0 += compute_DeltaPV0<45, d, m, rule>(P0, P0.V);
    if constexpr(46 < d) DeltaPV0 += compute_DeltaPV0<46, d, m, rule>(P0, P0.V);
    if constexpr(47 < d) DeltaPV0 += compute_DeltaPV0<47, d, m, rule>(P0, P0.V);
    if constexpr(48 < d) DeltaPV0 += compute_DeltaPV0<48, d, m, rule>(P0, P0.V);
    if constexpr(49 < d) DeltaPV0 += compute_DeltaPV0<49, d, m, rule>(P0, P0.V);
    if constexpr(50 < d) DeltaPV0 += compute_DeltaPV0<50, d, m, rule>(P0, P0.V);
    if constexpr(51 < d) DeltaPV0 += compute_DeltaPV0<51, d, m, rule>(P0, P0.V);
    if constexpr(52 < d) DeltaPV0 += compute_DeltaPV0<52, d, m, rule>(P0, P0.V);
    if constexpr(53 < d) DeltaPV0 += compute_DeltaPV0<53, d, m, rule>(P0, P0.V);
    if constexpr(54 < d) DeltaPV0 += compute_DeltaPV0<54, d, m, rule>(P0, P0.V);
    if constexpr(55 < d) DeltaPV0 += compute_DeltaPV0<55, d, m, rule>(P0, P0.V);
    if constexpr(56 < d) DeltaPV0 += compute_DeltaPV0<56, d, m, rule>(P0, P0.V);
    if constexpr(57 < d) DeltaPV0 += compute_DeltaPV0<57, d, m, rule>(P0, P0.V);
    if constexpr(58 < d) DeltaPV0 += compute_DeltaPV0<58, d, m, rule>(P0, P0.V);
    if constexpr(59 < d) DeltaPV0 += compute_DeltaPV0<59, d, m, rule>(P0, P0.V);
    if constexpr(60 < d) DeltaPV0 += compute_DeltaPV0<60, d, m, rule>(P0, P0.V);
    if constexpr(61 < d) DeltaPV0 += compute_DeltaPV0<61, d, m, rule>(P0, P0.V);
    if constexpr(62 < d) DeltaPV0 += compute_DeltaPV0<62, d, m, rule>(P0, P0.V);
    if constexpr(63 < d) DeltaPV0 += compute_DeltaPV0<63, d, m, rule>(P0, P0.V);
    MatrixXd K1 = P0.U*P0.S + tau*DeltaPV0;

    cout << "DeltaPV0: " << delta(a) << endl;
    cout << "b1 " << _times["b1"] << endl;
    cout << "b2 " << _times["b2"] << endl;
    cout << "Gamma12: " << _times["Gamma12"] << endl;
    cout << "Gamma12::alpha: " << _times["Gamma12::alpha"] << endl;
    cout << "Gamma12::pointwise: " << _times["Gamma12::pointwise"] << endl;
    cout << "Gamma12::matmul: " << _times["Gamma12::matmul"] << endl;
    cout << "DPV0: " << _times["DPV0"] << endl;

    // QR
    auto b = now();
    rectangular_QR qr1 = rectangular_QR(n[0], r);
    qr1.compute(K1);
    P1.U = qr1.Q();
    
    cout << "QR: " << delta(b) << endl;
    _times["QR"] += delta(b);
    auto c = now();

    // L step
    MatrixXd U1TDeltaPT = MatrixXd::Zero(n[1], r);
    if constexpr(0 < d)   U1TDeltaPT += compute_U1TDeltaPT<0, d, m, rule>(P0, P0.U);
    if constexpr(1 < d)   U1TDeltaPT += compute_U1TDeltaPT<1, d, m, rule>(P0, P0.U);
    if constexpr(2 < d)   U1TDeltaPT += compute_U1TDeltaPT<2, d, m, rule>(P0, P0.U);
    if constexpr(3 < d)   U1TDeltaPT += compute_U1TDeltaPT<3, d, m, rule>(P0, P0.U);
    if constexpr(4 < d)   U1TDeltaPT += compute_U1TDeltaPT<4, d, m, rule>(P0, P0.U);
    if constexpr(5 < d)   U1TDeltaPT += compute_U1TDeltaPT<5, d, m, rule>(P0, P0.U);
    if constexpr(6 < d)   U1TDeltaPT += compute_U1TDeltaPT<6, d, m, rule>(P0, P0.U);
    if constexpr(7 < d)   U1TDeltaPT += compute_U1TDeltaPT<7, d, m, rule>(P0, P0.U);
    if constexpr(8 < d)   U1TDeltaPT += compute_U1TDeltaPT<8, d, m, rule>(P0, P0.U);
    if constexpr(9 < d)   U1TDeltaPT += compute_U1TDeltaPT<9, d, m, rule>(P0, P0.U);
    if constexpr(10 < d)  U1TDeltaPT += compute_U1TDeltaPT<10, d, m, rule>(P0, P0.U);
    if constexpr(11 < d)  U1TDeltaPT += compute_U1TDeltaPT<11, d, m, rule>(P0, P0.U);
    if constexpr(12 < d)  U1TDeltaPT += compute_U1TDeltaPT<12, d, m, rule>(P0, P0.U);
    if constexpr(13 < d)  U1TDeltaPT += compute_U1TDeltaPT<13, d, m, rule>(P0, P0.U);
    if constexpr(14 < d)  U1TDeltaPT += compute_U1TDeltaPT<14, d, m, rule>(P0, P0.U);
    if constexpr(15 < d)  U1TDeltaPT += compute_U1TDeltaPT<15, d, m, rule>(P0, P0.U);
    if constexpr(16 < d)  U1TDeltaPT += compute_U1TDeltaPT<16, d, m, rule>(P0, P0.U);
    if constexpr(17 < d)  U1TDeltaPT += compute_U1TDeltaPT<17, d, m, rule>(P0, P0.U);
    if constexpr(18 < d)  U1TDeltaPT += compute_U1TDeltaPT<18, d, m, rule>(P0, P0.U);
    if constexpr(19 < d)  U1TDeltaPT += compute_U1TDeltaPT<19, d, m, rule>(P0, P0.U);
    if constexpr(20 < d)  U1TDeltaPT += compute_U1TDeltaPT<20, d, m, rule>(P0, P0.U);
    if constexpr(21 < d)  U1TDeltaPT += compute_U1TDeltaPT<21, d, m, rule>(P0, P0.U);
    if constexpr(22 < d)  U1TDeltaPT += compute_U1TDeltaPT<22, d, m, rule>(P0, P0.U);
    if constexpr(23 < d)  U1TDeltaPT += compute_U1TDeltaPT<23, d, m, rule>(P0, P0.U);
    if constexpr(24 < d)  U1TDeltaPT += compute_U1TDeltaPT<24, d, m, rule>(P0, P0.U);
    if constexpr(25 < d)  U1TDeltaPT += compute_U1TDeltaPT<25, d, m, rule>(P0, P0.U);
    if constexpr(26 < d)  U1TDeltaPT += compute_U1TDeltaPT<26, d, m, rule>(P0, P0.U);
    if constexpr(27 < d)  U1TDeltaPT += compute_U1TDeltaPT<27, d, m, rule>(P0, P0.U);
    if constexpr(28 < d)  U1TDeltaPT += compute_U1TDeltaPT<28, d, m, rule>(P0, P0.U);
    if constexpr(29 < d)  U1TDeltaPT += compute_U1TDeltaPT<29, d, m, rule>(P0, P0.U);
    if constexpr(30 < d)  U1TDeltaPT += compute_U1TDeltaPT<30, d, m, rule>(P0, P0.U);
    if constexpr(31 < d)  U1TDeltaPT += compute_U1TDeltaPT<31, d, m, rule>(P0, P0.U);
    if constexpr(32 < d)  U1TDeltaPT += compute_U1TDeltaPT<32, d, m, rule>(P0, P0.U);
    if constexpr(33 < d)  U1TDeltaPT += compute_U1TDeltaPT<33, d, m, rule>(P0, P0.U);
    if constexpr(34 < d)  U1TDeltaPT += compute_U1TDeltaPT<34, d, m, rule>(P0, P0.U);
    if constexpr(35 < d)  U1TDeltaPT += compute_U1TDeltaPT<35, d, m, rule>(P0, P0.U);
    if constexpr(36 < d)  U1TDeltaPT += compute_U1TDeltaPT<36, d, m, rule>(P0, P0.U);
    if constexpr(37 < d)  U1TDeltaPT += compute_U1TDeltaPT<37, d, m, rule>(P0, P0.U);
    if constexpr(38 < d)  U1TDeltaPT += compute_U1TDeltaPT<38, d, m, rule>(P0, P0.U);
    if constexpr(39 < d)  U1TDeltaPT += compute_U1TDeltaPT<39, d, m, rule>(P0, P0.U);
    if constexpr(40 < d)  U1TDeltaPT += compute_U1TDeltaPT<40, d, m, rule>(P0, P0.U);
    if constexpr(41 < d)  U1TDeltaPT += compute_U1TDeltaPT<41, d, m, rule>(P0, P0.U);
    if constexpr(42 < d)  U1TDeltaPT += compute_U1TDeltaPT<42, d, m, rule>(P0, P0.U);
    if constexpr(43 < d)  U1TDeltaPT += compute_U1TDeltaPT<43, d, m, rule>(P0, P0.U);
    if constexpr(44 < d)  U1TDeltaPT += compute_U1TDeltaPT<44, d, m, rule>(P0, P0.U);
    if constexpr(45 < d)  U1TDeltaPT += compute_U1TDeltaPT<45, d, m, rule>(P0, P0.U);
    if constexpr(46 < d)  U1TDeltaPT += compute_U1TDeltaPT<46, d, m, rule>(P0, P0.U);
    if constexpr(47 < d)  U1TDeltaPT += compute_U1TDeltaPT<47, d, m, rule>(P0, P0.U);
    if constexpr(48 < d)  U1TDeltaPT += compute_U1TDeltaPT<48, d, m, rule>(P0, P0.U);
    if constexpr(49 < d)  U1TDeltaPT += compute_U1TDeltaPT<49, d, m, rule>(P0, P0.U);
    if constexpr(50 < d)  U1TDeltaPT += compute_U1TDeltaPT<50, d, m, rule>(P0, P0.U);
    if constexpr(51 < d)  U1TDeltaPT += compute_U1TDeltaPT<51, d, m, rule>(P0, P0.U);
    if constexpr(52 < d)  U1TDeltaPT += compute_U1TDeltaPT<52, d, m, rule>(P0, P0.U);
    if constexpr(53 < d)  U1TDeltaPT += compute_U1TDeltaPT<53, d, m, rule>(P0, P0.U);
    if constexpr(54 < d)  U1TDeltaPT += compute_U1TDeltaPT<54, d, m, rule>(P0, P0.U);
    if constexpr(55 < d)  U1TDeltaPT += compute_U1TDeltaPT<55, d, m, rule>(P0, P0.U);
    if constexpr(56 < d)  U1TDeltaPT += compute_U1TDeltaPT<56, d, m, rule>(P0, P0.U);
    if constexpr(57 < d)  U1TDeltaPT += compute_U1TDeltaPT<57, d, m, rule>(P0, P0.U);
    if constexpr(58 < d)  U1TDeltaPT += compute_U1TDeltaPT<58, d, m, rule>(P0, P0.U);
    if constexpr(59 < d)  U1TDeltaPT += compute_U1TDeltaPT<59, d, m, rule>(P0, P0.U);
    if constexpr(60 < d)  U1TDeltaPT += compute_U1TDeltaPT<60, d, m, rule>(P0, P0.U);
    if constexpr(61 < d)  U1TDeltaPT += compute_U1TDeltaPT<61, d, m, rule>(P0, P0.U);
    if constexpr(62 < d)  U1TDeltaPT += compute_U1TDeltaPT<62, d, m, rule>(P0, P0.U);
    if constexpr(63 < d)  U1TDeltaPT += compute_U1TDeltaPT<63, d, m, rule>(P0, P0.U);
    MatrixXd L1 = P0.V*P0.S.transpose() + tau*U1TDeltaPT;
    
    auto e = now();

    // QR
    rectangular_QR qr2 = rectangular_QR(n[1], r);
    qr2.compute(L1);
    P1.V = qr2.Q();
    
    cout << "QR2: " << delta(e) << endl;
    _times["QR"] += delta(e);


    // Do the augmentation
    LR P_augm(2*r, {n[0], n[1]});

    P_augm.U.block(0, 0, n[0], r) = P0.U;
    P_augm.U.block(0, r, n[0], r) = P1.U;
    rectangular_QR qr3 = rectangular_QR(n[0], 2*r);
    qr3.compute(P_augm.U);
    P_augm.U = qr3.Q();

    P_augm.V.block(0, 0, n[1], r) = P0.V;
    P_augm.V.block(0, r, n[1], r) = P1.V;
    rectangular_QR qr4 = rectangular_QR(n[1], 2*r);
    qr4.compute(P_augm.V);
    P_augm.V = qr4.Q();


    // S step
    MatrixXd M = P_augm.U.transpose()*P0.U;
    MatrixXd N = P_augm.V.transpose()*P0.V;
    P_augm.S = M*P0.S*N.transpose();

    MatrixXd DeltaPV_unconv = MatrixXd::Zero(n[0], 2*r);
    if constexpr(0 < d)  DeltaPV_unconv += compute_DeltaPV0<0, d, m, rule>( P_augm, P_augm.V);
    if constexpr(1 < d)  DeltaPV_unconv += compute_DeltaPV0<1, d, m, rule>( P_augm, P_augm.V);
    if constexpr(2 < d)  DeltaPV_unconv += compute_DeltaPV0<2, d, m, rule>( P_augm, P_augm.V);
    if constexpr(3 < d)  DeltaPV_unconv += compute_DeltaPV0<3, d, m, rule>( P_augm, P_augm.V);
    if constexpr(4 < d)  DeltaPV_unconv += compute_DeltaPV0<4, d, m, rule>( P_augm, P_augm.V);
    if constexpr(5 < d)  DeltaPV_unconv += compute_DeltaPV0<5, d, m, rule>( P_augm, P_augm.V);
    if constexpr(6 < d)  DeltaPV_unconv += compute_DeltaPV0<6, d, m, rule>( P_augm, P_augm.V);
    if constexpr(7 < d)  DeltaPV_unconv += compute_DeltaPV0<7, d, m, rule>( P_augm, P_augm.V);
    if constexpr(8 < d)  DeltaPV_unconv += compute_DeltaPV0<8, d, m, rule>( P_augm, P_augm.V);
    if constexpr(9 < d)  DeltaPV_unconv += compute_DeltaPV0<9, d, m, rule>( P_augm, P_augm.V);
    if constexpr(10 < d) DeltaPV_unconv += compute_DeltaPV0<10, d, m, rule>(P_augm, P_augm.V);
    if constexpr(11 < d) DeltaPV_unconv += compute_DeltaPV0<11, d, m, rule>(P_augm, P_augm.V);
    if constexpr(12 < d) DeltaPV_unconv += compute_DeltaPV0<12, d, m, rule>(P_augm, P_augm.V);
    if constexpr(13 < d) DeltaPV_unconv += compute_DeltaPV0<13, d, m, rule>(P_augm, P_augm.V);
    if constexpr(14 < d) DeltaPV_unconv += compute_DeltaPV0<14, d, m, rule>(P_augm, P_augm.V);
    if constexpr(15 < d) DeltaPV_unconv += compute_DeltaPV0<15, d, m, rule>(P_augm, P_augm.V);
    if constexpr(16 < d) DeltaPV_unconv += compute_DeltaPV0<16, d, m, rule>(P_augm, P_augm.V);
    if constexpr(17 < d) DeltaPV_unconv += compute_DeltaPV0<17, d, m, rule>(P_augm, P_augm.V);
    if constexpr(18 < d) DeltaPV_unconv += compute_DeltaPV0<18, d, m, rule>(P_augm, P_augm.V);
    if constexpr(19 < d) DeltaPV_unconv += compute_DeltaPV0<19, d, m, rule>(P_augm, P_augm.V);
    if constexpr(20 < d) DeltaPV_unconv += compute_DeltaPV0<20, d, m, rule>(P_augm, P_augm.V);
    if constexpr(21 < d) DeltaPV_unconv += compute_DeltaPV0<21, d, m, rule>(P_augm, P_augm.V);
    if constexpr(22 < d) DeltaPV_unconv += compute_DeltaPV0<22, d, m, rule>(P_augm, P_augm.V);
    if constexpr(23 < d) DeltaPV_unconv += compute_DeltaPV0<23, d, m, rule>(P_augm, P_augm.V);
    if constexpr(24 < d) DeltaPV_unconv += compute_DeltaPV0<24, d, m, rule>(P_augm, P_augm.V);
    if constexpr(25 < d) DeltaPV_unconv += compute_DeltaPV0<25, d, m, rule>(P_augm, P_augm.V);
    if constexpr(26 < d) DeltaPV_unconv += compute_DeltaPV0<26, d, m, rule>(P_augm, P_augm.V);
    if constexpr(27 < d) DeltaPV_unconv += compute_DeltaPV0<27, d, m, rule>(P_augm, P_augm.V);
    if constexpr(28 < d) DeltaPV_unconv += compute_DeltaPV0<28, d, m, rule>(P_augm, P_augm.V);
    if constexpr(29 < d) DeltaPV_unconv += compute_DeltaPV0<29, d, m, rule>(P_augm, P_augm.V);
    if constexpr(30 < d) DeltaPV_unconv += compute_DeltaPV0<30, d, m, rule>(P_augm, P_augm.V);
    if constexpr(31 < d) DeltaPV_unconv += compute_DeltaPV0<31, d, m, rule>(P_augm, P_augm.V);
    if constexpr(32 < d) DeltaPV_unconv += compute_DeltaPV0<32, d, m, rule>(P_augm, P_augm.V);
    if constexpr(33 < d) DeltaPV_unconv += compute_DeltaPV0<33, d, m, rule>(P_augm, P_augm.V);
    if constexpr(34 < d) DeltaPV_unconv += compute_DeltaPV0<34, d, m, rule>(P_augm, P_augm.V);
    if constexpr(35 < d) DeltaPV_unconv += compute_DeltaPV0<35, d, m, rule>(P_augm, P_augm.V);
    if constexpr(36 < d) DeltaPV_unconv += compute_DeltaPV0<36, d, m, rule>(P_augm, P_augm.V);
    if constexpr(37 < d) DeltaPV_unconv += compute_DeltaPV0<37, d, m, rule>(P_augm, P_augm.V);
    if constexpr(38 < d) DeltaPV_unconv += compute_DeltaPV0<38, d, m, rule>(P_augm, P_augm.V);
    if constexpr(39 < d) DeltaPV_unconv += compute_DeltaPV0<39, d, m, rule>(P_augm, P_augm.V);
    if constexpr(40 < d) DeltaPV_unconv += compute_DeltaPV0<40, d, m, rule>(P_augm, P_augm.V);
    if constexpr(41 < d) DeltaPV_unconv += compute_DeltaPV0<41, d, m, rule>(P_augm, P_augm.V);
    if constexpr(42 < d) DeltaPV_unconv += compute_DeltaPV0<42, d, m, rule>(P_augm, P_augm.V);
    if constexpr(43 < d) DeltaPV_unconv += compute_DeltaPV0<43, d, m, rule>(P_augm, P_augm.V);
    if constexpr(44 < d) DeltaPV_unconv += compute_DeltaPV0<44, d, m, rule>(P_augm, P_augm.V);
    if constexpr(45 < d) DeltaPV_unconv += compute_DeltaPV0<45, d, m, rule>(P_augm, P_augm.V);
    if constexpr(46 < d) DeltaPV_unconv += compute_DeltaPV0<46, d, m, rule>(P_augm, P_augm.V);
    if constexpr(47 < d) DeltaPV_unconv += compute_DeltaPV0<47, d, m, rule>(P_augm, P_augm.V);
    if constexpr(48 < d) DeltaPV_unconv += compute_DeltaPV0<48, d, m, rule>(P_augm, P_augm.V);
    if constexpr(49 < d) DeltaPV_unconv += compute_DeltaPV0<49, d, m, rule>(P_augm, P_augm.V);
    if constexpr(50 < d) DeltaPV_unconv += compute_DeltaPV0<50, d, m, rule>(P_augm, P_augm.V);
    if constexpr(51 < d) DeltaPV_unconv += compute_DeltaPV0<51, d, m, rule>(P_augm, P_augm.V);
    if constexpr(52 < d) DeltaPV_unconv += compute_DeltaPV0<52, d, m, rule>(P_augm, P_augm.V);
    if constexpr(53 < d) DeltaPV_unconv += compute_DeltaPV0<53, d, m, rule>(P_augm, P_augm.V);
    if constexpr(54 < d) DeltaPV_unconv += compute_DeltaPV0<54, d, m, rule>(P_augm, P_augm.V);
    if constexpr(55 < d) DeltaPV_unconv += compute_DeltaPV0<55, d, m, rule>(P_augm, P_augm.V);
    if constexpr(56 < d) DeltaPV_unconv += compute_DeltaPV0<56, d, m, rule>(P_augm, P_augm.V);
    if constexpr(57 < d) DeltaPV_unconv += compute_DeltaPV0<57, d, m, rule>(P_augm, P_augm.V);
    if constexpr(58 < d) DeltaPV_unconv += compute_DeltaPV0<58, d, m, rule>(P_augm, P_augm.V);
    if constexpr(59 < d) DeltaPV_unconv += compute_DeltaPV0<59, d, m, rule>(P_augm, P_augm.V);
    if constexpr(60 < d) DeltaPV_unconv += compute_DeltaPV0<60, d, m, rule>(P_augm, P_augm.V);
    if constexpr(61 < d) DeltaPV_unconv += compute_DeltaPV0<61, d, m, rule>(P_augm, P_augm.V);
    if constexpr(62 < d) DeltaPV_unconv += compute_DeltaPV0<62, d, m, rule>(P_augm, P_augm.V);
    if constexpr(63 < d) DeltaPV_unconv += compute_DeltaPV0<63, d, m, rule>(P_augm, P_augm.V);


    P_augm.S = P_augm.S + tau*P_augm.U.transpose()*DeltaPV_unconv;

    cout << "S0tilde: " << delta(c) << endl;
    auto dd = now();


    // truncation
    Eigen::JacobiSVD<MatrixXd> svd(P_augm.S, Eigen::ComputeThinU | Eigen::ComputeThinV);
    MatrixXd Sigma = svd.singularValues().asDiagonal();
    P_augm.U = P_augm.U * svd.matrixU();
    P_augm.V = P_augm.V * svd.matrixV();

    P1.S = Sigma.block(0, 0, r, r);
    P1.U = P_augm.U.block(0, 0, n[0], r);
    P1.V = P_augm.V.block(0, 0, n[1], r);


    // timing output
    cout << "Gamma34: " << _times["Gamma34"] << endl;
    cout << "Gamma34::alpha: " << _times["Gamma34::alpha"] << endl;
    cout << "Gamma34::pointwise: " << _times["Gamma34::pointwise"] << endl;
    cout << "Gamma34::matmul: " << _times["Gamma34::matmul"] << endl;
    cout << "DPV0: " << _times["DPV0"] << endl;
    cout << "U1TDPT: " << _times["U1TDPT"] << endl;
    cout << "QR: " << _times["QR"] << endl;
    cout << "a1: " << _times["a1"] << endl;
    cout << "a2: " << _times["a2"] << endl;
    cout << "a3: " << _times["a3"] << endl;

    return P1;
}