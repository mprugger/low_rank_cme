import itertools as it
import numpy as np
#from scipy.linalg import expm
from scipy.linalg import svd, norm
from scipy.sparse import lil_matrix, csc_matrix, csr_matrix, save_npz, load_npz, eye
from scipy.sparse.linalg import expm_multiply, spsolve
import matplotlib.pylab as plt
from time import time, perf_counter
from os.path import isfile
from random import randint
import pickle
import sys


# MM
#ngrid = [10,10,10,10]
#tau = 0.0005  ## for MM use 0.002
#t_final = 3.0
#rank = 15

problem = "LARGE"  # check_SS, permutation, EMT or LARGE, LARGE_ART, test
load_matrix = False


def rules_large_art(states,x):
    if x == 0:  # ERK
        return states[0]  
    elif x == 1: #P27361 = ERK
        return states[0];
    elif x == 2: #RSK = RSK
        return states[2]; 
    elif x == 3: #Q15418 = RSK
        return states[2]; 
    elif x == 4: #mir-1976 = RSK
        return states[2]; 
    elif x == 5: #MLL = MLL
        return states[5]; 
    elif x == 6: #Q03164 = MLL and not mir-1976
        return states[5] and not states[4];    
    elif x == 7: #HOAX9 = Q03164
        return states[6];
    elif x == 8: #mir-196b = HOAX9
        return states[7]; 
    elif x == 9: #TSC1/2 = not (P27361 and Q15418) and not P31749
        return (not (states[1] and states[3])) and not states[10];
    elif x == 10: #P31749 = AKT
        return states[13]; 
    elif x == 11: #PDK1 = PDK1
        return states[11]; 
    elif x == 12: #Q15118 = PDK1  
        return states[11];
    elif x == 13: #AKT = Q15118 and Q6R327 and P42345 and Q9BVC4
        return states[12] and states[21] and states[19] and states[17];   
    elif x == 14: #RHEB = not TSC1/2
        return not states[9]; 
    elif x == 15: #Q15382 = RHEB
        return states[14];
    elif x == 16: #GBL = Q15382
        return states[15];  
    elif x == 17: #Q9BVC4 = GBL
        return states[16];   
    elif x == 18: #mTOR = Q15382
        return states[15]; 
    elif x == 19: #P42345 = mTOR
        return states[18];  
    elif x == 20: #RICTOR = Q15382
        return states[15]; 
    elif x == 21: #Q6R327 = RICTOR and not mir-196bs
        return states[20] and not states[8];  
    elif x == 22: 
        return states[22] and states[23];    
    elif x == 23: 
        return states[23];
    #elif x == 24:
    #    return states[3] and not states[25];
    #elif x == 25:
    #    return states[23] and states[24] and states[25];
    else:
        print("rule {} not valid!".format(x))


def rules_EMT(states, x):
    #'''
    if x == 0:
        return states[1] and not states[3] and not states[2]
    elif x == 1:
        return states[6] and not states[4]
    elif x == 2:
        return (states[7] or states[0] or states[4]) and not states[5] and not states[3]
    elif x == 3:
        return states[7] and not states[4] and not states[0] and not states[2]
    elif x == 4:
        return (states[2] or states[3]) and not states[5]
    elif x == 5:
        return states[0] and not states[4]
    elif x == 6:
        return states[6]
    elif x == 7:
        return states[7]
        '''
    s = list(states).copy()
    #s.reverse()
    if x == 0:
        return s[0] and s[2]
    elif x == 1:
        return s[1]
    elif x == 2:
        return s[0] and not s[3] #and not s[0]
    elif x == 3:
        return s[3] '''
    else:
        print("rule {} not valid!".format(x))


def rules_large(states,x):
    if x == 0:  # ERK
        return states[0]                                                 #ERK
    if x == 1:  # P27361
        return states[0]                                                 #ERK
    if x == 2:  # RSK
        return states[2]                                                 #RSK
    if x == 3:  # Q15418
        return states[2]                                                 #RSK
    if x == 4:  # mir-1976
        return states[2]                                                 #RSK
    if x == 5:  # MLL
        return states[5]                                                 #MLL
    if x == 6:  # Q03164
        return states[5] and not states[4]                               #MLL and not mir-1976
    if x == 7:  # HOAX9
        return states[6]                                                 #Q03164
    if x == 8:  # mir-196b
        return states[7]                                                 #HOAX9
    if x == 9:  # TSC1/2
        return (not (states[1] and states[3])) and not states[10]          #not (P27361 and Q15418) and not P31749
    if x == 10:  # P31749
        return states[13]                                                #AKT

    if x == 11:  # PDK1
        return states[11]                                                #PDK1
    if x == 12:  # Q15118
        return states[11]                                                #PDK1
    if x == 13:  # AKT
        return states[12] and states[21] and states[19] and states[17]   #Q15118 and Q6R327 and P42345 and Q9BVC4
    if x == 14:  # RHEB
        return not states[9]                                             #not TSC1/2
    if x == 15:  # Q15382
        return states[14]                                                #RHEB
    if x == 16:  # GBL
        return states[15]                                                #Q15382
    if x == 17:  # Q9BVC4
        return states[16]                                                #GBL
    if x == 18:  # mTOR
        return states[15]                                                #Q15382
    if x == 19:  # P42345
        return states[18]                                                #mTOR
    if x == 20:  # RICTOR
        return states[15]                                                #Q15382
    if x == 21:  # Q6R327
        return states[20] and not states[8]                              #RICTOR and not mir-196bs

    else:
        print("rule {} not valid!".format(x))


'''
def rules(states, x):
    
    # MM sam rules:
    if x == 0:
        return (not states[0] and states[2] and not states[1] and states[3]) or (states[2] and states[1]) or (states[0] and not states[2])
    elif x == 1:
        return (not states[1] and states[2] and not states[3]) or (states[1] and not states[2])
    elif x == 2:
        return (not states[2] and states[1] and states[0]) or (states[2] and not states[0]) or (states[2] and not states[1] and not states[3] and states[0])
    elif x == 3:
        return states[2] or states[3]

    

    
    # EMT paper rules:
    if x == 0:
        return states[1] and not states[3] and not states[2]
    elif x == 1:
        return states[6] and not states[4]
    elif x == 2:
        return (states[7] or states[0] or states[4]) and not states[5] and not states[3]
    elif x == 3:
        return states[7] and not states[4] and not states[0] and not states[2]
    elif x == 4:
        return (states[2] or states[3]) and not states[5]
    elif x == 5:
        return states[0] and not states[4]
    elif x == 6:
        return states[6]
    elif x == 7:
        return states[7]
    
    

    # gene protein/products paper, Benso, Di Carlo, et al 2014
    if x == 0:  # ERK
        return states[0]                                                 #ERK
    if x == 1:  # P27361
        return states[0]                                                 #ERK
    if x == 2:  # RSK
        return states[2]                                                 #RSK
    if x == 3:  # Q15418
        return states[2]                                                 #RSK
    if x == 4:  # mir-1976
        return states[2]                                                 #RSK
    if x == 5:  # MLL
        return states[5]                                                 #MLL
    if x == 6:  # Q03164
        return states[5] and not states[4]                               #MLL and not mir-1976
    if x == 7:  # HOAX9
        return states[6]                                                 #Q03164
    if x == 8:  # mir-196b
        return states[7]                                                 #HOAX9
    if x == 9:  # TSC1/2
        return (not (states[1] and states[3])) and not states[10]          #not (P27361 and Q15418) and not P31749
    if x == 10:  # P31749
        return states[13]                                                #AKT

    if x == 11:  # PDK1
        return states[11]                                                #PDK1
    if x == 12:  # Q15118
        return states[11]                                                #PDK1
    if x == 13:  # AKT
        return states[12] and states[21] and states[19] and states[17]   #Q15118 and Q6R327 and P42345 and Q9BVC4
    if x == 14:  # RHEB
        return not states[9]                                             #not TSC1/2
    if x == 15:  # Q15382
        return states[14]                                                #RHEB
    if x == 16:  # GBL
        return states[15]                                                #Q15382
    if x == 17:  # Q9BVC4
        return states[16]                                                #GBL
    if x == 18:  # mTOR
        return states[15]                                                #Q15382
    if x == 19:  # P42345
        return states[18]                                                #mTOR
    if x == 20:  # RICTOR
        return states[15]                                                #Q15382
    if x == 21:  # Q6R327
        return states[20] and not states[8]                              #RICTOR and not mir-196bs

    else:
        print("rule {} not valid!".format(x))
'''


def Perm_inv(Perm):
    inverse = [0] * len(Perm)
    for i, p in enumerate(Perm):
        inverse[p] = i
    return inverse


def apply_Perm(Perminv, states):
    N = len(Perminv)
    states_t = [states[Perminv[i]] for i in range(N)]
    return states_t

def generate_large_Perm(small_Perm):  # for initial state (permutate the original state) and final state (permute it back)
    large_Perm = [bool_to_int(apply_Perm(small_Perm, x)) for x in states]
    return large_Perm


def Perm_matrix(Perm):
    vals = np.ones(2**n)
    row_idx = np.array(range(2**n))
    col_idx = np.array(Perm)
    return csr_matrix((vals, (row_idx, col_idx)), (2**n, 2**n))



def check_systems_ss(states):
    is_ss = []
    check_ss = 0
    for i in range(len(states)):
        new_state = list(states[i])
        for j in range(n):
            new_state[j] = rules_large(states[i], j)
            if sum(states[i]) != sum(new_state):
                check_ss = 1
                break
        if check_ss == 0:
            is_ss.append(new_state)
        check_ss = 0

    print(len(is_ss))
    print(np.array(is_ss).astype(int))
    return


def sub2ind(coordinates, size):
    return np.ravel_multi_index(coordinates, size)


def ind2sub(index, size):
    return np.unravel_index(index, size)


def michaelis_menten_matrix(ngrid, k1, km1, k2, km2):
    total = np.prod(ngrid)
    n_E, n_S, n_ES, n_P = ngrid

    A = lil_matrix((total,total))
    for i in range(total):
        E, S, ES, P = ind2sub(i, ngrid)

        A[i,i] = -(k1*E*S + km1*ES + k2*ES + km2*E*P)

        if ES != 0 and E < n_E-1 and S < n_S-1:
            A[i, sub2ind([E+1,S+1,ES-1,P], ngrid)] = k1*(E+1.0)*(S+1.0)
        if E != 0 and S != 0 and ES < n_ES-1:
            A[i, sub2ind([E-1,S-1,ES+1,P], ngrid)] = km1*(ES+1.0)
        if E != 0 and P != 0 and ES < n_ES-1:
            A[i, sub2ind([E-1,S,ES+1,P-1], ngrid)] = k2*(ES+1.0)
        if ES != 0 and E < n_E-1 and P < n_P-1:
            A[i, sub2ind([E+1,S,ES-1,P+1], ngrid)] = km2*(E+1)*(P+1)

    return csc_matrix(A)


def print_mm(vec, ngrid, threshold):
    total = len(vec)
    for i in range(total):
        if vec[i] >= threshold:
            E, S, ES, P = ind2sub(i, ngrid)
            print('E, S, ES, P = {} {} {} {}:\t{}'.format(E, S, ES, P, vec[i]))


def print_mm_comparison(out, out_ref, ngrid, tol):
    total = len(out)
    for i in range(total):
        if abs(out[i]-out_ref[i]) >= tol:
            E, S, ES, P = ind2sub(i, ngrid)
            print('E, S, ES, P = {} {} {} {}:\t{: 0.3f} {: 0.3f} {: 0.3f}'.format(E, S, ES, P, out_ref[i], out[i], abs(out_ref[i]-out[i])))


def bool_to_int(state):
    start = time()
    symbol_length = len(state)
    #print(state)
    #exit()
    s = 0
    pwr = 1
    for i in range(symbol_length):
        s += state[symbol_length-1-i] * pwr
        pwr *= 2
    #print(time() - start)
    return s


"""
    start = time()
    symbol_length = len(state)
    translation = []
    a1 = time()

    #for position in range(symbol_length):
    #    translation.append(2**position)
    translation = [2**x for x in range(symbol_length)]
    a2 = time()

    translation = translation[::-1]
    scpd = sum([x*y for x,y in zip(translation, state)])
    a3 = time()

    #print(a1-start, a2-a1, a3-a2, a3-start)


    return scpd
"""



def generate_rule_matrices_dense(states, P, P_inv):
    rule_matrices = []
    for i in range(n):
        rule_matrices.append(np.zeros((len(states), len(states))))

    for r in range(n):
        for i in range(len(states)):
            # print(states[i], rules(states[i], 0))
            ns = list(states[i])
            ns[r] = rules_EMT(states[i], r)
            if ns[r] is not states[i][r]:
                # print("here change needs to happen")
                coord_new_state = bool_to_int(ns)
                coord_old_state = bool_to_int(states[i])
                # print(states[i], "has idx:", coord_new_state, ns, "has idx:", coord_old_state)
                rule_matrices[r][coord_new_state][coord_old_state] = 1
                rule_matrices[r][coord_old_state][coord_old_state] = rule_matrices[r][coord_old_state][coord_old_state] - 1

    return [P_inv.dot(mat.dot(P)) for mat in rule_matrices]


def generate_rule_matrices(states, P, Pinv, prob='np'):
    rule_matrices = []
    #for i in range(n):
    #    rule_matrices.append(dok_matrix((len(states), len(states))))

    if load_matrix is True and isfile("/home/martina/PycharmProjects/low_rank_CME/__mat_{}_{}.npz".format(problem, n-1)):
        for i in range(n):
            mat = load_npz("/home/martina/PycharmProjects/low_rank_CME/__mat_{}_{}.npz".format(problem, i))
            rule_matrices.append(Pinv.dot(mat.dot(P)))
        print('loading matrices from __mat files done.')
        return rule_matrices

    int_states = [bool_to_int(x) for x in states]
    #print(int_states)
    #exit()

    #mat  = [[[],[],[]]]*n
    for r in range(n):
        mat = {}
        print('r={}'.format(r))
        start = time()
        for i in range(len(states)):
            if i%100000==0:
                print('i={} {}'.format(i, time()-start))
                start = time()
            # print(states[i], rules(states[i], 0))
            ns = list(states[i])
            if prob == 'large':
                ns[r] = rules_large(states[i], r)
            elif prob == 'large_art':
                ns[r] = rules_large_art(states[i], r)
            elif prob == 'EMT':
                ns[r] = rules_EMT(states[i], r)
            elif prob == 'test':
                ns[r] = rules_large_art(states[i], r)
            else:
                print('no meaningful problem specified')
                exit()
            if ns[r] is not states[i][r]:
                # print("here change needs to happen")
                a = time()
                #coord_new_state = bool_to_int(ns)
                #coord_old_state = bool_to_int(states[i])
                coord_old_state = int_states[i]
                if states[i][r] == 1:
                    coord_new_state = coord_old_state - 2**(n-1-r)
                else:
                    coord_new_state = coord_old_state + 2 ** (n - 1 - r)
                #print(states[i], "has idx:", coord_new_state, ns, "has idx:", coord_old_state)
                b = time()
                mat[(coord_new_state,coord_old_state)] = 1.0;
                if (coord_old_state, coord_old_state) in mat:
                    mat[(coord_old_state, coord_old_state)] -= 1.0
                else:
                    mat[(coord_old_state, coord_old_state)] = -1.0;


                #mat[r][0].append(1.0)
                #mat[r][1].append(coord_new_state)
                #mat[r][2].append(coord_old_state)

                #mat[r][0].append(-1.0)
                #mat[r][1].append(coord_old_state)
                #mat[r][2].append(coord_old_state)

                #coordinates_plus.append((coord_new_state, coord_old_state))
                #coordinates_minus.append((coord_old_state, coord_old_state))
                #rule_matrices[r][coord_new_state,coord_old_state] = 1
                #rule_matrices[r][coord_old_state,coord_old_state] -= 1
                #print(b-a, time() - b)

        print('creating csr matrices...')
        data = []
        rowids = []
        colids = []
        for key, value in mat.items():
            rowids.append(key[0])
            colids.append(key[1])
            data.append(value)
        print('here')
        rule_matrices.append(csr_matrix((data, (rowids, colids)), shape=(len(states),len(states))))
        print('finished creating csr matrices.')

    for i in range(n):
        print('writing csr matrix to disk: {0}'.format(i))
        save_npz("__mat_{}_{}.npz".format(problem, i), rule_matrices[i])
        print(rule_matrices[i])
    print('finished writing csr matrices to disk.')
    return [P_inv.dot(mat.dot(P)) for mat in rule_matrices]


def prob_histogram(u):
    tols = 10.0**np.array(range(-1,-16,-1));
    hist = []
    for tol in tols:
        hist.append( np.count_nonzero(u>tol) )

    return hist


def matrix_step(T, A, u, tau, large_P):
    res = u.copy()
    t = 0.0
    nsteps = int(np.ceil(T/tau))
    fs = open("exact_{}_histogram.data".format(problem), "w")

    a = time()

    for _ in range(nsteps):
        if T-t < tau:
            tau = T-t
        print('matrix_step at time t={}'.format(t))

        hist = prob_histogram(res)
        if np.ceil(t)*100 % 1 == 0:
            res_normalorder = apply_Perm(large_P, res)
            np.save('prob_dist_exact_t'+str(round(t,2)), res_normalorder)#int(np.ceil(t))), res)
        fs.write(str(t) + '\t' + '\t'.join([str(x) for x in hist]) + '\n')

        res = res + tau*A.dot(res) + 0.5*tau**2*A.dot(A.dot(res))
        t += tau
    print('exact solution timing: ', time()-a)

    fs.close()
    return res


def lowrank_step(T, full_matrix, U, S, V, tau, large_P):
    U1, S1, V1 = U.copy(), S.copy(), V.copy()
    n_steps = int(np.ceil(t_final/tau))
    t = 0.0
    cnt = 0
    fs = open('lowrank_{0}_histogram.data'.format(problem), "w")
    for i in range(0,n_steps):
        if t_final-t < tau:
            tau = t_final-t
        print('low-rank at time t={}'.format(t))

        res = vector_P(recon_P(U1, S1, V1))
        hist = prob_histogram(res)
        if np.ceil(t)*100 % 1 == 0:
            res_normalorder = apply_Perm(large_P, res)
            np.save('prob_dist_lr_t' + str(round(t,2)) + '_blue', res_normalorder)#int(np.ceil(t))), res)
        fs.write(str(t) + '\t' + '\t'.join([str(x) for x in hist]) + '\n')

        a=time()
        U1, S1, V1 = low_rank_step(tau, full_matrix, U1, S1, V1)
        print('low rank step time: ', time()-a)
        t += tau
        cnt += 1
    fs.close()
    return U1, S1, V1


def full_rule_matrix(rule_mat):
    return sum([m for m in rule_mat])


def chop(A, eps = 1e-10):
    B = np.copy(A)
    B[np.abs(A) < eps] = 0
    return B


def get_low_rank_factors(P, r):
    #print(P)
    u, sv, v = svd(P, full_matrices=True)
    sv_trunc = sv[0:r]
    print('error of low-rank approx: {}'.format(sv[r-1]))
    S = np.diag(sv_trunc)
    U = u[:,0:r]
    V = v.transpose()[:,0:r]
    return U, S, V


def recon_P(U, S, V):
    return U.dot(S.dot(V.transpose()))  ## EXPENSIVE!!!


def vector_P(P):
    return np.reshape(P, np.prod(P.shape), order='F')


def low_rank_step(tau, A, U0, S0, V0):
    #print('S0: ',S0)
    #exit()
    # deltaP = tau*A(P)
    P = recon_P(U0,S0,V0)
    P = vector_P(P)
    Q = tau*A.dot(P) # explicit euler step
    #Q = spsolve(eye(A.shape[0]) - tau * A, tau * A.dot(P))  # inverse euler step
    #Q = spsolve(eye(A.shape[0]) - 0.5*tau*A, tau*A.dot(P))  # Crank-Nicolson
    #Q = tau*A.dot(P) + 0.5*tau**2*A.dot(A.dot(P))
    dP = Q.reshape(len(U0), len(U0), order='F')
    print('U ', U, '\nS ', S, '\nV ', V)
    np.set_printoptions(precision=16)
    print('dP.V0: ',dP.dot(V0))
    #exit()

    # step 1: K1 = U0*S0+(deltaP)V0
    K1 =  U0.dot(S0) + dP.dot(V0)
    #print(K1)

    # step 2: QR decomposition to get U1S1_h=K1
    U1, S1_h = np.linalg.qr(K1, mode='reduced')
    #print(U1, S1_h)
    #return U1, S1_h, V0
    #print(U1, S1_h, V0)
    #exit()
    
    # step 3: S0_t = S1_h-U1'(dP)V0
    S0_t = S1_h - U1.transpose().dot(dP.dot(V0))
    #print(S0_t)
    #exit()

    # step 4: L1 = V0*S0_t'+(U1'*dP)'
    L1 = V0.dot(S0_t.transpose()) + (U1.transpose().dot(dP)).transpose()
    #print(L1)
    #exit()

    print((U1.transpose().dot(dP)).transpose())
    #exit()

    #step 5: QR decomposition to get V1*S1'=L1
    V1, S1tr = np.linalg.qr(L1, mode='reduced')
    #print(V1, S1tr.transpose())
    #exit()

    return U1, S1tr.transpose(), V1


def compute_dP(tau, A, U, S, V):
    P = recon_P(U,S,V)
    P = vector_P(P)
    Q = tau*A.dot(P) # explicit Euler
    return Q.reshape(len(U), len(U), order = 'F')


def low_rank_step_lie(tau, A, U0, S0, V0):
    # step 1: K1 = U0*S0+(deltaP)V0
    dP = compute_dP(tau, A, U0, S0, V0)
    K1 =  U0.dot(S0) + dP.dot(V0)

    # step 2: QR decomposition to get U1S1_h=K1
    U1, S1_h = np.linalg.qr(K1, mode='reduced')

    # step 3: S0_t = S1_h-U1'(dP)V0
    dP = compute_dP(tau, A, U1, S1_h, V0)
    S0_t = S1_h - U1.transpose().dot(dP.dot(V0))

    # step 4: L1 = V0*S0_t'+(U1'*dP)'
    dP = compute_dP(tau, A, U1, S0_t, V0)
    L1 = V0.dot(S0_t.transpose()) + (U1.transpose().dot(dP)).transpose()

    #step 5: QR decomposition to get V1*S1'=L1
    V1, S1tr = np.linalg.qr(L1, mode='reduced')

    return U1, S1tr.transpose(), V1


def low_rank_step_strang(tau, A, U0, S0, V0):
    # step 1: K1 = U0*S0+(deltaP)V0
    dP = compute_dP(0.5*tau, A, U0, S0, V0)
    K1 =  U0.dot(S0) + dP.dot(V0)
    U1, S1_h = np.linalg.qr(K1, mode='reduced')

    # step 2: S0_t = S1_h-U1'(dP)V0
    #dP = compute_dP(0.5*tau, A, U1, S1_h, V0)
    S0_t = S1_h - U1.transpose().dot(dP.dot(V0))

    # step 3: L1 = V0*S0_t'+(U1'*dP)'
    #dP = compute_dP(tau, A, U1, S0_t, V0)
    L1 = V0.dot(S0_t.transpose()) + (U1.transpose().dot(2.0*dP)).transpose()
    V1, S1tr = np.linalg.qr(L1, mode='reduced')

    # step 4: S0_t = S1_h-U1'(dP)V0
    #dP = compute_dP(0.5*tau, A, U1, S1tr.transpose(), V1)
    S0_t = S1tr.transpose() - U1.transpose().dot(dP.dot(V1))

    # step 5: K1 = U0*S0+(deltaP)V0
    #dP = compute_dP(0.5*tau, A, U1, S0_t, V1)
    K1 =  U1.dot(S0_t) + dP.dot(V1)
    U1, S1_h = np.linalg.qr(K1, mode='reduced')

    return U1, S1_h, V1


def print_bool(states, sol_exact, sol_lr, tol):
    print('states with probability > {} for the exact solution, left: exact, right: lr'.format(tol))
    for i in range(len(states)):
        if sol_exact[i] > tol:
            print('{}\t{:0.6f}%\t{:0.6f}%'.format(str(states[i]),sol_exact[i]*100, sol_lr[i]*100))


def print_comparison(states, exact_sol, lr_sol, all_states, tol, disp):
    cor_tol = 1e-9
    np.set_printoptions(formatter={'float': '{: 0.3f}'.format})

    large_error_tol = tol
    disp_ct_err = 0
    disp_ct_cor = 0
    max_error = 0
    max_error_idx = 0

    print('\n\ncomparing the low rank solution to the exact solution: \n')

    if(all_states == 1):
        print('all states:')
        print('state    correct sol    low rank sol            error')
        for i in range(len(states)):
            print('{}      {}        {}            {}'.format(states[i], exact_sol[i], lr_sol[i],abs(exact_sol[i] - lr_sol[i])))

    else:
        print('error larger than '+str(large_error_tol)+':')
        print('state    correct sol    low rank sol            error')
        for i in range(len(states)):
            if abs(exact_sol[i] - lr_sol[i]) > large_error_tol:
                if disp_ct_err < disp:
                    print('{}      {}        {}            {}'.format(states[i], exact_sol[i], lr_sol[i], abs(exact_sol[i] - lr_sol[i])))
                    disp_ct_err = disp_ct_err+1
                    if abs(exact_sol[i] - lr_sol[i]) > max_error:
                        max_error = abs(exact_sol[i] - lr_sol[i])
                        max_error_idx = i
                else:
                    disp_ct_err = disp_ct_err+1
        if disp_ct_err > disp:
            print('there are in total {} states with a large error'.format(disp_ct_err))
        print('the largest error is at state {} with an error of {}'.format(states[max_error_idx], max_error))

        print('\n')
        print('correct states:')
        for i in range(len(states)):
            if abs(exact_sol[i] - lr_sol[i]) < cor_tol:
                if disp_ct_cor < disp:
                    print('{}      {}        {}            {}'.format(states[i], exact_sol[i], lr_sol[i], abs(exact_sol[i] - lr_sol[i])))
                    disp_ct_cor = disp_ct_cor+1
                else:
                    disp_ct_cor = disp_ct_cor+1
        if disp_ct_cor > disp:
            print('there are in total {} states that account as correct'.format(disp_ct_cor))
        print('\n')
    return


def remap_states():
    return


'''
#####
##### MM
#####

n_total = np.prod(ngrid)

full_matrix = michaelis_menten_matrix(ngrid, 10.0, 1.0, 5.0, 2.0)
#plt.spy(A)
#plt.show()

iv = np.zeros(ngrid)
#iv[1,3,0,0] = 0.5
#iv[4,7,8,7] = 0.25
#iv[2,2,3,3] = 0.25
#iv[9,9,9,9] = 1.0
iv = iv.reshape(n_total)

# reference
a1 = time()
out_ref = expm_multiply(t_final*full_matrix,iv)
b1 = time()
print(np.sum(out_ref))
print_mm(out_ref, ngrid, 1e-1)

# low-rank
P = iv.reshape(ngrid[0]*ngrid[1],ngrid[2]*ngrid[3])
U, S, V = get_low_rank_factors(P, rank)

n_steps = int(np.ceil(t_final/tau))
a2 = time()
for i in range(0,n_steps):
    U, S, V = low_rank_step_strang(tau, full_matrix, U, S, V)
b2 = time()

P_out = recon_P(U, S, V)
out = vector_P(P_out)
print(i, np.sum(out))
out /= np.sum(out)
print_mm(out, ngrid, 1e-1)


print('errors of lr')
print_mm_comparison(out_ref, out, ngrid, 1e-2)

print('runtime (stupid!): ', b1-a1, b2-a2)
'''


if problem == 'check_SS':
    n = 22
    states = [x for x in it.product((False, True), repeat=n)]
    check_systems_ss(states)


elif problem == 'permutation':
    n = 8
    Perm =   [7, 3, 4, 1, 0, 2, 5, 6]
    #states = [10, 20, 30, 40, 50, 60, 70, 80]
    states = [x for x in it.product((False, True), repeat=n)]
    idx = [bool_to_int(x) for x in states]
    print(idx)





    lp = generate_large_Perm(Perm)
    lpinv = Perm_inv(lp)
    new = apply_Perm(lpinv, states)
    old = apply_Perm(lp, new)
    print(sum(sum(np.array(old) == np.array(states))))
    exit()

    print(Perm_inv(Perm))
    Perminv = Perm_inv(Perm)
    print(apply_Perm(Perminv, states))
    new_states = apply_Perm(Perminv, states)
    print(apply_Perm(Perm,new_states))

    exit()


#####
##### EMT
#####
elif problem == "EMT":
    # EMT
    n = 8
    tau = 0.1
    t_final = 100.0
    rank = 4

    IV = 'uni'



    states = [list(reversed(list(x))) for x in it.product((False, True), repeat=n)]
    str_states = [''.join([str(y) for y in x]).replace('False','0').replace('True','1') for x in states]
    print("states: ", states)
    int_states = [bool_to_int(x) for x in states]
    print('int states: ', int_states)
    #exit()
    #small_P = list(range(n))
    small_P = [0, 1, 2, 3, 4, 5, 6, 7]
    #small_P = [5, 4, 7, 3, 0, 6, 1, 2]
    #small_P = [0, 1, 2, 3]
    small_Pinv = Perm_inv(small_P)
    large_P = generate_large_Perm(small_P)
    large_Pinv = generate_large_Perm(small_Pinv)

    P = Perm_matrix(large_P)
    print(type(P))
    P_inv = Perm_matrix(large_Pinv)
    print(type(P_inv))


    rule_mat_dense = generate_rule_matrices_dense(states, P.todense(), P_inv.todense())
    #print(rule_mat_dense)
    rule_mat = generate_rule_matrices(states, P, P_inv, prob='EMT')
    #print('type', type(rule_mat))
    for i in range(n):
        print(norm(rule_mat[i].todense()-rule_mat_dense[i]))
    full_matrix = full_rule_matrix(rule_mat)

    # set type of initial distribution
    # set initial condition as random
    if IV == 'rand':
        randvec = np.random.rand(2**n)
        randvec = randvec/np.sum(randvec)

    # set initial condition as uniform
    elif IV == 'uni':
        randvec = np.ones(2 ** n) / (2 ** n)
    elif IV == 'single':
        IV = randvec = np.zeros(2 ** n)
        randvec[randint(0,2**n-1)] = 1.0
    else:
        print('IV not correctly set. Choose between rand, uni, or single')
        exit()


    randvec = np.array(apply_Perm(large_Pinv, randvec))


    P = randvec.reshape(2**int(n/2),2**int(n/2), order = 'F')



    U, S, V = get_low_rank_factors(P,rank)

    #print(vector_P(recon_P(U, S, V)))
    #exit()
    '''
    U = np.array([[ 0, -1.110223024625157e-16],
[ -0.316227766016838, -0.9486832980505138],
[ -0.9486832980505139, 0.3162277660168379],
[ 0, 0]])
    V = np.array([[ -0.7071067811865477, 0],
[ 0, 1],
[ -0.7071067811865476, 0],
[ 0, 0]])
    S = np.array([[ 4.472135954999578, 0],
[ 0, 0]])


    U = np.array([[ -0.25, 0.0645497, 0.0690066, 0.0741249],
                [ -0.25, -0.968246, 5.20417e-18, 1.04083e-17],
                [ -0.25, 0.0645497, -0.966092, 1.73472e-18],
                [ -0.25, 0.0645497, 0.0690066, -0.963624],
                [ -0.25, 0.0645497, 0.0690066, 0.0741249],
                [ -0.25, 0.0645497, 0.0690066, 0.0741249],
                [ -0.25, 0.0645497, 0.0690066, 0.0741249],
                [ -0.25, 0.0645497, 0.0690066, 0.0741249],
                [ -0.25, 0.0645497, 0.0690066, 0.0741249],
                [ -0.25, 0.0645497, 0.0690066, 0.0741249],
                [ -0.25, 0.0645497, 0.0690066, 0.0741249],
                [ -0.25, 0.0645497, 0.0690066, 0.0741249],
                [ -0.25, 0.0645497, 0.0690066, 0.0741249],
                [ -0.25, 0.0645497, 0.0690066, 0.0741249],
                [ -0.25, 0.0645497, 0.0690066, 0.0741249],
                [ -0.25, 0.0645497, 0.0690066, 0.0741249]])
    V = np.array([[ -0.25, 0.0645497, 0.0690066, 0.0741249],
                [ -0.25, -0.968246, 5.20417e-18, 1.04083e-17],
                [ -0.25, 0.0645497, -0.966092, 1.73472e-18],
                [ -0.25, 0.0645497, 0.0690066, -0.963624],
                [ -0.25, 0.0645497, 0.0690066, 0.0741249],
                [ -0.25, 0.0645497, 0.0690066, 0.0741249],
                [ -0.25, 0.0645497, 0.0690066, 0.0741249],
                [ -0.25, 0.0645497, 0.0690066, 0.0741249],
                [ -0.25, 0.0645497, 0.0690066, 0.0741249],
                [ -0.25, 0.0645497, 0.0690066, 0.0741249],
                [ -0.25, 0.0645497, 0.0690066, 0.0741249],
                [ -0.25, 0.0645497, 0.0690066, 0.0741249],
                [ -0.25, 0.0645497, 0.0690066, 0.0741249],
                [ -0.25, 0.0645497, 0.0690066, 0.0741249],
                [ -0.25, 0.0645497, 0.0690066, 0.0741249],
                [ -0.25, 0.0645497, 0.0690066, 0.0741249]])
    S = np.array([[ 0.0625, 0, 0, 0],
                [ -0, 0, 0, 0],
                [ -0, 0, 0, 0],
                [ -0, 0, 0, 0]])

    
    U = np.array([[-0.5, 0.288675],[-0.5, -0.866025],[-0.5, 0.288675],[-0.5, 0.288675]])
    V = np.array([[-0.5, 0.288675],[-0.5, -0.866025],[-0.5, 0.288675],[-0.5, 0.288675]])
    S = np.array([[0.25, 0],[0, 0]])


    U = np.array([[-0.5, 0.288675],[-0.5, -0.866025],[-0.5, 0.288675],[-0.5, 0.288675]])
    V = np.array([[-0.5, 0.288675],[-0.5, -0.866025],[-0.5, 0.288675],[-0.5, 0.288675]])
    S = np.array([[0.25, 0],[0, 0]])

    U = np.array([[0, 1],[1, 0],[0, 0],[0, 0]])
    V = np.array([[1, 0],[0, 1],[0, 0],[0, 0]])
    S = np.array([[1, 0],[0, 0]])


    randvec = vector_P(recon_P(U,S,V))
    '''
    

    print('recon P: ', recon_P(U,S,V))
    outvec_lr = vector_P(recon_P(U, S, V))
    print('IV: ', outvec_lr)
    print(sum(outvec_lr))
    #print_comparison(str_states, outvec, outvec_lr, 0, 0.01, 5)

    #print_bool(str_states, outvec, outvec_lr, 1e-5)

    #print(P)
    #print(vector_P(P))
    print('initial error: ', np.amax(recon_P(U,S,V)-P))
    #U, S, V = low_rank_step(0.0, full_matrix, U, S, V)
    #print('recon_P: ', recon_P(U,S,V))

    print("\n\n")

    U, S, V = lowrank_step(t_final, full_matrix, U, S, V, tau)
    outvec_lr = vector_P(recon_P(U, S, V))

    #outvec_lr = outvec_lr/np.sum(outvec_lr)
    outvec_lr = outvec_lr.reshape(2**n, order = 'F')
    #print(np.sum(outvec_lr))
    #print(outvec_lr)
    #print(U)
    #print(S)
    #print(np.matmul(U,S))
    #print(full_matrix, np.linalg.eig(full_matrix))

    outvec = matrix_step(t_final, full_matrix, randvec, tau)
    #outvec = expm_multiply(t_final*full_matrix,randvec)
    #print(outvec)

    outvec_lr = apply_Perm(large_P, outvec_lr)
    outvec = apply_Perm(large_P, outvec)
    #print(type(outvec_lr), type(outvec))

    print(sum(outvec_lr))
    print_comparison(str_states, outvec, outvec_lr, 0, 0.01, 5)

    print_bool(str_states, outvec, outvec_lr, 1e-5)

elif problem=="LARGE":
    #####
    ##### gene protein/products paper, Benso, Di Carlo, et al 2014
    #####


    # gene protein/products paper, Benso, Di Carlo, et al 2014
    '''
    n = 22
    IV = sys.argv[1]       # 'uni, rand, single'
    rank = int(sys.argv[2])     # 4
    tau = float(sys.argv[3])      # 0.1
    t_final = float(sys.argv[4])  # 50
    perm_type = sys.argv[5]   # 'id', 'tot_rand', 'fix_rand', 'green', 'purple_cut', 'red_large_cut', 'yellow'
    
    '''
    n = 22
    IV = 'uni'
    rank = 2
    tau = 0.1
    t_final = 30
    perm_type = 'mTor_6cuts'
    #'''
    # '''
    #print(IV, type(IV))
    #print(rank, type(rank))
    #print(tau, type(tau))
    #print(t_final, -type(t_final))


    states = [x for x in it.product((False, True), repeat=n)]
    str_states = [''.join([str(y) for y in x]).replace('False','0').replace('True','1') for x in states]


    if perm_type == 'id': #blue
        small_P = list(range(n))
        #small_P = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21] #identity
    elif perm_type == 'tot_rand':
        small_P = np.random.permutation(n)
    elif perm_type == 'fix_rand':
        small_P = [ 8,  3,  6, 14,  1, 12, 17, 10, 16,  9, 15, 19, 18, 11,  2, 20,  7, 21,  0, 13,  5,  4] #fix
    elif perm_type == 'yellow':
        small_P = [ 0,  11,  1, 12,  2, 3, 13, 4, 14,  15, 16, 5, 17, 6,  18, 19,  20, 21,  7, 8,  9,  10] #fix
    elif perm_type == 'green': #green
        small_P = [0, 1, 2, 3, 20, 18, 16, 15, 14, 9, 10, 11, 12, 13, 8, 7, 6, 17, 5, 19, 4, 21]  #red cut small print
    elif perm_type == 'purple_cut':
        small_P = [0, 1, 2, 3, 4, 5, 6, 7, 8, 20, 21, 11, 12, 13, 14, 15, 16, 17, 18, 19, 9, 10] #purple cut
    elif perm_type == 'red_large_cut':
        small_P = [0, 1, 2, 3, 4, 11, 12, 13, 14, 9, 10, 5, 6, 7, 8, 15, 16, 17, 18, 19, 20, 21] #red large cut
    elif perm_type == 'mTor23':
        small_P = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 14, 10, 11, 12, 13, 15, 16, 17, 18, 19, 20, 21]
    elif perm_type == 'mTor17':
        small_P = [0, 1, 2, 3, 4, 5, 6, 7, 9, 10, 14, 8, 11, 12, 13, 15, 16, 17, 18, 19, 20, 21]
    elif perm_type == 'mTor5':
        small_P = [0, 1, 9, 10, 13, 14, 15, 16, 17, 18, 19, 2, 3, 4, 5, 6, 7, 8, 11, 12, 20, 21]
    elif perm_type == 'mTor1_15':
        small_P = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 10, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21]    
    elif perm_type == 'mTor1_17':
        small_P = [0, 1, 2, 3, 4, 5, 6, 7, 8, 11, 12, 9, 10, 13, 14, 15, 16, 17, 18, 19, 20, 21] 
    elif perm_type == 'mTor_6cuts':
        small_P = [2, 3, 4, 5, 6, 7, 8, 14, 15, 16, 20, 0, 1, 9, 10, 11, 12, 13, 17, 18, 19, 21]
    else:
        print('no valid permutation pattern selected')
        exit()

    
    small_Pinv = Perm_inv(small_P)
    large_P = generate_large_Perm(small_P)
    large_Pinv = generate_large_Perm(small_Pinv)

    P = Perm_matrix(large_P)
    P_inv = Perm_matrix(large_Pinv)

    rule_mat = generate_rule_matrices(states, P, P_inv, prob='large')
    full_matrix = full_rule_matrix(rule_mat)

    # set type of initial distribution
    # set initial condition as random
    np.random.seed(123)
    if IV == 'rand':
        randvec = np.random.rand(2**n)
        randvec = randvec/np.sum(randvec)

    # set initial condition as uniform
    elif IV == 'uni':
        randvec = np.ones(2 ** n) / (2 ** n)
    elif IV == 'single':
        IV = randvec = np.zeros(2 ** n)
        #randvec[randint(0,2**n-1)] = 1.0
        randvec[387] = 1.0
    else:
        print('IV not correctly set. Choose between rand, uni, or single')
        exit()

    randvec = np.array(apply_Perm(large_Pinv, randvec))
    P = randvec.reshape(2**int(n/2),2**int(n/2), order = 'F')
    print(P)
    # exit()

    
    print('starting low-rank solution...')
    U, S, V = get_low_rank_factors(P,rank)

    #print(P)
    #print(vector_P(P))
    print('initial error: ', np.amax(recon_P(U,S,V)-P))
    #U, S, V = low_rank_step(0.0, full_matrix, U, S, V)
    #print('recon_P: ', recon_P(U,S,V))

    print("\n\n")


    U, S, V = lowrank_step(t_final, full_matrix, U, S, V, tau, large_P)
    outvec_lr = vector_P(recon_P(U, S, V))

    outvec_lr = outvec_lr/np.sum(outvec_lr)
    outvec_lr = outvec_lr.reshape(2**n, order = 'F')
    #print(np.sum(outvec_lr))
    #print(outvec_lr)
    #print(U)
    #print(S)
    #print(np.matmul(U,S))
    #print(full_matrix, np.linalg.eig(full_matrix))
    

    print('starting exact solution...')
    outvec = matrix_step(t_final, full_matrix, randvec, tau, large_P)

    #outvec = expm_multiply(t_final*full_matrix,randvec)
    #print(outvec)

    outvec_lr = apply_Perm(large_P, outvec_lr)
    outvec = apply_Perm(large_P, outvec)
    
    print_comparison(str_states, outvec, outvec_lr, 0, 0.001, 20)

    print_bool(str_states, outvec, outvec_lr, 1e-6)


elif problem=="LARGE_ART":
    n = 24
    IV = 'uni'
    rank = 11
    tau = 0.1
    t_final = 50

    states = [x for x in it.product((False, True), repeat=n)]
    str_states = [''.join([str(y) for y in x]).replace('False','0').replace('True','1') for x in states]

    small_P = list(range(n))
    small_Pinv = Perm_inv(small_P)
    large_P = generate_large_Perm(small_P)
    large_Pinv = generate_large_Perm(small_Pinv)

    P = Perm_matrix(large_P)
    P_inv = Perm_matrix(large_Pinv)

    rule_mat = generate_rule_matrices(states, P, P_inv, prob='large_art')
    full_matrix = full_rule_matrix(rule_mat)

    # set type of initial distribution
    # set initial condition as random
    np.random.seed(123)
    if IV == 'rand':
        randvec = np.random.rand(2**n)
        randvec = randvec/np.sum(randvec)

    # set initial condition as uniform
    elif IV == 'uni':
        randvec = np.ones(2 ** n) / (2 ** n)
    elif IV == 'single':
        IV = randvec = np.zeros(2 ** n)
        #randvec[randint(0,2**n-1)] = 1.0
        randvec[387] = 1.0
    else:
        print('IV not correctly set. Choose between rand, uni, or single')
        exit()

    randvec = np.array(apply_Perm(large_Pinv, randvec))
    P = randvec.reshape(2**int(n/2),2**int(n/2), order = 'F')


    print('starting low-rank solution...')
    U, S, V = get_low_rank_factors(P,rank)

    #print(P)
    #print(vector_P(P))
    print('initial error: ', np.amax(recon_P(U,S,V)-P))
    #U, S, V = low_rank_step(0.0, full_matrix, U, S, V)
    #print('recon_P: ', recon_P(U,S,V))

    print("\n\n")


    U, S, V = lowrank_step(t_final, full_matrix, U, S, V, tau)
    outvec_lr = vector_P(recon_P(U, S, V))

    outvec_lr = outvec_lr/np.sum(outvec_lr)
    outvec_lr = outvec_lr.reshape(2**n, order = 'F')
    #print(np.sum(outvec_lr))
    #print(outvec_lr)
    #print(U)
    #print(S)
    #print(np.matmul(U,S))
    #print(full_matrix, np.linalg.eig(full_matrix))


    print('starting exact solution...')
    outvec = matrix_step(t_final, full_matrix, randvec, tau)

    #outvec = expm_multiply(t_final*full_matrix,randvec)
    #print(outvec)

    outvec_lr = apply_Perm(large_P, outvec_lr)
    outvec = apply_Perm(large_P, outvec)
    
    print_comparison(str_states, outvec, outvec_lr, 0, 0.001, 20)

    print_bool(str_states, outvec, outvec_lr, 1e-6)
elif problem=="test":
    n = 24
    IV = 'uni'
    rank = 11
    tau = 0.1
    t_final = 50

    states = [x for x in it.product((False, True), repeat=n)]
    str_states = [''.join([str(y) for y in x]).replace('False','0').replace('True','1') for x in states]

    small_P = list(range(n))
    small_Pinv = Perm_inv(small_P)
    large_P = generate_large_Perm(small_P)
    large_Pinv = generate_large_Perm(small_Pinv)

    P = Perm_matrix(large_P)
    P_inv = Perm_matrix(large_Pinv)

    t1 = perf_counter()
    rule_mat = generate_rule_matrices(states, P, P_inv, prob='large_art')
    t2 = perf_counter()
    full_matrix = full_rule_matrix(rule_mat)
    t3 = perf_counter()

    # set type of initial distribution
    # set initial condition as random
    np.random.seed(123)
    randvec = np.random.rand(2**n)
    randvec = randvec/np.sum(randvec)
    
    t4 = perf_counter()
    outvec = matrix_step(t_final, full_matrix, randvec, tau)
    t5 = perf_counter()

    print(t2-t1, t3-t2, t5-t4)

else:
    print('There is no such problem: {}'.format(problem))



