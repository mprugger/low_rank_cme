from sympy import *

names = ["ERK", "P27361", "RSK", "Q15418", "mir_1976", "MLL", "Q03164", "HOAX9", "mir_196b", "TSC12", "P31749", "PDK1", "Q15118", "AKT", "RHEB", "Q15382", "GBL", "Q9BVC4", "mTOR", "P42345", "RICTOR", "Q6R327"]

def idx_from_rule(rule):
    return [names.index(y) for y in [str(x) for x in rule.free_symbols]]

for name in names:
    locals()[name] = symbols(name)

def apply_perm(perm, states):
    N = len(perm)
    states_t = [states[perm[i]] for i in range(N)]
    return states_t


rules = [ERK,                              # ERK
        ERK,                               # P27361 
        RSK,                               # RSK 
        RSK,                               # Q15418  
        RSK,                               # mir-1976 
        MLL,                               # MLL 
        MLL & (~mir_1976),                 # Q03164 
        Q03164,                            # HOAX9  
        HOAX9,                             # mir-196b 
        (~(P27361 & Q15418)) & ~(P31749),  # TSC1/2  
        AKT,                               # P31749  
        PDK1,                              # PDK1 
        PDK1,                              # Q15118 
        Q15118 & Q6R327 & P42345 & Q9BVC4, # AKT  
        ~(TSC12),                          # RHEB  
        RHEB,                              # Q15382   
        Q15382,                            # GBL  
        GBL,                               # Q9BVC4  
        Q15382,                            # mTOR  
        mTOR,                              # P42345  
        Q15382,                            # RICTOR  
        RICTOR & ~(mir_196b)]              # Q6R327


def generate_rules(fn, label, perm):
    d = len(names)
    perm_names = apply_perm(perm, names)
    perm_rules = apply_perm(perm, rules)

    fs = open(fn, 'w')
    fs.write("RULE_SET({}, {}, {})\n".format(label, d, ','.join(['"'+x+'"' for x in perm_names])));

    str_template = """
template<> bool {}::rule<{}>(bitset<{}> x) {{
    return {};
}}
template<> vector<ind> {}::depends_on<{}>() {{
    return {{ {} }};
}}
"""
    complete_deps_list = []
    for i, name in zip(range(d), perm_names):
        deps = sorted(idx_from_rule(perm_rules[i]))
        complete_deps_list.append(deps)

        code = ccode(str(perm_rules[i]))
        for j, name in zip(range(d), perm_names):
            code = code.replace(name, 'x[{}]'.format(j))

        fs.write(str_template.format(label, i, d, code, label, i, ','.join([str(x) for x in deps])))

    fs.close()
    return complete_deps_list

part1 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 14]
part2 = [10, 11, 12, 13, 15, 16, 17, 18, 19, 20, 21]
perm = part1+part2
complete_deps_list = generate_rules('mTor1.hpp', "mTor1", perm)
part1 = [3, 2, 4, 0, 1, 5, 6, 7, 8, 9, 10]
part2 = [11, 12, 13, 19, 15, 16, 14, 18, 17, 20, 21]
perm = part1+part2
complete_deps_list = generate_rules('MixMtor.hpp', "MixMtor", perm)
part1 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
part2 = [11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21]
perm = part1+part2
complete_deps_list = generate_rules('orig.hpp', "orig", perm)
part1 = [0, 2, 4, 5, 7, 11, 13, 18, 19, 20, 21]
part2 = [1, 3, 6, 8, 9, 10, 12, 14, 15, 16, 17]
perm = part1+part2
complete_deps_list = generate_rules('yellow.hpp', "yellow", perm)

exit()

# import metis
import networkx as nx
from pylab import *
#print(f"Complete list = {complete_deps_list}")

def metis2Edgeset(g):
    edgelist = []
    for (i, l) in enumerate(g):
        for e in l:
            if i != e:
                edgelist.append((i,e))
    return edgelist
    
G = nx.Graph(metis2Edgeset(complete_deps_list))
GD = nx.DiGraph(metis2Edgeset(complete_deps_list))
#startpartition = ({8, 3, 6, 14, 1, 12, 17, 10, 16, 9, 15}, {19, 18, 11, 2, 20, 7, 21, 0, 13, 5, 4})
bisection = nx.algorithms.community.kernighan_lin_bisection(G, max_iter=2**32)# partition=startpartition)
print(bisection)
#color_map = ['green' if node in bisection[0]  else 'white' for node in GD]
#color_map_e = ['blue' if (e1 in bisection[0] and e2 in bisection[1]) or (e1 in bisection[1] and e2 in bisection[0]) else 'grey' for e1, e2 in GD.edges()]
#pos = nx.nx_agraph.pygraphviz_layout(GD, prog="dot", root=1)
#nx.draw(GD, pos, node_color=color_map, edge_color=color_map_e)
#plt.draw()
#print(f"{len(bisection[0])} {len(bisection[1])}")
#part1 = sort(list(bisection[0]))
#part2 = sort(list(bisection[1]))


#edgecuts = []
#for e1, e2 in GD.edges():
#    if (e1 in bisection[0] and e2 in bisection[1]) or (e1 in bisection[1] and e2 in bisection[0]):
#        edgecuts.append((e1,e2))
#parts = np.zeros(d)
#parts[list(bisection[1])] = 1 

'''
# how many edges go from one partition to the other
def metric_cuts(part1, part2, deps1, deps2):
    if len(part1)!=len(deps1) or len(part2)!=len(deps2):
        print('ERROR part1/2 and deps1/2 have to be equal in length')
        exit(1)
    part1_alldeps = sorted([x for l in deps1 for x in l if x not in part1])
    part2_alldeps = sorted([x for l in deps2 for x in l if x not in part2])
    return len(part1_alldeps)/len(part1), len(part2_alldeps)/len(part2)

def metric_gamma(part1, part2, deps1, deps2):
    if len(part1)!=len(deps1) or len(part2)!=len(deps2):
        print('ERROR part1/2 and deps1/2 have to be equal in length')
        exit(1)
    deps1_r = [set(x).intersection(part2) for x in deps1]
    deps2_r = [set(x).intersection(part1) for x in deps2]
    deps1_l = [len(x) for x in deps1_r]
    deps2_l = [len(x) for x in deps2_r]
    return max(deps1_l), max(deps2_l)
    # print(deps1_l, deps2_l)
    # return sum(deps1_l)/len(part1), sum(deps2_l)/len(part2)



d = len(complete_deps_list)
print('partitioning_metric: ', metric_cuts(range(int(d/2)), range(int(d/2),d), complete_deps_list[0:int(d/2)], complete_deps_list[int(d/2):]))


import metis
import networkx as nx
from pylab import *
(edgecuts, parts) = metis.part_graph(complete_deps_list, 2)
print(edgecuts, parts)

part1 = [i for i in range(d) if parts[i]==0]
part2 = [i for i in range(d) if parts[i]==1]
print(len(part1), len(part2))
print(part1)
print(part2)

deps_part1 = [x for (i,x) in zip(range(d),complete_deps_list) if parts[i]==0]
deps_part2 = [x for (i,x) in zip(range(d),complete_deps_list) if parts[i]==1]
print('partitioning_metric new: ', metric_cuts(part1, part2, deps_part1, deps_part2))


perm = part1+part2
complete_deps_list = generate_rules('pancreaticcancer1b.hpp', "PANCREATICCANCER1b", perm)

#d = len(complete_deps_list)
#adj = np.zeros((d,d))
#for i, deps in zip(range(d), complete_deps_list):
#   for e in deps:
#       adj[i, e] = 1.0
#
#G = nx.from_numpy_matrix(adj)
## pos = {i:(-1.0 if parts[i]==0 else 1.0,float(i)) for i in range(d)}
#pos = {i:(-1.0 if i<int(d/2) else 1.0,float(i)) for i in range(d)}
#print(pos)
##for i in range(d):
##    if parts[i]==0:
##        G.nodes[i]['pos'] = (-1.0, float(i))
##    else:
##        G.nodes[i]['pos'] = (1.0, float(i))
## G.nodes[0]['pos'] = (-10.0, -5.0)
#
#nx.draw(G, pos)
#show()
'''
