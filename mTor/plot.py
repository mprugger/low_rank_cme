import matplotlib.pyplot as plt

print(sum([23, 21, 17, 5, 3, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]))
columns = ('nw23', 'BLUE21', 'nw17', 'nw5', 'PURPLE3', 'nw2_0', 'nw2_1', 'nw2_2', 'nw2_3', 'GREEN1', 'nw1_1', 'nw1_2', 'nw1_3', 'nw1_4', 'nw1_5', 'nw1_6', 'nw1_7', 'nw1_8', 'nw1_9', 'nw1_10', 'nw1_11', 'nw1_12', 'nw1_13', 'nw1_14', 'nw1_15', 'nw1_16', 'nw1_17', 'nw1_18', 'nw1_19', 'nw1_20', 'nw1_21', 'nw1_22')
plt.bar(range(32), [23, 21, 17, 5, 3, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], color = ['skyblue', 'mediumblue', 'navy', 'plum', 'purple', 'lightgrey', 'lightgrey', 'lightgrey', 'lightgrey', 'green', 'lightgrey', 'lightgrey', 'lightgrey', 'lightgrey', 'lightgrey', 'lightgrey', 'lightgrey', 'lightgrey', 'lightgrey', 'lightgrey', 'lightgrey', 'lightgrey', 'lightgrey', 'lightgrey', 'cornflowerblue', 'lightgrey', 'blueviolet', 'lightgrey', 'lightgrey', 'lightgrey', 'lightgrey', 'lightgrey'])
plt.xticks(range(32), columns, rotation =90)
plt.tight_layout()
plt.show()
