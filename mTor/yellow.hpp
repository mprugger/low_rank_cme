RULE_SET(yellow, 22, "ERK","RSK","mir_1976","MLL","HOAX9","PDK1","AKT","mTOR","P42345","RICTOR","Q6R327","P27361","Q15418","Q03164","mir_196b","TSC12","P31749","Q15118","RHEB","Q15382","GBL","Q9BVC4")

template<> bool yellow::rule<0>(bitset<22> x) {
    return x[0];
}
template<> vector<ind> yellow::depends_on<0>() {
    return { 0 };
}

template<> bool yellow::rule<1>(bitset<22> x) {
    return x[1];
}
template<> vector<ind> yellow::depends_on<1>() {
    return { 1 };
}

template<> bool yellow::rule<2>(bitset<22> x) {
    return x[1];
}
template<> vector<ind> yellow::depends_on<2>() {
    return { 1, 2 };
}

template<> bool yellow::rule<3>(bitset<22> x) {
    return x[3];
}
template<> vector<ind> yellow::depends_on<3>() {
    return { 3 };
}

template<> bool yellow::rule<4>(bitset<22> x) {
    return x[13];
}
template<> vector<ind> yellow::depends_on<4>() {
    return { 4, 13 };
}

template<> bool yellow::rule<5>(bitset<22> x) {
    return x[5];
}
template<> vector<ind> yellow::depends_on<5>() {
    return { 5 };
}

template<> bool yellow::rule<6>(bitset<22> x) {
    return x[8] && x[17] && x[10] && x[21];
}
template<> vector<ind> yellow::depends_on<6>() {
    return { 6, 8, 10, 17, 21 };
}

template<> bool yellow::rule<7>(bitset<22> x) {
    return x[19];
}
template<> vector<ind> yellow::depends_on<7>() {
    return { 7, 19 };
}

template<> bool yellow::rule<8>(bitset<22> x) {
    return x[7];
}
template<> vector<ind> yellow::depends_on<8>() {
    return { 7, 8 };
}

template<> bool yellow::rule<9>(bitset<22> x) {
    return x[19];
}
template<> vector<ind> yellow::depends_on<9>() {
    return { 9, 19 };
}

template<> bool yellow::rule<10>(bitset<22> x) {
    return x[9] && !x[14];
}
template<> vector<ind> yellow::depends_on<10>() {
    return { 9, 10, 14 };
}

template<> bool yellow::rule<11>(bitset<22> x) {
    return x[0];
}
template<> vector<ind> yellow::depends_on<11>() {
    return { 0, 11 };
}

template<> bool yellow::rule<12>(bitset<22> x) {
    return x[1];
}
template<> vector<ind> yellow::depends_on<12>() {
    return { 1, 12 };
}

template<> bool yellow::rule<13>(bitset<22> x) {
    return x[3] && !x[2];
}
template<> vector<ind> yellow::depends_on<13>() {
    return { 2, 3, 13};
}

template<> bool yellow::rule<14>(bitset<22> x) {
    return x[4];
}
template<> vector<ind> yellow::depends_on<14>() {
    return { 4, 14 };
}

template<> bool yellow::rule<15>(bitset<22> x) {
    return !x[16] && !(x[11] && x[12]);
}
template<> vector<ind> yellow::depends_on<15>() {
    return { 11, 12, 15, 16};
}

template<> bool yellow::rule<16>(bitset<22> x) {
    return x[6];
}
template<> vector<ind> yellow::depends_on<16>() {
    return { 6, 16 };
}

template<> bool yellow::rule<17>(bitset<22> x) {
    return x[5];
}
template<> vector<ind> yellow::depends_on<17>() {
    return { 5, 17 };
}

template<> bool yellow::rule<18>(bitset<22> x) {
    return !x[15];
}
template<> vector<ind> yellow::depends_on<18>() {
    return { 15, 18 };
}

template<> bool yellow::rule<19>(bitset<22> x) {
    return x[18];
}
template<> vector<ind> yellow::depends_on<19>() {
    return { 18, 19 };
}

template<> bool yellow::rule<20>(bitset<22> x) {
    return x[19];
}
template<> vector<ind> yellow::depends_on<20>() {
    return { 19, 20 };
}

template<> bool yellow::rule<21>(bitset<22> x) {
    return x[20];
}
template<> vector<ind> yellow::depends_on<21>() {
    return { 20, 21 };
}
