RULE_SET(PANCREATICCANCER17, 34, "HMGB1","TLR24","RAGE","MYD88","RAS","RAC1","RAF","PI3K","PIP3","AKT","PTEN","MDM2","P53","ARF","BAX","BclXL","Apoptosis","Proliferate","IRAKs","MEK","ERK","AP1","TAB1","Myc","INK4a","IKK","CyclinD","A20","E2F","IkB","RB","NFkB","P21","CyclinE")

template<> bool PANCREATICCANCER17::rule<0>(bitset<34> x) {
    return x[0];
}
template<> vector<ind> PANCREATICCANCER17::depends_on<0>() {
    return { 0 };
}

template<> bool PANCREATICCANCER17::rule<1>(bitset<34> x) {
    return x[0] || x[1];
}
template<> vector<ind> PANCREATICCANCER17::depends_on<1>() {
    return { 0,1 };
}

template<> bool PANCREATICCANCER17::rule<2>(bitset<34> x) {
    return x[0] || x[2];
}
template<> vector<ind> PANCREATICCANCER17::depends_on<2>() {
    return { 0,2 };
}

template<> bool PANCREATICCANCER17::rule<3>(bitset<34> x) {
    return x[3] || x[1];
}
template<> vector<ind> PANCREATICCANCER17::depends_on<3>() {
    return { 1,3 };
}

template<> bool PANCREATICCANCER17::rule<4>(bitset<34> x) {
    return x[2] || x[4];
}
template<> vector<ind> PANCREATICCANCER17::depends_on<4>() {
    return { 2,4 };
}

template<> bool PANCREATICCANCER17::rule<5>(bitset<34> x) {
    return x[3] || x[5];
}
template<> vector<ind> PANCREATICCANCER17::depends_on<5>() {
    return { 3,5 };
}

template<> bool PANCREATICCANCER17::rule<6>(bitset<34> x) {
    return x[9] || x[6] || x[4];
}
template<> vector<ind> PANCREATICCANCER17::depends_on<6>() {
    return { 4,6,9 };
}

template<> bool PANCREATICCANCER17::rule<7>(bitset<34> x) {
    return x[7] || x[5] || x[4];
}
template<> vector<ind> PANCREATICCANCER17::depends_on<7>() {
    return { 4,5,7 };
}

template<> bool PANCREATICCANCER17::rule<8>(bitset<34> x) {
    return !x[10] && (x[7] || x[8]);
}
template<> vector<ind> PANCREATICCANCER17::depends_on<8>() {
    return { 7,8,10 };
}

template<> bool PANCREATICCANCER17::rule<9>(bitset<34> x) {
    return x[9] || x[8];
}
template<> vector<ind> PANCREATICCANCER17::depends_on<9>() {
    return { 8,9 };
}

template<> bool PANCREATICCANCER17::rule<10>(bitset<34> x) {
    return x[12] || x[10];
}
template<> vector<ind> PANCREATICCANCER17::depends_on<10>() {
    return { 10,12 };
}

template<> bool PANCREATICCANCER17::rule<11>(bitset<34> x) {
    return !x[13] && (x[9] || x[11] || x[12]);
}
template<> vector<ind> PANCREATICCANCER17::depends_on<11>() {
    return { 9,11,12,13 };
}

template<> bool PANCREATICCANCER17::rule<12>(bitset<34> x) {
    return x[12] && !x[11];
}
template<> vector<ind> PANCREATICCANCER17::depends_on<12>() {
    return { 11,12 };
}

template<> bool PANCREATICCANCER17::rule<13>(bitset<34> x) {
    return x[13] || x[28];
}
template<> vector<ind> PANCREATICCANCER17::depends_on<13>() {
    return { 13,28 };
}

template<> bool PANCREATICCANCER17::rule<14>(bitset<34> x) {
    return x[14] || x[12];
}
template<> vector<ind> PANCREATICCANCER17::depends_on<14>() {
    return { 12,14 };
}

template<> bool PANCREATICCANCER17::rule<15>(bitset<34> x) {
    return !x[12] && (x[15] || x[31]);
}
template<> vector<ind> PANCREATICCANCER17::depends_on<15>() {
    return { 12,15,31 };
}

template<> bool PANCREATICCANCER17::rule<16>(bitset<34> x) {
    return !x[15] && (x[16] || x[14]);
}
template<> vector<ind> PANCREATICCANCER17::depends_on<16>() {
    return { 14,15,16 };
}

template<> bool PANCREATICCANCER17::rule<17>(bitset<34> x) {
    return x[33] && x[17];
}
template<> vector<ind> PANCREATICCANCER17::depends_on<17>() {
    return { 17,33 };
}

template<> bool PANCREATICCANCER17::rule<18>(bitset<34> x) {
    return x[18] || x[3];
}
template<> vector<ind> PANCREATICCANCER17::depends_on<18>() {
    return { 3,18 };
}

template<> bool PANCREATICCANCER17::rule<19>(bitset<34> x) {
    return x[19] || x[6];
}
template<> vector<ind> PANCREATICCANCER17::depends_on<19>() {
    return { 6,19 };
}

template<> bool PANCREATICCANCER17::rule<20>(bitset<34> x) {
    return x[20] || x[18] || x[19];
}
template<> vector<ind> PANCREATICCANCER17::depends_on<20>() {
    return { 18,19,20 };
}

template<> bool PANCREATICCANCER17::rule<21>(bitset<34> x) {
    return x[21] || x[20];
}
template<> vector<ind> PANCREATICCANCER17::depends_on<21>() {
    return { 20,21 };
}

template<> bool PANCREATICCANCER17::rule<22>(bitset<34> x) {
    return x[18] || x[22];
}
template<> vector<ind> PANCREATICCANCER17::depends_on<22>() {
    return { 18,22 };
}

template<> bool PANCREATICCANCER17::rule<23>(bitset<34> x) {
    return x[20] || x[23] || x[31];
}
template<> vector<ind> PANCREATICCANCER17::depends_on<23>() {
    return { 20,23,31 };
}

template<> bool PANCREATICCANCER17::rule<24>(bitset<34> x) {
    return x[24];
}
template<> vector<ind> PANCREATICCANCER17::depends_on<24>() {
    return { 24 };
}

template<> bool PANCREATICCANCER17::rule<25>(bitset<34> x) {
    return !x[27] && (x[9] || x[20] || x[25] || x[22]);
}
template<> vector<ind> PANCREATICCANCER17::depends_on<25>() {
    return { 9,20,22,25,27 };
}

template<> bool PANCREATICCANCER17::rule<26>(bitset<34> x) {
    return !(x[24] || x[32]) && (x[21] || x[26] || x[23] || x[31]);
}
template<> vector<ind> PANCREATICCANCER17::depends_on<26>() {
    return { 21,23,24,26,31,32 };
}

template<> bool PANCREATICCANCER17::rule<27>(bitset<34> x) {
    return x[27] || x[31];
}
template<> vector<ind> PANCREATICCANCER17::depends_on<27>() {
    return { 27,31 };
}

template<> bool PANCREATICCANCER17::rule<28>(bitset<34> x) {
    return !x[30] && (x[28] || x[23]);
}
template<> vector<ind> PANCREATICCANCER17::depends_on<28>() {
    return { 23,28,30 };
}

template<> bool PANCREATICCANCER17::rule<29>(bitset<34> x) {
    return !x[25] && (x[29] || x[31]);
}
template<> vector<ind> PANCREATICCANCER17::depends_on<29>() {
    return { 25,29,31 };
}

template<> bool PANCREATICCANCER17::rule<30>(bitset<34> x) {
    return x[30] && !(x[26] || x[33]);
}
template<> vector<ind> PANCREATICCANCER17::depends_on<30>() {
    return { 26,30,33 };
}

template<> bool PANCREATICCANCER17::rule<31>(bitset<34> x) {
    return x[31] && !x[29];
}
template<> vector<ind> PANCREATICCANCER17::depends_on<31>() {
    return { 29,31 };
}

template<> bool PANCREATICCANCER17::rule<32>(bitset<34> x) {
    return x[32] || x[12];
}
template<> vector<ind> PANCREATICCANCER17::depends_on<32>() {
    return { 12,32 };
}

template<> bool PANCREATICCANCER17::rule<33>(bitset<34> x) {
    return !x[32] && (x[33] || x[28]);
}
template<> vector<ind> PANCREATICCANCER17::depends_on<33>() {
    return { 28,32,33 };
}
