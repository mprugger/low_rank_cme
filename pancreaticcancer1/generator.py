from sympy import *

names = ["HMGB1", "TLR24", "RAGE", "MYD88", "RAS", "RAC1", "IRAKs", "RAF", "MEK", "PI3K", "ERK", "AP1", "TAB1", "PIP3", "AKT", "Myc", "INK4a", "IKK", "CyclinD", "PTEN", "MDM2", "A20", "E2F", "IkB", "RB", "P53", "NFkB", "ARF", "P21", "BAX", "BclXL", "CyclinE", "Apoptosis", "Proliferate"]

def idx_from_rule(rule):
    return [names.index(y) for y in [str(x) for x in rule.free_symbols]]

for name in names:
    locals()[name] = symbols(name)

def apply_perm(perm, states):
    N = len(perm)
    states_t = [states[perm[i]] for i in range(N)]
    return states_t


rules = [HMGB1,        # HMGB1
        TLR24 | HMGB1, # TLR24
        RAGE | HMGB1,  # RAGE
        MYD88 | TLR24, # MYD88
        RAS | RAGE,    # RAS
        RAC1 | MYD88,  # RAC1
        IRAKs | MYD88, # IRAKs
        RAF | RAS | AKT, # RAF
        MEK | RAF,       # MEK
        PI3K | RAC1 | RAS, # PI3k
        ERK | IRAKs | MEK, # ERK
        AP1 | ERK,         # AP1
        TAB1 | IRAKs,      # TAB1
        (PIP3 | PI3K) & (~PTEN), # PIP3
        AKT | PIP3,              # AKT
        Myc |ERK | NFkB,         # Myc
        INK4a,                   # INK4a
        (IKK | AKT | ERK | TAB1) & (~A20), # IKK
        (CyclinD | AP1 | Myc | NFkB) & (~(INK4a | P21)), # CyclinD
        PTEN | P53, # PTEN
        (MDM2 | AKT | P53) & (~ARF), # MDM2
        A20 | NFkB, # A20
        (E2F | Myc) & (~RB), # E2F
        (IkB | NFkB) & (~IKK), # IkB
        RB & (~(CyclinD | CyclinE)), # RB
        P53 & (~MDM2), # P53
        NFkB & (~IkB), # NFkB
        ARF | E2F, # ARF
        P21 | P53, # P21
        BAX | P53, # BAX
        (BclXL | NFkB) & (~P53), # BclXL
        (CyclinE | E2F) & (~P21), # CyclinE
        (Apoptosis | BAX)  & (~BclXL), # Apoptosis
        Proliferate & CyclinE] # Proliferate


def generate_rules(fn, label, perm):
    d = len(names)
    perm_names = apply_perm(perm, names)
    perm_rules = apply_perm(perm, rules)

    fs = open(fn, 'w')
    fs.write("RULE_SET({}, {}, {})\n".format(label, d, ','.join(['"'+x+'"' for x in perm_names])));

    str_template = """
template<> bool {}::rule<{}>(bitset<{}> x) {{
    return {};
}}
template<> vector<ind> {}::depends_on<{}>() {{
    return {{ {} }};
}}
"""
    complete_deps_list = []
    for i, name in zip(range(d), perm_names):
        deps = sorted(idx_from_rule(perm_rules[i]))
        complete_deps_list.append(deps)

        code = ccode(str(perm_rules[i]))
        for j, name in zip(range(d), perm_names):
            code = code.replace(name, 'x[{}]'.format(j))

        fs.write(str_template.format(label, i, d, code, label, i, ','.join([str(x) for x in deps])))

    fs.close()
    return complete_deps_list

complete_deps_list = generate_rules('pancreaticcancer1a.hpp', "PANCREATICCANCER1a", range(len(names)))


import networkx as nx
from pylab import *
#print(f"Complete list = {complete_deps_list}")

def metis2Edgeset(g):
    edgelist = []
    for (i, l) in enumerate(g):
        for e in l:
            if i != e:
                edgelist.append((i,e))
    return edgelist
    
G = nx.Graph(metis2Edgeset(complete_deps_list))
GD = nx.DiGraph(metis2Edgeset(complete_deps_list))
#startpartition = ({8, 3, 6, 14, 1, 12, 17, 10, 16, 9, 15}, {19, 18, 11, 2, 20, 7, 21, 0, 13, 5, 4})
bisection = nx.algorithms.community.kernighan_lin_bisection(G, max_iter=2**32)# partition=startpartition)
print(bisection)
exit()


# how many edges go from one partition to the other
def metric_cuts(part1, part2, deps1, deps2):
    if len(part1)!=len(deps1) or len(part2)!=len(deps2):
        print('ERROR part1/2 and deps1/2 have to be equal in length')
        exit(1)
    part1_alldeps = sorted([x for l in deps1 for x in l if x not in part1])
    part2_alldeps = sorted([x for l in deps2 for x in l if x not in part2])
    return len(part1_alldeps)/len(part1), len(part2_alldeps)/len(part2)

def metric_gamma(part1, part2, deps1, deps2):
    if len(part1)!=len(deps1) or len(part2)!=len(deps2):
        print('ERROR part1/2 and deps1/2 have to be equal in length')
        exit(1)
    deps1_r = [set(x).intersection(part2) for x in deps1]
    deps2_r = [set(x).intersection(part1) for x in deps2]
    deps1_l = [len(x) for x in deps1_r]
    deps2_l = [len(x) for x in deps2_r]
    return max(deps1_l), max(deps2_l)
    # print(deps1_l, deps2_l)
    # return sum(deps1_l)/len(part1), sum(deps2_l)/len(part2)



d = len(complete_deps_list)
print('partitioning_metric: ', metric_cuts(range(int(d/2)), range(int(d/2),d), complete_deps_list[0:int(d/2)], complete_deps_list[int(d/2):]))


import metis
import networkx as nx
from pylab import *
(edgecuts, parts) = metis.part_graph(complete_deps_list, 2)
print(edgecuts, parts)

part1 = [i for i in range(d) if parts[i]==0]
part2 = [i for i in range(d) if parts[i]==1]
print(len(part1), len(part2))
print(part1)
print(part2)

deps_part1 = [x for (i,x) in zip(range(d),complete_deps_list) if parts[i]==0]
deps_part2 = [x for (i,x) in zip(range(d),complete_deps_list) if parts[i]==1]
print('partitioning_metric new: ', metric_cuts(part1, part2, deps_part1, deps_part2))


perm = part1+part2
complete_deps_list = generate_rules('pancreaticcancer1b.hpp', "PANCREATICCANCER1b", perm)

#d = len(complete_deps_list)
#adj = np.zeros((d,d))
#for i, deps in zip(range(d), complete_deps_list):
#   for e in deps:
#       adj[i, e] = 1.0
#
#G = nx.from_numpy_matrix(adj)
## pos = {i:(-1.0 if parts[i]==0 else 1.0,float(i)) for i in range(d)}
#pos = {i:(-1.0 if i<int(d/2) else 1.0,float(i)) for i in range(d)}
#print(pos)
##for i in range(d):
##    if parts[i]==0:
##        G.nodes[i]['pos'] = (-1.0, float(i))
##    else:
##        G.nodes[i]['pos'] = (1.0, float(i))
## G.nodes[0]['pos'] = (-10.0, -5.0)
#
#nx.draw(G, pos)
#show()
