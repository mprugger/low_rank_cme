RULE_SET(PANCREATICCANCER1b, 34, "HMGB1","AP1","Myc","INK4a","CyclinD","PTEN","E2F","IkB","RB","P53","NFkB","ARF","P21","BAX","BclXL","CyclinE","Apoptosis","TLR24","RAGE","MYD88","RAS","RAC1","IRAKs","RAF","MEK","PI3K","ERK","TAB1","PIP3","AKT","IKK","MDM2","A20","Proliferate")

template<> bool PANCREATICCANCER1b::rule<0>(bitset<34> x) {
    return x[0];
}
template<> vector<ind> PANCREATICCANCER1b::depends_on<0>() {
    return { 0 };
}

template<> bool PANCREATICCANCER1b::rule<1>(bitset<34> x) {
    return x[1] || x[26];
}
template<> vector<ind> PANCREATICCANCER1b::depends_on<1>() {
    return { 10,11 };
}

template<> bool PANCREATICCANCER1b::rule<2>(bitset<34> x) {
    return x[26] || x[2] || x[10];
}
template<> vector<ind> PANCREATICCANCER1b::depends_on<2>() {
    return { 10,15,26 };
}

template<> bool PANCREATICCANCER1b::rule<3>(bitset<34> x) {
    return x[3];
}
template<> vector<ind> PANCREATICCANCER1b::depends_on<3>() {
    return { 16 };
}

template<> bool PANCREATICCANCER1b::rule<4>(bitset<34> x) {
    return !(x[3] || x[12]) && (x[1] || x[4] || x[2] || x[10]);
}
template<> vector<ind> PANCREATICCANCER1b::depends_on<4>() {
    return { 11,15,16,18,26,28 };
}

template<> bool PANCREATICCANCER1b::rule<5>(bitset<34> x) {
    return x[9] || x[5];
}
template<> vector<ind> PANCREATICCANCER1b::depends_on<5>() {
    return { 19,25 };
}

template<> bool PANCREATICCANCER1b::rule<6>(bitset<34> x) {
    return !x[8] && (x[6] || x[2]);
}
template<> vector<ind> PANCREATICCANCER1b::depends_on<6>() {
    return { 15,22,24 };
}

template<> bool PANCREATICCANCER1b::rule<7>(bitset<34> x) {
    return !x[30] && (x[7] || x[10]);
}
template<> vector<ind> PANCREATICCANCER1b::depends_on<7>() {
    return { 17,23,26 };
}

template<> bool PANCREATICCANCER1b::rule<8>(bitset<34> x) {
    return x[8] && !(x[4] || x[15]);
}
template<> vector<ind> PANCREATICCANCER1b::depends_on<8>() {
    return { 18,24,31 };
}

template<> bool PANCREATICCANCER1b::rule<9>(bitset<34> x) {
    return x[9] && !x[31];
}
template<> vector<ind> PANCREATICCANCER1b::depends_on<9>() {
    return { 20,25 };
}

template<> bool PANCREATICCANCER1b::rule<10>(bitset<34> x) {
    return x[10] && !x[7];
}
template<> vector<ind> PANCREATICCANCER1b::depends_on<10>() {
    return { 23,26 };
}

template<> bool PANCREATICCANCER1b::rule<11>(bitset<34> x) {
    return x[11] || x[6];
}
template<> vector<ind> PANCREATICCANCER1b::depends_on<11>() {
    return { 22,27 };
}

template<> bool PANCREATICCANCER1b::rule<12>(bitset<34> x) {
    return x[12] || x[9];
}
template<> vector<ind> PANCREATICCANCER1b::depends_on<12>() {
    return { 25,28 };
}

template<> bool PANCREATICCANCER1b::rule<13>(bitset<34> x) {
    return x[13] || x[9];
}
template<> vector<ind> PANCREATICCANCER1b::depends_on<13>() {
    return { 25,29 };
}

template<> bool PANCREATICCANCER1b::rule<14>(bitset<34> x) {
    return !x[9] && (x[14] || x[10]);
}
template<> vector<ind> PANCREATICCANCER1b::depends_on<14>() {
    return { 25,26,30 };
}

template<> bool PANCREATICCANCER1b::rule<15>(bitset<34> x) {
    return !x[12] && (x[15] || x[6]);
}
template<> vector<ind> PANCREATICCANCER1b::depends_on<15>() {
    return { 22,28,31 };
}

template<> bool PANCREATICCANCER1b::rule<16>(bitset<34> x) {
    return !x[14] && (x[16] || x[13]);
}
template<> vector<ind> PANCREATICCANCER1b::depends_on<16>() {
    return { 29,30,32 };
}

template<> bool PANCREATICCANCER1b::rule<17>(bitset<34> x) {
    return x[0] || x[17];
}
template<> vector<ind> PANCREATICCANCER1b::depends_on<17>() {
    return { 0,1 };
}

template<> bool PANCREATICCANCER1b::rule<18>(bitset<34> x) {
    return x[0] || x[18];
}
template<> vector<ind> PANCREATICCANCER1b::depends_on<18>() {
    return { 0,2 };
}

template<> bool PANCREATICCANCER1b::rule<19>(bitset<34> x) {
    return x[19] || x[17];
}
template<> vector<ind> PANCREATICCANCER1b::depends_on<19>() {
    return { 1,3 };
}

template<> bool PANCREATICCANCER1b::rule<20>(bitset<34> x) {
    return x[18] || x[20];
}
template<> vector<ind> PANCREATICCANCER1b::depends_on<20>() {
    return { 2,4 };
}

template<> bool PANCREATICCANCER1b::rule<21>(bitset<34> x) {
    return x[19] || x[21];
}
template<> vector<ind> PANCREATICCANCER1b::depends_on<21>() {
    return { 3,5 };
}

template<> bool PANCREATICCANCER1b::rule<22>(bitset<34> x) {
    return x[22] || x[19];
}
template<> vector<ind> PANCREATICCANCER1b::depends_on<22>() {
    return { 3,6 };
}

template<> bool PANCREATICCANCER1b::rule<23>(bitset<34> x) {
    return x[29] || x[23] || x[20];
}
template<> vector<ind> PANCREATICCANCER1b::depends_on<23>() {
    return { 4,7,14 };
}

template<> bool PANCREATICCANCER1b::rule<24>(bitset<34> x) {
    return x[24] || x[23];
}
template<> vector<ind> PANCREATICCANCER1b::depends_on<24>() {
    return { 7,8 };
}

template<> bool PANCREATICCANCER1b::rule<25>(bitset<34> x) {
    return x[25] || x[21] || x[20];
}
template<> vector<ind> PANCREATICCANCER1b::depends_on<25>() {
    return { 4,5,9 };
}

template<> bool PANCREATICCANCER1b::rule<26>(bitset<34> x) {
    return x[26] || x[22] || x[24];
}
template<> vector<ind> PANCREATICCANCER1b::depends_on<26>() {
    return { 6,8,10 };
}

template<> bool PANCREATICCANCER1b::rule<27>(bitset<34> x) {
    return x[22] || x[27];
}
template<> vector<ind> PANCREATICCANCER1b::depends_on<27>() {
    return { 6,12 };
}

template<> bool PANCREATICCANCER1b::rule<28>(bitset<34> x) {
    return !x[5] && (x[25] || x[28]);
}
template<> vector<ind> PANCREATICCANCER1b::depends_on<28>() {
    return { 9,13,19 };
}

template<> bool PANCREATICCANCER1b::rule<29>(bitset<34> x) {
    return x[29] || x[28];
}
template<> vector<ind> PANCREATICCANCER1b::depends_on<29>() {
    return { 13,14 };
}

template<> bool PANCREATICCANCER1b::rule<30>(bitset<34> x) {
    return !x[32] && (x[29] || x[26] || x[30] || x[27]);
}
template<> vector<ind> PANCREATICCANCER1b::depends_on<30>() {
    return { 10,12,14,17,21 };
}

template<> bool PANCREATICCANCER1b::rule<31>(bitset<34> x) {
    return !x[11] && (x[29] || x[31] || x[9]);
}
template<> vector<ind> PANCREATICCANCER1b::depends_on<31>() {
    return { 14,20,25,27 };
}

template<> bool PANCREATICCANCER1b::rule<32>(bitset<34> x) {
    return x[32] || x[10];
}
template<> vector<ind> PANCREATICCANCER1b::depends_on<32>() {
    return { 21,26 };
}

template<> bool PANCREATICCANCER1b::rule<33>(bitset<34> x) {
    return x[15] || x[33];
}
template<> vector<ind> PANCREATICCANCER1b::depends_on<33>() {
    return { 31,33 };
}
