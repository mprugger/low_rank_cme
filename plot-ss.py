import matplotlib as mpl
mpl.use('Agg')

from sys import argv, exit
from netCDF4 import *
from pylab import *
import seaborn as sb
from time import time



if len(argv)<5:
    print('ERROR: at least five arguments, the input file name, the tolerance, a list of simulations to compare, the order of the species, and the output file name, are required.')
    exit(1)

fn = argv[1]
tol = float(argv[2])
fn_comp = []
for i in range(3,len(argv)-2):
    fn_comp.append(argv[i])
if argv[-2]=='':
    names_order = []
else:
    names_order = argv[-2].split(' ')
fn_out = argv[-1]

def load_lr(fn):
    with Dataset(fn,'r') as fs:
        d = fs.dimensions['d'].size
        r = fs.dimensions['r'].size
        n1 = fs.dimensions['n1'].size
        n2 = fs.dimensions['n2'].size

        U = fs.variables['U'][:].transpose()
        V = fs.variables['V'][:].transpose()
        S = fs.variables['S'][:].transpose()
        names = fs.variables['names'][:]

    return U, S, V, d, names

def mass(U, S, V):
    n1 = U.shape[0]
    n2 = V.shape[0]
    US = U.dot(S)
    mass = 0.0
    n_patch = 128 if n2>256 else 1
    # Larger patches (here of size 128) increase speed dramatically
    for K in range(round(n2/n_patch)):
        P_rowwise = US.dot(V[K*n_patch:(K+1)*n_patch,:].transpose())
        mass += sum(P_rowwise)
    print('mass: ', mass)
    return mass

def steadystates(U, S, V, d):
    n1 = U.shape[0]
    n2 = V.shape[0]

    m = mass(U, S, V)
    print('Done computing mass {} {} {} {}'.format(U.shape, S.shape,U.dtype, S.dtype))
    US = U.dot(S)
    l_ss = []
    # Larger patches (here of size 128) increase speed dramatically
    n_patch = 128 if n2>256 else 1
    for K in range(round(n2/n_patch)):
        P_rowwise = US.dot(V[K*n_patch:(K+1)*n_patch,:].transpose())

        ind1, ind2 = where(abs(P_rowwise)>tol*m)
        for j, k_sub in zip(ind1, ind2):
            k = k_sub + K*n_patch
            bin_str = bin(j + k*n1)[2:]
            bin_str = '0'*(d-len(bin_str)) + bin_str
            l_ss.append([P_rowwise[j,k_sub]/m, [int(x) for x in list(bin_str)]])

    l_ss = sorted(l_ss, reverse=True)
    return l_ss

def prob_ss(l_ss):
    return [x[0] for x in l_ss]

def prob(U, S, V, m, ss):
    d1 = int(round(log(U.shape[0])/log(2.0)))
    d2 = int(round(log(V.shape[0])/log(2.0)))
    ss1 = sum(array(ss)[-d1:]*array([2**(d1-i-1) for i in range(d1)]))
    ss2 = sum(array(ss)[:-d1]*array([2**(d2-i-1) for i in range(d2)]))
    out = U[ss1,:].dot(S).dot(V[ss2,:].transpose())/m
    return out

def reorder_to(ss, names):
    ind = [list(names).index(name) for name in names_order]
    # Since the steady states have the bit with index 0 on the right,
    # (which is not true for the names list) we have to reverse the
    # list here.
    reversed_ss = list(reversed(ss))
    return list(reversed([reversed_ss[i] for i in ind]))

def reorder_from(ss, names):
    ind = [list(names_order).index(nameo) for nameo in names]
    # Since the steady states have the bit with index 0 on the right,
    # (which is not true for the names list) we have to reverse the
    # list here.
    reversed_ss = list(reversed(ss))
    return list(reversed([reversed_ss[i] for i in ind]))

def matched_prob(U, S, V, m, l_ss, names):
    return [prob(U, S, V, m, reorder_from(ss[1], names)) for ss in l_ss]

figure(figsize=(12,9))

# plot the heatmap
subplot(1,2,1)
plt.rcParams['xtick.bottom'] = plt.rcParams['xtick.labelbottom'] = False
plt.rcParams['xtick.top'] = plt.rcParams['xtick.labeltop'] = True
title('Steady states (ordered by probability)')

print('File: {}'.format(fn))
U, S, V, d, names = load_lr(fn)
print('Determining steady steates')
l_ss = steadystates(U, S, V, d)
if len(names_order)==0:
    names_order = names.copy()
l_ss = [[ss[0], reorder_to(ss[1], names)] for ss in l_ss]
if len(l_ss) > 30:
    print('WARNING: {} steady states satisfy the tolerance. Plot will only show 30 steady states'.format(len(l_ss)))
    l_ss = l_ss[:30]
elif len(l_ss) == 0:
    print('ERROR: no steady states found that satisfy the given tolerance')
    exit(1)
hm = zeros((d, len(l_ss)))
for i, ss in zip(range(len(l_ss)),l_ss):
    hm[:,i] = array(ss[1])

print('Plotting')
g = sb.heatmap(hm, linewidth=0.5, cbar=False, cmap='vlag', square=True)
yticks(array(range(d))+0.5, reversed(names_order), rotation=0)
xticks(array(range(len(l_ss)))+0.5, ['SS{}'.format(i+1) for i in range(len(l_ss))], rotation=90)
gca().xaxis.set_ticks_position('top')

# plot the comparison
subplot(2,2,2)
title('Probability of steady states for different ranks')

hm = zeros((1+len(fn_comp),len(l_ss)))
hm[0,:] = prob_ss(l_ss)
l_r = []
for i, fnc in zip(range(1,len(fn_comp)+1),fn_comp):
    print('File: {}'.format(fnc))
    Uc, Sc, Vc, _, names_cmp = load_lr(fnc)
    l_r.append(Uc.shape[1])
    print('Compute mass')
    m = mass(Uc, Sc, Vc)
    print('Compute result')
    hm[i, :] = matched_prob(Uc, Sc, Vc, m, l_ss, names_cmp)

print('Plotting')
sb.heatmap(hm, linewidth=0.5, cmap='rocket', square=True, cbar_kws = dict(use_gridspec=False,location="bottom",pad=0.1))
yticks(array(range(0,len(fn_comp)+1))+0.5, ['r={}'.format(U.shape[1])]+['r={}'.format(r) for r in l_r], rotation=0)
xticks(array(range(len(l_ss)))+0.5, ['SS{}'.format(i+1) for i in range(len(l_ss))], rotation=90)



savefig(fn_out, bbox_inches='tight', pad_inches=0.05)

