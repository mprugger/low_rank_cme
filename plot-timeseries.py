import matplotlib as mpl
mpl.use('Agg')

from sys import argv, exit
from netCDF4 import *
from pylab import *
import seaborn as sb
from time import time
import numpy as np

fn_ts = []
for i in range(1,len(argv)):
    fn_ts.append(argv[i])

def load_lr(fn):
    with Dataset(fn,'r') as fs:
        d = fs.dimensions['d'].size
        r = fs.dimensions['r'].size
        n1 = fs.dimensions['n1'].size
        n2 = fs.dimensions['n2'].size

        U = fs.variables['U'][:].transpose()
        V = fs.variables['V'][:].transpose()
        S = fs.variables['S'][:].transpose()
        names = fs.variables['names'][:]

    return U, S, V, d, names

def mass(U, S, V):
    n1 = U.shape[0]
    n2 = V.shape[0]
    US = U.dot(S)
    return ones(n1).dot(US.dot(V.transpose().dot(ones(n2).transpose())))

def prob_integrated(m, U, S, V):
    n1 = U.shape[0]
    n2 = V.shape[0]
    d1 = int(round(log(n1)/log(2.0)))
    d2 = int(round(log(n2)/log(2.0)))
    US = U.dot(S)

    if m < d1:
        # first partition
        vec = array([(x % 2**(m+1)) < 2**m for x in range(n1)])
        vec = invert(vec)
        #print(vec[0:21])
        return vec.dot(US.dot(V.transpose().dot(ones(n2).transpose())))
    else:
        # second partition
        m -= d1
        vec = array([(x % 2**(m+1)) < 2**m for x in range(n2)])
        vec = invert(vec)
        #print(vec[0:21])
        return ones(n1).dot(US.dot(V.transpose().dot(vec.transpose())))

ts = []
for i in range(len(fn_ts)):
    probs = []
    U, S, V, d, names = load_lr(fn_ts[i])
    for j in range(len(names)):
        print(j, prob_integrated(j, U, S, V))
        probs.append(prob_integrated(j, U, S, V))
    ts.append(probs)

#print(ts)

print(np.array(ts).T)

for i in [40]:
    plt.plot(range(len(np.array(ts).T[i])), np.array(ts).T[i], label = names[i])
plt.legend()
savefig('test', bbox_inches='tight', pad_inches=0.05)

#for i in range(len(fn_ts)):
#    plt.plot([0,1],ts[0])
#plt.show()

#U, S, V, d, names = load_lr('/data1/PycharmProjects/low_rank_cme/low_rank_cme/build/apoptosisa_specinit_01/init_apop1_spec_TNF0_GF1.nc')
#U2, S2, V2, d, names = load_lr('/data1/PycharmProjects/low_rank_cme/low_rank_cme/build/apoptosisa_specinit_01/P-t0.nc')

#print('mass: ', mass(U,S,V))
#for i in range(41):
#    print(i, prob_integrated(i, U, S, V))
#    print(i, prob_integrated(i, U2, S2, V2))
