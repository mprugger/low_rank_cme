import numpy as np
import matplotlib.pyplot as plt


comp_rand_r2 = []
comp_rand_r4 = []
comp_rand_r8 = []
comp_rand_r16 = []
comp_single_r2 = []
comp_single_r4 = []
comp_single_r8 = []
comp_single_r16 = []
comp_uni_r2 = []
comp_uni_r4 = []
comp_uni_r8 = []
comp_uni_r16 = []


comp_uni_r2_purple = []
comp_uni_r4_purple = []
comp_uni_r8_purple = []
comp_uni_r16_purple = []

comp_uni_r2_green = []
comp_uni_r4_green = []
comp_uni_r8_green = []
comp_uni_r16_green = []

comp_uni_r2_red = []
comp_uni_r4_red = []
comp_uni_r8_red = []
comp_uni_r16_red = []

comp_uni_r2_rand = []
comp_uni_r4_rand = []
comp_uni_r8_rand = []
comp_uni_r16_rand = []


norm_type=np.inf

for i in range(0,60,10):
    exact = np.load('LARGE_rand2_r2_01_100/prob_dist_exact_t'+str(i)+'.npy')
    lr = np.load('LARGE_rand2_r2_01_100/prob_dist_lr_t'+str(i)+'.npy')
    comp_rand_r2.append(np.linalg.norm(exact-lr, ord=norm_type))
    exact = np.load('LARGE_rand2_r4_01_100/prob_dist_exact_t'+str(i)+'.npy')
    lr = np.load('LARGE_rand2_r4_01_100/prob_dist_lr_t'+str(i)+'.npy')
    comp_rand_r4.append(np.linalg.norm(exact-lr, ord=norm_type))
    exact = np.load('LARGE_rand2_r8_01_100/prob_dist_exact_t'+str(i)+'.npy')
    lr = np.load('LARGE_rand2_r8_01_100/prob_dist_lr_t'+str(i)+'.npy')
    comp_rand_r8.append(np.linalg.norm(exact-lr, ord=norm_type))
    exact = np.load('LARGE_rand2_r16_01_100/prob_dist_exact_t'+str(i)+'.npy')
    lr = np.load('LARGE_rand2_r16_01_100/prob_dist_lr_t'+str(i)+'.npy')
    comp_rand_r16.append(np.linalg.norm(exact-lr, ord=norm_type))
    exact = np.load('LARGE_single2_r2_01_100/prob_dist_exact_t'+str(i)+'.npy')
    lr = np.load('LARGE_single2_r2_01_100/prob_dist_lr_t'+str(i)+'.npy')
    comp_single_r2.append(np.linalg.norm(exact-lr, ord=norm_type))
    exact = np.load('LARGE_single2_r4_01_100/prob_dist_exact_t'+str(i)+'.npy')
    lr = np.load('LARGE_single2_r4_01_100/prob_dist_lr_t'+str(i)+'.npy')
    comp_single_r4.append(np.linalg.norm(exact-lr, ord=norm_type))
    exact = np.load('LARGE_single2_r8_01_100/prob_dist_exact_t'+str(i)+'.npy')
    lr = np.load('LARGE_single2_r8_01_100/prob_dist_lr_t'+str(i)+'.npy')
    comp_single_r8.append(np.linalg.norm(exact-lr, ord=norm_type))
    exact = np.load('LARGE_single2_r16_01_100/prob_dist_exact_t'+str(i)+'.npy')
    lr = np.load('LARGE_single2_r16_01_100/prob_dist_lr_t'+str(i)+'.npy')
    comp_single_r16.append(np.linalg.norm(exact-lr, ord=norm_type))
    exact = np.load('LARGE_uni2_r2_01_100/prob_dist_exact_t'+str(i)+'.npy')
    lr = np.load('LARGE_uni2_r2_01_100/prob_dist_lr_t'+str(i)+'.npy')
    comp_uni_r2.append(np.linalg.norm(exact-lr, ord=norm_type))
    exact = np.load('LARGE_uni2_r4_01_100/prob_dist_exact_t'+str(i)+'.npy')
    lr = np.load('LARGE_uni2_r4_01_100/prob_dist_lr_t'+str(i)+'.npy')
    comp_uni_r4.append(np.linalg.norm(exact-lr, ord=norm_type))
    exact = np.load('LARGE_uni2_r8_01_100/prob_dist_exact_t'+str(i)+'.npy')
    lr = np.load('LARGE_uni2_r8_01_100/prob_dist_lr_t'+str(i)+'.npy')
    comp_uni_r8.append(np.linalg.norm(exact-lr, ord=norm_type))
    exact = np.load('LARGE_uni2_r16_01_100/prob_dist_exact_t'+str(i)+'.npy')
    lr = np.load('LARGE_uni2_r16_01_100/prob_dist_lr_t'+str(i)+'.npy')
    comp_uni_r16.append(np.linalg.norm(exact-lr, ord=norm_type))

    exact = np.load('LARGE_uni_r2_01_100_purple/prob_dist_exact_t'+str(i)+'.npy')
    lr = np.load('LARGE_uni_r2_01_100_purple/prob_dist_lr_t'+str(i)+'.npy')
    comp_uni_r2_purple.append(np.linalg.norm(exact-lr, ord=norm_type))
    exact = np.load('LARGE_uni_r4_01_100_purple/prob_dist_exact_t'+str(i)+'.npy')
    lr = np.load('LARGE_uni_r4_01_100_purple/prob_dist_lr_t'+str(i)+'.npy')
    comp_uni_r4_purple.append(np.linalg.norm(exact-lr, ord=norm_type))
    exact = np.load('LARGE_uni_r8_01_100_purple/prob_dist_exact_t'+str(i)+'.npy')
    lr = np.load('LARGE_uni_r8_01_100_purple/prob_dist_lr_t'+str(i)+'.npy')
    comp_uni_r8_purple.append(np.linalg.norm(exact-lr, ord=norm_type))
    exact = np.load('LARGE_uni_r16_01_100_purple/prob_dist_exact_t'+str(i)+'.npy')
    lr = np.load('LARGE_uni_r16_01_100_purple/prob_dist_lr_t'+str(i)+'.npy')
    comp_uni_r16_purple.append(np.linalg.norm(exact-lr, ord=norm_type))

    exact = np.load('LARGE_uni_r2_01_100_red_small_cut/prob_dist_exact_t'+str(i)+'.npy')
    lr = np.load('LARGE_uni_r2_01_100_red_small_cut/prob_dist_lr_t'+str(i)+'.npy')
    comp_uni_r2_green.append(np.linalg.norm(exact-lr, ord=norm_type))
    exact = np.load('LARGE_uni_r4_01_100_red_small_cut/prob_dist_exact_t'+str(i)+'.npy')
    lr = np.load('LARGE_uni_r4_01_100_red_small_cut/prob_dist_lr_t'+str(i)+'.npy')
    comp_uni_r4_green.append(np.linalg.norm(exact-lr, ord=norm_type))
    exact = np.load('LARGE_uni_r8_01_100_red_small_cut/prob_dist_exact_t'+str(i)+'.npy')
    lr = np.load('LARGE_uni_r8_01_100_red_small_cut/prob_dist_lr_t'+str(i)+'.npy')
    comp_uni_r8_green.append(np.linalg.norm(exact-lr, ord=norm_type))
    exact = np.load('LARGE_uni_r16_01_100_red_small_cut/prob_dist_exact_t'+str(i)+'.npy')
    lr = np.load('LARGE_uni_r16_01_100_red_small_cut/prob_dist_lr_t'+str(i)+'.npy')
    comp_uni_r16_green.append(np.linalg.norm(exact-lr, ord=norm_type))
    
    exact = np.load('LARGE_uni_r2_01_100_red_large/prob_dist_exact_t'+str(i)+'.npy')
    lr = np.load('LARGE_uni_r2_01_100_red_large/prob_dist_lr_t'+str(i)+'.npy')
    comp_uni_r2_red.append(np.linalg.norm(exact-lr, ord=norm_type))
    exact = np.load('LARGE_uni_r4_01_100_red_large/prob_dist_exact_t'+str(i)+'.npy')
    lr = np.load('LARGE_uni_r4_01_100_red_large/prob_dist_lr_t'+str(i)+'.npy')
    comp_uni_r4_red.append(np.linalg.norm(exact-lr, ord=norm_type))
    exact = np.load('LARGE_uni_r8_01_100_red_large/prob_dist_exact_t'+str(i)+'.npy')
    lr = np.load('LARGE_uni_r8_01_100_red_large/prob_dist_lr_t'+str(i)+'.npy')
    comp_uni_r8_red.append(np.linalg.norm(exact-lr, ord=norm_type))
    exact = np.load('LARGE_uni_r16_01_100_red_large/prob_dist_exact_t'+str(i)+'.npy')
    lr = np.load('LARGE_uni_r16_01_100_red_large/prob_dist_lr_t'+str(i)+'.npy')
    comp_uni_r16_red.append(np.linalg.norm(exact-lr, ord=norm_type))

    exact = np.load('LARGE_uni_r2_01_100_fix/prob_dist_exact_t'+str(i)+'.npy')
    lr = np.load('LARGE_uni_r2_01_100_fix/prob_dist_lr_t'+str(i)+'.npy')
    comp_uni_r2_rand.append(np.linalg.norm(exact-lr, ord=norm_type))
    exact = np.load('LARGE_uni_r4_01_100_fix/prob_dist_exact_t'+str(i)+'.npy')
    lr = np.load('LARGE_uni_r4_01_100_fix/prob_dist_lr_t'+str(i)+'.npy')
    comp_uni_r4_rand.append(np.linalg.norm(exact-lr, ord=norm_type))
    exact = np.load('LARGE_uni_r8_01_100_fix/prob_dist_exact_t'+str(i)+'.npy')
    lr = np.load('LARGE_uni_r8_01_100_fix/prob_dist_lr_t'+str(i)+'.npy')
    comp_uni_r8_rand.append(np.linalg.norm(exact-lr, ord=norm_type))
    exact = np.load('LARGE_uni_r16_01_100_fix/prob_dist_exact_t'+str(i)+'.npy')
    lr = np.load('LARGE_uni_r16_01_100_fix/prob_dist_lr_t'+str(i)+'.npy')
    comp_uni_r16_rand.append(np.linalg.norm(exact-lr, ord=norm_type))

#compare initial states

epsilon = 2.5
plt.subplot(2,2,1)
plt.title('rank = 2')
plt.plot(comp_rand_r2, label='random',   color='red', linestyle='dashdot')
plt.plot(comp_single_r2, label='single',  color='green', linestyle=(0, (5, 10)))
plt.plot(comp_uni_r2, label='uniform',   color='orange', linestyle=(0, (5, 1)))
#plt.legend()
plt.ylabel('error')
#plt.xlabel('time')
plt.ylim((1e-8, 1e0+epsilon))
plt.yscale('log')

plt.subplot(2,2,2)
plt.title('rank = 4')
plt.plot(comp_rand_r4,  label='_nolegend_',  color='red', linestyle='dashdot')
plt.plot(comp_single_r4, label='_nolegend_',  color='green', linestyle=(0, (5, 10)))
plt.plot(comp_uni_r4, label='_nolegend_',   color='orange', linestyle=(0, (5, 1)))
#plt.legend()
#plt.ylabel('error')
#plt.xlabel('time')
plt.ylim((1e-8, 1e0+epsilon))
plt.yscale('log')

plt.subplot(2,2,3)
plt.title('rank = 8')
plt.plot(comp_rand_r8,  label='_nolegend_',  color='red', linestyle='dashdot')
plt.plot(comp_single_r8, label='_nolegend_', color='green', linestyle=(0, (5, 10)))
plt.plot(comp_uni_r8, label='_nolegend_',  color='orange', linestyle=(0, (5, 1)))
#plt.legend()
plt.yscale('log')
plt.xlabel('time')
plt.ylabel('error')
plt.ylim((1e-8, 1e0+epsilon))

plt.subplot(2,2,4)
plt.title('rank = 16')
plt.plot(comp_rand_r16, label='_nolegend_', color='red', linestyle='dashdot')
plt.plot(comp_single_r16, label='_nolegend_', color='green', linestyle=(0, (5, 10)))
plt.plot(comp_uni_r16, label='_nolegend_',  color='orange', linestyle=(0, (5, 1)))
#plt.legend(loc = (-0.7, 1.0))
plt.yscale('log')
plt.xlabel('time')
plt.ylim((1e-8, 1e0+epsilon))
#plt.ylabel('error')
#plt.ylim(1e-15, 2)

plt.figlegend(loc = (0.4725, 0.445))
plt.suptitle('compare various types of initial setups')
plt.tight_layout(rect=[0, 0.03, 1, 0.95])
plt.savefig('comp_IVs.png',dpi=300)
plt.show()


# compare partitioning
#'''
plt.subplot(2,2,1)
plt.title('rank = 2')
plt.plot(comp_uni_r2, label='_nolegend_', color = 'blue', linestyle = 'dashdot')#,   color='orange', linestyle='dashdot')
plt.plot(comp_uni_r2_green, label='_nolegend_', color = 'green', linestyle = (0, (5, 10)))#,   color='orange', linestyle='dashdot')
plt.plot(comp_uni_r2_purple, label='_nolegend_', color = 'purple', linestyle = 'dashdot')#,   color='orange', linestyle='dashdot')
plt.plot(comp_uni_r2_red, label='_nolegend_', color = 'red', linestyle = 'dashdot')#,   color='orange', linestyle='dashdot')
plt.plot(comp_uni_r2_rand, label='_nolegend_', color = 'yellow', linestyle = (0, (5, 1)))#,   color='orange', linestyle='dashdot')
plt.yscale('log')
#plt.xlabel('time')
plt.ylabel('error')
plt.ylim((1e-8, 1e0))

plt.subplot(2,2,2)
plt.title('rank = 4')
plt.plot(comp_uni_r4, label='_nolegend_', color = 'blue', linestyle = 'dashdot')# ,   color='orange', linestyle=(0, (5, 10)))
plt.plot(comp_uni_r4_green, label='_nolegend_', color = 'green', linestyle = (0, (5, 10)))#',   color='orange', linestyle=(0, (5, 10)))
plt.plot(comp_uni_r4_purple, label='_nolegend_', color = 'purple', linestyle = 'dashdot')# ,   color='orange', linestyle=(0, (5, 10)))
plt.plot(comp_uni_r4_red, label='_nolegend_', color = 'red', linestyle = 'dashdot')# ,   color='orange', linestyle=(0, (5, 10)))
plt.plot(comp_uni_r4_rand, label='_nolegend_', color = 'yellow', linestyle = (0, (5, 1)))#',   color='orange', linestyle=(0, (5, 10)))
plt.yscale('log')
plt.ylim((1e-8, 1e0))
#plt.xlabel('time')
#plt.ylabel('error')

plt.subplot(2,2,3)
plt.title('rank = 8')
plt.plot(comp_uni_r8, label='_nolegend_', color = 'blue', linestyle ='dashdot')#,  color='orange', linestyle=(0, (5, 1)))
plt.plot(comp_uni_r8_green, label='_nolegend_', color = 'green', linestyle =(0, (5, 10)))#',  color='orange', linestyle=(0, (5, 1)))
plt.plot(comp_uni_r8_purple, label='_nolegend_', color = 'purple', linestyle ='dashdot')#,  color='orange', linestyle=(0, (5, 1)))
plt.plot(comp_uni_r8_red, label='_nolegend_', color = 'red', linestyle ='dashdot')#,  color='orange', linestyle=(0, (5, 1)))
plt.plot(comp_uni_r8_rand, label='_nolegend_', color = 'yellow', linestyle =(0, (5, 1)))#',  color='orange', linestyle=(0, (5, 1)))
plt.yscale('log')
plt.xlabel('time')
plt.ylabel('error')
plt.ylim((1e-8, 1e0))

plt.subplot(2,2,4)
plt.title('rank = 16')
plt.plot(comp_uni_r16, label='blue', color = 'blue', linestyle ='dashdot')#,  color='orange', linestyle='solid')
plt.plot(comp_uni_r16_green, label='green', color = 'green', linestyle =(0, (5, 10)))#',  color='orange', linestyle='solid')
plt.plot(comp_uni_r16_purple, label='purple', color = 'purple', linestyle ='dashdot')#,  color='orange', linestyle='solid')
plt.plot(comp_uni_r16_red, label='red', color = 'red', linestyle ='dashdot')#,  color='orange', linestyle='solid')
plt.plot(comp_uni_r16_rand, label='yellow', color = 'yellow', linestyle =(0, (5, 1)))#',  color='orange', linestyle='solid')
plt.yscale('log')
plt.xlabel('time')
plt.ylim((1e-8, 1e0))
#plt.ylabel('error')

plt.figlegend(loc = (0.4725, 0.445))
plt.tight_layout(rect=[0, 0.03, 1, 0.95])
plt.suptitle('compare partitioning')
plt.savefig('comp_partitions.png',dpi=300)
plt.show()
#'''


#compare initial states reverse rank and type
 
epsilon = 0
plt.subplot(2,2,1)
plt.title('initial type = random')
plt.plot(comp_rand_r2,  label='r2',   color='red', linestyle='dashdot')
plt.plot(comp_rand_r4,  label='r4',  color='green', linestyle=(0, (5, 10)))
plt.plot(comp_rand_r8,  label='r8',  color='blue', linestyle=(0, (5, 1)))
plt.plot(comp_rand_r16, label='r16', color='orange', linestyle='solid')

#plt.legend()
plt.ylabel('error')
#plt.xlabel('time')
plt.ylim((1e-5, 1e0+epsilon))
plt.yscale('log')

plt.subplot(2,2,2)
plt.title('initial type = single')
plt.plot(comp_single_r2, label='_nolegend_',  color='red', linestyle='dashdot')
plt.plot(comp_single_r4, label='_nolegend_',  color='green', linestyle=(0, (5, 10)))
plt.plot(comp_single_r8, label='_nolegend_', color='blue', linestyle=(0, (5, 1)))
plt.plot(comp_single_r16, label='_nolegend_', color='orange', linestyle='solid')


#plt.legend()
#plt.ylabel('error')
#plt.xlabel('time')
plt.ylim((1e-5, 1e0+epsilon))
plt.yscale('log')

plt.subplot(2,2,3)
plt.title('initial type = uniform')
plt.plot(comp_uni_r2, label='_nolegend_',   color='red', linestyle='dashdot')
plt.plot(comp_uni_r4, label='_nolegend_',   color='green', linestyle=(0, (5, 10)))
plt.plot(comp_uni_r8, label='_nolegend_',  color='blue', linestyle=(0, (5, 1)))
plt.plot(comp_uni_r16, label='_nolegend_',  color='orange', linestyle='solid')
#plt.legend()
plt.yscale('log')
plt.xlabel('time')
plt.ylabel('error')
plt.ylim((1e-5, 1e0+epsilon))


plt.figlegend(loc = (0.6725, 0.245))
plt.suptitle('compare different ranks')
plt.tight_layout(rect=[0, 0.03, 1, 0.95])
plt.savefig('comp_IVs_reverse.png',dpi=300)
plt.show()
 
