import itertools as it

from sympy.logic.boolalg import to_dnf, simplify_logic
from sympy.logic.boolalg import Xor
from sympy.parsing.sympy_parser import parse_expr

model_name = 'sarah'
fn = 'rules.txt'

rules_dict = {}
ln = []
names = []
depends = []
truths = []
rules = []
with open(fn) as infile:
    #infile.readline()
    #infile.readline()
    for line in infile:
        line = line.strip().split('|')
        tt = [float(x) >= 0.5 for x in line[2].split(',')]
        names.append(line[0])
        depends.append(line[1].split(','))
        truths.append(tt)

x_names = {}
x_number = {}
i = 0
for name in names:
    x_names[name] = 'x{}__'.format(i)
    x_number[name] = i
    i += 1
print(x_names)

for i in range(len(names)):
    rule = []
    j = 0;

    dep = depends[i]
    truth = truths[i]
    for bits in it.product(range(2), repeat=len(dep)):
        subrule = []
        if truth[j] == True:
            for k in range(len(dep)):
                subrule.append(dep[k] if bits[k]==1 else '~{}'.format(dep[k]))
            rule.append(' & '.join(subrule))
        
        j += 1
    rule = ' | '.join(rule)
    for name in dep:
        rule = rule.replace(name, x_names[name])
    print(len(dep), dep)
    print(rule)
    print('')
    print('')
    #print(to_dnf(parse_expr(rule),simplify=True))
    rule = str(simplify_logic(parse_expr(rule)))
    rules.append(rule)
    print(rule)
    print('')

write_rules = open('new_rule.hpp', 'w')
str_names = ', '.join(names)
write_rules.write("RULE_SET("+model_name+", "+str(len(names))+", "+str_names+");\n")

i=0
for name in names:
    rule = rules[i]
    dep = depends[i]
    print(name, rule)

    rule = rule.replace('&', '&&')
    rule = rule.replace('|', '||')
    rule = rule.replace('~', '!')   
    for j in range(len(names)):
        rule = rule.replace('x{}__'.format(j), 'x[{}]'.format(j))

    deps_list = []
    for dep_name in dep:
        deps_list.append(x_number[dep_name])
    if i not in deps_list:
        deps_list.append(i)
    deps_list = sorted(deps_list)

    deps = ', '.join([str(d) for d in deps_list])
    write_rules.write('RULE({}, {}, {}, {});\n'.format(model_name, i, rule, deps))

    i += 1

write_rules.close()