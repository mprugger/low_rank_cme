#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN TEST_LOW_RANK_CME
#include <boost/test/unit_test.hpp>
#include <boost/test/tools/floating_point_comparison.hpp>

#include "common.hpp"
#include "common_rules.hpp"
#include "low_rank_cme.hpp"
#include "netcdf.hpp"

BOOST_AUTO_TEST_CASE( low_rank_init ) {
    ind len = 1<<4;
    ind r = 4;

    LR P0(r, {len,len});
    MatrixXd U = MatrixXd::Ones(len, 1)/double(len);
    MatrixXd V = MatrixXd::Ones(len, 1)/double(len);

    P0.init(1, U, V);

    BOOST_CHECK_SMALL( (P0.full() - 1.0/pow(2.0,8)*VectorXd::Ones(1<<8)).norm(), 1e-14 );

    BOOST_CHECK_SMALL( (P0.U.transpose()*P0.U - MatrixXd::Identity(r,r)).norm(), 1e-14 );
    BOOST_CHECK_SMALL( (P0.V.transpose()*P0.V - MatrixXd::Identity(r,r)).norm(), 1e-14 );
}

BOOST_AUTO_TEST_CASE( test_T_flip ) {
    bitset<8> a("01010100");
    a = T_flip<3, 0, 8, 8>(a);
    BOOST_CHECK( a[3] == 1 );
    bitset<8> b = T_flip<10, 0, 8, 8>(a);
    BOOST_CHECK( a == b );

    bitset<9> c("101010100");
    bitset<9> d = T_flip<14, 1, 8, 9>(c);
    BOOST_CHECK( d[6] == 0 );
    d = T_flip<3, 1, 8, 9>(c);
    BOOST_CHECK( d == c );
}

BOOST_AUTO_TEST_CASE( test_write_dependent_bits ) {
    bitset<8> a("01010000");
    vector<ind> don = EMT::template depends_on<0>();
    write_dependent_bits<0, 0, 4, 8, EMT>(a, 5, don);
    BOOST_CHECK( a == bitset<8>("01010101") );

    bitset<8> b("01011010");
    don = EMT::template depends_on<2>();
    write_dependent_bits<2, 0, 4, 8, EMT>(b, 3, don);
    BOOST_CHECK( b == bitset<8>("01010111") );

    bitset<8> c("01100100");
    don = EMT::template depends_on<4>();
    write_dependent_bits<4, 1, 4, 8, EMT>(c, 1, don);
    BOOST_CHECK( c == bitset<8>("01010100") );

}

BOOST_AUTO_TEST_CASE( test_linear_idx_rule ) {

    bitset<4> a("1100");
    ind i1 = linear_idx_rule<2, 0, 4, 4, EMT>(a);
    BOOST_CHECK( i1 == 6 );

    ind i2 = linear_idx_rule<2, 1, 4, 4, EMT>(a);
    BOOST_CHECK( i2 == 4 );
}

BOOST_AUTO_TEST_CASE( test_small_model ) {
    ind len = 1<<2;

    LR P0(2, {len,len});
    MatrixXd U = MatrixXd::Ones(len, 1)/double(len);
    MatrixXd V = MatrixXd::Ones(len, 1)/double(len);

    P0.init(1, U, V);

    LR P1;
    double t=0.0;
    for(int i=0;i<200;i++) {
        P1 = time_step<2,4,SIMPLE>(0.1, P0);
        P0 = P1;
        t += 0.1;
    }

    VectorXd P_full = P1.full();

    BOOST_CHECK( P_full[0]  >= 0.05 );
    BOOST_CHECK( P_full[2]  >= 0.05 );
    BOOST_CHECK( P_full[5]  >= 0.05 );
    BOOST_CHECK( P_full[7]  >= 0.05 );
    BOOST_CHECK( P_full[8]  >= 0.05 );
    BOOST_CHECK( P_full[10] >= 0.05 );

    BOOST_CHECK_SMALL( P_full[0]+P_full[2]+P_full[5]+P_full[7]+P_full[8]+P_full[10] - 1.0 , 1e-8 );
}


BOOST_AUTO_TEST_CASE( test_small_model_odd_part2larger ) {
    ind len = 1<<2;

    LR P0(2, {len,2*len});
    MatrixXd U = MatrixXd::Ones(len, 1)/double(len);
    MatrixXd V = MatrixXd::Ones(len, 1)/double(len);

    P0.init(1, U, V);

    LR P1;
    double t=0.0;
    for(int i=0;i<200;i++) {
        P1 = time_step<2,5,SIMPLE_ODD>(0.1, P0);
        P0 = P1;
        t += 0.1;
    }

    VectorXd P_full = P1.full();

    BOOST_CHECK( P_full[0]  >= 0.05 );
    BOOST_CHECK( P_full[2]  >= 0.05 );
    BOOST_CHECK( P_full[5]  >= 0.05 );
    BOOST_CHECK( P_full[7]  >= 0.05 );
    BOOST_CHECK( P_full[8]  >= 0.05 );
    BOOST_CHECK( P_full[10] >= 0.05 );

    BOOST_CHECK_SMALL( P_full[0]+P_full[2]+P_full[5]+P_full[7]+P_full[8]+P_full[10] - 1.0 , 1e-8 );

    write<5>(P_full);
}


BOOST_AUTO_TEST_CASE( test_small_model_odd_part1larger ) {
    ind len = 1<<2;

    LR P0(2, {2*len,len});
    MatrixXd U = MatrixXd::Ones(len, 1)/double(len);
    MatrixXd V = MatrixXd::Ones(len, 1)/double(len);

    P0.init(1, U, V);

    LR P1;
    double t=0.0;
    for(int i=0;i<200;i++) {
        P1 = time_step<3,5,SIMPLE_ODD>(0.1, P0);
        P0 = P1;
        t += 0.1;
    }

    VectorXd P_full = P1.full();

    BOOST_CHECK( P_full[0]  >= 0.05 );
    BOOST_CHECK( P_full[2]  >= 0.05 );
    BOOST_CHECK( P_full[5]  >= 0.05 );
    BOOST_CHECK( P_full[7]  >= 0.05 );
    BOOST_CHECK( P_full[8]  >= 0.05 );
    BOOST_CHECK( P_full[10] >= 0.05 );

    BOOST_CHECK_SMALL( P_full[0]+P_full[2]+P_full[5]+P_full[7]+P_full[8]+P_full[10] - 1.0 , 2e-3 );

    write<5>(P_full);
}

BOOST_AUTO_TEST_CASE( netcdf ) {

    ind len = 1<<2;

    LR P0(2, {len,len});
    MatrixXd U = MatrixXd::Ones(len, 1)/double(len);
    MatrixXd V = MatrixXd::Ones(len, 1)/double(len);

    P0.init(1, U, V);

    vector<std::string> simple_names = SIMPLE::names();
    write_nc("netcdf_test.nc", P0.U, P0.S, P0.V, simple_names);

    LR P1;
    vector<std::string> names;
    read_nc("netcdf_test.nc", P1.U, P1.S, P1.V, names);

    BOOST_CHECK( (P0.U-P1.U).norm()  <= 1e-15 );
    BOOST_CHECK( (P0.V-P1.V).norm()  <= 1e-15 );
    BOOST_CHECK( (P0.S-P1.S).norm()  <= 1e-15 );

    for(ind i=0;i<(ind)simple_names.size();i++)
        BOOST_CHECK( simple_names[i] == names[i] );
}

BOOST_AUTO_TEST_CASE( netcdf_from_python ) {

    LR P;
    vector<std::string> names;
    read_nc("../test-data/test.nc", P.U, P.S, P.V, names);

    cout << P.U << endl << endl;
    cout << P.V << endl << endl;
    cout << P.S << endl << endl;

    vector<std::string> simple_names = SIMPLE::names();
    for(ind i=0;i<(ind)simple_names.size();i++)
        BOOST_CHECK( simple_names[i] == names[i] );
}

