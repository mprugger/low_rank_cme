name = 'wooten'
fn = 'wooten_network.csv'


rules_dict = {}
with open(fn) as infile:
    infile.readline()
    infile.readline()
    for line in infile:
        line = line.strip().split(',')
        #print(line)
        source = line[0].strip()
        target = line[1].strip()
        weight = int(line[2].strip())
        if weight != 0:
            if target in rules_dict.keys():
                rules_dict[target].append((source, weight))
            else:
                rules_dict[target]=[(source, weight)]

def name_to_x(name, keys):
    return 'x[{}]'.format(keys.index(name))


write_rules = open('new_rule.hpp', 'w')


keys = list(rules_dict.keys())
print(keys)

write_rules.write("RULE_SET("+name+", "+str(len(keys))+");\n")

i=0
complete_deps_list = []
for key in keys:
    src = rules_dict[key]
    #print(key, src)
    summation = ' + '.join(['({})*{}'.format(s[1],name_to_x(s[0],keys)) for s in src])
    deps_list = [keys.index(s[0]) for s in src]
    if i not in deps_list:
        deps_list.append(i)
    deps_list = sorted(deps_list)
    complete_deps_list.append(deps_list)
    print('dependency list: ', len(deps_list), deps_list)
    deps = ', '.join([str(d) for d in deps_list])
    write_rules.write('RULE({}, {}, {} > 0, {});\n'.format(name, i, summation, deps))
    i += 1 



